import { defineStore } from 'pinia';
import { Session } from '/@/utils/storage';
import { AccountApi } from '/@/api/system/Account';
import { SysConfigItemApi } from '/@/api/system/SysConfigItem';
import Watermark from '/@/utils/watermark';
import { GetConfigItemValueInputDto, UserPermissionOutputDto, ViewListDto } from '/@/api/system/data-contracts'

// 菜单树
let menu = [] as ViewListDto[] | null | undefined;

/**
 * 用户信息
 * @methods setSysInfos 设置系统
 */
export const useUserInfo = defineStore('userInfo', {
	state: (): UserInfosState => ({
		userDto: {
			userInfo: {
				departmentName: '',
				headPic: '',
				name: '',
				remark: '',
				phone: ''
			},
			permissionCodes: []
		},
	}),
	actions: {
		setCurrentUserInfo() {
			return new AccountApi().getCurrentUserInfo().then((res: UserPermissionOutputDto) => {
				this.userDto = res as UserDto;
				Session.set('userInfo', {
					permissionCodes: res.permissionCodes,
					userInfo: res.userInfo
				});


				menu = res.menus;
			});
		},
		setConfigItemInfo() {
			const postData = {
				configKeys: ['Watermark.Show', 'tenant']
			} as GetConfigItemValueInputDto;
			return new SysConfigItemApi().getConfigItemValues(postData).then((r: Record<string, string>) => {
				Session.set('configItem', r);
			});
		},
		async setSysInfos() {
			await Promise.all([
				this.setCurrentUserInfo(),
				this.setConfigItemInfo(),
			])

			var configItem = Session.get('configItem');
			if (configItem['Watermark.Show'] === '1') Watermark.set(this.userDto.userInfo.name);

			return menu;
		},
	},
});
