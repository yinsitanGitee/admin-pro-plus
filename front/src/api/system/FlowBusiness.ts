/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { FlowBusinessListDto, FlowProcVM, GetFlowNotApprovalDto, InputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class FlowBusinessApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags FlowBusiness
	 * @name QueryFlowOperateLog
	 * @summary 查询
	 * @request POST:/api/FlowBusiness/QueryFlowOperateLog
	 */
	queryFlowOperateLog = (data: InputDto, params: RequestParams = {}) =>
		this.request<FlowBusinessListDto[], any>({
			path: `/api/FlowBusiness/QueryFlowOperateLog`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowBusiness
	 * @name QueryFlowNotApprovalProc
	 * @summary 查询
	 * @request POST:/api/FlowBusiness/QueryFlowNotApprovalProc
	 */
	queryFlowNotApprovalProc = (data: GetFlowNotApprovalDto, params: RequestParams = {}) =>
		this.request<FlowProcVM[], any>({
			path: `/api/FlowBusiness/QueryFlowNotApprovalProc`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
