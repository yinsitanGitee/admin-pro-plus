/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	ApprovalAPIDto,
	BusinessResult,
	FormBillDetailDtoBusinessResult,
	FormBillEditDto,
	FormBillEditDtoBusinessResult,
	FormBillListDtoCommonPageOutputDto,
	GetFormBillInputDto,
	InputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class FormBillApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/FormBill/Query
	 */
	query = (data: GetFormBillInputDto, params: RequestParams = {}) =>
		this.request<FormBillListDtoCommonPageOutputDto, any>({
			path: `/api/FormBill/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/FormBill/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<FormBillEditDtoBusinessResult, any>({
			path: `/api/FormBill/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name GetDetail
	 * @summary 获取详情
	 * @request POST:/api/FormBill/GetDetail
	 */
	getDetail = (data: InputDto, params: RequestParams = {}) =>
		this.request<FormBillDetailDtoBusinessResult, any>({
			path: `/api/FormBill/GetDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name Approval
	 * @summary 审批
	 * @request POST:/api/FormBill/Approval
	 */
	approval = (data: ApprovalAPIDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FormBill/Approval`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/FormBill/Add
	 */
	add = (data: FormBillEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FormBill/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/FormBill/Edit
	 */
	edit = (data: FormBillEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FormBill/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FormBill
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/FormBill/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FormBill/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
