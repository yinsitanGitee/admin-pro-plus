/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	DepartmentEditDto,
	DepartmentEditDtoBusinessResult,
	DepartmentListDto,
	GetDepartmentInputDto,
	InputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class DepartmentApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Department
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Department/Query
	 */
	query = (data: GetDepartmentInputDto, params: RequestParams = {}) =>
		this.request<DepartmentListDto[], any>({
			path: `/api/Department/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Department
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Department/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<DepartmentEditDtoBusinessResult, any>({
			path: `/api/Department/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Department
	 * @name Add
	 * @summary 添加
	 * @request POST:/api/Department/Add
	 */
	add = (data: DepartmentEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Department/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Department
	 * @name Edit
	 * @summary 编辑
	 * @request POST:/api/Department/Edit
	 */
	edit = (data: DepartmentEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Department/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Department
	 * @name Delete
	 * @summary 删除
	 * @request POST:/api/Department/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Department/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
