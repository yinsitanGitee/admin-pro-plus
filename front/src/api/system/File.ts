/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { FileListDtoListBusinessResult } from './data-contracts';
import { HttpClient, RequestParams } from './http-client';

export class FileApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags File
	 * @name Upload
	 * @summary 上传文件
	 * @request POST:/api/File/Upload
	 */
	upload = (params: RequestParams = {}) =>
		this.request<FileListDtoListBusinessResult, any>({
			path: `/api/File/Upload`,
			method: 'POST',
			format: 'json',
			...params,
		});
}
