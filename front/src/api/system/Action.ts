/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	ActionEditDto,
	ActionEditDtoBusinessResult,
	ActionListDtoCommonPageOutputDto,
	BusinessResult,
	CommonSelectDto,
	GetActionInputDto,
	InputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class ActionApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Action
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Action/Query
	 */
	query = (data: GetActionInputDto, params: RequestParams = {}) =>
		this.request<ActionListDtoCommonPageOutputDto, any>({
			path: `/api/Action/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Action
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Action/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<ActionEditDtoBusinessResult, any>({
			path: `/api/Action/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Action
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/Action/Add
	 */
	add = (data: ActionEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Action/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Action
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/Action/Edit
	 */
	edit = (data: ActionEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Action/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Action
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/Action/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Action/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Action
	 * @name GetSelect
	 * @summary 获取下拉框
	 * @request GET:/api/Action/GetSelect
	 */
	getSelect = (params: RequestParams = {}) =>
		this.request<CommonSelectDto[], any>({
			path: `/api/Action/GetSelect`,
			method: 'GET',
			format: 'json',
			...params,
		});
}
