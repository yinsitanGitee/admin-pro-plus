/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BillCodeEditDto,
	BillCodeEditDtoBusinessResult,
	BillCodeListDtoCommonPageOutputDto,
	BusinessResult,
	GetBillCodeInputDto,
	InputDto,
	SysBillCodeDetailEntityListBusinessResult,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class BillCodeApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags BillCode
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/BillCode/Query
	 */
	query = (data: GetBillCodeInputDto, params: RequestParams = {}) =>
		this.request<BillCodeListDtoCommonPageOutputDto, any>({
			path: `/api/BillCode/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags BillCode
	 * @name GetDetail
	 * @summary 获取明细
	 * @request POST:/api/BillCode/GetDetail
	 */
	getDetail = (data: InputDto, params: RequestParams = {}) =>
		this.request<SysBillCodeDetailEntityListBusinessResult, any>({
			path: `/api/BillCode/GetDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags BillCode
	 * @name Get
	 * @summary 获取明细
	 * @request POST:/api/BillCode/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<BillCodeEditDtoBusinessResult, any>({
			path: `/api/BillCode/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags BillCode
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/BillCode/Add
	 */
	add = (data: BillCodeEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/BillCode/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags BillCode
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/BillCode/Edit
	 */
	edit = (data: BillCodeEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/BillCode/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags BillCode
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/BillCode/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/BillCode/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
