/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { HttpClient, RequestParams } from './http-client';

export class TestApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Test
	 * @name Index
	 * @summary 测试定时任务
	 * @request POST:/api/Test/Index
	 */
	index = (params: RequestParams = {}) =>
		this.request<string, any>({
			path: `/api/Test/Index`,
			method: 'POST',
			format: 'json',
			...params,
		});
}
