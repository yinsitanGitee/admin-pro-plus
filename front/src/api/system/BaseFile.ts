/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { BusinessResult, FileEditDto, FileListDto, InputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class BaseFileApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags BaseFile
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/BaseFile/Query
	 */
	query = (data: InputDto, params: RequestParams = {}) =>
		this.request<FileListDto[], any>({
			path: `/api/BaseFile/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags BaseFile
	 * @name Submit
	 * @summary 保存
	 * @request POST:/api/BaseFile/Submit
	 */
	submit = (data: FileEditDto[], params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/BaseFile/Submit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
