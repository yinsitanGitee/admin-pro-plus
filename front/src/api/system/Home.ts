/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { HttpClient, RequestParams } from './http-client';

export class HomeApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Home
	 * @name GetCurrentTaskCount
	 * @summary 获取当前用户待办、消息、预警数量
	 * @request POST:/api/Home/GetCurrentTaskCount
	 */
	getCurrentTaskCount = (params: RequestParams = {}) =>
		this.request<Record<string, number>, any>({
			path: `/api/Home/GetCurrentTaskCount`,
			method: 'POST',
			format: 'json',
			...params,
		});
}
