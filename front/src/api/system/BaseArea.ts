/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { BaseAreaListDtoListBusinessResult, GetBaseAreaInputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class BaseAreaApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags BaseArea
	 * @name Query
	 * @summary 获取省市区
	 * @request POST:/api/BaseArea/Query
	 */
	query = (data: GetBaseAreaInputDto, params: RequestParams = {}) =>
		this.request<BaseAreaListDtoListBusinessResult, any>({
			path: `/api/BaseArea/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
