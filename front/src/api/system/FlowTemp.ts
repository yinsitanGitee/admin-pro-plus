/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	FlowTempEditDto,
	FlowTempEditDtoBusinessResult,
	FlowTempListDtoCommonPageOutputDto,
	GetFlowTempInputDto,
	InputDto,
	SchemeTempDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class FlowTempApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/FlowTemp/Query
	 */
	query = (data: GetFlowTempInputDto, params: RequestParams = {}) =>
		this.request<FlowTempListDtoCommonPageOutputDto, any>({
			path: `/api/FlowTemp/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/FlowTemp/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<FlowTempEditDtoBusinessResult, any>({
			path: `/api/FlowTemp/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/FlowTemp/Add
	 */
	add = (data: FlowTempEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FlowTemp/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/FlowTemp/Edit
	 */
	edit = (data: FlowTempEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FlowTemp/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/FlowTemp/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FlowTemp/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name GetFlowScheme
	 * @summary 获取流程图
	 * @request POST:/api/FlowTemp/GetFlowScheme
	 */
	getFlowScheme = (data: InputDto, params: RequestParams = {}) =>
		this.request<SchemeTempDto, any>({
			path: `/api/FlowTemp/GetFlowScheme`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags FlowTemp
	 * @name SaveFlowScheme
	 * @summary 保存流程图
	 * @request POST:/api/FlowTemp/SaveFlowScheme
	 */
	saveFlowScheme = (data: SchemeTempDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/FlowTemp/SaveFlowScheme`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
