/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	CommonSelectDto,
	GetRoleInputDto,
	InputDto,
	RoleEditDto,
	RoleEditDtoBusinessResult,
	RoleListDtoCommonPageOutputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class RoleApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Role
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Role/Query
	 */
	query = (data: GetRoleInputDto, params: RequestParams = {}) =>
		this.request<RoleListDtoCommonPageOutputDto, any>({
			path: `/api/Role/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Role
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Role/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<RoleEditDtoBusinessResult, any>({
			path: `/api/Role/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Role
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/Role/Add
	 */
	add = (data: RoleEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Role/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Role
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/Role/Edit
	 */
	edit = (data: RoleEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Role/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Role
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/Role/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Role/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Role
	 * @name GetRoleSelect
	 * @summary 获取角色下拉框
	 * @request POST:/api/Role/GetRoleSelect
	 */
	getRoleSelect = (params: RequestParams = {}) =>
		this.request<CommonSelectDto[], any>({
			path: `/api/Role/GetRoleSelect`,
			method: 'POST',
			format: 'json',
			...params,
		});
}
