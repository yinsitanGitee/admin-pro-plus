/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { InputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class AccountRoleApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags AccountRole
	 * @name GetAccountRole
	 * @summary 获取用户授权的角色
	 * @request POST:/api/AccountRole/GetAccountRole
	 */
	getAccountRole = (data: InputDto, params: RequestParams = {}) =>
		this.request<string[], any>({
			path: `/api/AccountRole/GetAccountRole`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
