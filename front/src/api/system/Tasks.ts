/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	CommonTreeDto,
	GetCommTasksInputDto,
	InputDto,
	StringListBusinessResult,
	TasksCronExDto,
	TasksEditDto,
	TasksEditDtoBusinessResult,
	TasksListDtoCommonPageOutputDto,
	TasksOperateDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class TasksApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Tasks/Query
	 */
	query = (data: GetCommTasksInputDto, params: RequestParams = {}) =>
		this.request<TasksListDtoCommonPageOutputDto, any>({
			path: `/api/Tasks/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name GetAll
	 * @summary 获取所有任务数据
	 * @request GET:/api/Tasks/GetAll
	 */
	getAll = (params: RequestParams = {}) =>
		this.request<CommonTreeDto[], any>({
			path: `/api/Tasks/GetAll`,
			method: 'GET',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Tasks/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<TasksEditDtoBusinessResult, any>({
			path: `/api/Tasks/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/Tasks/Add
	 */
	add = (data: TasksEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Tasks/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/Tasks/Edit
	 */
	edit = (data: TasksEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Tasks/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name Operate
	 * @summary 数据操作
	 * @request POST:/api/Tasks/Operate
	 */
	operate = (data: TasksOperateDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Tasks/Operate`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Tasks
	 * @name GetCronResults
	 * @summary 计算Cron表达式
	 * @request POST:/api/Tasks/GetCronResults
	 */
	getCronResults = (data: TasksCronExDto, params: RequestParams = {}) =>
		this.request<StringListBusinessResult, any>({
			path: `/api/Tasks/GetCronResults`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
