/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	CommonOrgDepartDto,
	GetOrganizeInputDto,
	InputDto,
	OrganizeEditDto,
	OrganizeEditDtoBusinessResult,
	OrganizeListDto,
	TreeNode,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class OrganizeApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
 * No description
 *
 * @tags Organize
 * @name GetDepartmentTree
 * @summary 获取机构树
V9使用
 * @request POST:/api/Organize/GetDepartmentTree
 */
	getDepartmentTree = (params: RequestParams = {}) =>
		this.request<TreeNode[], any>({
			path: `/api/Organize/GetDepartmentTree`,
			method: 'POST',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Organize
	 * @name GetOrgDeparts
	 * @summary 获取机构部门树形数据
	 * @request POST:/api/Organize/GetOrgDeparts
	 */
	getOrgDeparts = (params: RequestParams = {}) =>
		this.request<CommonOrgDepartDto[], any>({
			path: `/api/Organize/GetOrgDeparts`,
			method: 'POST',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Organize
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Organize/Query
	 */
	query = (data: GetOrganizeInputDto, params: RequestParams = {}) =>
		this.request<OrganizeListDto[], any>({
			path: `/api/Organize/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Organize
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Organize/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<OrganizeEditDtoBusinessResult, any>({
			path: `/api/Organize/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Organize
	 * @name Add
	 * @summary 添加
	 * @request POST:/api/Organize/Add
	 */
	add = (data: OrganizeEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Organize/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Organize
	 * @name Edit
	 * @summary 编辑
	 * @request POST:/api/Organize/Edit
	 */
	edit = (data: OrganizeEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Organize/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Organize
	 * @name Delete
	 * @summary 删除
	 * @request POST:/api/Organize/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Organize/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
