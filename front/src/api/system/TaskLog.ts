/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { GetTaskLogInputDto, TaskLogListDtoCommonPageOutputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class TaskLogApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags TaskLog
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/TaskLog/Query
	 */
	query = (data: GetTaskLogInputDto, params: RequestParams = {}) =>
		this.request<TaskLogListDtoCommonPageOutputDto, any>({
			path: `/api/TaskLog/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
