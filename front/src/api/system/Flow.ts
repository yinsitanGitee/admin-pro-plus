/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	ExcutorWorkListAPIVMListBusinessResult,
	FlowNextProcsVMBusinessResult,
	FlowOperateProcVM,
	FlowProcInformerVM,
	FlowProcsInfoVM,
	FlowTempProcBaseInfoVM,
	FlowTempProcInfoVM,
	GetFlowApproveNextProcsDto,
	GetFlowProcExecutorDto,
	GetFlowTempInfo,
	GetNextProcExecutorVM,
	InputDto,
	NextProcInfoVMBusinessResult,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class FlowApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name VerificationApproval
	 * @summary 验证是否有审批权限
	 * @request POST:/api/Flow/VerificationApproval
	 */
	verificationApproval = (data: InputDto, params: RequestParams = {}) =>
		this.request<boolean, any>({
			path: `/api/Flow/VerificationApproval`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetCurrentFlowInfo
	 * @summary 获取当前流程节点信息
	 * @request POST:/api/Flow/GetCurrentFlowInfo
	 */
	getCurrentFlowInfo = (data: InputDto, params: RequestParams = {}) =>
		this.request<NextProcInfoVMBusinessResult, any>({
			path: `/api/Flow/GetCurrentFlowInfo`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetFlowTempInfoByOrgId
	 * @summary 获取流程模板
	 * @request POST:/api/Flow/GetFlowTempInfoByOrgId
	 */
	getFlowTempInfoByOrgId = (data: GetFlowTempInfo, params: RequestParams = {}) =>
		this.request<FlowTempProcInfoVM, any>({
			path: `/api/Flow/GetFlowTempInfoByOrgId`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetFlowApproveNextProcs
	 * @summary 获取流程需要选择的下级节点
	 * @request POST:/api/Flow/GetFlowApproveNextProcs
	 */
	getFlowApproveNextProcs = (data: GetFlowApproveNextProcsDto, params: RequestParams = {}) =>
		this.request<FlowNextProcsVMBusinessResult, any>({
			path: `/api/Flow/GetFlowApproveNextProcs`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetFlowProcComputeExecutor
	 * @summary 实时计算下个节点执行人
	 * @request POST:/api/Flow/GetFlowProcComputeExecutor
	 */
	getFlowProcComputeExecutor = (data: GetNextProcExecutorVM, params: RequestParams = {}) =>
		this.request<ExcutorWorkListAPIVMListBusinessResult, any>({
			path: `/api/Flow/GetFlowProcComputeExecutor`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetFlowProcExecutor
	 * @summary 实时计算下个节点执行人
	 * @request POST:/api/Flow/GetFlowProcExecutor
	 */
	getFlowProcExecutor = (data: GetFlowProcExecutorDto, params: RequestParams = {}) =>
		this.request<ExcutorWorkListAPIVMListBusinessResult, any>({
			path: `/api/Flow/GetFlowProcExecutor`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetFlowTempProcBaseInfoVm
	 * @summary 获取机构下流程模板信息
	 * @request POST:/api/Flow/GetFlowTempProcBaseInfoVM
	 */
	getFlowTempProcBaseInfoVm = (data: InputDto, params: RequestParams = {}) =>
		this.request<FlowTempProcBaseInfoVM, any>({
			path: `/api/Flow/GetFlowTempProcBaseInfoVM`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name GetFlowApproveProcs
	 * @summary 获取流程已经审批过的节点
	 * @request POST:/api/Flow/GetFlowApproveProcs
	 */
	getFlowApproveProcs = (data: InputDto, params: RequestParams = {}) =>
		this.request<FlowProcsInfoVM, any>({
			path: `/api/Flow/GetFlowApproveProcs`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name FlowAppendProc
	 * @summary 追加处理人
	 * @request POST:/api/Flow/FlowAppendProc
	 */
	flowAppendProc = (data: FlowOperateProcVM, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Flow/FlowAppendProc`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name FlowChangeProcExecutor
	 * @summary 更改处理人
	 * @request POST:/api/Flow/FlowChangeProcExecutor
	 */
	flowChangeProcExecutor = (data: FlowOperateProcVM, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Flow/FlowChangeProcExecutor`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Flow
	 * @name FlowInformer
	 * @summary 知会
	 * @request POST:/api/Flow/FlowInformer
	 */
	flowInformer = (data: FlowProcInformerVM, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Flow/FlowInformer`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
