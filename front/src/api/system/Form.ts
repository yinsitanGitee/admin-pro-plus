/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	CommonSelectDto,
	FormEditDto,
	FormEditDtoBusinessResult,
	FormListDtoCommonPageOutputDto,
	GetFormInputDto,
	InputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class FormApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Form
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Form/Query
	 */
	query = (data: GetFormInputDto, params: RequestParams = {}) =>
		this.request<FormListDtoCommonPageOutputDto, any>({
			path: `/api/Form/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Form
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Form/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<FormEditDtoBusinessResult, any>({
			path: `/api/Form/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Form
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/Form/Add
	 */
	add = (data: FormEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Form/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Form
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/Form/Edit
	 */
	edit = (data: FormEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Form/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Form
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/Form/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Form/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Form
	 * @name GetSelect
	 * @summary 获取下拉框
	 * @request GET:/api/Form/GetSelect
	 */
	getSelect = (params: RequestParams = {}) =>
		this.request<CommonSelectDto[], any>({
			path: `/api/Form/GetSelect`,
			method: 'GET',
			format: 'json',
			...params,
		});
}
