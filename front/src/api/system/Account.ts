/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	AccountEditDto,
	AccountEditDtoBusinessResult,
	AccountEditImageDto,
	AccountListDto,
	AccountListDtoCommonPageOutputDto,
	AccountSimgpleDto,
	BusinessResult,
	ChangePwdDto,
	GetAccountInputDto,
	InputDto,
	UserInfoDto,
	UserPermissionOutputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class AccountApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Account
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Account/Query
	 */
	query = (data: GetAccountInputDto, params: RequestParams = {}) =>
		this.request<AccountListDtoCommonPageOutputDto, any>({
			path: `/api/Account/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name QueryAll
	 * @summary 查询
	 * @request POST:/api/Account/QueryAll
	 */
	queryAll = (data: GetAccountInputDto, params: RequestParams = {}) =>
		this.request<AccountListDto[], any>({
			path: `/api/Account/QueryAll`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Account/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<AccountEditDtoBusinessResult, any>({
			path: `/api/Account/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name Add
	 * @summary 添加
	 * @request POST:/api/Account/Add
	 */
	add = (data: AccountEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name Edit
	 * @summary 编辑
	 * @request POST:/api/Account/Edit
	 */
	edit = (data: AccountEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name EditAccountImage
	 * @summary 修改头像
	 * @request POST:/api/Account/EditAccountImage
	 */
	editAccountImage = (data: AccountEditImageDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/EditAccountImage`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name Delete
	 * @summary 删除
	 * @request POST:/api/Account/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name ChangePassword
	 * @summary 修改密码
	 * @request POST:/api/Account/ChangePassword
	 */
	changePassword = (data: ChangePwdDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/ChangePassword`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name PersonCenterChangePassword
	 * @summary 个人中心修改密码
	 * @request POST:/api/Account/PersonCenterChangePassword
	 */
	personCenterChangePassword = (data: ChangePwdDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/PersonCenterChangePassword`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name EditSimpleInfo
	 * @summary 修改个人信息
	 * @request POST:/api/Account/EditSimpleInfo
	 */
	editSimpleInfo = (data: AccountSimgpleDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Account/EditSimpleInfo`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name GetCurrentUserInfo
	 * @summary 获取当前用户信息
	 * @request GET:/api/Account/GetCurrentUserInfo
	 */
	getCurrentUserInfo = (params: RequestParams = {}) =>
		this.request<UserPermissionOutputDto, any>({
			path: `/api/Account/GetCurrentUserInfo`,
			method: 'GET',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Account
	 * @name GetCurrentUser
	 * @summary 获取当前用户信息
	 * @request GET:/api/Account/GetCurrentUser
	 */
	getCurrentUser = (params: RequestParams = {}) =>
		this.request<UserInfoDto, any>({
			path: `/api/Account/GetCurrentUser`,
			method: 'GET',
			format: 'json',
			...params,
		});
}
