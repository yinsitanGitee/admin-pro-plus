/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { OrmConfigOutDto } from './data-contracts';
import { HttpClient, RequestParams } from './http-client';

export class TenantApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Tenant
	 * @name Query
	 * @summary 获取租户
	 * @request GET:/api/Tenant/Query
	 */
	query = (params: RequestParams = {}) =>
		this.request<OrmConfigOutDto[], any>({
			path: `/api/Tenant/Query`,
			method: 'GET',
			format: 'json',
			...params,
		});
}
