/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	DictionaryDetailEditDto,
	DictionaryDetailEditDtoBusinessResult,
	DictionaryDetailTreeListDto,
	DictionaryEditDto,
	DictionaryEditDtoBusinessResult,
	DictionaryListDtoCommonPageOutputDto,
	GetDictionaryDetailInputDto,
	GetDictionaryInputDto,
	InputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class DictionaryApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Dictionary/Query
	 */
	query = (data: GetDictionaryInputDto, params: RequestParams = {}) =>
		this.request<DictionaryListDtoCommonPageOutputDto, any>({
			path: `/api/Dictionary/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Dictionary/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<DictionaryEditDtoBusinessResult, any>({
			path: `/api/Dictionary/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name Add
	 * @summary 添加
	 * @request POST:/api/Dictionary/Add
	 */
	add = (data: DictionaryEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Dictionary/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name Edit
	 * @summary 编辑
	 * @request POST:/api/Dictionary/Edit
	 */
	edit = (data: DictionaryEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Dictionary/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name Delete
	 * @summary 删除
	 * @request POST:/api/Dictionary/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Dictionary/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name QueryDetail
	 * @summary 查询明细
	 * @request POST:/api/Dictionary/QueryDetail
	 */
	queryDetail = (data: GetDictionaryDetailInputDto, params: RequestParams = {}) =>
		this.request<DictionaryDetailTreeListDto[], any>({
			path: `/api/Dictionary/QueryDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name GetDetail
	 * @summary 获取明细单条
	 * @request POST:/api/Dictionary/GetDetail
	 */
	getDetail = (data: InputDto, params: RequestParams = {}) =>
		this.request<DictionaryDetailEditDtoBusinessResult, any>({
			path: `/api/Dictionary/GetDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name AddDetail
	 * @summary 添加明细
	 * @request POST:/api/Dictionary/AddDetail
	 */
	addDetail = (data: DictionaryDetailEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Dictionary/AddDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name EditDetail
	 * @summary 编辑明细
	 * @request POST:/api/Dictionary/EditDetail
	 */
	editDetail = (data: DictionaryDetailEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Dictionary/EditDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name DeleteDetail
	 * @summary 删除明细
	 * @request POST:/api/Dictionary/DeleteDetail
	 */
	deleteDetail = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Dictionary/DeleteDetail`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Dictionary
	 * @name GetCache
	 * @summary 获取缓存
	 * @request GET:/api/Dictionary/GetCache/{code}
	 */
	getCache = (code: string, params: RequestParams = {}) =>
		this.request<DictionaryDetailTreeListDto[], any>({
			path: `/api/Dictionary/GetCache/${code}`,
			method: 'GET',
			format: 'json',
			...params,
		});
}
