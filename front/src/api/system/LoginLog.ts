/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { GetLoginLogInputDto, LoginLogListDtoCommonPageOutputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class LoginLogApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags LoginLog
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/LoginLog/Query
	 */
	query = (data: GetLoginLogInputDto, params: RequestParams = {}) =>
		this.request<LoginLogListDtoCommonPageOutputDto, any>({
			path: `/api/LoginLog/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
