/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	GetMsgTaskInputDto,
	InputDto,
	MsgTaskListDto,
	MsgTaskListDtoBusinessResult,
	MsgTaskListDtoCommonPageOutputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class MsgTaskApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags MsgTask
	 * @name QueryMyWaitTask
	 * @summary 查询我的待办
	 * @request POST:/api/MsgTask/QueryMyWaitTask
	 */
	queryMyWaitTask = (params: RequestParams = {}) =>
		this.request<MsgTaskListDto[], any>({
			path: `/api/MsgTask/QueryMyWaitTask`,
			method: 'POST',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags MsgTask
	 * @name QueryMyTopRightWaitTask
	 * @summary 查询我的待办
	 * @request POST:/api/MsgTask/QueryMyTopRightWaitTask
	 */
	queryMyTopRightWaitTask = (params: RequestParams = {}) =>
		this.request<MsgTaskListDto[], any>({
			path: `/api/MsgTask/QueryMyTopRightWaitTask`,
			method: 'POST',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags MsgTask
	 * @name Query
	 * @summary 查询我的待办
	 * @request POST:/api/MsgTask/Query
	 */
	query = (data: GetMsgTaskInputDto, params: RequestParams = {}) =>
		this.request<MsgTaskListDtoCommonPageOutputDto, any>({
			path: `/api/MsgTask/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags MsgTask
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/MsgTask/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<MsgTaskListDtoBusinessResult, any>({
			path: `/api/MsgTask/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags MsgTask
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/MsgTask/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/MsgTask/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags MsgTask
	 * @name SetRead
	 * @summary 设置已阅
	 * @request POST:/api/MsgTask/SetRead
	 */
	setRead = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/MsgTask/SetRead`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
