/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { BusinessResult, GetReminderInputDto, InputDto, ReminderListDto, ReminderListDtoCommonPageOutputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class ReminderApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Reminder
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/Reminder/Query
	 */
	query = (data: GetReminderInputDto, params: RequestParams = {}) =>
		this.request<ReminderListDtoCommonPageOutputDto, any>({
			path: `/api/Reminder/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Reminder
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/Reminder/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<ReminderListDto, any>({
			path: `/api/Reminder/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Reminder
	 * @name SetRead
	 * @summary 设置已阅
	 * @request POST:/api/Reminder/SetRead
	 */
	setRead = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Reminder/SetRead`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
