/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { BusinessResult, DataPermissionEditDto, GetPermissionInputDto, InputDto, PermissionEditDto, PermissionListOutputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class PermissionApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Permission
	 * @name Query
	 * @summary 查询菜单授权
	 * @request POST:/api/Permission/Query
	 */
	query = (data: GetPermissionInputDto, params: RequestParams = {}) =>
		this.request<PermissionListOutputDto, any>({
			path: `/api/Permission/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Permission
	 * @name RoleSubmit
	 * @summary 保存菜单授权
	 * @request POST:/api/Permission/RoleSubmit
	 */
	roleSubmit = (data: PermissionEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Permission/RoleSubmit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Permission
	 * @name AccountSubmit
	 * @summary 保存菜单授权
	 * @request POST:/api/Permission/AccountSubmit
	 */
	accountSubmit = (data: PermissionEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Permission/AccountSubmit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Permission
	 * @name GetDataAuth
	 * @summary 查询数据权限授权
	 * @request POST:/api/Permission/GetDataAuth
	 */
	getDataAuth = (data: InputDto, params: RequestParams = {}) =>
		this.request<DataPermissionEditDto, any>({
			path: `/api/Permission/GetDataAuth`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Permission
	 * @name DataAuthSubmit
	 * @summary 保存数据权限授权
	 * @request POST:/api/Permission/DataAuthSubmit
	 */
	dataAuthSubmit = (data: DataPermissionEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/Permission/DataAuthSubmit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
