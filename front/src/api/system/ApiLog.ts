/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { ApiLogListDtoCommonPageOutputDto, GetApiLogInputDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class ApiLogApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags ApiLog
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/ApiLog/Query
	 */
	query = (data: GetApiLogInputDto, params: RequestParams = {}) =>
		this.request<ApiLogListDtoCommonPageOutputDto, any>({
			path: `/api/ApiLog/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
