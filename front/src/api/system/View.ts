/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { BusinessResult, InputDto, ViewEditDto, ViewEditDtoBusinessResult, ViewListDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class ViewApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags View
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/View/Query
	 */
	query = (params: RequestParams = {}) =>
		this.request<ViewListDto[], any>({
			path: `/api/View/Query`,
			method: 'POST',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags View
	 * @name Get
	 * @summary 查询单条
	 * @request POST:/api/View/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<ViewEditDtoBusinessResult, any>({
			path: `/api/View/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags View
	 * @name Add
	 * @summary 添加
	 * @request POST:/api/View/Add
	 */
	add = (data: ViewEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/View/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags View
	 * @name Edit
	 * @summary 编辑
	 * @request POST:/api/View/Edit
	 */
	edit = (data: ViewEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/View/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags View
	 * @name Delete
	 * @summary 删除
	 * @request POST:/api/View/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/View/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
