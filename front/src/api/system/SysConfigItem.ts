/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
	BusinessResult,
	GetConfigItemValueInputDto,
	GetSysConfigItemInputDto,
	InputDto,
	SysConfigItemEditDto,
	SysConfigItemEditDtoBusinessResult,
	SysConfigItemListDtoCommonPageOutputDto,
} from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class SysConfigItemApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags SysConfigItem
	 * @name Query
	 * @summary 查询
	 * @request POST:/api/SysConfigItem/Query
	 */
	query = (data: GetSysConfigItemInputDto, params: RequestParams = {}) =>
		this.request<SysConfigItemListDtoCommonPageOutputDto, any>({
			path: `/api/SysConfigItem/Query`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags SysConfigItem
	 * @name Get
	 * @summary 获取单条
	 * @request POST:/api/SysConfigItem/Get
	 */
	get = (data: InputDto, params: RequestParams = {}) =>
		this.request<SysConfigItemEditDtoBusinessResult, any>({
			path: `/api/SysConfigItem/Get`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags SysConfigItem
	 * @name GetConfigItemValues
	 * @summary 获取词典值
	 * @request POST:/api/SysConfigItem/GetConfigItemValues
	 */
	getConfigItemValues = (data: GetConfigItemValueInputDto, params: RequestParams = {}) =>
		this.request<Record<string, string>, any>({
			path: `/api/SysConfigItem/GetConfigItemValues`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags SysConfigItem
	 * @name Add
	 * @summary 数据添加
	 * @request POST:/api/SysConfigItem/Add
	 */
	add = (data: SysConfigItemEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/SysConfigItem/Add`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags SysConfigItem
	 * @name Edit
	 * @summary 数据编辑
	 * @request POST:/api/SysConfigItem/Edit
	 */
	edit = (data: SysConfigItemEditDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/SysConfigItem/Edit`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags SysConfigItem
	 * @name Delete
	 * @summary 数据删除
	 * @request POST:/api/SysConfigItem/Delete
	 */
	delete = (data: InputDto, params: RequestParams = {}) =>
		this.request<BusinessResult, any>({
			path: `/api/SysConfigItem/Delete`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
