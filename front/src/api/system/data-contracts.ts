/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/**
 * 任务请求方式<div>0：Post，Post请求；1：Get，Get请求；</div>
 * @format int32
 */
export type APIRequestTypeEnum = 0 | 1;

/**
 * 任务状态<div>0：Normal；1：Parse；</div>
 * @format int32
 */
export type APIStatusEnum = 0 | 1;

/**
 * 任务类型<div>0：System；1：Third；</div>
 * @format int32
 */
export type APITaskType = 0 | 1;

/** 定义编辑dto */
export interface AccountEditDto {
	/** Id */
	id?: string | null;
	/** 姓名 */
	name?: string | null;
	/** 账户和 */
	account?: string | null;
	/** 描述 */
	remark?: string | null;
	/** 手机号 */
	phone?: string | null;
	/** 头像 */
	headPic?: string | null;
	/** 密码 */
	password?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
	/** 部门id */
	departmentId?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 角色 */
	role?: string[] | null;
}

/** 统一返回业务结果数据 */
export interface AccountEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: AccountEditDto;
}

/** 定义AccountEditImageDto */
export interface AccountEditImageDto {
	/** 头像 */
	headPic?: string | null;
}

/** 定义列表dto */
export interface AccountListDto {
	/** Id */
	id?: string | null;
	/** 姓名 */
	name?: string | null;
	/** 账号 */
	account?: string | null;
	organizeName?: string | null;
	/** 部门名称 */
	departmentName?: string | null;
	/** 描述 */
	remark?: string | null;
	/** 部门id */
	departmentId?: string | null;
	/** 头像 */
	headPic?: string | null;
	/** 手机号 */
	phone?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
}

/** 定义分页通用返回dto */
export interface AccountListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: AccountListDto[] | null;
}

/** 定义AccountSimgpleDto */
export interface AccountSimgpleDto {
	/** 姓名 */
	name?: string | null;
	/** 账号 */
	account?: string | null;
	/** 描述 */
	remark?: string | null;
}

/** 定义编辑dto */
export interface ActionEditDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 流程数据源 */
	flowDataProvider?: string | null;
	/** 流程事件 */
	flowEvent?: string | null;
	/** 环节事件 */
	procEvent?: string | null;
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	enabledMark?: EnumBool;
	description?: string | null;
}

/** 统一返回业务结果数据 */
export interface ActionEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: ActionEditDto;
}

/** 定义列表dto */
export interface ActionListDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 流程数据源 */
	flowDataProvider?: string | null;
	/** 流程事件 */
	flowEvent?: string | null;
	/** 环节事件 */
	procEvent?: string | null;
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	enabledMark?: EnumBool;
	description?: string | null;
}

/** 定义分页通用返回dto */
export interface ActionListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: ActionListDto[] | null;
}

/** 定义列表dto */
export interface ApiLogListDto {
	/** Id */
	id?: string | null;
	/** 接口地址 */
	api?: string | null;
	/** 账号 */
	account?: string | null;
	/** 姓名 */
	name?: string | null;
	/** IP地址 */
	ip?: string | null;
	/** 请求参数 */
	request?: string | null;
	/** 响应参数 */
	response?: string | null;
	/** 执行时长 */
	executeTime?: string | null;
	/**
	 * 请求时间
	 * @format date-time
	 */
	requestTime?: string;
	/** 请求人 */
	requestUserId?: string | null;
	/** 异常信息 */
	error?: string | null;
	/** 通过状态<div>0：Fail；1：Success；</div> */
	status?: SuccessStateEnum;
}

/** 定义分页通用返回dto */
export interface ApiLogListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: ApiLogListDto[] | null;
}

export interface ApprovalAPIDto {
	appId?: string | null;
	/** 环节审批状态<div>0：None，无；1：Agree，同意；2：DisAgree，不同意；3：Termination，终止；4：BusinessDisAgree，不同意；5：SystemSkip，系统自动跳过；</div> */
	status?: EnumProcApprovalStatus;
	checkOpinion?: string | null;
	auditMemo?: string | null;
	flowId?: string | null;
	procId?: string | null;
	/** @format int32 */
	rejectType?: number;
	rejectProcId?: string | null;
	nextProcExecutor?: string[] | null;
	nextProcTempId?: string | null;
	nextProcId?: string | null;
}

/** 登录输入Dto */
export interface AuthInputDto {
	/** 账户 */
	account?: string | null;
	/** 密码 */
	password?: string | null;
}

/** 定义用户登录鉴权输入dto */
export interface AuthOutputDto {
	/** Token */
	token?: string | null;
}

/** 统一返回业务结果数据 */
export interface AuthOutputDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义用户登录鉴权输入dto */
	resultData?: AuthOutputDto;
}

/** 定义列表dto */
export interface BaseAreaListDto {
	/** Id */
	id?: string | null;
	/** 父节点编码 */
	parentId?: string | null;
	/** 地域编码 */
	areaCode?: string | null;
	/** 地域名称 */
	areaName?: string | null;
	/** 快记符 */
	quickQuery?: string | null;
	/**
	 * 层级
	 * @format int32
	 */
	layer?: number;
	/** 排序 */
	sortCode?: string | null;
	/** DataId */
	dataId?: string | null;
	/**
	 * Lon
	 * @format double
	 */
	lon?: number;
	/**
	 * Lat
	 * @format double
	 */
	lat?: number;
	/** 附加说明 */
	description?: string | null;
}

/** 统一返回业务结果数据 */
export interface BaseAreaListDtoListBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 返回结果数据 */
	resultData?: BaseAreaListDto[] | null;
}

/** 定义编辑Dto */
export interface BillCodeDetailEditDto {
	/** Id */
	id?: string | null;
	/** 主表id */
	billId?: string | null;
	/** 单据编号明细类型<div>0：Cust；1：Date；</div> */
	type?: BillCodeDetailTypeEnum;
	/** 规则 */
	value?: string | null;
	/** 描述 */
	description?: string | null;
	/**
	 * 排序
	 * @format int32
	 */
	sort?: number;
}

/**
 * 单据编号明细类型<div>0：Cust；1：Date；</div>
 * @format int32
 */
export type BillCodeDetailTypeEnum = 0 | 1;

/** 定义编辑dto */
export interface BillCodeEditDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/**
	 * 流水长度
	 * @format int32
	 */
	length?: number;
	/**
	 * 当前下标
	 * @format int64
	 */
	currentNum?: number;
	/** 初始值 */
	seedValue?: string | null;
	/** 描述 */
	description?: string | null;
	/** 单据编号配置明细 */
	details?: BillCodeDetailEditDto[] | null;
}

/** 统一返回业务结果数据 */
export interface BillCodeEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: BillCodeEditDto;
}

/** 定义列表dto */
export interface BillCodeListDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/**
	 * 流水长度
	 * @format int32
	 */
	length?: number;
	/**
	 * 当前下标
	 * @format int64
	 */
	currentNum?: number;
	/** 描述 */
	description?: string | null;
}

/** 定义分页通用返回dto */
export interface BillCodeListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: BillCodeListDto[] | null;
}

/** 统一返回结果(不带结果数据) */
export interface BusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
}

/**
 * <div>200：Success；500：Fail；</div>
 * @format int32
 */
export type BusinessState = 200 | 500;

/** 定义修改密码dto */
export interface ChangePwdDto {
	/** 用户id */
	ids?: string[] | null;
	/** 新密码 */
	password?: string | null;
}

export interface CommonOrgDepartDto {
	id?: string | null;
	parentId?: string | null;
	/** @format int32 */
	type?: number;
	organizeId?: string | null;
	name?: string | null;
	children?: CommonOrgDepartDto[] | null;
}

export interface CommonSelectDto {
	value?: string | null;
	label?: string | null;
}

/**
 * 通过状态<div>0：Ineffective；1：Effective；</div>
 * @format int32
 */
export type CommonStateEnum = 0 | 1;

export interface CommonTreeDto {
	id?: string | null;
	label?: string | null;
	attr1?: string | null;
	attr2?: string | null;
	attr3?: string | null;
	children?: CommonTreeDto[] | null;
}

export interface CondTempEntity {
	/** 主键 */
	id?: string | null;
	/** 是否删除 */
	isDelete?: boolean;
	/** 公司Id */
	tenant?: string | null;
	/** 流程模板Id */
	flowTempId?: string | null;
	/** 关联Id */
	refId?: string | null;
	/** <div>0：Flow；1：Proc；2：Line；3：WorkList；-1：None；</div> */
	refType?: EnumFlowEle;
	/** 表达式 */
	expression?: string | null;
}

/** 定义编辑dto */
export interface DataPermissionEditDto {
	/** 用户ids | 角色ids */
	ids?: string[] | null;
	/** 数据授权<div>0：All，全部机构；1：OwnOrganize，本人机构；2：CustOrganize，指定机构；3：OwnUser，本人数据；</div> */
	dataAuth?: EnumDataAuth;
	/** 自定义权限 */
	custDataAuth?: string | null;
}

/** 定义编辑dto */
export interface DepartmentEditDto {
	/** Id */
	id?: string | null;
	/** Ids */
	ids?: string[] | null;
	/** Code */
	billCode?: string[] | null;
	/** 名称 */
	name?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
	/** 父级id */
	parentId?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 描述 */
	remark?: string | null;
	/**
	 * 排序
	 * @format int32
	 */
	sort?: number;
}

/** 统一返回业务结果数据 */
export interface DepartmentEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: DepartmentEditDto;
}

export interface DepartmentListDto {
	id?: string | null;
	organizeId?: string | null;
	parentId?: string | null;
	organizeName?: string | null;
	name?: string | null;
	/** @format int32 */
	sort?: number;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
	remark?: string | null;
	children?: DepartmentListDto[] | null;
}

/** 定义词典编辑dto */
export interface DictionaryDetailEditDto {
	/** 主键id */
	billId?: string | null;
	/** Id */
	id?: string | null;
	/** 值 */
	value?: string | null;
	/** 父级id */
	parentId?: string | null;
	/** 是否有效 */
	isEffective?: boolean;
	/**
	 * 排序
	 * @format int32
	 */
	sort?: number;
}

/** 统一返回业务结果数据 */
export interface DictionaryDetailEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义词典编辑dto */
	resultData?: DictionaryDetailEditDto;
}

/** 定义词典树形dto */
export interface DictionaryDetailTreeListDto {
	/** Id */
	id?: string | null;
	/** 值 */
	value?: string | null;
	/** 父级id */
	parentId?: string | null;
	/** 是否有效 */
	isEffective?: boolean;
	/** 是否树 */
	isTree?: boolean;
	/**
	 * 排序
	 * @format int32
	 */
	sort?: number;
	/** 子级 */
	children?: DictionaryDetailTreeListDto[] | null;
}

/** 定义词典编辑dto */
export interface DictionaryEditDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 是否树级 */
	isTree?: boolean;
}

/** 统一返回业务结果数据 */
export interface DictionaryEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义词典编辑dto */
	resultData?: DictionaryEditDto;
}

/** 定义词典列表dto */
export interface DictionaryListDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 是否树 */
	isTree?: boolean;
}

/** 定义分页通用返回dto */
export interface DictionaryListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: DictionaryListDto[] | null;
}

/**
 * <div>0：FALSE；1：TRUE；-1：None；</div>
 * @format int32
 */
export type EnumBool = 0 | 1 | -1;

/**
 * 数据授权<div>0：All，全部机构；1：OwnOrganize，本人机构；2：CustOrganize，指定机构；3：OwnUser，本人数据；</div>
 * @format int32
 */
export type EnumDataAuth = 0 | 1 | 2 | 3;

/**
 * 执行人关联类型<div>0：Account，指定账号；3：Role，角色；</div>
 * @format int32
 */
export type EnumExecutorRefType = 0 | 3;

/**
 * <div>0：Flow；1：Proc；2：Line；3：WorkList；-1：None；</div>
 * @format int32
 */
export type EnumFlowEle = 0 | 1 | 2 | 3 | -1;

/**
 * 审批通用状态<div>0：NoSubmit，待提交；1：Approval，审批中；2：Through，审批通过；3：NotThrough，审批不通过；4：Invalid，作废；</div>
 * @format int32
 */
export type EnumFlowState = 0 | 1 | 2 | 3 | 4;

/**
 * 流程状态<div>0：Executing，执行中；1：Complete，已归档；2：Termination，已终止；3：ReStarted，重新开始；</div>
 * @format int32
 */
export type EnumFlowStatus = 0 | 1 | 2 | 3;

/**
 * <div>0：None，无；1：Agree，同意；2：DisAgree，不同意；3：CustDefine，自定义；</div>
 * @format int32
 */
export type EnumLineCondType = 0 | 1 | 2 | 3;

/**
 * 操作日志状态<div>1：Agree；2：DisAgree；3：ChangeExecutor；4：BusinessDisAgree；5：BillBack；6：Skip；</div>
 * @format int32
 */
export type EnumOperateState = 1 | 2 | 3 | 4 | 5 | 6;

/**
 * 环节审批状态<div>0：None，无；1：Agree，同意；2：DisAgree，不同意；3：Termination，终止；4：BusinessDisAgree，不同意；5：SystemSkip，系统自动跳过；</div>
 * @format int32
 */
export type EnumProcApprovalStatus = 0 | 1 | 2 | 3 | 4 | 5;

/**
 * 环节节点审批类型<div>1：OneApproval；2：AllApproval；</div>
 * @format int32
 */
export type EnumProcApprovalType = 1 | 2;

/**
 * 环节节点执行人审批类型<div>1：OneProc；2：EveryOneProc；</div>
 * @format int32
 */
export type EnumProcExecutorType = 1 | 2;

/**
 * 环节状态<div>0：Wait，待处理；1：Handing，处理中；2：Complete，已处理；3：Finished，已完结；4：ProcException，异常；</div>
 * @format int32
 */
export type EnumProcStatus = 0 | 1 | 2 | 3 | 4;

/**
 * 环节类型<div>0：Normal；1：Start；2：End；4：Branch；5：OA；</div>
 * @format int32
 */
export type EnumProcType = 0 | 1 | 2 | 4 | 5;

/**
 * <div>0：SystemRemin；1：Mesage；2：BusinessWarn；</div>
 * @format int32
 */
export type EnumReminderContentType = 0 | 1 | 2;

/**
 * <div>0：Detail；1：Dialog；2：Tab；3：NewTarget；</div>
 * @format int32
 */
export type EnumReminderOpenType = 0 | 1 | 2 | 3;

/**
 * <div>0：Normal；1：Strong；</div>
 * @format int32
 */
export type EnumReminderType = 0 | 1;

/**
 * <div>0：None，不发；1：SelectUser，指定人员；2：FlowExcutor，流程中的执行人；3：FlowAllExcutor，流程中所有执行人；</div>
 * @format int32
 */
export type EnumSendMessageToExcuterType = 0 | 1 | 2 | 3;

export interface ExcutorWorkListAPIVM {
	name?: string | null;
	id?: string | null;
	phone?: string | null;
	headPic?: string | null;
	/** 环节审批状态<div>0：None，无；1：Agree，同意；2：DisAgree，不同意；3：Termination，终止；4：BusinessDisAgree，不同意；5：SystemSkip，系统自动跳过；</div> */
	approvalStatus?: EnumProcApprovalStatus;
	/** @format date-time */
	complateTime?: string | null;
	checkOpinion?: string | null;
	/** @format int32 */
	state?: number;
}

/** 统一返回业务结果数据 */
export interface ExcutorWorkListAPIVMListBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 返回结果数据 */
	resultData?: ExcutorWorkListAPIVM[] | null;
}

export interface ExecutorTempEntity {
	/** 主键 */
	id?: string | null;
	/** 是否删除 */
	isDelete?: boolean;
	/** 公司Id */
	tenant?: string | null;
	/** 流程模板Id */
	flowTempId?: string | null;
	/** 关联人Id */
	refId?: string | null;
	/** 关联人名称 */
	refName?: string | null;
	/** 执行人关联类型<div>0：Account，指定账号；3：Role，角色；</div> */
	refType?: EnumExecutorRefType;
	/** 环节编号 */
	procId?: string | null;
	/** 是否限定和提交人同机构 */
	isLimtOrgan?: boolean;
}

/** 定义编辑dto */
export interface FileEditDto {
	/** Id */
	id?: string | null;
	/** 地址 */
	url?: string | null;
	/** 名称 */
	name?: string | null;
	/**
	 * 关联类型
	 * @format int32
	 */
	refType?: number | null;
	refId?: string | null;
}

/** 定义列表dto */
export interface FileListDto {
	/** Id */
	id?: string | null;
	/** 路径 */
	url?: string | null;
	/** 名称 */
	name?: string | null;
	/**
	 * 关联类型
	 * @format int32
	 */
	refType?: number | null;
	/** RefId */
	refId?: string | null;
}

/** 统一返回业务结果数据 */
export interface FileListDtoListBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 返回结果数据 */
	resultData?: FileListDto[] | null;
}

/** 定义列表dto */
export interface FlowBusinessListDto {
	/** Id */
	id?: string | null;
	/**
	 * 主表数据审批状态
	 * @format int32
	 */
	state?: number;
	/** 日志类型 */
	type?: string | null;
	/** 日志标题 */
	title?: string | null;
	/** 知会人 */
	informerNames?: string | null;
	/**
	 * 操作日期
	 * @format date-time
	 */
	operateDate?: string;
	/** 处理人 */
	executors?: FlowOperateRecordExecutors[] | null;
}

export interface FlowInfoAPIVM {
	actionCode?: string | null;
	orgId?: string | null;
	unitId?: string | null;
	flowTempId?: string | null;
	nextProcExecutor?: string[] | null;
	rejectNextProcTempId?: string | null;
	nextProcTempId?: string | null;
	nextProcId?: string | null;
}

export interface FlowNextBaseProcsVM {
	procName?: string | null;
	procId?: string | null;
	isSelectNextExecutor?: boolean;
}

export interface FlowNextProcsVM {
	lists?: FlowNextBaseProcsVM[] | null;
	flowTempId?: string | null;
	orgId?: string | null;
}

/** 统一返回业务结果数据 */
export interface FlowNextProcsVMBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	resultData?: FlowNextProcsVM;
}

export interface FlowOperateProcVM {
	account?: FlowProcAccountVM;
	flowId?: string | null;
}

/** 审批记录执行人 */
export interface FlowOperateRecordExecutors {
	/** 记录主表Id */
	recordId?: string | null;
	/** 操作日志状态<div>1：Agree；2：DisAgree；3：ChangeExecutor；4：BusinessDisAgree；5：BillBack；6：Skip；</div> */
	state?: EnumOperateState;
	/** 内容 */
	content?: string | null;
	/** 操作人 */
	operateUserName?: string | null;
	/**
	 * 操作时间
	 * @format date-time
	 */
	operateDate?: string;
	/** 头像地址 */
	headPic?: string | null;
}

export interface FlowProcAccountVM {
	id?: string | null;
	name?: string | null;
}

export interface FlowProcInformerVM {
	informerIds?: string[] | null;
	informerNames?: string[] | null;
	flowId?: string | null;
}

/** FlowProcVM */
export interface FlowProcVM {
	/** Id */
	id?: string | null;
	/**
	 * State
	 * @format int32
	 */
	state?: number;
	/** 环节节点审批类型<div>1：OneApproval；2：AllApproval；</div> */
	procApprovalType?: EnumProcApprovalType;
	/** 日志类型 */
	type?: string | null;
	/** 环节类型<div>0：Normal；1：Start；2：End；4：Branch；5：OA；</div> */
	procType?: EnumProcType;
	/** 日志标题 */
	name?: string | null;
	/** 环节状态<div>0：Wait，待处理；1：Handing，处理中；2：Complete，已处理；3：Finished，已完结；4：ProcException，异常；</div> */
	procStatus?: EnumProcStatus;
	/** IsSelectNextProc */
	isSelectNextProc?: boolean;
	/** IsSelectNextExecutor */
	isSelectExecutor?: boolean;
	/** 描述 */
	description?: string | null;
	/** 知会人 */
	informerNames?: string | null;
	/** 是否处理中节点 */
	isHangindg?: boolean;
	/** 处理人 */
	executors?: ExcutorWorkListAPIVM[] | null;
}

export interface FlowProcsInfoVM {
	procs?: FlowProcsVM[] | null;
	currentProcName?: string | null;
}

export interface FlowProcsVM {
	name?: string | null;
	originalSchemeProcId?: string | null;
	schemeProcId?: string | null;
	id?: string | null;
	/** 环节状态<div>0：Wait，待处理；1：Handing，处理中；2：Complete，已处理；3：Finished，已完结；4：ProcException，异常；</div> */
	procStatus?: EnumProcStatus;
}

/** 定义编辑dto */
export interface FlowTempEditDto {
	/** Id */
	id?: string | null;
	/** 业务标识编号 */
	actionCode?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 所属机构 */
	orgId?: string | null;
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	enabledMark?: EnumBool;
	/**
	 * 限定完成时间(天)
	 * @format int32
	 */
	limtTime?: number;
	description?: string | null;
	/** 是否发送所有消息给执行人 */
	enableSendMessageToAllExcuter?: boolean;
	/** <div>0：None，不发；1：SelectUser，指定人员；2：FlowExcutor，流程中的执行人；3：FlowAllExcutor，流程中所有执行人；</div> */
	sendMessageToExcuterType?: EnumSendMessageToExcuterType;
	/** 归档后发消息指定人员 */
	sendMessageSelectUsers?: string | null;
	/** 是否自动通过 */
	isApprovalPass?: boolean;
}

/** 统一返回业务结果数据 */
export interface FlowTempEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: FlowTempEditDto;
}

/** 定义列表dto */
export interface FlowTempListDto {
	/** Id */
	id?: string | null;
	/** 业务标识编号 */
	actionCode?: string | null;
	/** 单据名称 */
	actionName?: string | null;
	/** 编号 */
	code?: string | null;
	/** 机构名称 */
	orgName?: string | null;
	/** 名称 */
	name?: string | null;
	/** 所属机构 */
	orgId?: string | null;
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	enabledMark?: EnumBool;
	/**
	 * 限定完成时间(天)
	 * @format int32
	 */
	limtTime?: number;
	description?: string | null;
	/** 是否发送所有消息给执行人 */
	enableSendMessageToAllExcuter?: boolean;
	/** <div>0：None，不发；1：SelectUser，指定人员；2：FlowExcutor，流程中的执行人；3：FlowAllExcutor，流程中所有执行人；</div> */
	sendMessageToExcuterType?: EnumSendMessageToExcuterType;
	/** 归档后发消息指定人员 */
	sendMessageSelectUsers?: string | null;
	/** 是否自动通过 */
	isApprovalPass?: boolean;
}

/** 定义分页通用返回dto */
export interface FlowTempListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: FlowTempListDto[] | null;
}

export interface FlowTempListVM {
	name?: string | null;
	orgId?: string | null;
	orgName?: string | null;
	code?: string | null;
	id?: string | null;
}

export interface FlowTempProcBaseInfoVM {
	nextProcId?: string | null;
	rejectNextProcTempId?: string | null;
	currentProcSchemeId?: string | null;
	flowTempId?: string | null;
	isSelectNextProc?: boolean;
	isSelectNextExecutor?: boolean;
}

export interface FlowTempProcInfoVM {
	nextProcId?: string | null;
	rejectNextProcTempId?: string | null;
	currentProcSchemeId?: string | null;
	flowTempId?: string | null;
	isSelectNextProc?: boolean;
	isSelectNextExecutor?: boolean;
	/** @format int32 */
	count?: number;
	temps?: FlowTempListVM[] | null;
}

/** 定义编辑dto */
export interface FormBillDetailDto {
	/** Id */
	id?: string | null;
	/** 表单id */
	formId?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 审批通用状态<div>0：NoSubmit，待提交；1：Approval，审批中；2：Through，审批通过；3：NotThrough，审批不通过；4：Invalid，作废；</div> */
	state?: EnumFlowState;
	/** 机构id */
	organizeId?: string | null;
	/** 机构名称 */
	organizeName?: string | null;
	/** 流程id */
	processId?: string | null;
	/** 表单详情 */
	formDetail?: string | null;
	/** 表单值详情 */
	formDetailData?: string | null;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createDateTime?: string;
	/** 创建者名称 */
	createUserName?: string | null;
}

/** 统一返回业务结果数据 */
export interface FormBillDetailDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: FormBillDetailDto;
}

/** 定义编辑dto */
export interface FormBillEditDto {
	/** Id */
	id?: string | null;
	/**
	 * 操作类型 1是提交
	 * @format int32
	 */
	opeartType?: number;
	/** 表单id */
	formId?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 流程id */
	processId?: string | null;
	/** 表单详情 */
	formDetail?: string | null;
	/** 表单值详情 */
	formDetailData?: string | null;
	flowInfo?: FlowInfoAPIVM;
}

/** 统一返回业务结果数据 */
export interface FormBillEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: FormBillEditDto;
}

/** 定义列表dto */
export interface FormBillListDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 机构名称 */
	organizeName?: string | null;
	/** 流程id */
	processId?: string | null;
	/** 审批通用状态<div>0：NoSubmit，待提交；1：Approval，审批中；2：Through，审批通过；3：NotThrough，审批不通过；4：Invalid，作废；</div> */
	state?: EnumFlowState;
	/** 下级节点处理人 */
	nextApprovalName?: string | null;
	/** 下级节点名称 */
	nextApprovalProcName?: string | null;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createDateTime?: string;
	/** 创建者名称 */
	createUserName?: string | null;
}

/** 定义分页通用返回dto */
export interface FormBillListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: FormBillListDto[] | null;
}

/** 定义编辑dto */
export interface FormEditDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
	/** 类型 */
	type?: string | null;
	/** 表单详情 */
	formDetail?: string | null;
	/** 描述 */
	description?: string | null;
}

/** 统一返回业务结果数据 */
export interface FormEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: FormEditDto;
}

/** 定义列表dto */
export interface FormListDto {
	/** Id */
	id?: string | null;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 机构名称 */
	organizeName?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
	/** 类型 */
	type?: string | null;
	/** 表单详情 */
	formDetail?: string | null;
	/** 描述 */
	description?: string | null;
}

/** 定义分页通用返回dto */
export interface FormListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: FormListDto[] | null;
}

/** 定义查询dto */
export interface GetAccountInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 姓名 */
	name?: string | null;
	/** 手机号 */
	phone?: string | null;
	organizeId?: string | null;
	departmentId?: string | null;
}

/** 定义获取分页输入dto */
export interface GetActionInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
}

/** 定义查询dto */
export interface GetApiLogInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 接口地址 */
	api?: string | null;
}

/** 定义查询dto */
export interface GetBaseAreaInputDto {
	parentId?: string | null;
}

/** 定义查询输入dto */
export interface GetBillCodeInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 名称 */
	name?: string | null;
}

/** 定义获取任务列表输入dto */
export interface GetCommTasksInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 任务名称 */
	taskName?: string | null;
	/** Content */
	content?: string | null;
	/** 编号 */
	code?: string | null;
}

/** 定义获取参数键输入dto */
export interface GetConfigItemValueInputDto {
	/** 键 */
	configKey?: string | null;
	/** 键 */
	configKeys?: string[] | null;
}

/** 定义获取输入dto */
export interface GetDepartmentInputDto {
	/** 部门 */
	departmentIds?: string[] | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
	/** 机构id */
	organizeId?: string | null;
}

/** 定义词典查询输入dto */
export interface GetDictionaryDetailInputDto {
	/** Value */
	value?: string | null;
	/** 单据id */
	billId?: string | null;
	/** 是否树 */
	isTree?: boolean;
}

/** 定义查询词典dto */
export interface GetDictionaryInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
}

export interface GetFlowApproveNextProcsDto {
	flowId?: string | null;
	orgId?: string | null;
	flowTempId?: string | null;
	currentProcSchemeId?: string | null;
}

/** 获取流程未审批节点dto */
export interface GetFlowNotApprovalDto {
	/** 流程Id */
	flowId?: string | null;
	/** 处理中执行人是否特殊处理 */
	isHandlePro?: boolean;
}

export interface GetFlowProcExecutorDto {
	procId?: string | null;
	flowId?: string | null;
}

export interface GetFlowTempInfo {
	actionCode?: string | null;
	sourceId?: string | null;
	orgId?: string | null;
}

/** 定义获取分页输入dto */
export interface GetFlowTempInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 编号 */
	code?: string | null;
	/** 名称 */
	name?: string | null;
	/** 业务标识 */
	actionCode?: string | null;
	/** 机构Id */
	orgId?: string | null;
}

/** 定义查询dto */
export interface GetFormBillInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 名称 */
	name?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 编号 */
	code?: string | null;
	/**
	 * 查询类型
	 * 1、我提交的
	 * 2、待我审批的
	 * 3、我审批过的
	 * @format int32
	 */
	type?: number;
}

/** 定义查询dto */
export interface GetFormInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 名称 */
	name?: string | null;
	/** 机构id */
	organizeId?: string | null;
	/** 编号 */
	code?: string | null;
}

/** 定义查询dto */
export interface GetLoginLogInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 名称 */
	name?: string | null;
	/** 账号 */
	account?: string | null;
}

export interface GetMsgTaskInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** @format int32 */
	state?: number | null;
	/** @format int32 */
	sysType?: number | null;
	title?: string | null;
	taskContent?: string | null;
	description?: string | null;
}

export interface GetNextProcExecutorVM {
	createUserId?: string | null;
	orgId?: string | null;
	flowTempId?: string | null;
	procTempId?: string | null;
	unitId?: string | null;
	appCode?: string | null;
	appId?: string | null;
	id?: string | null;
	flowDataProvider?: string | null;
	proZoneId?: string | null;
	pavZoneId?: string | null;
}

/** 定义获取输入dto */
export interface GetOrganizeInputDto {
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	state?: EnumBool;
}

/** 定义输入dto */
export interface GetPermissionInputDto {
	/**
	 * 1 => 用户
	 * 2 => 角色
	 * @format int32
	 */
	type?: number;
	/** Id */
	id?: string | null;
}

export interface GetReminderInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	isRead?: boolean | null;
	/** <div>0：SystemRemin；1：Mesage；2：BusinessWarn；</div> */
	contentType?: EnumReminderContentType;
	content?: string | null;
}

/** 定义查询dto */
export interface GetRoleInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 名称 */
	name?: string | null;
}

/** 定义获取参数分页输入dto */
export interface GetSysConfigItemInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 键 */
	configKey?: string | null;
	/** 描述 */
	remark?: string | null;
}

/** 定义查询dto */
export interface GetTaskLogInputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/** 任务Id */
	taskId?: string | null;
	/** 请求参数 */
	requestBody?: string | null;
	/** 请求头 */
	requestHeader?: string | null;
}

/** 定义输入dto */
export interface InputDto {
	/** Id */
	id?: string | null;
	/** Ids */
	ids?: string[] | null;
	/** Code */
	billCode?: string[] | null;
}

/**
 * 任务操作类型<div>1：Add；2：Delete；3：Update；4：Parse；5：Start；6：ExecuteImmediately；</div>
 * @format int32
 */
export type JobOperateEnum = 1 | 2 | 3 | 4 | 5 | 6;

export interface LineTempDto {
	/** 主键 */
	id?: string | null;
	/** 是否删除 */
	isDelete?: boolean;
	/** 公司Id */
	tenant?: string | null;
	/** 流程模板Id */
	flowTempId?: string | null;
	/** 流程图中线Id */
	schemeLineId?: string | null;
	/** 业务标识编号 */
	actionCode?: string | null;
	/** 名称 */
	name?: string | null;
	/** 编号 */
	code?: string | null;
	/** 来源环节 */
	fromSchemeProcId?: string | null;
	/** 目标环节 */
	toSchemeProcId?: string | null;
	/** <div>0：None，无；1：Agree，同意；2：DisAgree，不同意；3：CustDefine，自定义；</div> */
	lineCondType?: EnumLineCondType;
	/** 流程线图形其他信息 */
	schemeLineInfo?: string | null;
	/** 条件信息 */
	conds?: CondTempEntity[] | null;
}

/** 定义列表dto */
export interface LoginLogListDto {
	/** Id */
	id?: string | null;
	/** IP地址 */
	ip?: string | null;
	/** 账号 */
	account?: string | null;
	/** 用户姓名 */
	name?: string | null;
	/** 请求参数 */
	request?: string | null;
	/** 响应参数 */
	response?: string | null;
	/** 执行时长 */
	executeTime?: string | null;
	/**
	 * 请求时间
	 * @format date-time
	 */
	requestTime?: string;
	/** 通过状态<div>0：Fail；1：Success；</div> */
	status?: SuccessStateEnum;
}

/** 定义分页通用返回dto */
export interface LoginLogListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: LoginLogListDto[] | null;
}

export interface MsgTaskListDto {
	/** Id */
	id?: string | null;
	title?: string | null;
	taskContent?: string | null;
	description?: string | null;
	sendUserName?: string | null;
	/** @format date-time */
	sendDate?: string;
	/** @format int32 */
	sysType?: number;
	/** 待办来源<div>1：None；2：Form；</div> */
	sourceTypeValue?: MsgTaskTypeEnum;
	sourceId?: string | null;
	/** @format date-time */
	completeTime?: string | null;
	/** @format int32 */
	state?: number;
}

/** 统一返回业务结果数据 */
export interface MsgTaskListDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	resultData?: MsgTaskListDto;
}

/** 定义分页通用返回dto */
export interface MsgTaskListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: MsgTaskListDto[] | null;
}

/**
 * 待办来源<div>1：None；2：Form；</div>
 * @format int32
 */
export type MsgTaskTypeEnum = 1 | 2;

export interface NextProcInfoVM {
	currentProcId?: string | null;
	currentProcSchemeId?: string | null;
	nextProcId?: string | null;
	flowTempId?: string | null;
	isSelectNextProc?: boolean;
	isSelectNextExecutor?: boolean;
	nextProcName?: string | null;
	nextProcExecutors?: string | null;
}

/** 统一返回业务结果数据 */
export interface NextProcInfoVMBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	resultData?: NextProcInfoVM;
}

/** 定义编辑dto */
export interface OrganizeEditDto {
	/** Id */
	id?: string | null;
	/** Ids */
	ids?: string[] | null;
	/** Code */
	billCode?: string[] | null;
	/** @format int32 */
	category?: number;
	parentId?: string | null;
	name?: string | null;
	/** @format int32 */
	sort?: number;
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	state?: EnumBool;
	remark?: string | null;
}

/** 统一返回业务结果数据 */
export interface OrganizeEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: OrganizeEditDto;
}

export interface OrganizeListDto {
	id?: string | null;
	parentId?: string | null;
	name?: string | null;
	/** @format int32 */
	sort?: number;
	/** <div>0：FALSE；1：TRUE；-1：None；</div> */
	state?: EnumBool;
	remark?: string | null;
	children?: OrganizeListDto[] | null;
}

/** 定义orm输入dto */
export interface OrmConfigOutDto {
	/** TenantName */
	tenantName?: string | null;
	/** TenantKey */
	tenantKey?: string | null;
}

/** 定义编辑dto */
export interface PermissionEditDto {
	halfViews?: string | null;
	views?: string | null;
	/** 用户ids | 角色ids */
	ids?: string[] | null;
}

/** 定义列表dto */
export interface PermissionListOutputDto {
	halfViews?: string | null;
	views?: string | null;
	/** 所有视图 */
	allViews?: ViewListDto[] | null;
}

export interface ProcTempDto {
	/** 主键 */
	id?: string | null;
	/** 是否删除 */
	isDelete?: boolean;
	/** 公司Id */
	tenant?: string | null;
	/** 流程图中环节Id */
	schemeProcId?: string | null;
	/** 流程模板Id */
	flowTempId?: string | null;
	/** 业务标识编号 */
	actionCode?: string | null;
	/** 环节名称 */
	name?: string | null;
	/** 环节编号 */
	code?: string | null;
	/** 环节类型<div>0：Normal；1：Start；2：End；4：Branch；5：OA；</div> */
	type?: EnumProcType;
	/**
	 * 限定完成时间
	 * @format int32
	 */
	limtTime?: number | null;
	/** 限定完成类型 */
	limtTimeType?: string | null;
	/** 图形图形其他信息 */
	schemeProcInfo?: string | null;
	/** 环节节点审批类型<div>1：OneApproval；2：AllApproval；</div> */
	procApprovalType?: EnumProcApprovalType;
	/** 环节节点执行人审批类型<div>1：OneProc；2：EveryOneProc；</div> */
	procExecutorType?: EnumProcExecutorType;
	/** 是否选择下级节点 */
	isSelectNextProc?: boolean;
	/** 审批用户是否由前一个节点选择 */
	isSelectExecutor?: boolean;
	/**
	 * 原始模板节点ID
	 * 用于节点属性、 当前节点用户一人一个节点
	 */
	originalSchemeProcId?: string | null;
	/**
	 * 排序
	 * 从递归中按照遍历顺序排
	 * @format int32
	 */
	sort?: number;
	/** 执行人 */
	executors?: ExecutorTempEntity[] | null;
}

export interface ReminderListDto {
	/** Id */
	id?: string | null;
	/** 待办来源<div>1：None；2：Form；</div> */
	sourceTypeValue?: MsgTaskTypeEnum;
	accountId?: string | null;
	title?: string | null;
	content?: string | null;
	url?: string | null;
	/** <div>0：Detail；1：Dialog；2：Tab；3：NewTarget；</div> */
	openType?: EnumReminderOpenType;
	/** <div>0：Normal；1：Strong；</div> */
	remainType?: EnumReminderType;
	/** <div>0：SystemRemin；1：Mesage；2：BusinessWarn；</div> */
	contentType?: EnumReminderContentType;
	isRead?: boolean;
	/** @format date-time */
	readTime?: string | null;
	msgId?: string | null;
	sourceId?: string | null;
	/** @format int32 */
	sourceType?: number;
	actionCode?: string | null;
	appId?: string | null;
	/** @format date-time */
	createTime?: string;
}

/** 定义分页通用返回dto */
export interface ReminderListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: ReminderListDto[] | null;
}

/** 定义编辑dto */
export interface RoleEditDto {
	/** Id */
	id?: string | null;
	/** 名称 */
	name?: string | null;
	/** 描述 */
	remark?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
}

/** 统一返回业务结果数据 */
export interface RoleEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: RoleEditDto;
}

/** 定义列表dto */
export interface RoleListDto {
	/** Id */
	id?: string | null;
	/** 名称 */
	name?: string | null;
	/** 描述 */
	remark?: string | null;
	/** 通过状态<div>0：Ineffective；1：Effective；</div> */
	state?: CommonStateEnum;
}

/** 定义分页通用返回dto */
export interface RoleListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: RoleListDto[] | null;
}

/** 流程dto */
export interface SchemeTempDto {
	/** 主键 */
	id?: string | null;
	/** 是否删除 */
	isDelete?: boolean;
	/** 创建者Id */
	createUserId?: string | null;
	/** 创建者名称 */
	createUserName?: string | null;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createDateTime?: string;
	/** 修改者Id */
	modifyUserId?: string | null;
	/** 修改者名称 */
	modifyUserName?: string | null;
	/**
	 * 修改时间
	 * @format date-time
	 */
	modifyDateTime?: string | null;
	/** 公司Id */
	tenant?: string | null;
	/** 业务标识编号 */
	actionCode?: string | null;
	/** 流程模板编号 */
	flowTempId?: string | null;
	/** 业务Id */
	appId?: string | null;
	/** 业务编号 */
	appCode?: string | null;
	/** 业务摘要 */
	appSummary?: string | null;
	/** 流程事件 */
	flowEvent?: string | null;
	/** 所属机构 */
	orgId?: string | null;
	/** 流程状态<div>0：Executing，执行中；1：Complete，已归档；2：Termination，已终止；3：ReStarted，重新开始；</div> */
	flowStatus?: EnumFlowStatus;
	/**
	 * 开始时间
	 * @format date-time
	 */
	beginTime?: string;
	/**
	 * 限定完成时间
	 * @format int32
	 */
	limtTime?: number | null;
	/**
	 * 结束时间
	 * @format date-time
	 */
	endTime?: string | null;
	description?: string | null;
	/** 终止理由 */
	abandonReason?: string | null;
	/** 是否发送所有消息给执行人 */
	enableSendMessageToAllExcuter?: boolean;
	/** <div>0：None，不发；1：SelectUser，指定人员；2：FlowExcutor，流程中的执行人；3：FlowAllExcutor，流程中所有执行人；</div> */
	sendMessageToExcuterType?: EnumSendMessageToExcuterType;
	/** 归档后发消息指定人员 */
	sendMessageSelectUsers?: string | null;
	/** 核算机构Id */
	unitId?: string | null;
	/** 是否自动通过 */
	isApprovalPass?: boolean;
	proZoneId?: string | null;
	/** 楼栋片区负责人 */
	pavZoneId?: string | null;
	/** 待办标题 */
	taskTitle?: string | null;
	/** 待办内容 */
	taskContent?: string | null;
	/** 待办描述 */
	taskDescription?: string | null;
	/** 待办来源<div>1：None；2：Form；</div> */
	sourceTypeValue?: MsgTaskTypeEnum;
	/** 环节 */
	procs?: ProcTempDto[] | null;
	/** 线 */
	lines?: LineTempDto[] | null;
}

/** 统一返回业务结果数据 */
export interface StringListBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 返回结果数据 */
	resultData?: string[] | null;
}

/** 定义升级dto */
export interface SubDbUpdateItemDto {
	/** 执行信息 */
	excuteMsg?: string | null;
	/** 方法签名 */
	methodName?: string | null;
	/** 命名空间 */
	assemFullName?: string | null;
	/**
	 * 版本号
	 * @format double
	 */
	ver?: number;
}

/**
 * 通过状态<div>0：Fail；1：Success；</div>
 * @format int32
 */
export type SuccessStateEnum = 0 | 1;

/** 单据编号明细配置 */
export interface SysBillCodeDetailEntity {
	/** 主键 */
	id?: string | null;
	/** 是否删除 */
	isDelete?: boolean;
	/** 创建者Id */
	createUserId?: string | null;
	/** 创建者名称 */
	createUserName?: string | null;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createDateTime?: string;
	/** 修改者Id */
	modifyUserId?: string | null;
	/** 修改者名称 */
	modifyUserName?: string | null;
	/**
	 * 修改时间
	 * @format date-time
	 */
	modifyDateTime?: string | null;
	/** 公司Id */
	tenant?: string | null;
	/** 主表id */
	billId?: string | null;
	/** 单据编号明细类型<div>0：Cust；1：Date；</div> */
	type?: BillCodeDetailTypeEnum;
	/** 值 */
	value?: string | null;
	/** 描述 */
	description?: string | null;
	/**
	 * 排序
	 * @format int32
	 */
	sort?: number;
}

/** 统一返回业务结果数据 */
export interface SysBillCodeDetailEntityListBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 返回结果数据 */
	resultData?: SysBillCodeDetailEntity[] | null;
}

export interface SysConfigItemEditDto {
	/** Id */
	id?: string | null;
	/** ConfigKey */
	configKey?: string | null;
	/** ConfigValue */
	configValue?: string | null;
	/** Remark */
	remark?: string | null;
}

/** 统一返回业务结果数据 */
export interface SysConfigItemEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	resultData?: SysConfigItemEditDto;
}

export interface SysConfigItemListDto {
	/** Id */
	id?: string | null;
	/** ConfigKey */
	configKey?: string | null;
	/** ConfigValue */
	configValue?: string | null;
	/** Remark */
	remark?: string | null;
}

/** 定义分页通用返回dto */
export interface SysConfigItemListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: SysConfigItemListDto[] | null;
}

/** 定义列表dto */
export interface TaskLogListDto {
	/** Id */
	id?: string | null;
	/** 任务Id */
	taskId?: string | null;
	/** 任务名称 */
	taskName?: string | null;
	/** 任务编号 */
	taskCode?: string | null;
	/** 响应数据 */
	response?: string | null;
	/**
	 * 请求时间
	 * @format date-time
	 */
	requestTime?: string;
	/** 执行时长 */
	executeTime?: string | null;
	/** 通过状态<div>0：Fail；1：Success；</div> */
	status?: SuccessStateEnum;
	/** 请求头信息 */
	requestHeader?: string | null;
	/** 请求参数 */
	requestBody?: string | null;
}

/** 定义分页通用返回dto */
export interface TaskLogListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: TaskLogListDto[] | null;
}

/** 定义任务Cron dto */
export interface TasksCronExDto {
	/** 间隔Cron表达式 */
	interval?: string | null;
}

/** 定义任务编辑dto */
export interface TasksEditDto {
	id?: string | null;
	/** 任务名称 */
	taskName?: string | null;
	/** 编号 */
	code?: string | null;
	/** 任务类型<div>0：System；1：Third；</div> */
	type?: APITaskType;
	/** Content */
	content?: string | null;
	/** 间隔Cron表达式 */
	interval?: string | null;
	/** 任务请求方式<div>0：Post，Post请求；1：Get，Get请求；</div> */
	requestType?: APIRequestTypeEnum;
	/** 描述 */
	remark?: string | null;
	/** 请求头信息 */
	requestHeader?: string | null;
	/** 请求参数 */
	requestBody?: string | null;
}

/** 统一返回业务结果数据 */
export interface TasksEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义任务编辑dto */
	resultData?: TasksEditDto;
}

/** 定义任务列表dto */
export interface TasksListDto {
	/** Id */
	id?: string | null;
	/** 任务名称 */
	taskName?: string | null;
	/** 编号 */
	code?: string | null;
	/** 任务类型<div>0：System；1：Third；</div> */
	type?: APITaskType;
	/** 执行内容 */
	content?: string | null;
	/** 间隔Cron表达式 */
	interval?: string | null;
	/** 任务请求方式<div>0：Post，Post请求；1：Get，Get请求；</div> */
	requestType?: APIRequestTypeEnum;
	/** 任务状态<div>0：Normal；1：Parse；</div> */
	status?: APIStatusEnum;
	/** 描述 */
	remark?: string | null;
	/** @format date-time */
	lastRunTime?: string | null;
	/** 请求头信息 */
	requestHeader?: string | null;
	/** 请求参数 */
	requestBody?: string | null;
	/** @format date-time */
	createDateTime?: string;
}

/** 定义分页通用返回dto */
export interface TasksListDtoCommonPageOutputDto {
	/**
	 * 页数
	 * @format int32
	 */
	curPage?: number;
	/**
	 * 条数
	 * @format int32
	 */
	pageSize?: number;
	/**
	 * 总数
	 * @format int64
	 */
	total?: number;
	/** 数据集 */
	info?: TasksListDto[] | null;
}

/** 定义任务操作dto */
export interface TasksOperateDto {
	/** Ids */
	ids?: string[] | null;
	/** 任务操作类型<div>1：Add；2：Delete；3：Update；4：Parse；5：Start；6：ExecuteImmediately；</div> */
	operate?: JobOperateEnum;
}

export interface TreeNode {
	id?: string | null;
	parentId?: string | null;
	attr1?: string | null;
	name?: string | null;
	type?: string | null;
	code?: string | null;
	/** @deprecated */
	icon?: string | null;
	iconSkin?: string | null;
	url?: string | null;
	open?: boolean;
	children?: TreeNode[] | null;
}

/** UserInfoDto */
export interface UserInfoDto {
	/** 头像 */
	headPic?: string | null;
	/** 部门名称 */
	departmentName?: string | null;
	/** 姓名 */
	name?: string | null;
	/** zhangh */
	account?: string | null;
	/** 描述 */
	remark?: string | null;
	/** 手机号 */
	phone?: string | null;
}

/** 定义用户权限返回dto */
export interface UserPermissionOutputDto {
	/** UserInfoDto */
	userInfo?: UserInfoDto;
	/** 拥有的权限编码 */
	permissionCodes?: string[] | null;
	/** 菜单信息 */
	menus?: ViewListDto[] | null;
}

/** 定义编辑dto */
export interface ViewEditDto {
	/** Id */
	id?: string | null;
	/**
	 * 父级id
	 * @minLength 1
	 */
	parentId: string;
	/** 父级名称 */
	parentName?: string | null;
	/**
	 * 名称
	 * @minLength 1
	 */
	name: string;
	/** 组件 */
	component?: string | null;
	/**
	 * 编号
	 * @minLength 1
	 */
	code: string;
	/** 菜单类型<div>1：Group；2：Menu；3：Permission；</div> */
	type: ViewTypeEnum;
	/** 路径 */
	path?: string | null;
	/** 外链地址 */
	link?: string | null;
	/** 图标 */
	icon?: string | null;
	/** 描述 */
	remark?: string | null;
	/** 是否显示 */
	isHide?: boolean | null;
	/**
	 * 排序
	 * @format int32
	 */
	sort?: number | null;
	/** 重定向 */
	redirect?: string | null;
	/** 页面是否缓存 */
	isKeepAlive?: boolean;
	/** 页面是否固定，不能关闭 */
	isAffix?: boolean;
	/** 是否内嵌 */
	isIframe?: boolean;
}

/** 统一返回业务结果数据 */
export interface ViewEditDtoBusinessResult {
	/** 返回结果信息 */
	message?: string | null;
	/** <div>200：Success；500：Fail；</div> */
	code?: BusinessState;
	/** 定义编辑dto */
	resultData?: ViewEditDto;
}

/** 定义列表dto */
export interface ViewListDto {
	/** Id */
	id?: string | null;
	/** 父级id */
	parentId?: string | null;
	/** 组件 */
	component?: string | null;
	/** 名称 */
	name?: string | null;
	/** 外链地址 */
	link?: string | null;
	/** 编码 */
	code?: string | null;
	/** 菜单类型<div>1：Group；2：Menu；3：Permission；</div> */
	type?: ViewTypeEnum;
	/** 路径 */
	path?: string | null;
	/** 图标 */
	icon?: string | null;
	/** 排序 */
	sort?: string | null;
	/** 是否显示 */
	isHide?: boolean | null;
	/** 重定向 */
	redirect?: string | null;
	/** 页面是否缓存 */
	isKeepAlive?: boolean;
	/** 页面是否固定，不能关闭 */
	isAffix?: boolean;
	/** 是否内嵌 */
	isIframe?: boolean;
	/** 子级 */
	children?: ViewListDto[] | null;
}

/**
 * 菜单类型<div>1：Group；2：Menu；3：Permission；</div>
 * @format int32
 */
export type ViewTypeEnum = 1 | 2 | 3;
