/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { SubDbUpdateItemDto } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class VersonApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Verson
	 * @name IsNeedSubUpdate
	 * @summary 判断是否需要升级
	 * @request GET:/api/Verson/IsNeedSubUpdate
	 */
	isNeedSubUpdate = (params: RequestParams = {}) =>
		this.request<string, any>({
			path: `/api/Verson/IsNeedSubUpdate`,
			method: 'GET',
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Verson
	 * @name ExecuteDbUpdateMethod
	 * @summary 执行脚本
	 * @request POST:/api/Verson/ExecuteDbUpdateMethod
	 */
	executeDbUpdateMethod = (data: SubDbUpdateItemDto, params: RequestParams = {}) =>
		this.request<string, any>({
			path: `/api/Verson/ExecuteDbUpdateMethod`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
	/**
	 * No description
	 *
	 * @tags Verson
	 * @name GetUpdateInfoList
	 * @summary 获取升级脚本
	 * @request GET:/api/Verson/GetUpdateInfoList
	 */
	getUpdateInfoList = (params: RequestParams = {}) =>
		this.request<SubDbUpdateItemDto[], any>({
			path: `/api/Verson/GetUpdateInfoList`,
			method: 'GET',
			format: 'json',
			...params,
		});
}
