/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { AuthInputDto, AuthOutputDtoBusinessResult } from './data-contracts';
import { ContentType, HttpClient, RequestParams } from './http-client';

export class AuthApi<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
	/**
	 * No description
	 *
	 * @tags Auth
	 * @name Login
	 * @summary 登录
	 * @request POST:/api/Auth/Login
	 */
	login = (data: AuthInputDto, params: RequestParams = {}) =>
		this.request<AuthOutputDtoBusinessResult, any>({
			path: `/api/Auth/Login`,
			method: 'POST',
			body: data,
			type: ContentType.Json,
			format: 'json',
			...params,
		});
}
