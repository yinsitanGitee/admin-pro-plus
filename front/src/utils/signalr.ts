import * as signalR from '@microsoft/signalr';
import { Local, Session } from '/@/utils/storage';

let connection = new signalR.HubConnectionBuilder()
	.withUrl(`${window.Base_url}bmessageHub`, {
		skipNegotiation: true,
		transport: signalR.HttpTransportType.WebSockets,
		accessTokenFactory: () => Session.get('token') + ',' + Local.get('Tenant'),
	})
	.configureLogging(signalR.LogLevel.Information)
	.build();

async function start() {
	try {
		await connection.start();
	} catch (err) {
		setTimeout(start, 10000); //错误重连
	}
}

//开始signalr连接
const connect = async () => {
	await start();
};

connection.onclose(async (error: any) => {
	//断线重连 error是空的话则是手动断开，不需要重连
	if (error) await start();
});

//调用服务端方法
async function send(methodName: string, param: any) {
	try {
		await connection.invoke(methodName, param);
	} catch (err) { /* empty */ }
}

//断开连接
const disconnect = async () => {
	await connection.stop();
};

export { connection, connect, send, disconnect };
