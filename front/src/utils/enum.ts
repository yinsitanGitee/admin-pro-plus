export class enumData {
    static sysReceiptTypeEnum = {
        1: '竞拍保证金',
        2: '合同预付款',
        3: '履约保证金',
        4: '订单收款',
    };
    static receiptStateEnum = {
        0: '未收',
        1: '已收',
        2: '已退',
        3: '部分收',
    };
    static businessTypeEnum = {
        1: '竞拍销售合同',
        2: '散卖销售订单',
        3: '竞拍采购合同',
    };
    static biddingStateEnum = {
        0: '待提交',
        1: '审批中',
        2: '审批通过',
        3: '审批不通过',
        4: '作废',
        5: '公示中',
        6: '竞拍中',
        7: '已终止',
        8: '已流拍',
        9: '系统定标',
        10: '生成合同',
        11: '待公示'
    };
    static saleContractStateEnum = {
        0: '待提交',
        1: '审批中',
        2: '审批通过',
        3: '审批不通过',
        4: '作废',
        5: '待签约',
        6: '已签约',
    };
    static invoiceTypeEnum = {
        0: "不开票",
        1: "纸质普通发票",
        2: "纸质专用发票",
        3: "电子普通发票",
        4: "电子专用发票",
    };
    static invoiceWayEnum = {
        0: "不开票",
        1: "先款后票",
        2: "先票后款"
    };
    stateCommonSelect = () =>
        [{
            label: '有效',
            value: 1,
        },
        {
            label: '无效',
            value: 0,
        }];
    boolCommonSelect = () =>
        [{
            label: '是',
            value: true,
        },
        {
            label: '否',
            value: false,
        }];
}