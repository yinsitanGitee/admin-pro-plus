import { useUserInfo } from '/@/stores/userInfo';

/**
 * 权限设置
 * 用于在TS中控制权限设置
 */
export const Permission = {
	ver(code: string) {
		const stores = useUserInfo();
		if (!stores.userDto.permissionCodes.some((v: string) => v === code)) return false;
		return true;
	},
};
