import { BigNumber } from 'bignumber.js';

export const number = {
    // 加
    add(a: number, b: number) {
        return BigNumber(a).plus(BigNumber(b)).toNumber();
    },
    // 减
    minus(a: number, b: number) {
        return BigNumber(a).minus(BigNumber(b)).toNumber();
    },
    // 乘
    mutiply(a: number, b: number) {
        return BigNumber(a).multipliedBy(BigNumber(b)).toNumber();
    },
    // 除
    devide(a: number, b: number) {
        return BigNumber(a).dividedBy(BigNumber(b)).toNumber();
    }
};
