import { useRouter } from 'vue-router';
import { ElMessage } from 'element-plus';
import { MsgTaskListDto } from '/@/api/system/data-contracts'

const msgTaskSourceTypes = [{
    type: 2,
    name: "单据详情",
    src: "/form/billDetail"
}]

export const MsgTask = () => {
    const { push } = useRouter()
    const goto = (data: MsgTaskListDto) => {
        const d = msgTaskSourceTypes.find(a => a.type == data.sourceTypeValue);
        if (d) {
            const params: EmptyObjectType = { tagsViewName: data.title, id: data.sourceId };
            push({
                path: d.src,
                query: params,
            });
        }
        else {
            ElMessage.error('未找到待办类型为:' + data.sourceTypeValue + ' 的配置');
        }
    };
    return { goto };
};
