import { createApp } from 'vue';
import pinia from '/@/stores/index';
import App from '/@/App.vue';
import router from '/@/router';
import { directive } from '/@/directive/index';
import { i18n } from '/@/i18n/index';
import other from '/@/utils/other';

import ElementPlus from 'element-plus';
import '/@/theme/index.scss';
import VueGridLayout from 'vue-grid-layout';
import VForm3 from '@/../lib/vform/designer.umd.js'
import '../lib/vform/designer.style.css'

import dayjs from "dayjs"

const app = createApp(App);
app.config.globalProperties.$filters = {
    timeFormat(value: string, formatString: string) {
        formatString = formatString || 'YYYY-MM-DD HH:mm';
        if (!value) return '';
        return dayjs(value).format(formatString);
    }
}

directive(app);
other.elSvg(app);

app.use(VForm3).use(pinia).use(router).use(ElementPlus).use(i18n).use(VueGridLayout).mount('#app');