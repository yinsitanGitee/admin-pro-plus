﻿(function ($) {
    var ph = window.ph = window.ph || {};
    var baiduMmap = function (options) {
        var self = this,
            defaults = {
                mapId: "mapcontainer",
                map: {},
                myIcon: {},
                myIconActive: {},
                mPoint: {},
                cateList: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"],
                tabKeys: ["tran", "edu", "med", "shop", "live", "game"],
                searchResultList: null,
                lat: $("#lat").val(),
                lng: $("#lng").val(),
                array: [],
                isSearching: false
            };
        _init = function () {
            self.options = $.extend(true, {}, defaults, options);
            __initMap();
            __initTabKey();
        }

        __initTabKey = function () {
            $(".keytab").click(function () {
                if (self.options.isSearching) return false;
                ph.baiduMmap.RenderMap({
                    keyword: $(this).attr("data-keyword"),  //关键字
                    distance: 500,  //半径距离（米）
                    menuKey: $(this).attr("data-menu")
                });
            })
        }


        __initMap = function () {
            self.options.map = new BMap.Map(self.options.mapId);
            self.options.mPoint = new BMap.Point(self.options.lng, self.options.lat);

            var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
            self.options.map.addControl(top_left_navigation); 
            self.options.map.centerAndZoom(new BMap.Point(self.options.lng, self.options.lat), 15); 
            self.options.map.enableScrollWheelZoom(true);

            self.options.map.clearOverlays();
            __renderMainPoint();

            this._renderMap({
                keyword: $('li[data-menu="tran"]').attr("data-keyword"),  //关键字
                distance: 500,  //半径距离（米）
                menuKey: $('li[data-menu="tran"]').attr("data-menu")
            });

        }
        __renderMainPoint = function () {
            var houseIcon = new BMap.Icon("../../../../../Content/images/point-dis-around.png", new BMap.Size(27, 41));
            var marker = new BMap.Marker(self.options.mPoint, {
                icon: houseIcon
            });
            self.options.map.addOverlay(marker);
            //定义小区点击事件,暂不需要
            // marker.addEventListener("click", function (e) {
            //     console.log(e);
            // });
        }

        _renderMap = function (options) {
            var defaultOpt = {
                keyword: null,  //关键字
                distance: null,  //半径距离（米）
                menuKey: null,
            };
            defaultOpt = $.extend(this.defaultopt, options);

            var size = new BMap.Size(27, 41),
                sizeActive = new BMap.Size(27, 41),
                offsetSize = new BMap.Size(2, 0),
                offsetSize_hover = new BMap.Size(0, 0);
            self.options.myIcon = new BMap.Icon("../../../../../Content/images/aroundPos.png", size, {
                imageOffset: offsetSize
            });
            self.options.myIconActive = new BMap.Icon("../../../../../Content/images/aroundPosActive.png", sizeActive, {
                imageOffset: offsetSize_hover
            });

            var circle = new BMap.Circle(self.options.mPoint, defaultOpt.distance, {
                //strokeColor: "transparent",
                //fillColor: "transparent",
                fillColor: "#009688",
                strokeWeight: 1,
                fillOpacity: 0.3,
                strokeOpacity: 0.3
            });

            self.options.map.addOverlay(circle);
            var local = new BMap.LocalSearch(self.options.map, {
                onSearchComplete: function (result) {
                    //禁止多次点击搜索
                    self.options.isSearching = true;
                    self.options.map.clearOverlays();
                    self.options.map.addOverlay(circle);
                    __renderMainPoint();
                    self.options.array.push(self.options.mPoint);
                    var group, item, cateIndex = 0;
                    self.options.searchResultList = [];
                    if (!(result instanceof Array)) {
                        result = [result];
                    }
                    for (var j in result) {
                        if (result[j].getCurrentNumPois() != 0) {
                            cateIndex += 1;
                            var list = [];
                            for (var i = 0; i < result[j].getCurrentNumPois() ; i++) {
                                result[j].getPoi(i).no = i + 1;
                                list = list.concat(result[j].getPoi(i));
                                item = result[j].getPoi(i);
                                self.options.array.push(item.point);
                                _drawPoint(self.options.map, item, result[j].keyword, self.options.cateList[cateIndex - 1], i + 1);
                            }
                            self.options.searchResultList.push({
                                keyword: result[j].keyword,
                                list: list,
                                cate: self.options.cateList[cateIndex - 1]
                            });
                        }
                    }

                    self.options.map.setViewport(self.options.array);

                    function _drawPoint(m, t, keyword, category, index) {
                        var mk = new BMap.Marker(t.point, {
                            icon: self.options.myIcon
                        });
                        var label = new BMap.Label(category + index);
                        label.setStyle({
                            color: "#fff",
                            letterSpacing: '-2px',
                            fontSize: "12px",
                            border: "0",
                            background: "transparent",
                            textAlign: "center",
                            width: "26px",
                            height: "29px",
                            marginLeft: "5px",
                            paddingTop: "5px"
                        });
                        mk.setLabel(label);

                        m.addOverlay(mk);
                        var content = '<div style="font-size:12px; line-height:150%"><span style="color: #666">地址：</span>' + t.address + '</div>';
                        mk.addEventListener('click', function (e) {//气泡点击事件
                            //if(this.isActive == true) return false; 
                            // //右侧列表对应条目高亮且滚动到可见位置
                            // $("#env-list dt.on").removeClass("on");
                            // $("#env-list i").each(function () {
                            //     if ($(this).text() == mk.getLabel().content) {
                            //         $(this).parents("dt").eq(0).addClass("on");
                            //         $("#env-list").scrollTop(0);
                            //         $("#env-list").scrollTop($(this).parents("dt").eq(0).offset().top - $("#env-list").offset().top);
                            //     }
                            // });

                            //当前气泡高亮，其他气泡还原
                            var allOverlay = self.options.map.getOverlays();
                            for (var i = 0; i < allOverlay.length; i++) {
                                if (allOverlay[i].isActive == true) {
                                    allOverlay[i].setIcon(self.options.myIcon);
                                    allOverlay[i].setTop(false);
                                    allOverlay[i].closeInfoWindow();
                                    allOverlay[i].isActive = false;
                                }
                            }
                            this.setIcon(self.options.myIconActive);
                            this.isActive = true;
                            this.setTop(true);
                            this.openInfoWindow(new BMap.InfoWindow(content, {
                                title: '<div class="map_popInfoWin_title" title="' + t.title + '">' + t.title + '</div>',
                                offset: new BMap.Size(0, -13),
                                width: 80,
                                maxWidth: 80,
                                enableMessage: false,
                                enableAutoPan: true
                            }));
                        });
                        mk.addEventListener('mouseover', function (e) {//气泡mouseenter事件
                            if (this.isActive == true) return false;
                            //右侧列表对应条目高亮
                            // $("#env-list i").each(function () {
                            //     if ($(this).text() == mk.getLabel().content) {
                            //         $("#env-list dt.hover").removeClass("hover");
                            //         $(this).parents("dt").eq(0).addClass("hover");
                            //         $("#env-list").scrollTop(0);
                            //         $("#env-list").scrollTop($(this).parents("dt").eq(0).offset().top - $("#env-list").offset().top);
                            //         return false;
                            //     }
                            // });

                            //当前气泡高亮、置顶
                            this.setIcon(self.options.myIconActive);
                            this.setTop(true);
                        });
                        mk.addEventListener('mouseout', function (e) {//气泡mouseout事件
                            if (this.isActive == true) return false;

                            //右侧列表对应条目失去高亮
                            $("#env-list i").each(function () {
                                if ($(this).text() == mk.getLabel().content) {
                                    $(this).parents("dt").eq(0).removeClass("hover");
                                    return false;
                                }
                            });
                            //右侧条目滚动位置到激活的高亮条目位置
                            var allOverlay = self.options.map.getOverlays();
                            // for (var i = 0; i < allOverlay.length; i++) {
                            //     if (allOverlay[i].isActive == true) {
                            //         $("#env-list").scrollTop(0);
                            //         $("#env-list dt h4 i").each(function () {
                            //             if ($(this).text() == allOverlay[i].getLabel().content) {
                            //                 $("#env-list").scrollTop($(this).parents("dt").eq(0).offset().top - $("#env-list").offset().top);
                            //                 return false;
                            //             }
                            //         });
                            //     }
                            // }

                            //当前气泡失去高亮，取消置顶
                            this.setIcon(self.options.myIcon);
                            this.setTop(false);
                        });
                    }

                    var searchResultListHtml = '';
                    $.each(self.options.searchResultList, function (index, node) {
                        node.list = node.list.sort(function (p1, p2) {
                            return self.options.map.getDistance(self.options.mPoint, p1.point) - self.options.map.getDistance(self.options.mPoint, p2.point);
                        });

                        var listHtml = '<h4>' + node.keyword + '</h4>';
                        listHtml += '<ul>';
                        $.each(node.list, function (i, n) {
                            listHtml += '<li class="block clearfix DetailsFilter__Map__InsideBox__item__List insideboxitem">';
                            listHtml += '<i class="pull-left">' + node.cate + n.no + '</i>' + n.title;
                            listHtml += '<span class="pull-right">' + self.options.map.getDistance(self.options.mPoint, n.point).toFixed() + '米</span>';
                        });
                        listHtml += '</ul>';
                        searchResultListHtml += listHtml;
                    });
                    if (searchResultListHtml == '') {
                        searchResultListHtml = '<div class="DetailsFilter__Map__InsideBox__item"><p>暂无数据，请扩大范围试试。</p></div>';
                    }
                    $("." + defaultOpt.menuKey + "menu>ul>li").each(function (index, node) {
                        if ((index + 1) * 1 * 500 == defaultOpt.distance * 1) {
                            $(this).removeClass("selected").addClass("selected");
                        } else {
                            $(this).removeClass("selected");
                        }
                        $(this).attr("data-distance", 500 * (index + 1) * 1);
                        $(this).attr("data-keyWorld", defaultOpt.keyword);
                        $(this).off("click").on("click", function () {
                            if (self.options.isSearching) return false;
                            ph.baiduMmap.RenderMap({
                                keyword: $(this).attr("data-keyWorld"),  //关键字
                                distance: $(this).attr("data-distance") * 1,  //半径距离（米）
                                menuKey: defaultOpt.menuKey
                            });
                        });
                    });
                    $("." + defaultOpt.menuKey + "item").html(searchResultListHtml);
                    $.each(self.options.tabKeys, function (index, item) {
                        if (item == defaultOpt.menuKey) {
                            $("li[data-menu='" + item + "']").addClass('selected')
                            $("." + item + "tab").show();
                        } else {
                            $("li[data-menu='" + item + "']").removeClass('selected')
                            $("." + item + "tab").hide();
                        }
                    });

                    $(".insideboxitem").on("click", function () {
                        $(this).parent().find('.insideboxitem').removeClass('on')
                        $(this).addClass("on");
                        $(this).removeClass("hover");
                        var labelText = $(this).find("i.pull-left").text();

                        //左侧地图marker处理
                        var currentMarkerIndex = null;
                        var allOverlay = self.options.map.getOverlays();
                        for (var i = 0; i < allOverlay.length; i++) {//遍历找到已经高亮的地标还原之
                            if (allOverlay[i].getLabel && allOverlay[i].getLabel() && allOverlay[i].getLabel().content && allOverlay[i].getLabel().content == labelText) {
                                currentMarkerIndex = i;
                            }
                            if (allOverlay[i].isActive == true) {
                                allOverlay[i].setIcon(self.options.myIcon);
                                allOverlay[i].isActive = false;
                                allOverlay[i].setTop(false);
                                allOverlay[i].closeInfoWindow();
                            }
                        }
                        if (currentMarkerIndex) {//当前地标高亮处理
                            var t = null;
                            var c = allOverlay[currentMarkerIndex];
                            var isFound = false;
                            $.each(self.options.searchResultList, function (index, node) {
                                $.each(node.list, function (i, n) {
                                    if (n.point.lat == c.point.lat && n.point.lng == c.point.lng) {
                                        t = n;
                                        isFound = true;
                                        return false;
                                    }
                                });
                                if (isFound) { return false; }
                            });

                            allOverlay[currentMarkerIndex].setIcon(self.options.myIconActive);//换图标
                            allOverlay[currentMarkerIndex].isActive = true;//记录状态
                            allOverlay[currentMarkerIndex].setTop(true);//置顶
                            if (t) {//弹信息窗
                                var content = '<div><span style="color: #666">地址：</span>' + t.address + '</div>';
                                allOverlay[currentMarkerIndex].openInfoWindow(new BMap.InfoWindow(content, {
                                    title: '<div class="map_popInfoWin_title" title="' + t.title + '">' + t.title + '</div>',
                                    offset: new BMap.Size(0, -13),
                                    width: 80,
                                    maxWidth: 80,
                                    enableMessage: false,
                                    //enableAutoPan: false,
                                    enableAutoPan: true
                                }));
                            }
                        }
                    });

                    $(".insideboxitem").on("mouseenter", function () {
                        if ($(this).hasClass("on")) return false;
                        $(this).addClass("hover");
                        var labelText = $(this).find("i.pull-left").text();
                        //左侧地图marker处理
                        var allOverlay = self.options.map.getOverlays();
                        for (var i = 0; i < allOverlay.length; i++) {//遍历找到对应的气泡高亮、置顶
                            if (allOverlay[i].getLabel && allOverlay[i].getLabel() && allOverlay[i].getLabel().content && allOverlay[i].getLabel().content == labelText) {
                                allOverlay[i].setIcon(self.options.myIconActive);//换图标
                                allOverlay[i].setTop(true);//置顶
                                return false;
                            }
                        }
                    });
                    $(".insideboxitem").on("mouseout", function () {
                        if ($(this).hasClass("on")) return false;
                        $(this).removeClass("hover");
                        //左侧地图marker处理
                        var labelText = $(this).find("i.pull-left").text();
                        var allOverlay = self.options.map.getOverlays();
                        for (var i = 0; i < allOverlay.length; i++) {//遍历找到对应的气泡去除高亮和置顶
                            if (allOverlay[i].getLabel && allOverlay[i].getLabel() && allOverlay[i].getLabel().content && allOverlay[i].getLabel().content == labelText) {
                                allOverlay[i].setIcon(self.options.myIcon);//换图标
                                allOverlay[i].setTop(false);//置顶
                                return false;
                            }
                        }
                    });
                    self.options.isSearching = false;

                }
            })
            var bounds = __getSquareBounds(circle.getCenter(), circle.getRadius());
            local.searchInBounds(defaultOpt.keyword.split("|"), bounds);
        }

        /**
         * 得到圆的内接正方形bounds
         * @param {Point} centerPoi 圆形范围的圆心
         * @param {Number} r 圆形范围的半径
         * @return 无返回值
         */
        __getSquareBounds = function (centerPoi, r) {
            var a = Math.sqrt(2) * r; //正方形边长

            mPoi = __getMecator(centerPoi);
            var x0 = mPoi.x, y0 = mPoi.y;

            var x1 = x0 + a / 2, y1 = y0 + a / 2;//东北点
            var x2 = x0 - a / 2, y2 = y0 - a / 2;//西南点

            var ne = __getPoi(new BMap.Pixel(x1, y1)), sw = __getPoi(new BMap.Pixel(x2, y2));
            return new BMap.Bounds(sw, ne);
        }

        //根据球面坐标获得平面坐标。
        __getMecator = function (poi) {
            return self.options.map.getMapType().getProjection().lngLatToPoint(poi);
        }

        //根据平面坐标获得球面坐标。
        __getPoi = function (mecator) {
            return self.options.map.getMapType().getProjection().pointToLngLat(mecator);
        }
        return {
            RenderMap: _renderMap,
            init: _init
        }
    }
    $.extend(ph, { baiduMmap: new baiduMmap() });
})(jQuery);


$(function () {
    var ph = window.ph = window.ph || {};
    ph.baiduMmap.init();
})