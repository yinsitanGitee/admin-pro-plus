﻿(function ($) {

	$.fn.TimeHorizon = function (options, callback) {
		var getTodayDate = lotteryDataList.getTodayDate();
		var getCurrentWeek = lotteryDataList.getCurrentWeek();
		var getLastWeek = lotteryDataList.getLastWeek();
		var getCurrentMonth = lotteryDataList.getCurrentMonth();
		var getLastMonth = lotteryDataList.getLastMonth();
		var getFullYear = lotteryDataList.getFullYear();
		var getLastYear = lotteryDataList.getLastYear();
		var getYesterday = lotteryDataList.getYesterdayDate();
		var getAnnualYear = lotteryDataList.getAnnualYear();

		var data = [
			{ "key": 8, "value": "昨天", StartTime: getYesterday.StartTime, EndTime: getYesterday.EndTime },
			{ "key": 7, "value": "今天", StartTime: getTodayDate.StartTime, EndTime: getTodayDate.EndTime },
			{ "key": 2, "value": "上周", StartTime: getLastWeek.StartTime, EndTime: getLastWeek.EndTime },
			{ "key": 1, "value": "本周", StartTime: getCurrentWeek.StartTime, EndTime: getCurrentWeek.EndTime },
			{ "key": 4, "value": "上月", StartTime: getLastMonth.StartTime, EndTime: getLastMonth.EndTime },
			{ "key": 3, "value": "本月", StartTime: getCurrentMonth.StartTime, EndTime: getCurrentMonth.EndTime },
			{ "key": 6, "value": "上年", StartTime: getLastYear.StartTime, EndTime: getLastYear.EndTime },
			{ "key": 5, "value": "本年", StartTime: getFullYear.StartTime, EndTime: getFullYear.EndTime },
			{ "key": 9, "value": "全年", StartTime: getAnnualYear.StartTime, EndTime: getAnnualYear.EndTime },
			{ "key": 0, "value": "全部", StartTime: "", EndTime: "" }
		];


		var StartTime = "";
		var EndTime = "";
		var $this = $(this);

		var cilck = function (value) {
			data.forEach(function (val) {
				if (value == val.key) {
					StartTime = val.StartTime;
					EndTime = val.EndTime;
				}
			});
			callback(StartTime, EndTime);
		};


		var array = [];

		if (options.data) {
			data.forEach(function (val) {
				if (options.data.includes(val.key)) {
					array.push(val);
				}
			});

			options.data = array;
		}

		_options =
		{
			data: data,
			id: "key",
			value: 3,
			text: "value",
			description: false,
			click: function (item) {
				var value = item.attr('data-value');
				cilck(value);
			}
		};

		var _options = $.extend(_options, options);
		$this.ComboBox(_options);
		$this.ComboBoxSetValue(_options.value);
		cilck(_options.value);
		return {
			GetTime: function () {
				return {
					StartTime: StartTime,
					EndTime: EndTime
				}
			},
			SetTime: function (val) {
				$this.ComboBoxSetValue(val);
				cilck(_options.value);
			}
		}
	}

	//输出时间样式为 yyyy-mm-dd hh:mm:ss,不满 2 位则前面添 0；
	function add0(m) {
		return m < 10 ? '0' + m : m
	};
	//获取某月天数 
	function getMonthDays(year, month) {
		var new_year = year; //取当前的年份 var
		nextMonth = month++; if (month > 12) {
			nextMonth -= 12; //月份减
			new_year++; //年份增
		}
		var nextMonthFirstDay = new Date(new_year, nextMonth, 1);
		//下个月第一天
		var oneDay = 1000 * 60 * 60 * 24;
		var dateString = new Date(nextMonthFirstDay - oneDay);
		var dateTime = dateString.getDate();
		return dateTime;
	};
	function getPriorMonthFirstDay(year, month) {
		//年份为0代表,是本年的第一月,所以不能减
		if (month == 0) {
			month = 11; //月份为上年的最后月份
			year--; //年份减1
			return new Date(year, month, 1);
		}
		//否则,只减去月份
		month--;
		return new Date(year, month, 1);
	};

	var lotteryDataList = new Object({
		//获取当天开始时间结束时间
		getTodayDate: function () {
			var today = [];
			var todayDate = new Date();
			var y = todayDate.getFullYear();
			var m = todayDate.getMonth() + 1;
			var d = todayDate.getDate();
			var s = y + '-' + add0(m) + '-' + add0(d) + '';//今日开始
			var e = y + '-' + add0(m) + '-' + add0(d) + '';//今日结束

			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取昨天时间
		getYesterdayDate: function () {
			var dateTime = [];
			var today = new Date();
			var yesterday = new Date(today.setTime(today.getTime() - 24 * 60 * 60 * 1000));
			var y = yesterday.getFullYear();
			var m = yesterday.getMonth() + 1;
			var d = yesterday.getDate();
			var s = y + '-' + add0(m) + '-' + add0(d) + '';//开始
			var e = y + '-' + add0(m) + '-' + add0(d) + '';//结束
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取本周开始时间结束时间
		getCurrentWeek: function () {
			var startStop = new Array();
			//获取当前时间
			var currentDate = new Date();
			//返回date是一周中的某一天
			var week = currentDate.getDay();
			//返回date是一个月中的某一天
			var month = currentDate.getDate();
			//一天的毫秒数
			var millisecond = 1000 * 60 * 60 * 24;
			//减去的天数
			var minusDay = week != 0 ? week - 1 : 6;
			//alert(minusDay);
			//本周 周一
			var monday = new Date(currentDate.getTime() - (minusDay * millisecond));
			//本周 周日
			var sunday = new Date(monday.getTime() + (6 * millisecond));
			var sy = monday.getFullYear();
			var sm = monday.getMonth() + 1;
			var sd = monday.getDate();
			var ey = sunday.getFullYear();
			var em = sunday.getMonth() + 1;
			var ed = sunday.getDate();
			var s = sy + '-' + add0(sm) + '-' + add0(sd) + '';//开始
			var e = ey + '-' + add0(em) + '-' + add0(ed) + '';//结束
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取上周时间
		getLastWeek: function () {
			//起止日期数组
			var startStop = new Array();
			//获取当前时间
			var currentDate = new Date();
			//返回date是一周中的某一天
			var week = currentDate.getDay();
			//返回date是一个月中的某一天
			var month = currentDate.getDate();
			//一天的毫秒数
			var millisecond = 1000 * 60 * 60 * 24;
			//减去的天数
			var minusDay = week != 0 ? week - 1 : 6;
			//获得当前周的第一天
			var currentWeekDayOne = new Date(currentDate.getTime() - (millisecond * minusDay));
			//上周最后一天即本周开始的前一天
			var priorWeekLastDay = new Date(currentWeekDayOne.getTime() - millisecond);
			//上周的第一天
			var priorWeekFirstDay = new Date(priorWeekLastDay.getTime() - (millisecond * 6));
			var sy = priorWeekFirstDay.getFullYear();
			var sm = priorWeekFirstDay.getMonth() + 1;
			var sd = priorWeekFirstDay.getDate();
			var ey = priorWeekLastDay.getFullYear();
			var em = priorWeekLastDay.getMonth() + 1;
			var ed = priorWeekLastDay.getDate();
			var s = sy + '-' + add0(sm) + '-' + add0(sd) + '';//开始
			var e = ey + '-' + add0(em) + '-' + add0(ed) + '';//结束
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取本月时间
		getCurrentMonth: function () {
			//起止日期数组
			var startStop = new Array();
			//获取当前时间
			var currentDate = new Date();
			//获得当前月份0-11
			var currentMonth = currentDate.getMonth();
			//获得当前年份4位年
			var currentYear = currentDate.getFullYear();
			//求出本月第一天
			var firstDay = new Date(currentYear, currentMonth, 1);

			//当为12月的时候年份需要加1
			//月份需要更新为0 也就是下一年的第一个月
			if (currentMonth == 11) {
				currentYear++;
				currentMonth = 0; //就为
			} else {
				//否则只是月份增加,以便求的下一月的第一天
				currentMonth++;
			}
			//一天的毫秒数
			var millisecond = 1000 * 60 * 60 * 24;
			//下月的第一天
			var nextMonthDayOne = new Date(currentYear, currentMonth, 1);
			//求出上月的最后一天
			var lastDay = new Date(nextMonthDayOne.getTime() - millisecond);
			var sy = firstDay.getFullYear();
			var sm = firstDay.getMonth() + 1;
			var sd = firstDay.getDate();
			var ey = lastDay.getFullYear();
			var em = lastDay.getMonth() + 1;
			var ed = lastDay.getDate();
			var s = sy + '-' + add0(sm) + '-' + add0(sd) + '';//开始
			var e = ey + '-' + add0(em) + '-' + add0(ed) + '';//结束
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取上月时间
		getLastMonth: function () {
			var startStop = new Array();
			//获取当前时间
			var currentDate = new Date();
			//获得当前月份0-11
			var currentMonth = currentDate.getMonth();
			//获得当前年份4位年
			var currentYear = currentDate.getFullYear();
			//获得上一个月的第一天
			var priorMonthFirstDay = getPriorMonthFirstDay(currentYear, currentMonth);

			var sy = priorMonthFirstDay.getFullYear();
			var sm = priorMonthFirstDay.getMonth() + 1;
			var sd = priorMonthFirstDay.getDate();

			//获得上一月的最后一天
			var priorMonthLastDay = new Date(priorMonthFirstDay.getFullYear(), sm,
				getMonthDays(priorMonthFirstDay.getFullYear(), sm));

			//一天的毫秒数
			var millisecond = 1000 * 60 * 60 * 24;

			//求出上月的最后一天
			var lastDay = new Date(priorMonthLastDay.getTime() - millisecond);
			var ey = lastDay.getFullYear();
			var em = lastDay.getMonth();
			var ed = lastDay.getDate();
			if (em == 0) {
				em = 11; //月份为上年的最后月份
				ey--; //年份减1
			}
			var s = sy + '-' + add0(sm) + '-' + add0(sd) + '';//开始
			var e = ey + '-' + add0(em) + '-' + add0(ed) + '';//结束
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取本年时间
		getFullYear: function () {
			//获取当前时间
			var currentDate = new Date();
			var currentYear = currentDate.getFullYear();

			var y = currentDate.getFullYear();
			var m = currentDate.getMonth() + 1;
			var d = currentDate.getDate();


			var s = currentYear + "-01-01";
			var e = y + '-' + add0(m) + '-' + add0(d) + '';//今日结束
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取全年年时间
		getAnnualYear: function () {
			//获取当前时间
			var currentDate = new Date();
			var currentYear = currentDate.getFullYear();
			var priorYear = new Date(currentDate.getFullYear(), 12, 0);

			var em = priorYear.getFullYear();
			var ed = priorYear.getDate();

			var s = currentYear + "-01-01";
			var e = add0(em) + "-12-" + add0(ed) + "";
			return {
				StartTime: s,
				EndTime: e
			}
		},
		//获取上年数据
		getLastYear: function () {
			//获取当前时间
			var currentDate = new Date();
			var currentYear = currentDate.getFullYear() - 1;
			var priorYear = new Date(currentDate.getFullYear() - 1, 12, 0);

			var em = priorYear.getFullYear();
			var ed = priorYear.getDate();

			var s = currentYear + "-01-01";
			var e = add0(em) + "-12-" + add0(ed) + "";
			return {
				StartTime: s,
				EndTime: e
			}

		},
	})

})(jQuery)