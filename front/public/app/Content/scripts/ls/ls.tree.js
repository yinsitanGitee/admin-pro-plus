﻿/*
 
 
 
 
 
 
 
 
 
 */
+(function ($) {
    var self = null;
    var defaultSet, ZtreeId, $Ztree, ZtreeJson;
    var lsUiTree = function (id, opt, nodes) {
        //不允许调整特殊限制
        opt && opt.async && (delete opt.async.type);
        opt && opt.async && (delete opt.async.contentType);
        opt && opt.async && (delete opt.async.autoParam);
        opt && opt.async && (delete opt.async.dataFilter);

        var token = window.sessionStorage.getItem('token');
        if (token) token = token.replace('"', '').replace('"', '')

        var tenant = window.localStorage.getItem('Tenant');
        if (tenant) tenant = tenant.replace('"', '').replace('"', '')

        var defaultSetting = {
            view: {
                showLine: false,
                addDiyDom: addDiyDom // 自定义dom
            },
            select: false, // 是否是下拉选择树
            search: false, // 是否有搜索框
            searchBtn: false, // 是否有确定搜索按钮
            searchCallBack: "", // 确定搜索按钮回调
            width: "auto",
            defaultSet: "",
            async: {
                headers: {
                    Authorization: token,
                    Tenant: tenant
                },
                type: "POST",
                contentType: 'application/json',
                enable: true,
                autoParam: ["id=Id", "name=Name", "type=Type", "code=Code", "icon=Icon"],
                otherParam: {},
                dataFilter: filter
            },
            callback: {},
            searchOpt: {},
            showHistory: true,
        }

        defaultSetting = $.extend(true, defaultSetting, opt);
        
        defaultSetting.callback.onClick = function (event, treeId, treeNode) {
            opt && opt.callback && opt.callback.onClick && opt.callback.onClick(event, treeId, treeNode);
            zTreeOnClick(event, treeId, treeNode)
        }

        defaultSetting.callback.onExpand = function (event, treeId, treeNode) {
            opt && opt.callback && opt.callback.onExpand && opt.callback.onExpand(event, treeId, treeNode);

        }

        defaultSetting.callback.onAsyncSuccess = function () {
            opt && opt.callback && opt.callback.onAsyncSuccess && opt.callback.onAsyncSuccess();
            zTreeonAsyncSuccess();
        }

        defaultSet = defaultSetting;
        //利用 addDiyDom 回调将 展开按钮 转移到 <a> 标签内
        function addDiyDom(treeId, treeNode) {
            var spaceWidth = 5;
            var switchObj = $("#" + treeNode.tId + "_switch"),
                checkObl = $("#" + treeNode.tId + "_check"),
                icoObj = $("#" + treeNode.tId + "_ico");
            switchObj.remove();
            icoObj.before(switchObj);
            // 单选框或多选框位置更改
            if (!!checkObl) {
                icoObj.before(checkObl);
            }
            if (treeNode.level > 1) {
                var spaceStr = "<span style='display: inline-block;width:" + (spaceWidth * treeNode.level) + "px'></span>";
                switchObj.before(spaceStr);
            }
        }

        function getzTree(id, setting, nodes) {
            var childNodes = filter(id, null, nodes)
            if (!!childNodes) {
                $.fn.zTree.init($("#" + id), setting, childNodes);
            } else {
                $.fn.zTree.init($("#" + id), setting);
            }

            return $.fn.zTree.getZTreeObj(id);
        }
        var zTree = getzTree(id, defaultSetting, nodes);
        ZtreeId = id;
        // 下拉框ul赋值宽度
        if (!!defaultSet.select) {
            selectHtml(id);
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    defaultSet.width = $("#" + id).prev().find("div.selectZtreeName").css("width");
                    $("#" + id).css("width", defaultSet.width)
                }, 200);
                e.stopPropagation();
            });
            $(window).resize();

        }
        if (!!defaultSet.inputSeearch) {
            searchInput(id);
        }

        $Ztree = zTree;

        return zTree;
    };

    //数据递归修正
    function filter(treeId, parentNode, childNodes) {
        if (!childNodes) return null;
        for (var i = 0, l = childNodes.length; i < l; i++) {
            childNodes[i].name = childNodes[i].name;
            childNodes[i].id = childNodes[i].id;
            childNodes[i].type = childNodes[i].type;
            childNodes[i].open = childNodes[i].open;
            childNodes[i].iconSkin = childNodes[i].iconSkin + " ";
            //childNodes[i].icon = childNodes[i].Icon+" icon"; 
            if (childNodes[i].children != null) {
                var li = [];
                for (var j = 0; j < childNodes[i].children.length; j++) {
                    li.push(parseChildNode(childNodes[i].children[j]));
                }
                childNodes[i].children = li;
            } else {
                childNodes[i].children = null;
            }
        }
        ZtreeJson = childNodes; // json数据
        $("#" + treeId).find(".CRed").remove();
        $("#" + treeId).append("<li class='CRed' style='display:none;text-align:center;'>暂无数据</li>");
        // 判断有无数据
        if (ZtreeJson.length == 0) {
            $("#" + treeId).find(".CRed").show();
        }
        return childNodes;
    }
    function parseChildNode(node) {
        node.name = node.name;
        node.id = node.id;
        node.type = node.type;
        node.open = node.open;
        node.iconSkin = node.iconSkin + " ";
        //node.icon = node.Icon + " icon"; 
        if (node.children == null) {
            node.children = null;
            return node;
        }
        var li = [];
        for (var i = 0; i < node.children.length; i++) {
            li.push(parseChildNode(node.children[i]));
        }
        node.isParent = true;
        node.children = li;
        return node;
    }

    // 树加载完成回调
    function zTreeonAsyncSuccess(id, nodes) {
        // 赋值
        if (defaultSet.setValue) {
            var node = $Ztree.getNodeByParam("Id", defaultSet.setValue);//根据ID找到该节点
            if (!!node) {
                $Ztree.selectNode(node);//根据该节点选中
                $("#" + ZtreeId).prev().find("div.selectZtreeName").text(node.Name)
            }
        }

    }

    // 隐藏下拉框树
    function onBodyDownByActionType(event) {
        if (event.target.id.indexOf(ZtreeId) == -1) {
            if (event.target.class != 'selectZtreeName' && !defaultSet.searchBtn) {
                hideTree(ZtreeId);
            }
        }
    }
    // 隐藏下拉框树
    function hideTree(treeId) {
        if ($("#" + treeId + "").hasClass("selectZtree")) {
            $("#" + treeId + "").hide();
            $("#" + treeId + "").prev().find("i.fa-sort-up").removeClass("fa-sort-up").addClass("fa-sort-desc");
            $("#" + treeId + "").prev().find("input.keyZtreeValue").hide();
            $("#" + treeId + "").prev().find(".fa-search").hide();
            $("#" + treeId + "").next(".searchBtnDiv").hide();
            $("body").unbind("mousedown", onBodyDownByActionType);
            return false;
        }
    }
    // 下拉框树 点击事件
    function zTreeOnClick(event, treeId, treeNode) {
        if (!!defaultSet.select) {
            $("#" + treeId + "").attr("data-value", treeNode.Id)
            $("#" + treeId + "").prev().find("div").text(treeNode.name);
            hideTree(treeId);
        }
    };

    // 生成下拉框树
    function selectHtml(id) {
        if (!!defaultSet.select) {
            // 防止多次加载生成多个
            $("#" + id + "").siblings(".itemTreeDiv").remove();
            // 是否有搜索框 $Ztree
            var search_html = "";
            if (!!defaultSet.search) {
                search_html = '<input placeholder="模糊查询" class="form-control keyZtreeValue ' + id + '_keyZtreeValue" style="display:none;top:30px;width:100%;"/><i class="fa fa-search" style="top:38px;"></i>';
                $("#" + id + "").css("top", "60px")
            }
            var _html = '<div class="itemTreeDiv" style="width:' + defaultSet.width + ';"><div class="form-control selectZtreeName" style="cursor: pointer;"><span style="color:#999;">=请选择=</span></div>' +
                '<i class="fa fa-sort-desc"></i>' + search_html + '</div>';
            $("#" + id + "").before(_html);
            // 是否有搜索按钮
            if (!!defaultSet.searchBtn) {
                var top = 309;
                var searchBtnHtml = '<div class="abs searchBtnDiv bgF" style="border: 1px solid #ccc;top:' + top + 'px;width:calc(100% - 85px);padding: 3px 10px;;z-index:99;display: none;"><a class="btn btn-primary searchBtn fr" style="padding: 5px 10px;">查&nbsp;&nbsp;询</a></div>';
                $("#" + id + "").after(searchBtnHtml);
                $("#" + id + "").css("height", "250px")
                $(".searchBtnDiv a.searchBtn").on("click", function (e) {
                    e.stopPropagation();
                    defaultSet.searchCallBack();
                    hideTree(id);
                })
            }
            var obj = $("#" + id + "").prev(); // 当前下拉框
            // 获取当前下拉框距离可视区域底部的距离
            var xh = $(window).height() - (obj.height() + obj.offset().top - $(document).scrollTop());
            if (xh < 255 && xh > 0) {
                $("#" + id + "").css({ "bottom": "29px", "border-top": "1px solid #ccc" });
            }
            $("#" + id + "").prev().find("div").on("click", function (e) {
                e.stopPropagation();
                if ($(this).next().hasClass("fa-sort-desc")) {
                    $(this).siblings(".keyZtreeValue").show();
                    $(this).siblings(".fa-search").css("display", "block");
                    $("#" + id + "").show();
                    $("#" + id + "").next(".searchBtnDiv").show();
                    $(this).next().removeClass("fa-sort-desc").addClass("fa-sort-up");
                } else {
                    $(this).siblings(".keyZtreeValue").hide();
                    $(this).siblings(".fa-search").hide();
                    $("#" + id + "").hide();
                    $("#" + id + "").next(".searchBtnDiv").hide();
                    $(this).next().removeClass("fa-sort-up").addClass("fa-sort-desc");
                }
            })

            // 是否有搜索框 $Ztree
            if (!!defaultSet.search && defaultSet.select) {
                //console.log(id)
                fuzzySearch(id, '.' + id + '_keyZtreeValue', false, true); //初始化模糊搜索方法
            }
        }
    }

    // 左侧树生成是搜索框
    function searchInput(id) {
        // 防止多次加载生成多个
        $("#" + id + "").siblings(".searchAll").remove();
        var $html = $('<div class="searchAll tc" style="border:1px solid #ccc;"> </div>');
        var $input = $('<input id="SearchKeyword" style="line-height: 30px; width: calc(100% - 40px);" placeholder="请输入关键字 回车">');
        var $clear = $('<i class="fa fa-times-circle active" id="btn_clance" title="清空"></i>');
        var $history = $('<i class="fa fa-history ml10" id="btn_HistoryMark" title="显示历史记录"></i>');
        $html.append($input).append($clear);
        if (defaultSet.showHistory) $html.append($history);
        $clear.on("click", function () {
            defaultSet.clear({
                "HistoryMark": $history.hasClass("active") ? 1 : 0
            });
        });
        $input.bind('keypress', function (event) {
            if (event.keyCode == "13") {
                console.log($(this).val())
                defaultSet.searchInput(
                    {
                        "KeyWord": $(this).val(),
                        "HistoryMark": $history.hasClass("active") ? 1 : 0
                    }
                );
            }
        });
        $history.on("click", function () {
            var obj = $(this);
            if (obj.hasClass("active")) obj.removeClass("active").attr("title", "显示历史记录");
            else obj.addClass("active").attr("title", "隐藏历史记录");
            defaultSet.history(
                {
                    "KeyWord": "",
                    "HistoryMark": $history.hasClass("active") ? 1 : 0
                }
            );
        })
        $("#" + id + "").before($html);
        // 搜索框值不会掉
        var keyword = (defaultSet.async.otherParam.KeyWord) ? defaultSet.async.otherParam.KeyWord : "";
        $input.val(keyword);
        // 历史按钮不会掉
        (defaultSet.async.otherParam.HistoryMark == 1) ? $history.addClass("active") : $history.removeClass("active");
    }

    $.fn.extend({
        "lsTree": function (setting, nodes) {
            var id = $(this).attr('id');
            if (!!!id) throw 'Dom不存在';
            $("body").click(function () {
                if (!!defaultSet.select) {
                    $("body").bind("mousedown", onBodyDownByActionType);
                }
            })
            return new lsUiTree(id, setting, nodes);
        }
    });


})(jQuery)