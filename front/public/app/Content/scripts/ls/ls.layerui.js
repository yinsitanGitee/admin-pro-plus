(function($){
    var ls = window.ls = window.ls || {};
    var layerui = function(){}
    layerui.prototype.laypage = function(obj){
        layui.use('laypage', function(){
            var laypage = layui.laypage;
            laypage.render(obj);
        });
    }
    ls.ui = ls.ui || {};
    $.extend(ls.ui, { layerui: new layerui() });
})(jQuery);
