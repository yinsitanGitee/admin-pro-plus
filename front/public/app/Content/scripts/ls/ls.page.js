﻿(function ($) {
    var ls = window.ls = window.ls || {};

    var Page = function () {
        var self = this;

        self.isInit = false;
        self.window = window
    };

    Page.prototype._initPermission = function () {

    };

    Page.prototype.init = function (data) {
        var self = this;
        //只允许加载一次上下文信息
        if (self.isInit) {
            return;
        }
        self.isInit = true;
        self.window.init && self.window.init(data);
        self.window.checkViewElementPower && self.window.checkViewElementPower();//页面初始化完成后验证权限
    };

    window.page = new Page();
})(jQuery);

//使用Jquery页面加载完事件进行初始化
$(function () {
    var exculdParaNames = ["Ls_LoginName", "Ls_Token"];
    function getUrlData(url) {
        //注意:此处初始化只支持Url参数 
        var urlData = {};
        var urls = url.split('?');
        if (urls.length > 1) {
            for (var z = 1; z < urls.length; z++) {
                var kvs = urls[z].split('&');
                for (var i = 0; i < kvs.length; i++) {
                    var kv = kvs[i].split('=');
                    if (exculdParaNames.indexOf(kv[0]) != -1) continue;
                    if (kv.length == 2) {
                        urlData[kv[0]] = kv[1];
                    }
                }
            }
        }
        return urlData;
    }

    if (frameElement && !$(frameElement).attr("dialog") && !$(frameElement).attr("hasUseInitFunc")) {
        var urlData = getUrlData(frameElement.src);
        window.page.init(urlData);
    } else if (self === top) {
        var urlData = getUrlData(window.location.href);
        window.page.init(urlData);
    }
});