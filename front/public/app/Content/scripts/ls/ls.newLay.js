(function($){
    var ls = window.ls = window.ls || {};
    var newLay = function(){}
    newLay.prototype.laypage = function(obj){
        layui.use('laypage', function(){
            var laypage = layui.laypage;
            laypage.render(obj);
        });
    }
    ls.ui = ls.ui || {};
    $.extend(ls.ui, { newLay: new newLay() });
})(jQuery);
