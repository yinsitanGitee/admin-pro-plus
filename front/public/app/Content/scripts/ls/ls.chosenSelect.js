﻿/*
 * chosen-select 二次封装
 * Date: 2020-3-30
 */
(function ($) {
    $.fn.chosenSelect = function (options) {
        var self = this;
        var _options = $.extend({
            description: "=请选择=",
            id: "id",
            text: "text",
            url: null,
            param: null,
            data: null,
            method: "GET",
            no_results_text: "暂无数据！",//搜索无结果时显示的提示  
            search_contains: true,   //关键字模糊搜索。设置为true，只要选项包含搜索词就会显示；设置为false，则要求从选项开头开始匹配
            allow_single_deselect: true, //单选下拉框是否允许取消选择。如果允许，选中选项会有一个x号可以删除选项
            disable_search: false, //禁用搜索。设置为true，则无法搜索选项。
            disable_search_threshold: 0, //当选项少等于于指定个数时禁用搜索。
            inherit_select_classes: true, //是否继承原下拉框的样式类，此处设为继承
            width: '100%', //设置chosen下拉框的宽度。即使原下拉框本身设置了宽度，也会被width覆盖。
            max_shown_results: 1000, //下拉框最大显示选项数量
            display_disabled_options: false,
            single_backstroke_delete: false, //false表示按两次删除键才能删除选项，true表示按一次删除键即可删除
            case_sensitive_search: false, //搜索大小写敏感。此处设为不敏感
            group_search: false, //选项组是否可搜。此处搜索不可搜
        }, options);

        if (!!_options.url) {
            _options.data = loadData(_options);
        }

        $.each(_options.data, function (i, v) {
            var option = '<option value="' + v[_options.id] + '">' + v[_options.text] + '</option>';
            self.append(option);
        })
        self.attr("data-placeholder", _options.description);
        self.chosen(_options);
        self.on("change", function () {
            var r = $(this);
            if (_options.click) {
                _options.click(r[0].options[r[0].options.selectedIndex]);
            }
        })
        var chosenId = $(self)[0].id + "_chosen";
        $("#" + chosenId).find(".chosen-drop").hide();
        $("#" + chosenId).on("click", function () {
            var f = $(this).find(".chosen-drop");
            var positionBottom = document.documentElement.clientHeight - $("#" + chosenId).offset().top;
            if (positionBottom < 210) {
                f.show().css({
                    top: 0 - f.height(),
                    "border-top": "1px solid #aaa"
                })
            } else {
                f.show();
            }
        })
    }
    function loadData(i) {
        var data = [];
        data = asyncGet({
            url: i.url,
            data: i.param,
            type: i.method
        });
        return data;
    }
    function NewGuid() {
        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }
    function asyncGet(t) {
        var i = null,
            t = $.extend({
                type: "GET",
                dataType: "json",
                async: !1,
                cache: !1,
                success: function (n) {
                    i = n
                }
            },
                t);
        return $.ajax(t),
            i
    }
})(jQuery)
