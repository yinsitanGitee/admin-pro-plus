﻿(function ($) {
    var ls = window.ls = window.ls || {};

    var util = function () { };

    /*
     * 发送请求到服务端
     * @param url 请求连服务端链接地址
     * @param data 发送到服务器端的对象
     * @param successHandler 请求服务端成功后返回数据回调函数,function(data){}
     * @param errorHandler 请求服务端异常后回调函数,function(error){}
     * @param opts 扩展参数
     *  showLoading	是否显示加载中弹出框
     *  timeout	超时时间
     *  timeoutHandler	请求服务端超时后回调函数
     */
    util.prototype.request = function (url, data, successHandler, errorHandler, opts) {
        var self = this,
            json = data ? JSON.stringify(data) : undefined,
            reqOpts = {
                timeout: 600000,
                global: true,
                showLoading: true
            };

        var token = window.sessionStorage.getItem('token');
        if (token) token = token.replace('"', '').replace('"', '')

        var tenant = window.localStorage.getItem('Tenant');
        if (tenant) tenant = tenant.replace('"', '').replace('"', '')

        var ajaxObj = {
            type: "POST",
            url: url,
            cache: false,
            headers: {
                Authorization: token,
                Tenant: tenant,
            },
            global: reqOpts.global,
            timeout: reqOpts.timeout,
            processData: false,
            data: json,
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                try {
                    successHandler && successHandler(result);
                } catch (e) {
                    console.log(e);
                }
            },
            error: function (jqXHR, textStatus, error) {
                if (jqXHR && jqXHR.responseJSON) {
                    if (jqXHR.responseJSON.Type != undefined) {
                        if (errorHandler) {
                            errorHandler & errorHandler(jqXHR.responseJSON);
                        } else {
                            ls.ui.util.dialogAlertError(jqXHR.responseJSON.Message, function () {
                                location.reload();
                            });
                        }
                    }
                }
            }
        };

        ajaxObj = $.extend(ajaxObj, opts);

        $.ajax(ajaxObj);
    };

    ///*
    // * 获取Url中的参数
    // * @param name 参数名称
    // */
    //util.prototype.GetQueryString = function (name) {
    //    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    //    var r = window.location.search.substr(1).match(reg);
    //    if (r != null) return unescape(r[2]); return null;
    //}

    /*
     * 打开弹窗
     * @param title 窗体标题
     * @param url   页面地址
     * @param transData 传入子页面数据
     * @param submitCallback 提交关闭时候的回掉
     * @param opt 扩展参数
     *      width	    弹窗宽度
     *      height	    弹窗高度
     *      offset	    详细配置参见:http://www.layui.com/doc/modules/layer.html#offset
     *      shade	    详细配置参见:http://www.layui.com/doc/modules/layer.html#shade
     *      btn	        按钮显示,默认["确认", "关闭"],如果不需要下面两个按钮则设置为null
     */
    util.prototype.dialogOpen = function (title, url, transData, submitCallback, opt) {
        var nopt = {
            id: NewGuid(),
            title: title,
            url: url,
            transData: transData,
            submitCallback: submitCallback,
            cancel: opt && opt.cancelCallback,
            notShowCloseBtn: false // 是否隐藏右上角按钮
        };
        opt && opt.offset && (nopt["offset"] = opt.offset); //不常用项
        opt && typeof (opt.shade) != 'undefined' && (nopt["shade"] = opt.shade); //不常用项
        opt && opt.cancel && (nopt["cancel"] = opt.cancel); //不常用项
        //统一样式要求最小宽度为768px;
        if (opt && opt.width) {
            nopt["width"] = opt.width
        } else {
            nopt["width"] = "768px";
        }
        opt && opt.height && (nopt["height"] = opt.height);
        opt && typeof (opt.btn) != 'undefined' && (nopt["btn"] = opt.btn);
        nopt = $.extend(nopt, opt);
        logModule({ title: title, url: url, transData: transData });//记录页面访问
        window.pl.dialogOpen(nopt);
    }


    /*
 * 打开弹窗
 * @param title 窗体标题
 * @param url   页面地址
 * @param transData 传入子页面数据
 * @param submitCallback 提交关闭时候的回掉
 * @param opt 扩展参数
 *      width	    弹窗宽度
 *      height	    弹窗高度
 *      offset	    详细配置参见:http://www.layui.com/doc/modules/layer.html#offset
 *      shade	    详细配置参见:http://www.layui.com/doc/modules/layer.html#shade
 *      btn	        按钮显示,默认["确认", "关闭"],如果不需要下面两个按钮则设置为null
 *      notShowCloseBtn	        按钮显示,默认["确认", "关闭"],如果不需要下面两个按钮则设置为null
 */
    util.prototype.dialogOpen1 = function (title, url, transData, submitCallback, opt) {
        var nopt = {
            id: NewGuid(),
            title: title,
            url: url,
            transData: transData,
            submitCallback: submitCallback,
            notShowCloseBtn: true // 是否隐藏右上角按钮
        };
        opt && opt.offset && (nopt["offset"] = opt.offset); //不常用项
        opt && typeof (opt.shade) != 'undefined' && (nopt["shade"] = opt.shade); //不常用项
        opt && opt.cancel && (nopt["cancel"] = opt.cancel); //不常用项
        //统一样式要求最小宽度为768px;
        if (opt && opt.width) {
            nopt["width"] = opt.width
        } else {
            nopt["width"] = "768px";
        }
        opt && opt.height && (nopt["height"] = opt.height);
        opt && typeof (opt.btn) != 'undefined' && (nopt["btn"] = opt.btn);
        nopt = $.extend(nopt, opt);
        logModule({ title: title, url: url, transData: transData });//记录页面访问
        window.pl.dialogOpen1(nopt);
    }

    /*
     * 打开弹窗
     * @param options 上传控制参数
     * @param submitCallback 上传成功时的回调
     */
    util.prototype.fileUpload = function (options, submitCallback) {
        this.dialogOpen("文件上传", "/File/Index", options, function (r) {
            submitCallback && submitCallback(r);
        })
    }


    /*
    * 打开顶层Tab页面
    * @param title Tab页面标题
    * @param url   Tab页面地址
    * @param transData 传入子页面数据
    * @param opt 扩展参数
    *  id	        选项卡Id
    *  icon      选项卡左上角小图标
    */
    util.prototype.tabOpen = function (title, url, transData, opt) {
        //TODO:可以拓展一个关闭回调
        var opt = {
            id: opt ? (opt.id ? opt.id : NewGuid()) : NewGuid(),
            dataValue: opt ? (opt.id ? opt.id : NewGuid()) : NewGuid(),
            title: title,
            closed: opt ? (typeof (opt.close) == "undefined" ? true : !!opt.close) : true,
            replace: opt ? (typeof (opt.replace) == "undefined" ? true : !!opt.replace) : true,
            icon: opt && (opt.icon ? opt.icon : null),
            url: url,
            parentsId: (opt && opt.parentsId) ? opt.parentsId : top.menuLiId,//二级菜单id
            parentId: (opt && opt.parentId) ? opt.parentId : top.secNavId,//当前标签页id
        };
        top.tablist.addTab(opt, transData);
    }
    /**
     * 关闭顶层Tab页面
     * @param {any} tabId   顶层TabId
     * @param {any} cal       关闭后的回调事件
     */
    util.prototype.tabClose = function (tabId, cal) {
        if (!tabId) return;
        top.tablist.removeTabByTabId(tabId, function () {
            cal && cal();
        })
    }




    /*
     *  改变ifram的Url
     * @param   targetDivId                   Iframe生成放置的目表DIV
     * @param   url                                 生成的iframe地址
     * @param   transData                     传入的子窗体的数据
     * @param   loadCompleteCal         子窗体加载成功后的回调
     * @param   opt                               拓展参数配置
     */
    util.prototype.changeFrameUrl = function (targetDivId, url, transData, loadCompleteCal, opt) {
        var $div = $("#" + targetDivId).find('iframe').remove();
        $frame = $('<iframe src="' + url + '" height="100%" width="100%" id="" style="border: none;" hasUseInitFunc="true"></iframe>');
        $("#" + targetDivId).append($frame);
        $frame.load(function () {
            this.contentWindow.init && this.contentWindow.init(transData);
            this.contentWindow.checkViewElementPower && this.contentWindow.checkViewElementPower();//验证页面元素权限
            loadCompleteCal && loadCompleteCal();
        });
    }

    ///*
    //* 弹出框
    //* @param message Tab页面标题
    //* @param cal   Tab页面地址
    //* @param opt 扩展参数
    //*  type	        图标样式
    //*/
    //util.prototype.dialogAlert = function (message, cal, opt) {
    //    window.pl.dialogAlert({
    //        msg: message,
    //        callBack: cal,
    //        type: -2
    //    })
    //}

    /*
    * 成功提示框
    * @param    message Tab页面标题
    * @param    cal   Tab页面地址
    * @param    opt 扩展参数
    *       type	        图标样式
    */
    util.prototype.dialogAlertSuccess = function (message, cal, opt) {
        window.pl.dialogAlert({
            msg: message,
            callBack: cal,
            type: 1
        })
    }

    /*
    * 警告提示框
    * @param    message   不可空 提示内容
    * @param    cal             可空  回调方法
    * @param    opt            可空  扩展参数
    *       type	    图标样式
    */
    util.prototype.dialogAlertWarning = function (message, cal, opt) {
        window.pl.dialogAlert({
            msg: message,
            callBack: cal,
            type: 0
        })
    }

    /*
    * 失败提示框
    * @param    message   不可空 提示内容
    * @param    cal             可空  回调方法
    * @param    opt            可空  扩展参数
    *       type	    图标样式
    */
    util.prototype.dialogAlertError = function (message, cal, opt) {
        window.pl.dialogAlert({
            msg: message,
            callBack: cal,
            type: 2
        })
    }
    /*
    * 消息提示框 自动隐藏
    * @param    message   不可空 提示内容
    * @param    cal             可空  回调方法
    * @param    opt            可空  扩展参数
    *       type	    图标样式
    */
    util.prototype.dialogMsg = function (message, cal, opt) {
        window.pl.dialogMsg({
            msg: message,
            callBack: cal,
            type: 0
        })
    }

    /*
     * 对话框
     * @param message   不可空     对话提示语
     * @param succCal     可空        确定回调
     * @param failCal       可空        取消回调
     * @param opt           可空        扩展参数
     *  type	        图标样式
     */
    util.prototype.dialogConfirm = function (message, succCal, failCal, opt) {
        window.pl.dialogConfirm({
            msg: message,
            succCal: succCal,
            failCal: failCal
        })
    }


    /*
    * 关闭当前弹窗并且回调
    * @param cal  关闭时候的回调
    */
    util.prototype.dialogCloseSelf = function (cal) {
        window.pl.dialogClose(function () {
            cal && cal();
        });
    }



    /*
    * 关闭当前弹窗并且回调
    * @param cal  关闭时候的回调
    */
    util.prototype.dialogClose = function (cal) {
        window.pl.dialogClose(function () {
            cal && cal();
        });
    }

    /*
    * 关闭当前所有并且回调
    * @param cal  关闭时候的回调
    */
    util.prototype.dialogCloseAllChild = function (cal) {
        window.pl.dialogCloseAllChild(function () {
            cal && cal();
        });
    }

    /*
 * 打开弹窗
 * @param options 上传控制参数
 * @param submitCallback 上传成功时的回调
 */
    util.prototype.fileUpload2 = function (options, submitCallback) {
        this.dialogOpen2("文件上传", "/File/Index", options, function (r) {
            submitCallback && submitCallback(r);
        })
    }



    /*
     * 新页面打开弹窗
     * @param title 弹窗标题头
     * @param url   页面链接
     * @param transData  页面传递数据
     * @param submitCallback 请求服务端成功后返回数据回调函数,function(data){}
     * @param opt 扩展参数
     *      area:   ['660px', '350px']	弹出框高度，宽度
     *      resize	false   是否可放大缩小
     *      move	false   是否允许拖动
     *      btns    ['确定', '取消']    右下角按钮
     */
    util.prototype.dialogOpen2 = function (title, url, transData, submitCallback, opt) {
        var defaultOpts = {
            width: '888px',
            height: '880px',
            area: [

            ],
            resize: true,
            move: true,
            layerPageIndex: -1,
            btns: ['确定', '取消'],
            id: NewGuid(),
            title: title,
            url: url,
            transData: transData,
            submitCallback: submitCallback,
            shade: 0.3,
            notShowCloseBtn: false,
            scrollbar: true,
            offset: 'auto',
            isFull: false,
            maxmin: true,
        }

        opt && opt.offset && (defaultOpts["offset"] = opt.offset); //不常用项
        opt && typeof (opt.shade) != 'undefined' && (defaultOpts["shade"] = opt.shade); //不常用项
        opt && opt.cancel && (defaultOpts["cancel"] = opt.cancel); //不常用项
        opt && opt.width && (defaultOpts["width"] = opt.width);
        opt && opt.height && (defaultOpts["height"] = opt.height);
        opt && typeof (opt.btn) != 'undefined' && (defaultOpts["btns"] = opt.btn);
        opt && opt.isFull && (defaultOpts['isFull'] = opt.isFull);
        opt && (defaultOpts['maxmin'] = opt.maxmin === false ? false : true)
        opt && (defaultOpts['notShowCloseBtn'] = opt.notShowCloseBtn === false ? false : true)

        defaultOpts.area = [defaultOpts.width, defaultOpts.height];
        var isLogModuleArr = ['/Auth/SubDbUpdate']

        if (isLogModuleArr.indexOf(url) == -1) {
            logModule({ title: title, url: url, transData: transData }) //记录页面访问
        }

        var cal = function (data) {
            setTimeout(function () {
                try {
                    if (submitCallback) {
                        if (submitCallback(data) == false) {
                            return false;
                        }
                    }
                    layer.close(defaultOpts.layerPageIndex);
                } catch (e) {
                    throw e;
                }
            }, 0);
        }
        defaultOpts = $.extend(defaultOpts, opt);

        var layerIndex = layer.open({
            title: title,
            //width: opt.width,
            //height: opt.height,
            area: defaultOpts.area,
            offset: defaultOpts.offset,
            resize: defaultOpts.resize,
            move: '.layui-layer-title',
            shade: defaultOpts.shade,
            type: 2,
            content: url,
            maxmin: defaultOpts.maxmin,
            btn: defaultOpts.btns,
            scrollbar: defaultOpts.scrollbar,
            yes: function (index, layero) {
                defaultOpts.layerPageIndex = index
                if (!layero || layero.length <= 0) return
                var iframeEles = $(layero[0]).find('iframe')
                if (!iframeEles || iframeEles.length <= 0) return
                $($(layero[0]).find('a.layui-layer-btn0')[0]).hide()
                $($(layero[0]).find('span.layui-layer-btn0')[0]).css(
                    'display',
                    'inline-block'
                )
                iframeEles[0].contentWindow.submit &&
                    iframeEles[0].contentWindow.submit(cal)
                setTimeout(function () {
                    $($(layero[0]).find('a.layui-layer-btn0')[0]).show()
                    $($(layero[0]).find('span.layui-layer-btn0')[0]).hide()
                }, 3000)
            },
            cancel: function (layero, index) {
                opt && opt.cancel && opt.cancel()
            },
            success: function (layero, index) {
                if (!layero || layero.length <= 0) return
                var iframeEles = $(layero[0]).find('iframe')
                if (!iframeEles || iframeEles.length <= 0) return
                //写入前置逻辑脚本
                //var preScript = document.createElement("script");
                //preScript.type = "text/javascript";
                //preScript.text = "";
                //preScript.text += "            function preInit() {";
                //preScript.text += "                layui && layui.form && layui.form.render();";
                //preScript.text += "            }";
                //iframeEles[0].contentWindow.document.body.appendChild(preScript);
                ////执行前置逻辑
                //iframeEles[0].contentWindow.preInit && iframeEles[0].contentWindow.preInit();
                //执行页面初始化逻辑
                var $yesBtn = $($(layero[0]).find('a.layui-layer-btn0')[0])
                var yesBtnHtml = $yesBtn.html()
                $yesBtn.after(
                    '<span class="layui-layer-btn0" style="display: none;zoom: 1;vertical-align: top;background-color: #1E9FFF;color: #fff;height: 28px;line-height: 28px;margin: 5px 5px 0;padding: 0 15px;border: 1px solid #dedede;border-radius: 2px;font-weight: 400;cursor: pointer;text-decoration: none;font-size: 14px;">执行中,稍后...</span>'
                )
                //#ls#接管地址栏传值,和弹窗传参合并后传入子页面init方法
                //var urlData = {};
                //var urls = iframeEles[0].src.split('?');
                //if (urls.length > 1) {
                //	for (var z = 1; z < urls.length; z++) {
                //		var kvs = urls[z].split('&');
                //		for (var i = 0; i < kvs.length; i++) {
                //			var kv = kvs[i].split('=');
                //			if (kv.length == 2) {
                //				urlData[kv[0]] = kv[1];
                //			}
                //		}
                //	}
                //}
                //transData = $.extend(urlData, transData);
                //if (JSON.stringify(transData) == "{}") {
                //	transData = null;
                //}
                iframeEles[0].contentWindow.init &&
                    iframeEles[0].contentWindow.init(transData)
                iframeEles[0].contentWindow.checkViewElementPower &&
                    iframeEles[0].contentWindow.checkViewElementPower() //验证页面元素权限
                if (defaultOpts.notShowCloseBtn === true) {
                    layero.find('.layui-layer-close').remove()
                }
            }
        })
        /*是否全屏*/
        if (defaultOpts.isFull) {
            layer.full(layerIndex)
        }

    }

    /*
     * 对话框
     * @param message   不可空     对话提示语
     * @param succCal     可空        确定回调
     * @param failCal       可空        取消回调
     * @param opt           可空        扩展参数
     *  type	        图标样式
     */
    util.prototype.dialogConfirm2 = function (message, succCal, failCal, opts) {
        var defaultOptions = {
            btn: ['确定', '取消']
        };
        defaultOptions = $.extend(defaultOptions, opts);
        layer.confirm(message, {
            btn: defaultOptions.btn //按钮
        }, function (index) {
            succCal && succCal();
            layer.close(index);
        }, function () {
            failCal && failCal();
        });
    }


    /*
   * 成功提示框
   * @param    message Tab页面标题
   * @param    cal   Tab页面地址
   * @param    opt 扩展参数
   *       type	        图标样式
   */
    util.prototype.dialogAlertSuccess2 = function (message, cal, opts) {
        var defaultOptions = {
            type: 1,
            title: "系统提示"
        };
        defaultOptions = $.extend(defaultOptions, opts);
        layer.alert(message, {
            icon: defaultOptions.type,
            title: defaultOptions.title,
        }, function (index) {
            cal && cal();
            layer.close(index);
        });
    }

    /*
    * 警告提示框
    * @param    message   不可空 提示内容
    * @param    cal             可空  回调方法
    * @param    opt            可空  扩展参数
    *       type	    图标样式
    */
    util.prototype.dialogAlertWarning2 = function (message, cal, opts) {
        var defaultOptions = {
            type: 0,
            title: "系统提示"
        };
        defaultOptions = $.extend(defaultOptions, opts);
        layer.alert(message, {
            icon: defaultOptions.type,
            title: defaultOptions.title,
        }, function (index) {
            cal && cal();
            layer.close(index);
        });
    }

    /*
    * 失败提示框
    * @param    message   不可空 提示内容
    * @param    cal             可空  回调方法
    * @param    opt            可空  扩展参数
    *       type	    图标样式
    */
    util.prototype.dialogAlertError2 = function (message, cal, opts) {
        var defaultOptions = {
            type: 2,
            title: "系统提示"
        };
        defaultOptions = $.extend(defaultOptions, opts);
        layer.alert(message, {
            icon: defaultOptions.type,
            title: defaultOptions.title,
        }, function (index) {
            cal && cal();
            layer.close(index);
        });
    }


    util.prototype.dialogPrompt2 = function (title, cal, opts) {
        var defaultOptions = {
            formType: 2,//输入框类型，支持0（文本）默认1（密码）2（多行文本）
            value: '',
            title: '系统提示',
            area: ['550px', '300px'] //自定义文本域宽高
        };
        defaultOptions = $.extend(defaultOptions, opts);
        defaultOptions["title"] = title;

        layer.prompt(defaultOptions, function (value, index, elem) {
            cal && cal(value);
            layer.close(index);
        });
    }




    /*
    * 关闭当前弹窗并且回调,子页面调用
    * @param cal  关闭时候的回调
    */
    util.prototype.dialogClose2 = function (cal) {
        cal && cal();
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }




    /*
    *  获取前端Guid
    */
    util.prototype.getGuid = function () {
        return NewGuid();
    }


    util.prototype.getParam = function (name) {
        var search = document.location.search;
        var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
        var matcher = pattern.exec(search);
        var items = null;
        if (null != matcher) {
            try {
                items = matcher[1];
            } catch (e) {
                items = null;
            }
        }
        return items;
    };
    /*
     * 替换字符串
     */
    util.prototype.replaceAll = function (str, replaceStr, newStr) {
        var reg = new RegExp(replaceStr, "g");
        return str.replace(reg, newStr);
    }

    /*
    * jqgrid 导出excel
    */
    util.prototype.gridToExcel = function (options) {
        var defaults = {
            tableIdStr: "gridTable", // 表格id
            ignoreColumn: [], // 排除列的索引 [0,1]
            fileName: options.fileName,
            type: 'excel',
            pdfFontSize: 14,
            pdfLeftMargin: 20,
            escape: 'true',
            htmlContent: 'false',
            consoleLog: 'false',
            complexHeadId: "thereThead", // 复杂表格id 默认给"thereThead"
            complexHead: false // 是否是复杂表头
        };
        var options = $.extend(defaults, options);

        var idStr = options.tableIdStr;
        if (!defaults.complexHead) {
            var dd = $("#gbox_" + idStr + ' .ui-jqgrid-htable thead').clone();//找到<thead>
        } else {
            var dd = $("#thereThead thead").clone();//找到<thead>
        }
        var ee = $('#' + idStr).clone();
        ee.find('.jqgfirstrow').remove();//干掉多余的无效行
        ee.find('tbody').before(dd);//合并表头和表数据
        ee.find('tr.ui-search-toolbar').remove();//干掉搜索框
        ee.tableExport(
            {
                type: defaults.type,
                fileName: defaults.fileName,//,excel设置导出的表的默认名称
                escape: defaults.escape,
                ignoreColumn: defaults.ignoreColumn,
                complexHead: defaults.complexHead, // 复杂表头 需要自己单独写一个简单的
                consoleLog: defaults.consoleLog // 书否输入结果dom
            }
        );
    }

    /*
     * 局部loading
    */
    util.prototype.loadingArea = function (isShow, text, element) {
        var top = $("#" + element);
        top.hasClass("rel") ? "" : top.addClass("rel");
        var html = '<div class="loading_background" style="display: none;"></div>' +
            '<div class="loading_manage" style="display: none;">' + text + '</div>';
        top.append(html);
        var t = top.find(".loading_manage,.loading_background");
        isShow ? t.show() : t.hide();
        top.find(".loading_manage").css("left", (top.width() - top.find(".loading_manage").width()) / 2 - 54);
        top.find(".loading_manage").css("top", (top.height() - top.find(".loading_manage").height()) / 2);
    }

    /*
        * JSON数组去重
        * @param: [array] json Array
        * @param: [string] 唯一的key名，根据此键名进行去重
    */
    util.prototype.uniqueArray = function (array, key) {
        var result = [array[0]];
        for (var i = 1; i < array.length; i++) {
            var item = array[i];
            var repeat = false;
            for (var j = 0; j < result.length; j++) {
                if (item[key] == result[j][key]) {
                    repeat = true;
                    break;
                }
            }
            if (!repeat) {
                result.push(item);
            }
        }
        return result;
    }

    /*
        * 将null值替换成空字符
        * data 为数组参数
    */
    util.prototype.nullToStr = function (data) {
        for (var x in data) {
            if (data[x] === null) { // 如果是null 把直接内容转为 ''
                data[x] = '';
            } else {
                if (Array.isArray(data[x])) { // 是数组遍历数组 递归继续处理
                    data[x] = data[x].map(function (z) {
                        return ls.ui.util.nullToStr(z);
                    });
                }
                if (typeof (data[x]) === 'object') { // 是json 递归继续处理
                    data[x] = ls.ui.util.nullToStr(data[x])
                }
            }
        }
        return data;
    }

    /*
        * 详情页面的左侧按钮生成
        * opt 为数组参数
    */
    util.prototype.detailLeftBtn = function (opt) {
        if (!!opt && !!opt.dom && !!opt.btnData) {
            $.each(opt.btnData, function (i, v) {
                var active = (i == 0) ? "active" : "";
                var $li = $('<li id="' + v.id + '" class="' + active + '">' +
                    '<a href="#' + v.className + '" data-toggle="tab">' + v.text + '</a>' +
                    '</li>');
                $li.on("click", function () {
                    $(opt.dom).find("li").removeClass("active");
                    $li.addClass("active");
                    $(opt.switchDiv).children("div").hide();
                    $(opt.switchDiv).find("." + v.className + "").show();
                    v.click();
                    $(window).resize();
                })
                $(opt.dom).append($li);
            })
        }
    }

    /*
        * 计算两个日期相差的天数
        * opt 为数组参数
        * 调用
        var sss = ls.ui.util.diffDataRange({
            start: "2019/11/20",
            end: "2021-12-26",
            type: "string"  // string为几年几月几天|| day为多少天
        });
    */
    util.prototype.diffDataRange = function (opt) {
        if (!!opt && !!opt.start && !!opt.end && !!opt.type) {
            var dateBegin = new Date(opt.start);
            var dateEnd = new Date(opt.end);
            if (dateBegin > dateEnd) {
                dialogMsg('开始日期不能大于结束日期！', 0);
                return "";
            }
            if (opt.type == "string") {
                //文本显示
                var reg = new RegExp(
                    /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
                var beginArr = opt.start.match(reg);
                var endArr = opt.end.match(reg);
                var days = 0;
                var month = 0;
                var year = 0;

                days = endArr[4] - beginArr[4] + 1;

                if (days < 0) {
                    month = -1;
                    days = 30 + days;
                }

                month = month + (endArr[3] - beginArr[3]);
                if (month < 0) {
                    year = -1;
                    month = 12 + month;
                }
                year = year + (endArr[1] - beginArr[1]);

                // 闰月情况
                if (endArr[3] == "01" || endArr[3] == "03" || endArr[3] == "05" || endArr[3] == "07" || endArr[3] == "08" || endArr[3] == "10" || endArr[3] == "12") {
                    if (days == 31) {
                        days = 0;
                        month = month + 1;
                    }
                }
                // 奇月情况
                if (endArr[3] == "02" || endArr[3] == "04" || endArr[3] == "06" || endArr[3] == "09" || endArr[3] == "11") {
                    if (days == 30) {
                        days = 0;
                        month = month + 1;
                    }
                }
                if (month == 12) {
                    month = 0;
                    year = year + 1;
                }

                var yearString = year > 0 ? year + "年" : "";
                var mnthString = month > 0 ? month + "个月" : "";
                var dayString = days > 0 ? days + "天" : "";
                var resultString = "";
                if (yearString) {
                    resultString += yearString
                }
                if (mnthString) {
                    resultString += mnthString
                }
                if (dayString) {
                    resultString += dayString
                }
                return resultString;
            }
            //返回天数
            if (opt.type == "day") {
                var diff = parseInt((dateEnd.valueOf() / (1000 * 60 * 60 * 24)) - (dateBegin.valueOf() / (1000 * 60 * 60 * 24)) + 1);
                return diff;
            }
        }
    }

    /*
       * lastIndexOf ie内核浏览器不兼容
       * str 为字符串参数 item为指定字符
   */
    util.prototype.lastIndexOf = function (str, item) {
        var len = str.length;
        for (var i = len; i >= 0; i--) {
            if (str[i] === item) {
                return i;
            }
        }
        return -1;
    }

    // jqgrid列自适应宽度
    util.prototype.jqAutoWidth = function (opts) {
        function uniq(array) {
            var temp = []; //一个新的临时数组
            for (var i = 0; i < array.length; i++) {
                if (temp.indexOf(array[i]) == -1) {
                    temp.push(array[i]);
                }
            }
            return temp;
        }
        $('#' + opts.jqWrap).parent().append('<div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style = "position:absolute;top:-9999px" ><div class="ui-jqgrid-view"><div class="ui-jqgrid-bdiv"><div style="position: relative;"><table cellspacing="0" cellpadding="0" border="0"><tr class="ui-widget-content jqgrow ui-row-ltr" style="table-layout:table"><td id="tdCompute" style="background:#eee;width:auto"></td></tr></table></div></div></div></div>')
        //获取计算实际列长度的容器  
        var td = $('#gview_' + opts.jqWrap + ' #tdCompute'),
            //用于保存最大的列宽 
            tds = [],
            // 用于保存需要自适应的列的下标
            idxs = [];
        //遍历每行获得每行中的最大列宽  
        $('#gview_' + opts.jqWrap + ' .ui-jqgrid-htable tr,' + '#' + opts.jqWrap + ' tr:gt(0)').each(function () {
            $(this).find('td,th').each(function (idx) {
                if (!$(this).is(':hidden')) {
                    if ($(this).hasClass('autowidth')) {
                        if ($(this).text().length > 100) {
                            var _text = $(this).text()
                            $(this).text(_text.substr(0, 100) + '...')
                        }
                    }
                    tds[idx] = Math.max(tds[idx] ? tds[idx] : 0, td.html($(this).text())[0] ? td.html($(this).text())[0].offsetWidth : 0);
                    if ($(this).children().length && $(this).hasClass('autowidth')) {
                        tds[idx] = Math.max(tds[idx] ? tds[idx] : 0, $(this).children().width() + (td.html($(this).text())[0] ? td.html($(this).text())[0].offsetWidth : 0));
                    }
                } else {
                    tds[idx] = 0
                }
                if ($(this).hasClass('autowidth')) {
                    idxs.push(idx)
                }
            })
        });
        var maxWidth = $(window).width() / 4
        for (var i = 0; i < tds.length; i++) {
            if (tds[i] > maxWidth) {
                tds[i] = maxWidth
            }
        }
        idxs = uniq(idxs)
        $('#gview_' + opts.jqWrap + ' th').each(function (idx) {
            for (var i = 0; i < idxs.length; i++) {
                if (idx == idxs[i]) {
                    var wid = 0
                    if (!$(this).find('.s-ico').is(':hidden')) {
                        wid = 20
                    } else {
                        wid = 0
                    }
                    if (tds[idx] < 72) {
                        tds[idx] = tds[idx] + wid
                    }
                    $(this).css({ width: tds[idx] + 'px' })
                }
            }
        });
        $('#' + opts.jqWrap).find('tr:eq(0) td').each(function (idx) {
            for (var i = 0; i < idxs.length; i++) {
                if (idx == idxs[i]) {
                    $(this).css({ width: tds[idx] + 'px' })
                }
            }
        });

        // 表单页脚重置宽度
        $('#gview_' + opts.jqWrap + ' .ui-jqgrid-ftable tr td').each(function (idx) {
            for (var i = 0; i < idxs.length; i++) {
                if (idx == idxs[i]) {
                    $(this).css({ width: tds[idx] + 'px' })
                }
            }
        });
    }

    // 漏斗
    // $dom 渲染的节点
    // url 接口地址
    // data 默认数据
    // callback 回调函数
    // currentParams 设置默认的当前参数
    // 例：
    // params:{
    //      $dom: $('.tool-panel'),
    //      url: '',
    //      data: [{ "name": "机构", "id": "2" }, { "name": "区域", "id": "1" }],
    //      callback: GetTree,
    //      currentParams: {
    //          name: '区域',
    //          id: 1,
    //      }
    // }
    util.prototype.funnel = function (params) {
        var tpl = ''
        if (params.url != '' && params.url != null) { // 接口数据
            this.request(params.url, {}, function (responese) {
                tpl += '<p class="clearfix">'
                    + '<span class="tool-selected-text fl">' + responese[0].text + '</span>'
                    + '<i class="fa fa-filter fa-lg fr" aria-hidden="true"></i>'
                    + '</p>'
                    + '<ul class="tool-lists">';
                for (var i = 0; i < responese.length; i++) {
                    tpl += '<li id="' + (i + 1) + '">' + responese[i].text + '</li>';
                }
            }, null, { async: false })
        } else if (params.data && params.data.length) {
            tpl += '<p class="clearfix">'
                + '<span class="tool-selected-text fl">' + params.data[0].name + '</span>'
                + '<i class="fa fa-filter fa-lg fr" aria-hidden="true"></i>'
                + '</p>'
                + '<ul class="tool-lists">'
            for (var i = 0; i < params.data.length; i++) {
                tpl += '<li id="' + params.data[i].id + '">' + params.data[i].name + '</li>'
            }
        } else {
            this.dialogAlertError('url或data为空！');
        }
        params.$dom.html(tpl);
        if (params.currentParams && params.currentParams.id) {
            params.$dom.find('.tool-lists li').each(function (idx) {
                if ($(this).attr('id') == params.currentParams.id) {
                    $(this).addClass('active')
                }
            });
        } else {
            params.$dom.find('.tool-lists li').eq(0).addClass('active')
            params.currentParams = {
                name: params.$dom.find('.tool-lists li').eq(0).text(),
                id: params.$dom.find('.tool-lists li').eq(0).attr('id'),
            }
        }
        // 默认执行一次回调函数
        params.callback(params.currentParams);
        // 点击工具切换tab
        var $tabBox = params.$dom.find('ul'),
            $tabBoxItem = $tabBox.find("li");
        params.$dom.find("p i").click(function (e) {
            event.stopPropagation();
            //判断容器是否隐藏
            if ($tabBox.is(":hidden")) {
                $tabBox.fadeIn();
                return false;
            }
            $tabBox.fadeOut()
        })
        // 点击boxitem切换数据
        $tabBoxItem.click(function () {
            event.stopPropagation();
            var tool_id = $(this).attr('id');
            $(this).addClass("active").siblings().removeClass('active');
            params.$dom.find(".tool-selected-text").text($(this).text());
            $tabBox.fadeOut();
            if (params.currentParams.id != tool_id) {
                var itemData = {}
                if (params.data.length) {
                    for (var i = 0, item; item = params.data[i++];) {
                        if (item.id == tool_id) {
                            itemData = item
                        }
                    }
                }
                params.callback(itemData)
                params.currentParams = itemData
            }
        })
        $(document).click(function () {
            $tabBox.fadeOut()
        })
    }

    function NewGuid() {
        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    function logModule(item) {

    }

    /*
        tabs
        * opts 
        ** initData
        ** hasMultiple
        ** $multiple
        ** $container
        ** $box
        ** callback
    */
    util.prototype.tabs = function (opts) {
        if (opts.hasMultiple !== undefined) {
            if (opts.hasMultiple) {
                opts.$multiple.show()
            } else {
                opts.$multiple.hide()
            }
        }
        if (opts.initData && opts.initData.length) {
            opts.initData.map(function (item, index) {
                var $li = $('<li data-value="' + item.value + '" class="fl tc ' + (item.active ? "active" : "") + '" style="width:calc(100%/' + opts.initData.length + ' - 14px);">' + item.name + '</li>')
                opts.$container.append($li)
                $li.click(function () {
                    $(this).addClass('active').siblings().removeClass('active')
                    opts.callback && opts.callback($(this).attr('data-value'))
                })
            })
            if (opts.initData.length <= 1) {
                opts.$container.hide()
                opts.$box.css({ 'height': 'calc(100vh - 40px)' })
            }
        } else {
            ls.ui.util.request("/Conf/Get", { key: "Ls.AMS.Tabs" }, function (res) {
                var data = JSON.parse(res)
                if (data.initData) {
                    data.initData.map(function (item, index) {
                        var $li = $('<li data-value="' + item.value + '" class="fl tc ' + (item.active ? "active" : "") + '" style="width:calc(100%/' + data.initData.length + ' - 14px);">' + item.name + '</li>')
                        opts.$container.append($li)
                        $li.click(function () {
                            $(this).addClass('active').siblings().removeClass('active')
                            opts.callback && opts.callback($(this).attr('data-value'))
                        })
                    })
                }
                if (data.hasMultiple) {
                    opts.$multiple.show()
                } else {
                    opts.$multiple.hide()
                }
                if (data.initData.length <= 1) {
                    opts.$container.hide()
                    opts.$box.css({ 'height': 'calc(100vh - 40px)' })
                }
            });
        }
    }

    ls.ui = ls.ui || {};
    $.extend(ls.ui, { util: new util() });
})(jQuery);
