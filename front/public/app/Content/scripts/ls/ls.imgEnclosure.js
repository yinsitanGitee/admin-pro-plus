//上传附件&图片&渲染
(function ($) {
    var self = null;
    var ls = window.ls = window.ls || {};
    var imgEnclosure = function () {
        self = this;
    };
    imgEnclosure.prototype.addImg = function (type,postData,FileType,reqUrl,picListUrl,deleteUrl) {
        ls.ui.util.dialogOpen("文件上传", "/File/Index", {}, function (r) {
            r = r.map(function (t) {
                return {
                    PicAddr: t.filePath,
                    FileName: t.fileName,
                    HasSetTitle: false,
                };
            });
            saveOnePic(r,FileType,reqUrl,function () {
                self.initPicList(type,postData,picListUrl,function () {
                    initSortList(deleteUrl);
                })
            });
        })
    }
    imgEnclosure.prototype.initPicList = function(type,keyValue,picListUrl,deleteUrl,cal) {
        // var postData = { keyValue: intiData.keyValue };
        // var reqUrl = "/CS/Util/GetFileList";
        var reqUrl = picListUrl;
        ls.ui.util.request(reqUrl, {keyValue:keyValue}, function (r) {
            if (!r) return;
            //创建图片列表
            $("#picTable").html('');
            createTable(type,r,deleteUrl);
            cal && cal();
        });
    }
    // 保存文件
    function saveOnePic(r, FileType,reqUrl, cal) {
        var postData = [];
        $.each(r, function (index, item) {
            postData.push({
                BillId: intiData.keyValue,
                FileUrl: item.PicAddr,
                FileName: item.FileName,
                Type: FileType,
            });
        });
        // var reqUrl = "/CS/Util/AddFileList";
        var reqUrl = reqUrl;
        ls.ui.util.request(reqUrl, postData, function (r) {
            if (r.IsSuccess) {
                cal && cal();
            } else {
                ls.ui.util.dialogAlertError(r.Message);
            }
        });
    }
    // 文件删除
    function initSortList(deleteUrl) {
        picTableList = $("#picTable").sortable({
            animation: 150
        });
        $("#picTable").on("click", ".js-remove", function () {
            console.log(123)
            var dataId = $(this).attr('data-id');
            ls.ui.util.dialogConfirm("是否确定删除此文件", function () {
                var postData = { keyValue: dataId };
                // var reqUrl = "/CS/Util/DeleteFile";
                var reqUrl = deleteUrl
                //console.log(postData)
                ls.ui.util.request(reqUrl, postData, function (r) {
                    if (r) {
                        intiData.reflashPin && intiData.reflashPin();
                        self.initPicList(postData,picListUrl,deleteUrl,function () {
                            initSortList(deleteUrl);
                        });
                        dialogMsg("删除成功", 1);
                        $("#picTable li .js-smartPhoto").smartPhoto(); // 图片查看初始化
                        // 关闭所有图片弹出框
                        $('.smartphoto-dismiss').click(function () {
                            $(".smartphoto").attr("aria-hidden", true)
                        })
                    } else {
                        ls.ui.util.dialogAlertError("删除失败");
                    }
                })
            })
        })
    }
    // 附件生成
    function createTable(type,r,deleteUrl) {
        for (var i in r) {
            if (r[i]['Type'] == 1) {
                var item = r[i];
                var fileHouzhui = item.FileUrl;
                var point = fileHouzhui.lastIndexOf(".");
                var type = fileHouzhui.substr(point);
                if (type == ".pdf" || type == "PDF") {
                    var $li = $('<li data-FileUrl="' + item.FileUrl + '" data-FileName="' + item.FileName + '" data-FileType="' + item.Type + '"></li>');
                    var $btnGroup = $('<div class="btn-group"></div>');
                    var $btn = $('<a class="btn LeV9-btn LeV9-danger-btn js-remove" data-id="' + item.Id + '">删除</a>');
                    $btnGroup.append($btn);
                    $li.append('<a target="_blank" class="notImage" onclick="preview(\'' + type + '\',\'' + item.FileUrl + '\',\'' + item.FileName + '\')"><img style="height:80%;" src="/Content/images/filetype/pdf.png" /><div class="fileHide" title="' + item.FileName + '">' + item.FileName + '</div></a>');
                    $li.append($btnGroup);
                    $("#picTable").append($li);
                } else if (type == ".doc" || type == ".DOC" || type == ".DOCX" || type == ".docx" || type == ".xlsx") {
                    var $li = $('<li data-FileUrl="' + item.FileUrl + '" data-FileName="' + item.FileName + '" data-FileType="' + item.Type + '"></li>');
                    var $btnGroup = $('<div class="btn-group"></div>');
                    var $btn = $('<a class="btn LeV9-btn LeV9-danger-btn js-remove" data-id="' + item.Id + '">删除</a>');
                    $btnGroup.append($btn);
                    if (type == ".xlsx") {
                        $li.append('<a target="_blank" class="notImage" onclick="preview(\'' + type + '\',\'' + item.FileUrl + '\',\'' + item.FileName + '\')"><img style="height:80%;" src="/Content/images/filetype/xlsx.png" /><div class="fileHide" title="' + item.FileName + '">' + item.FileName + '</div></a>');
                    } else {
                        $li.append('<a target="_blank" class="notImage" onclick="preview(\'' + type + '\',\'' + item.FileUrl + '\',\'' + item.FileName + '\')"><img style="height:80%;" src="/Content/images/filetype/doc.png" /><div class="fileHide" title="' + item.FileName + '">' + item.FileName + '</div></a>');
                    }
                    $li.append($btnGroup);
                    $("#picTable").append($li);
                } else {
                    var $li = $('<li data-FileUrl="' + item.FileUrl + '" data-FileName="' + item.FileName + '" data-FileType="' + item.FileType + '" data-pic="' + item.Url + '"></li>');
                    var $btnGroup = $('<div class="btn-group"></div>');
                    var $btn = $('<a class="btn LeV9-btn LeV9-danger-btn js-remove" data-id="' + item.Id + '">删除</a>');
                    $btnGroup.append($btn);
                    $li.append('<a href="' + document.defaultView.location.origin + item.FileUrl + '" class="js-smartPhoto"><img style="height:80%;" src="' + document.defaultView.location.origin + item.FileUrl + '" /><div class="fileHide" title="' + item.FileName + '">' + item.FileName + '</div></a>');
                    $li.append($btnGroup);
                    $("#picTable").append($li);
                }
            }
        }
        $("#picTable li .js-smartPhoto").smartPhoto(); // 图片查看初始化
        // 关闭所有图片弹出框
        $('.smartphoto-dismiss').click(function () {
            $(".smartphoto").attr("aria-hidden", true)
        })
        $("#picTable li").on('mouseenter', function () {
            $(this).find(".btn-group").stop().animate({ height: 50 });
        });
        $("#picTable li").on('mouseleave', function () {
            $(this).find(".btn-group").stop().animate({ height: 0 });
        });
        initSortList(deleteUrl);
    }
    //非图片文件预览
    function preview(type,filePath, fileName) {//预览
        // if (initData.type == "1") {
        if (type == "1") {
            dialogMsg('该文件尚未保存，无法在线预览！', 0);
            return;
        }
        var isCunzai = false;
        for (var i = 0; i < fileData.length; i++) {
            var obj = fileData[i];
            if (obj.FileUrl.indexOf(filePath) != -1) {
                isCunzai = true;
            }
        }
        if (isCunzai) {
            var docType = fileName.substr(fileName.lastIndexOf(".")).toLowerCase();//获得文件后缀名
            if (docType == '.xlsx' || docType == '.docx' || docType == '.doc' || docType == '.pptx') {
                window.open("http://view.officeapps.live.com/op/view.aspx?src=http://" + document.domain + filePath)
            } else {
                window.open("http://" + document.domain + filePath)
            }
        } else {
            dialogMsg('该文件尚未保存，无法在线预览！', 0);
        }
    }
    ls.ui = ls.ui || {};
    $.extend(ls.ui, { imgEnclosure: new imgEnclosure() });
})(jQuery)
