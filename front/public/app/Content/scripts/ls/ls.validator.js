﻿(function ($) {
    var ls = window.ls = window.ls || {};
    var validator = function () { };

    /*
    * 限制Input输入
    * @obj 当前input对象
    * @maxLangth 数字最大长度;整数位长度+小数位长度
    * @dcNum       在最大长度下保留多少位小数,不输入则默认两位,TODO:未实现
    */
    validator.prototype.num = function (obj, maxLangth, dcNum) {
        maxLangth && (obj.value = obj.value.substring(0, maxLangth + 1));//限制最大长度
        obj.value = obj.value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
        obj.value = obj.value.replace(/^\./g, ""); //验证第一个字符是数字
        obj.value = obj.value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
        obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'); //只能输入两个小数 
    }

    /*
    * 显示验证不通过信息
    * @obj 当前input对象
    * @msg 提示信息
    */
    validator.prototype.showValidationMessage = function ($obj, msg) {
        ValidationMessage($obj, msg);
    }

    /*
    * 隐藏验证不通过信息
    * @obj 当前input对象
    */
    validator.prototype.hideValidationMessage = function ($obj) {
        removeMessage($obj);
    }
    /*
    * 验证电话号码是否正确
    * @obj 需要检测的值
    *  返回true/false
    */
    validator.prototype.checkTelephone = function (obj) {
        return isTelephone($.trim(obj));
    }
    /*
    * 验证手机号码是否正确
    * @obj 需要检测的值
    *  返回true/false
    */
    validator.prototype.checkMobile = function (obj) {
        return isMobile($.trim(obj));
    }

    /*
      * 验证Email
      * @obj 需要检测的值
      *  返回true/false
      */
    validator.prototype.checkEmail = function (obj) {
        return isMobile($.trim(obj));
    }



    /*以下方法未开放调用入口 */
    /*验证方法库,如需要则开放调用入口 */

    //Email验证 email
    function isEmail(obj) {
        reg = /^\w{3,}@\w+(\.\w+)+$/;
        if (!reg.test(obj)) {
            return false;
        } else {
            return true;
        }
    }

    //验证是否电话号码 phone
    function isTelephone(obj) {
        reg = /^(\d{3,4}\-)?[1-9]\d{6,7}$/;
        if (!reg.test(obj)) {
            return false;
        } else {
            return true;
        }
    }

    //验证是否手机号 mobile
    function isMobile(obj) {
        reg = /^(\+\d{2,3}\-)?\d{11}$/;
        if (!reg.test(obj)) {
            return false;
        } else {
            return true;
        }
    }

    //判断输入的邮编(只能为六位)是否正确 zip
    function isZip(obj) {
        if (obj.length != 0) {
            reg = /^\d{6}$/;
            if (!reg.test(str)) {
                return false;
            }
            else {
                return true;
            }
        }
    }


    //判断是否为身份证 idcard
    function isIDCard(obj) {
        if (obj.length != 0) {
            reg = /^\d{15}(\d{2}[A-Za-z0-9;])?$/;
            if (!reg.test(obj))
                return false;
            else
                return true;
        }
    }

    //判断是否为IP地址格式
    function isIP(obj) {
        var re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g //匹配IP地址的正则表达式 
        if (re.test(obj)) {
            if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256) return true;
        }
        return false;
    }

    //判断是否为脚本或者特殊字符
    function isScript(obj) {
        var re = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
        var re1 = /["]+/;
        var re2 = /[^/]+/;
        if (re.test(obj) || re.test(obj) || re1.test(obj)) {
            return true;
        }
        else
            return false;
    }


    ls.ui = ls.ui || {};
    $.extend(ls.ui, { v: new validator() });
})(jQuery);