﻿$.ajaxSetup({
    beforeSend: function (jqXHR, settings) {
        var token = window.sessionStorage.getItem('token');
        if (token) token = token.replace('"', '').replace('"', '')

        var tenant = window.localStorage.getItem('Tenant');
        if (tenant) tenant = tenant.replace('"', '').replace('"', '')

        jqXHR.setRequestHeader("Authorization", token);
        jqXHR.setRequestHeader("Tenant", tenant);
    },
    complete: function (xhr) {
        if (xhr.status == 401) {
            layer.alert('登录状态失效,请重新登陆', {
                icon: 5,
                title: "提示"
            }, function () {
                location.reload();
            });
        }
        else if (xhr.status == 500) {
            layer.msg("后端未启动或处理失败，请重试！");
        }
        else if (xhr.status == 404) {
            layer.msg("未找到资源，请重试！");
        }
    }
});