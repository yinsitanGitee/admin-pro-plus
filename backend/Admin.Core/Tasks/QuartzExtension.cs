﻿using Mapster;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using Quartz.Impl.Triggers;
using Quartz.Spi;
using Admin.Common.Ioc;
using Admin.Common.Log;
using Admin.Common.SysStatic;

namespace Admin.Core.Tasks
{
    /// <summary>
    /// 拓展
    /// </summary>
    public static class QuartzExtension
    {
        private static List<SysTaskSchedulingEntity> _taskList = new();

        /// <summary>
        /// 添加作业
        /// </summary>
        /// <param name="SysTaskSchedulingEntity">任务</param>
        /// <param name="schedulerFactory">调度工厂</param>
        /// <param name="init">是否初始化</param>
        /// <param name="jobFactory">任务工厂</param>
        /// <returns></returns>
        public static async Task<string> AddJob(this ISchedulerFactory schedulerFactory, SysTaskSchedulingEntity SysTaskSchedulingEntity, bool init = false, IJobFactory? jobFactory = null)
        {
            try
            {
                var entity = SysTaskSchedulingEntity.Adapt<SysTaskSchedulingEntity>();
                string.Format("{0}_{1}_任务启动", entity.TaskName, entity.Id).Loger("TaskInitJobStart");
                if (init)
                {
                    if (entity.Type == APITaskType.System)
                        entity.Content = SystemConfig.SystemKestrelHttpUrl + entity.Content;

                    _taskList.Add(entity);
                }

                var group = entity.Tenant + entity.Id;
                IJobDetail job = JobBuilder.Create<HttpJob>()
                 .WithIdentity(entity.TaskName, group)
                 .Build();
                ITrigger trigger = TriggerBuilder.Create()
                   .WithIdentity(entity.TaskName, group)
                   .StartNow()
                   .WithDescription(entity.Remark)
                   .WithCronSchedule(entity.Interval)
                   .Build();

                IScheduler scheduler = await schedulerFactory.GetScheduler();

                if (jobFactory == null)
                    jobFactory = IocManager.Instance.GetService<IJobFactory>();

                if (jobFactory != null)
                    scheduler.JobFactory = jobFactory;

                await scheduler.ScheduleJob(job, trigger);
                if (entity.Status == APIStatusEnum.Normal) await scheduler.Start();
                else await schedulerFactory.Parse(SysTaskSchedulingEntity);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return string.Empty;
        }

        /// <summary>
        /// 更新作业
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="sysTaskSchedulingEntity"></param>
        /// <returns></returns>
        public static async Task<string> Update(this ISchedulerFactory schedulerFactory, SysTaskSchedulingEntity sysTaskSchedulingEntity)
        {
            return await schedulerFactory.TriggerAction(JobOperateEnum.Update, sysTaskSchedulingEntity);
        }

        /// <summary>
        /// 暂停作业
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="sysTaskSchedulingEntity"></param>
        /// <returns></returns>
        public static async Task<string> Parse(this ISchedulerFactory schedulerFactory, SysTaskSchedulingEntity sysTaskSchedulingEntity)
        {
            return await schedulerFactory.TriggerAction(JobOperateEnum.Parse, sysTaskSchedulingEntity);
        }

        /// <summary>
        /// 删除作业
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="sysTaskSchedulingEntity"></param>
        /// <returns></returns>
        public static async Task<string> Delete(this ISchedulerFactory schedulerFactory, SysTaskSchedulingEntity sysTaskSchedulingEntity)
        {
            return await schedulerFactory.TriggerAction(JobOperateEnum.Delete, sysTaskSchedulingEntity);
        }

        /// <summary>
        /// 启动作业
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="sysTaskSchedulingEntity"></param>
        /// <returns></returns>
        public static async Task<string> Start(this ISchedulerFactory schedulerFactory, SysTaskSchedulingEntity sysTaskSchedulingEntity)
        {
            return await schedulerFactory.TriggerAction(JobOperateEnum.Start, sysTaskSchedulingEntity);
        }

        /// <summary>
        /// 立即执行一次作业
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="sysTaskSchedulingEntity"></param>
        /// <returns></returns>
        public static async Task<string> ExecuteImmediately(this ISchedulerFactory schedulerFactory, SysTaskSchedulingEntity sysTaskSchedulingEntity)
        {
            return await schedulerFactory.TriggerAction(JobOperateEnum.ExecuteImmediately, sysTaskSchedulingEntity);
        }

        public static void ModifyTaskEntity(this SysTaskSchedulingEntity SysTaskSchedulingEntity, ISchedulerFactory schedulerFactory, JobOperateEnum action)
        {
            SysTaskSchedulingEntity? options = null;
            switch (action)
            {
                case JobOperateEnum.Delete:
                    _taskList = _taskList.Where(a => a.Id != SysTaskSchedulingEntity.Id).ToList();
                    break;
                case JobOperateEnum.Update:
                    options = _taskList.Where(x => x.Id == SysTaskSchedulingEntity.Id).FirstOrDefault();
                    //移除以前的配置
                    if (options != null)
                    {
                        _taskList.Remove(options);
                    }

                    //生成任务并添加新配置
                    schedulerFactory.AddJob(SysTaskSchedulingEntity, true).GetAwaiter().GetResult();
                    break;
                case JobOperateEnum.Parse:
                case JobOperateEnum.Start:
                case JobOperateEnum.ExecuteImmediately:
                    options = _taskList.Where(x => x.Id == SysTaskSchedulingEntity.Id).FirstOrDefault();
                    if (action == JobOperateEnum.Parse)
                    {
                        options.Status = APIStatusEnum.Parse;
                    }
                    else
                    {
                        options.Status = APIStatusEnum.Normal;
                    }
                    break;
            }
        }

        /// <summary>
        /// 触发新增、删除、修改、暂停、启用、立即执行事件
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="action"></param>
        /// <param name="sysTaskSchedulingEntity"></param>
        /// <returns></returns>
        public static async Task<string> TriggerAction(this ISchedulerFactory schedulerFactory, JobOperateEnum action, SysTaskSchedulingEntity sysTaskSchedulingEntity)
        {
            try
            {
                var group = sysTaskSchedulingEntity.Tenant + sysTaskSchedulingEntity.Id;
                IScheduler scheduler = await schedulerFactory.GetScheduler();
                List<JobKey>? jobKeys = scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(group)).Result.ToList();
                if (jobKeys == null || jobKeys.Count == 0)
                {
                    return $"未找到分组[{group}]";
                }
                JobKey? jobKey = jobKeys.Where(s => scheduler.GetTriggersOfJob(s).Result.Any(x => (x as CronTriggerImpl).Group == group)).FirstOrDefault();
                if (jobKey == null)
                {
                    return $"未找到触发器[{group}]";
                }
                var triggers = await scheduler.GetTriggersOfJob(jobKey);
                ITrigger? trigger = triggers?.Where(x => (x as CronTriggerImpl).Group == group).FirstOrDefault();

                if (trigger == null)
                {
                    return $"未找到触发器[{group}]";
                }
                switch (action)
                {
                    case JobOperateEnum.Delete:
                    case JobOperateEnum.Update:
                        await scheduler.PauseTrigger(trigger.Key);
                        await scheduler.UnscheduleJob(trigger.Key);// 移除触发器
                        await scheduler.DeleteJob(trigger.JobKey);
                        sysTaskSchedulingEntity.ModifyTaskEntity(schedulerFactory, action);
                        break;
                    case JobOperateEnum.Start:
                        await scheduler.PauseTrigger(trigger.Key);
                        await scheduler.UnscheduleJob(trigger.Key);// 移除触发器
                        await scheduler.DeleteJob(trigger.JobKey);
                        sysTaskSchedulingEntity.ModifyTaskEntity(schedulerFactory, action);
                        await schedulerFactory.AddJob(sysTaskSchedulingEntity);
                        break;
                    case JobOperateEnum.Parse:
                        sysTaskSchedulingEntity.ModifyTaskEntity(schedulerFactory, action);
                        if (action == JobOperateEnum.Parse)
                            await scheduler.PauseTrigger(trigger.Key);
                        else if (action == JobOperateEnum.Start)
                            await scheduler.ResumeTrigger(trigger.Key);
                        else
                            await scheduler.Shutdown();
                        break;
                    case JobOperateEnum.ExecuteImmediately:
                        await scheduler.TriggerJob(jobKey);
                        break;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }

        /// <summary>
        /// 通过作业上下文获取作业对应的配置参数
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static SysTaskSchedulingEntity? GetTaskOptions(this IJobExecutionContext context)
        {
            if ((context as JobExecutionContextImpl).Trigger is not AbstractTrigger trigger) throw new ArgumentNullException("trigger为null");
            SysTaskSchedulingEntity? taskOptions = _taskList.Where(x => x.TaskName == trigger.Name && x.Tenant + x.Id == trigger.Group).FirstOrDefault();
            return taskOptions ?? _taskList.Where(x => x.TaskName == trigger.JobName && x.Tenant + x.Id == trigger.JobGroup).FirstOrDefault();
        }

        /// <summary>
        /// 验证表达式
        /// </summary>
        /// <param name="cronExpression"></param>
        /// <returns></returns>
        public static (bool, string) IsValidExpression(this string cronExpression)
        {
            try
            {
                CronTriggerImpl trigger = new()
                {
                    CronExpressionString = cronExpression
                };
                DateTimeOffset? date = trigger.ComputeFirstFireTimeUtc(null);
                return (date != null, date == null ? $"请确认表达式{cronExpression}是否正确!" : "");
            }
            catch (Exception e)
            {
                return (false, $"请确认表达式{cronExpression}是否正确!{e.Message}");
            }
        }
    }
}
