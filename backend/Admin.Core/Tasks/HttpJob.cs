﻿using FreeSql;
using Mapster;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Admin.Repository.Tenant;
using Admin.Core.HttpHelper;
using Quartz;
using System.Diagnostics;
using Admin.Common.Ioc;
using Admin.Common.Json;

namespace Admin.Core.Tasks
{
    /// <summary>
    /// 定义Http执行任务
    /// </summary>
    public class HttpJob : IJob
    {
        /// <summary>
        /// 执行任务
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Execute(IJobExecutionContext context)
        {
            DateTime dateTime = DateTime.Now;
            SysTaskSchedulingEntity? taskEntity = context.GetTaskOptions();
            if (taskEntity == null) return;
            string message = string.Empty;
            var status = SuccessStateEnum.Success;
            Stopwatch sw = new Stopwatch();
            Dictionary<string, string>? headers = null;
            sw.Start();
            try
            {
                if (string.IsNullOrEmpty(taskEntity.Content)) return;
                if (!string.IsNullOrWhiteSpace(taskEntity.RequestHeader))
                    headers = JsonHelper.DeserializeDictionary<string, string>(taskEntity.RequestHeader);

                if (taskEntity.Type == APITaskType.System)
                {
                    if (headers == null) headers = new Dictionary<string, string>();
                    headers.Add("Tenant", taskEntity.Tenant);
                }

                message = taskEntity.RequestType switch
                {
                    APIRequestTypeEnum.Get => await IocManager.Instance.GetService<IHttpService>().GetAsync(taskEntity.Content, headers: headers),
                    APIRequestTypeEnum.Post => await IocManager.Instance.GetService<IHttpService>().PostAsync(taskEntity.Content, taskEntity.RequestBody, contentType: "application/json", encoding: "UTF-8", headers: headers),
                    _ => await IocManager.Instance.GetService<IHttpService>().PostAsync(taskEntity.Content, taskEntity.RequestBody, contentType: "application/json", encoding: "UTF-8", headers: headers),
                };
            }
            catch (Exception ex)
            {
                message = ex.Message;
                status = SuccessStateEnum.Fail;
            }
            sw.Stop();

            try
            {
                var freesql = IocManager.Instance.GetService<FreeSqlCloud<string>>();
                freesql.Change(taskEntity.Tenant);
                TenantManager.Current = taskEntity.Tenant;
                await freesql.Update<SysTaskSchedulingEntity>().Set(a => a.LastRunTime == DateTime.Now).Where(a => a.Id == taskEntity.Id).ExecuteAffrowsAsync();
                var log = taskEntity.Adapt<SysTaskLogEntity>();
                log.Id = Guid.NewGuid().ToString("N");
                log.TaskId = taskEntity.Id;
                log.RequestTime = dateTime;
                log.Status = status;
                log.Response = message;
                log.RequestHeader = JsonHelper.SerializeObject(headers);
                log.ExecuteTime = sw.ElapsedMilliseconds.ToString();
                await freesql.Insert<SysTaskLogEntity>(log).ExecuteAffrowsAsync();
            }
            catch
            {

            }
        }
    }
}
