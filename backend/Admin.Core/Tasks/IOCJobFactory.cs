﻿using Quartz;
using Quartz.Spi;

namespace Admin.Core.Tasks
{
    /// <summary>
    /// IOCJobFactory
    /// </summary>
    /// <param name="serviceProvider"></param>
    public class IOCJobFactory(IServiceProvider serviceProvider) : IJobFactory
    {
        /// <summary>
        /// NewJob
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return serviceProvider.GetService(bundle.JobDetail.JobType) as IJob;
        }

        /// <summary>
        /// ReturnJob
        /// </summary>
        /// <param name="job"></param>
        public void ReturnJob(IJob job)
        {
            (job as IDisposable)?.Dispose();
        }
    }
}
