﻿using FreeSql;
using Rougamo.Context;
using System.Data;

namespace Admin.Core.Transactional
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class TransactionalAttribute : Rougamo.MoAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public Propagation Propagation { get; set; } = Propagation.Required;

        /// <summary>
        /// 
        /// </summary>
        public IsolationLevel IsolationLevel { get => m_IsolationLevel.Value; set => m_IsolationLevel = value; }

        /// <summary>
        /// 
        /// </summary>
        IsolationLevel? m_IsolationLevel;

        /// <summary>
        /// 
        /// </summary>
        static AsyncLocal<IServiceProvider> m_ServiceProvider = new AsyncLocal<IServiceProvider>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        public static void SetServiceProvider(IServiceProvider serviceProvider) => m_ServiceProvider.Value = serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        IUnitOfWork? _uow;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnEntry(MethodContext context)
        {
            var uowManager = m_ServiceProvider?.Value?.GetService(typeof(UnitOfWorkManager)) as UnitOfWorkManager;
            _uow = uowManager?.Begin(Propagation, m_IsolationLevel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnExit(MethodContext context)
        {
            if (typeof(Task).IsAssignableFrom(context.RealReturnType))
            {
                if (context.ReturnValue != null) ((Task)context.ReturnValue).ContinueWith(t => _OnExit());
                return;
            }

            _OnExit();

            void _OnExit()
            {
                if (_uow != null)
                {
                    try
                    {
                        if (context.Exception == null) _uow.Commit();
                        else _uow.Rollback();
                    }
                    catch { }
                    finally
                    {
                        _uow.Dispose();
                    }
                }
            }
        }
    }
}
