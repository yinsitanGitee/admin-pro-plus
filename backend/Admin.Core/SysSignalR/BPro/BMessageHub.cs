﻿using Microsoft.AspNetCore.SignalR;
using Admin.Core.Auth;

namespace Admin.Core.SysSignalR.BPro
{
    /// <summary>
    /// B端socket中心（发送通知给用户客户端）
    /// </summary>
    public class BMessageHub : Hub
    {
        /// <summary>
        /// 重写连接事件 添加连接用户
        /// </summary>
        /// <returns></returns>
        public override Task OnConnectedAsync()
        {
            var context = Context.User;
            var user = BHubState.Users.SingleOrDefault(x => x.ConnectionID == Context?.ConnectionId);

            if (user == null)
            {
                var newUser = new BHubUser
                {
                    ConnectionID = Context?.ConnectionId ?? string.Empty,
                    Tenant = context?.FindFirst(TokenAuthConfig.Tenant)?.Value ?? "",
                    UserId = context?.FindFirst(TokenAuthConfig.UserId)?.Value ?? "",
                    Name = context?.FindFirst(TokenAuthConfig.UserName)?.Value ?? "",
                };
                BHubState.Users.Add(newUser);
            }
            return base.OnConnectedAsync();
        }

        /// <summary>
        /// 重写断开事件 移除断开用户
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public override Task OnDisconnectedAsync(Exception? exception)
        {
            var user = BHubState.Users.SingleOrDefault(x => x.ConnectionID == Context.ConnectionId);
            if (user != null)
                BHubState.Users.Remove(user);
            return base.OnDisconnectedAsync(exception);
        }
    }
}