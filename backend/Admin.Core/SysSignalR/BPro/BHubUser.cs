﻿using System.ComponentModel.DataAnnotations;

namespace Admin.Core.SysSignalR.BPro
{
    /// <summary>
    /// 存储Hub的状态
    /// </summary>
    public static class BHubState
    {
        /// <summary>
        /// 创建用户集合，用于存储所有链接的用户数据
        /// </summary>
        public static List<BHubUser> Users = [];
    }

    /// <summary>
    /// 连接到中心的用户
    /// </summary>
    public class BHubUser
    {
        /// <summary>
        /// 连接ID
        /// </summary>
        [Key]
        public string ConnectionID { get; set; } = string.Empty;

        /// <summary>
        /// 客户id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 租户
        /// </summary>
        public string Tenant { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
    }
}
