﻿using Admin.Common.Json;
using Admin.Common.Log;
using System.Net;
using System.Text;

namespace Admin.Core.HttpHelper
{
    /// <summary>
    /// HttpService
    /// </summary>
    public class HttpService : IHttpService
    {
        /// <summary>
        /// _httpClientFactory
        /// </summary>
        public IHttpClientFactory _httpClientFactory;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public HttpService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        /// <summary>
        /// 发起POST异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="contentType">application/xml、application/json、application/text、application/x-www-form-urlencoded</param>
        /// <param name="timeOut">填充消息头</param>        
        /// <param name="encoding">填充消息头</param>        
        /// <param name="headers">填充消息头</param>        
        /// <returns></returns>
        public async Task<string> PostAsync(string url, string? postData = null, string? contentType = null, int timeOut = 100, string encoding = "UTF-8", Dictionary<string, string>? headers = null)
        {
            using var client = _httpClientFactory.CreateClient(url);

            postData ??= "";
            client.Timeout = new TimeSpan(0, 0, timeOut);
            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            using HttpContent httpContent = new StringContent(postData, Encoding.GetEncoding(encoding));
            if (contentType != null)
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(contentType);

            using HttpResponseMessage response = await client.PostAsync(url, httpContent);
            var r = await response.Content.ReadAsStringAsync();
            $"url:{url},postData:{postData},response:{r}".Loger("HttpPostResponse");
            return r;
        }

        /// <summary>
        /// 发起POST异步请求
        /// </summary>
        /// <typeparam name="T?"></typeparam>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="contentType"></param>
        /// <param name="timeOut"></param>
        /// <param name="encoding"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public async Task<T?> PostAsync<T>(string url, string? postData = null, string? contentType = null, int timeOut = 100, string encoding = "UTF-8", Dictionary<string, string>? headers = null)
        {
            using var client = _httpClientFactory.CreateClient(url);

            postData ??= "";
            client.Timeout = new TimeSpan(0, 0, timeOut);
            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            using HttpContent httpContent = new StringContent(postData, Encoding.GetEncoding(encoding));
            if (contentType != null)
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(contentType);

            using HttpResponseMessage response = await client.PostAsync(url, httpContent);
            var r = await response.Content.ReadAsStringAsync();
            $"url:{url},postData:{postData},response:{r}".Loger("HttpPostResponse");
            return JsonHelper.DeserializeObject<T>(r);
        }

        /// <summary>
        /// 发起GET异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="contentType"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        public async Task<string> GetAsync(string url, string? contentType = null, Dictionary<string, string>? headers = null, int timeOut = 100)
        {
            using var client = _httpClientFactory.CreateClient(url);

            if (contentType != null)
                client.DefaultRequestHeaders.Add("ContentType", contentType);

            client.Timeout = new TimeSpan(0, 0, timeOut);

            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            using HttpResponseMessage response = await client.GetAsync(url);
            var r = await response.Content.ReadAsStringAsync();
            $"url:{url},response:{r}".Loger("HttpGetResponse");
            return r;
        }

        /// <summary>
        /// 发起GET异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="contentType"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        public async Task<T?> GetAsync<T>(string url, string? contentType = null, Dictionary<string, string>? headers = null, int timeOut = 100)
        {
            using var client = _httpClientFactory.CreateClient(url);

            if (contentType != null)
                client.DefaultRequestHeaders.Add("ContentType", contentType);

            client.Timeout = new TimeSpan(0, 0, timeOut);

            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            using HttpResponseMessage response = await client.GetAsync(url);
            var r = await response.Content.ReadAsStringAsync();
            $"url:{url},response:{r}".Loger("HttpGetResponse");
            return JsonHelper.DeserializeObject<T>(r);
        }
    }
}
