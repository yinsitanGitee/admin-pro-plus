﻿namespace Admin.Core.HttpHelper
{
    /// <summary>
    /// StatusCode
    /// </summary>
    public class HttpStatus
    {
        /// <summary>
        /// 未鉴权
        /// 登录鉴权失败
        /// </summary>
        public const int Unauthorized = 401;

        /// <summary>
        /// 未授权
        /// 权限验证失败
        /// </summary>
        public const int Forbidden = 403;

        /// <summary>
        /// 500错误
        /// </summary>
        public const int InternalServerError = 500;
    }
}
