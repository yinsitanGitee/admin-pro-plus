﻿namespace Admin.Core.HttpHelper
{
    /// <summary>
    /// IHttpService
    /// </summary>
    public interface IHttpService
    {
        /// <summary>
        /// 发起POST异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="contentType">application/xml、application/json、application/text、application/x-www-form-urlencoded</param>
        /// <param name="timeOut">填充消息头</param>        
        /// <param name="encoding">填充消息头</param>        
        /// <param name="headers">填充消息头</param>        
        /// <returns></returns>
        Task<string> PostAsync(string url, string? postData = null, string? contentType = null, int timeOut = 100, string encoding = "UTF-8", Dictionary<string, string>? headers = null);

        /// <summary>
        /// 发起POST异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="contentType">application/xml、application/json、application/text、application/x-www-form-urlencoded</param>
        /// <param name="timeOut">填充消息头</param>        
        /// <param name="encoding">填充消息头</param>        
        /// <param name="headers">填充消息头</param>        
        /// <returns></returns>
        Task<T?> PostAsync<T>(string url, string? postData = null, string? contentType = null, int timeOut = 100, string encoding = "UTF-8", Dictionary<string, string>? headers = null);

        /// <summary>
        /// 发起GET异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="contentType"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        Task<string> GetAsync(string url, string? contentType = null, Dictionary<string, string>? headers = null, int timeOut = 100);

        /// <summary>
        /// 发起GET异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="contentType"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        Task<T?> GetAsync<T>(string url, string? contentType = null, Dictionary<string, string>? headers = null, int timeOut = 100);
    }
}
