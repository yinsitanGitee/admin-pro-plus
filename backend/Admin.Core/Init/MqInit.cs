﻿using Admin.Core.Service.Queue;
using Admin.Mq;
using Admin.Mq.AppettingConfig;
using Microsoft.Extensions.DependencyInjection;

namespace Admin.Core.Init
{
    /// <summary>
    /// mq注入
    /// </summary>
    public static class MqInit
    {
        /// <summary>
        /// 生产者初始化
        /// </summary>
        /// <param name="services"></param>
        /// <param name="mqConfigDto"></param>
        public static void MqProducerIn(this IServiceCollection services, MqConfigDto mqConfigDto)
        {
            string virtualHost = "/";
            var arguments = new Dictionary<string, object>() { { "x-queue-type", "classic" } };

            services.AddRabbitProducer("SimplePattern", options =>
            {
                options.Hosts = mqConfigDto.Hosts;
                options.Password = mqConfigDto.Password;
                options.Port = mqConfigDto.Port;
                options.UserName = mqConfigDto.UserName;
                options.VirtualHost = virtualHost;
                options.Arguments = arguments;
                options.Durable = true;
                options.AutoDelete = true;

                options.InitializeCount = 3;
            });

            services.AddTransient<IQueueService, QueueService>();
        }
    }
}
