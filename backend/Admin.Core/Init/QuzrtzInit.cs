﻿using FreeSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Admin.Repository.Dto;
using Admin.Repository.Entities.Sys;
using Admin.Core.Tasks;
using Quartz;
using Quartz.Spi;

namespace Admin.Core.Init
{
    /// <summary>
    /// 定时任务初始化
    /// </summary>
    public static class QuzrtzInit
    {
        /// <summary>
        /// 初始化作业
        /// </summary>
        /// <param name="applicationBuilder"></param>
        /// <param name="fssql"></param>
        /// <param name="ormConfigDtos"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseQuartz(this IApplicationBuilder applicationBuilder, FreeSqlCloud<string> fssql, List<OrmConfigDto> ormConfigDtos)
        {
            IServiceProvider services = applicationBuilder.ApplicationServices;
            ISchedulerFactory? schedulerFactory = services.GetService<ISchedulerFactory>();
            if (schedulerFactory == null) throw new Exception("ISchedulerFactory注入异常");
            foreach (var item in ormConfigDtos)
            {
                fssql.Change(item.TenantKey);
                var entities = fssql.Select<SysTaskSchedulingEntity>().ToList();

                if (entities.Any())
                {
                    entities.ForEach(async x =>
                    {
                        if (x != null) await schedulerFactory.AddJob(x, true, jobFactory: services.GetService<IJobFactory>());
                    });
                }
            }

            return applicationBuilder;
        }
    }
}
