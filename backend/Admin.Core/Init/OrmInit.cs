﻿using FreeSql;
using FreeSql.Aop;
using Microsoft.Extensions.DependencyInjection;
using Admin.Repository;
using Admin.Repository.Dto;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Tenant;
using Admin.Core.Auth;
using System.Reflection;
using Admin.Common.Json;
using Admin.Common.SysStatic;
using Admin.Repository.Enum;
using Autofac;

namespace Admin.Core.Init
{
    /// <summary>
    /// Orm注入
    /// </summary>
    public static class OrmInit
    {
        /// <summary>
        /// 执行过CodeFirst和初始化插入的租户
        /// </summary>
        private static readonly List<string> InitTen = [];

        /// <summary>
        /// Orm注入
        /// </summary>
        /// <param name="services"></param>
        /// <param name="ormConfigDto"></param>
        public static void OrmIn(this IServiceCollection services, List<OrmConfigDto> ormConfigDto, FreeSqlCloud<string> fsql)
        {
            var currentUser = services.BuildServiceProvider().GetService<ICurrentUser>();

            List<Type>? tableAssembies = null;

            if (ormConfigDto.Any(a => a.SyncStructureSql))
                tableAssembies = Assembly.Load(SystemConfig.SystemName + ".Repository").GetExportedTypes().Where(type => type.Namespace.StartsWith(SystemConfig.SystemName + ".Repository.Entities") && type.IsClass).ToList();

            foreach (var item in ormConfigDto)
            {
                fsql.Register(item.TenantKey, () =>
                {
                    var fq = new FreeSqlBuilder().UseConnectionString(item.DbType, item.ConnectionString);
                    if (item.SlaveConnectionStrings != null && item.SlaveConnectionStrings.Length > 0)
                        fq.UseSlave(item.SlaveConnectionStrings);

                    var fsSql = fq.Build();

                    if (InitTen.IndexOf(item.TenantKey) == -1)
                    {
                        InitTen.Add(item.TenantKey);

                        if (item.SyncStructureSql && tableAssembies != null) fsSql.CodeFirst.SyncStructure([.. tableAssembies]);

                        SyncSysAccount(fsql, item.TenantKey);
                    }

                    // 软删除全局过滤器
                    fsSql.GlobalFilter.ApplyOnly<IIsDelete>(FilterSetting.IsDelete, a => !a.IsDelete);
                    // 租户全局过滤器
                    fsSql.GlobalFilter.ApplyOnlyIf<ITenant>(FilterSetting.Tenant, () => !string.IsNullOrWhiteSpace(TenantManager.Current), a => a.Tenant == TenantManager.Current);
                    // 数据权限过滤器
                    fsSql.GlobalFilter.ApplyOnlyIf<IDataAuth>(FilterSetting.DataAuth,
                        () =>
                        {
                            if (currentUser != null && currentUser.DataAuth != EnumDataAuth.All && currentUser.OrganizeId.Length != 0)
                                return true;
                            return false;
                        },
                        a => currentUser.DataAuth != EnumDataAuth.OwnUser ? currentUser.CustDataAuth.Contains(a.OrganizeId) : a.CreateUserId == currentUser.UserId
                    );

                    fsSql.Aop.AuditValue += (s, e) =>
                    {
                        switch (e.AuditValueType)
                        {
                            case AuditValueType.Insert:
                                switch (e.Property.Name)
                                {
                                    case "Id":
                                        e.Value ??= Guid.NewGuid().ToString("N");
                                        break;

                                    case "CreateUserId":
                                        e.Value = currentUser?.UserId;
                                        break;

                                    case "CreateUserName":
                                        e.Value = currentUser?.UserName;
                                        break;

                                    case "ModifyUserId":
                                        e.Value = currentUser?.UserId;
                                        break;

                                    case "ModifyUserName":
                                        e.Value = currentUser?.UserName;
                                        break;

                                    case "Tenant":
                                        e.Value ??= TenantManager.Current;
                                        break;      
                                    
                                    case "OrganizeId":
                                        e.Value ??= currentUser?.OrganizeId;
                                        break;
                                }
                                break;
                            case AuditValueType.Update:
                                switch (e.Property.Name)
                                {
                                    case "ModifyUserId":
                                        e.Value = currentUser?.UserId;
                                        break;

                                    case "ModifyUserName":
                                        e.Value = currentUser?.UserName;
                                        break;
                                }
                                break;
                        }
                    };

                    return fsSql;
                });
            }

            services.AddSingleton<IFreeSql>(fsql);
            services.AddSingleton(fsql);
            services.AddScoped<UnitOfWorkManager>();
        }

        /// <summary>
        /// 同步用户
        /// </summary>
        /// <param name="fsql"></param>
        /// <param name="tenant"></param>
        private static void SyncSysAccount(FreeSqlCloud<string> fsql, string tenant)
        {
            if (!fsql.Select<SysAccountEntity>().Any())
            {
                var t = JsonHelper.GetFileJson<List<SysAccountEntity>>(AppContext.BaseDirectory + "InitData/SysAccountEntity.json");
                if (t != null && t.Count > 0)
                {
                    t.ForEach(a => a.Tenant = tenant);
                    fsql.InsertOrUpdate<SysAccountEntity>().SetSource(t).ExecuteAffrows();
                }
            }
        }
    }
}
