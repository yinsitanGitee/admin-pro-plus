﻿using Admin.Core.Service.Flow;
using Autofac;
using System.Reflection;
using Module = Autofac.Module;

namespace Admin.Core.Init
{
    /// <summary>
    /// 服务IOC注入
    /// </summary>
    public class ServiceStartup : Module
    {
        private readonly string _assembly;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="assembly">assembly</param>
        public ServiceStartup(string assembly)
        {
            _assembly = assembly;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            Assembly application = Assembly.Load(_assembly);

            builder.RegisterAssemblyTypes(application)
                    .Where(a => a.Name.EndsWith("Service"))
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope();

            // 流程服务注入
            foreach (var item in application.ExportedTypes.Where(a => a.Name.EndsWith("Flow")))
                builder.RegisterType(item).Named<IFlowEvent>(item.Name).InstancePerLifetimeScope();
        }
    }
}