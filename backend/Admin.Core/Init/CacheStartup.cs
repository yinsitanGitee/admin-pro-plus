﻿using Admin.Cache.Setting;
using Autofac;
using Module = Autofac.Module;

namespace Admin.Core.Init
{
    /// <summary>
    /// Redis IOC注入
    /// </summary>
    public class CacheStartup : Module
    {
        private readonly RedisConfigDto _redisConfig;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="redisConfig">redisConfig</param>
        public CacheStartup(RedisConfigDto redisConfig)
        {
            _redisConfig = redisConfig;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            RedisHelper.Initialization(new CSRedis.CSRedisClient(_redisConfig.ConnectionString));
        }
    }
}
