﻿using Autofac;
using Admin.Repository;
using System.Reflection;
using Module = Autofac.Module;

namespace Admin.Core.Init
{
    /// <summary>
    /// 仓储ioc注入
    /// </summary>
    public class RepositoryStartup : Module
    {
        private readonly string _assembly;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="assembly">assembly</param>
        public RepositoryStartup(string assembly)
        {
            _assembly = assembly;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<,>)).As(typeof(IRepository<,>)).InstancePerLifetimeScope().PropertiesAutowired();

            builder.RegisterAssemblyTypes(Assembly.Load(_assembly))
                    .Where(a => a.Name.EndsWith("Repository"))
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope();
        }
    }
}
