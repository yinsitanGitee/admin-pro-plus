﻿using System.Security.Claims;

namespace Admin.Core.Auth
{
    /// <summary>
    /// 定义token接口
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// 生成token
        /// </summary>
        /// <param name="id">加密参数</param>
        /// <returns></returns>
        string GenerateToken(string id);

        /// <summary>
        /// 解密token
        /// </summary>
        /// <param name="token">加密参数</param>
        /// <returns></returns>
        Task<(bool isSuccess, ClaimsPrincipal? clAdmin)> DecryptToken(string token);
    }
}
