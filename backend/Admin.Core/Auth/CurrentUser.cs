﻿using Admin.Repository.Enum;
using Microsoft.AspNetCore.Http;

namespace Admin.Core.Auth
{
    /// <summary>
    /// 定义当前用户
    /// </summary>
    public class CurrentUser : ICurrentUser
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CurrentUser(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 用户id
        /// </summary>
        public string UserId
        {
            get
            {
                return _httpContextAccessor.HttpContext?.User?.FindFirst(TokenAuthConfig.UserId)?.Value ?? "";
            }
        }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName
        {
            get
            {
                return _httpContextAccessor.HttpContext?.User?.FindFirst(TokenAuthConfig.UserName)?.Value ?? "";
            }
        }

        /// <summary>
        /// 用户机构id
        /// </summary>
        public string OrganizeId
        {
            get
            {
                return _httpContextAccessor.HttpContext?.User?.FindFirst(TokenAuthConfig.OrganizeId)?.Value ?? "";
            }
        }

        /// <summary>
        /// 用户租户
        /// </summary>
        public string Tenant
        {
            get
            {
                return _httpContextAccessor.HttpContext?.User?.FindFirst(TokenAuthConfig.Tenant)?.Value ?? "";
            }
        }

        /// <summary>
        /// 数据授权范围
        /// </summary>
        public EnumDataAuth DataAuth
        {
            get
            {
                var v = _httpContextAccessor.HttpContext?.User?.FindFirst(TokenAuthConfig.DataAuth)?.Value;
                if (v == null) return EnumDataAuth.OwnUser;
                if (Enum.TryParse(v, out EnumDataAuth type))
                    return type;
                else return EnumDataAuth.OwnUser;
            }
        }

        /// <summary>
        /// 自定义权限
        /// </summary>
        public List<string> CustDataAuth
        {
            get
            {
                var v = _httpContextAccessor.HttpContext?.User?.FindFirst(TokenAuthConfig.CustDataAuth)?.Value;
                if (v == null) return [];
                return DataAuth switch
                {
                    EnumDataAuth.OwnOrganize => [OrganizeId],
                    EnumDataAuth.CustOrganize => [.. v.Split(",")],
                    _ => [],
                };
            }
        }
    }
}
