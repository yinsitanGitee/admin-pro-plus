﻿using Admin.Common.Ioc;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Tenant;
using Admin.Common.Security;
using Admin.Core.Service.Cache;
using System.Security.Claims;

namespace Admin.Core.Auth
{
    /// <summary>
    /// 定义token服务
    /// </summary>
    public class TokenService : BaseService, ITokenService
    {
        #region 生成token

        /// <summary>
        /// 生成token
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public string GenerateToken(string id)
        {
            return $"admin|{id}".AESEncrypt();
        }

        #endregion

        #region 解析token

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<(bool isSuccess, ClaimsPrincipal? clAdmin)> DecryptToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token)) return (false, default);

            var (isSuccess, text) = token.AESDecrypt();
            if (!isSuccess || string.IsNullOrWhiteSpace(text)) return (false, default);

            var sText = text.Split('|');

            var calms = await GetSysAccountClAdmin(sText);
            if (calms == null) return (false, null);
            return (true, new ClaimsPrincipal(new ClaimsIdentity(calms, TokenAuthConfig.DefaultTokenAuthenticationType)));
        }

        #region 登录鉴权

        /// <summary>
        /// GetSysAccountClAdmin
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private async Task<List<Claim>?> GetSysAccountClAdmin(string[] input)
        {
            var cache = await IocManager.Instance.GetService<ICacheService>().GetUser(input[1]);
            if (cache != null)
            {
                if (cache.Tenant != TenantManager.Current) // 租户对应不上，直接401
                    return null;

                return GetSysAccountClaim(cache);
            }

            return null;
        }

        /// <summary>
        /// GetSysAccountClaim
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private List<Claim> GetSysAccountClaim(SysAccountEntity dto)
        {
            return [
                 new(TokenAuthConfig.UserName, dto.Name ?? ""),
                new Claim(TokenAuthConfig.UserId, dto.Id),
                new Claim(TokenAuthConfig.OrganizeId, dto.OrganizeId ?? ""),
                new Claim(TokenAuthConfig.Tenant, dto.Tenant ?? ""),
                new Claim(TokenAuthConfig.DataAuth, dto.DataAuth.ToString() ?? ""),
                new Claim(TokenAuthConfig.CustDataAuth, dto.CustDataAuth ?? ""),
            ];
        }

        #endregion

        #endregion
    }
}