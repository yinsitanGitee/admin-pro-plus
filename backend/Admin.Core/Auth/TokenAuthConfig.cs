﻿namespace Admin.Core.Auth
{
    /// <summary>
    /// 定义token配置
    /// </summary>
    public class TokenAuthConfig
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public const string UserId = "UserId";

        /// <summary>
        /// 用户名
        /// </summary>
        public const string UserName = "UserName";

        /// <summary>
        /// 机构id
        /// </summary>
        public const string OrganizeId = "OrganizeId";

        /// <summary>
        /// 租户
        /// </summary>
        public const string Tenant = "Tenant";

        /// <summary>
        /// 数据权限类型
        /// </summary>
        public const string DataAuth = "DataAuth";


        /// <summary>
        /// 自定义数据权限
        /// </summary>
        public const string CustDataAuth = "CustDataAuth";

        /// <summary>
        /// 
        /// </summary>
        public const string DefaultTokenAuthenticationType = "local";

        /// <summary>
        /// 
        /// </summary>
        public const string TokenAuthentication = "Admin.Token.Auth";
    }
}
