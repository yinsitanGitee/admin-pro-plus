﻿using Admin.Repository.Enum;

namespace Admin.Core.Auth
{
    /// <summary>
    /// 定义当前用户
    /// </summary>
    public interface ICurrentUser
    {
        /// <summary>
        /// 主键
        /// </summary>
        string UserId { get; }

        /// <summary>
        /// 用户名称
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// 机构id
        /// </summary>
        string OrganizeId { get; }

        /// <summary>
        /// 租户
        /// </summary>
        string Tenant { get; }

        /// <summary>
        /// 数据授权范围
        /// </summary>
        public EnumDataAuth DataAuth { get; }

        /// <summary>
        /// 自定义权限
        /// </summary>
        List<string> CustDataAuth { get; }
    }
}
