﻿using MapsterMapper;
using Admin.Common.Ioc;

namespace Admin.Core
{
    /// <summary>
    /// 定义服务基层
    /// </summary>
    public class BaseService
    {
        #region IMapper

        /// <summary>
        /// _MapperObject
        /// </summary>
        private IMapper? _MapperObject;

        /// <summary>
        /// MapperObject
        /// </summary>
        protected IMapper MapperObject { get => GetMapper(); }

        /// <summary>
        /// GetMapper
        /// </summary>
        /// <returns></returns>
        private IMapper GetMapper()
        {
            _MapperObject ??= IocManager.Instance.GetService<IMapper>();
            return _MapperObject;
        }

        #endregion

        /// <summary>
        /// 委托try
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public async Task<string?> TryFun(Func<Task> action)
        {
            try
            {
                await action();
                return null;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
