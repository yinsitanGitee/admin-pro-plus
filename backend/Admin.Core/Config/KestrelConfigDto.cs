﻿namespace Admin.Core.Config
{
    /// <summary>
    /// KestrelConfigDto
    /// </summary>
    public class KestrelConfigDto
    {
        /// <summary>
        /// Http
        /// </summary>
        public KestrelEndpointsConfigDto Endpoints { get; set; }
    }

    /// <summary>
    /// KestrelConfigDto
    /// </summary>
    public class KestrelEndpointsConfigDto
    {
        /// <summary>
        /// Http
        /// </summary>
        public KestrelHttpConfigDto Http { get; set; }
    }

    /// <summary>
    /// KestrelHttpConfigDto
    /// </summary>
    public class KestrelHttpConfigDto
    {
        /// <summary>
        /// Url
        /// </summary>
        public string Url { get; set; }
    }
}
