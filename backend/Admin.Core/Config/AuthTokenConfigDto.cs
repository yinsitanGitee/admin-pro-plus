﻿using System.ComponentModel.DataAnnotations;

namespace Admin.Core.Config
{
    /// <summary>
    /// AuthTokenConfigDto
    /// </summary>
    public class AuthTokenConfigDto
    {
        /// <summary>
        /// Token有效时间
        /// </summary>
        public int ExpiresIn { get; set; }
    }
}
