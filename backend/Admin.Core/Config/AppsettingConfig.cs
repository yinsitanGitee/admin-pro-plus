﻿using Admin.Repository.Dto;
using Admin.Cache.Setting;
using Admin.Mq.AppettingConfig;

namespace Admin.Core.Config
{
    /// <summary>
    /// AppsettingConfig
    /// </summary>
    public class AppsettingConfig
    {
        /// <summary>
        /// 系统地址
        /// </summary>
        public string SystemUrl { get; set; }

        /// <summary>
        /// Kestrel配置
        /// </summary>
        public KestrelConfigDto Kestrel { get; set; }

        /// <summary>
        /// 登录token配置
        /// </summary>
        public AuthTokenConfigDto AuthTokenConfig { get; set; }

        /// <summary>
        /// 文件上传配置
        /// </summary>
        public FileUploadConfigDto FileUploadConfig { get; set; }

        /// <summary>
        /// reids配置
        /// </summary>
        public RedisConfigDto RedisConfig { get; set; }

        /// <summary>
        /// 跨域配置
        /// </summary>
        public CorsConfigDto CorsConfig { get; set; }

        /// <summary>
        /// 数据库配置
        /// </summary>
        public List<OrmConfigDto> OrmConfig { get; set; }

        /// <summary>
        /// mq配置
        /// </summary>
        public MqConfigDto MqConfig { get; set; }
    }
}
