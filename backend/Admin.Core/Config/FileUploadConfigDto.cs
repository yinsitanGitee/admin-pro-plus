﻿namespace Admin.Core.Config
{
    /// <summary>
    /// 文件上传配置信息
    /// </summary>
    public class FileUploadConfigDto
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
    }
}
