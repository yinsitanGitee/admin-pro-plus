﻿namespace Admin.Core.Config
{
    /// <summary>
    /// 跨域配置信息
    /// </summary>
    public class CorsConfigDto
    {
        /// <summary>
        /// Ip地址
        /// </summary>
        public string[] Hosts { get; set; }
    }
}
