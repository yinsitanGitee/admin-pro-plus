﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Admin.Core.Auth;
using System.Net;
using System.Security.Claims;

namespace Admin.Core.Filter
{
    /// <summary>
    /// 定义Token认证器
    /// </summary>
    public class TokenAuthenticationFilter : IAuthenticationHandler
    {
        /// <summary>
        /// token服务
        /// </summary>
        private readonly ITokenService _tokenService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tokenService"></param>
        public TokenAuthenticationFilter(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        /// <summary>
        /// CurrentContext
        /// </summary>
        public HttpContext CurrentContext { get; private set; }

        /// <summary>
        /// Scheme
        /// </summary>
        public AuthenticationScheme Scheme { get; private set; }

        /// <summary>
        /// 自定义token验证器
        /// </summary>
        /// <returns></returns>
        public async Task<AuthenticateResult> AuthenticateAsync()
        {
            if (CurrentContext.Request.Method != "OPTIONS")
            {
                var (isValid, clams) = await VerifyToken(CurrentContext.Request.Headers["Authorization"].ToString());

                if (!isValid || clams == null) return AuthenticateResult.Fail("未登录或授权已过期。");

                AuthenticationTicket ticket = new(clams, Scheme.Name);
                return AuthenticateResult.Success(ticket);
            }
            else return AuthenticateResult.NoResult();
        }

        /// <summary>
        /// ChallengeAsync
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        public Task ChallengeAsync(AuthenticationProperties properties)
        {
            CurrentContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return Task.CompletedTask;
        }

        /// <summary>
        /// ForbidAsync
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        public Task ForbidAsync(AuthenticationProperties properties)
        {
            CurrentContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return Task.CompletedTask;
        }

        /// <summary>
        /// InitializeAsync
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task InitializeAsync(AuthenticationScheme scheme, HttpContext context)
        {
            Scheme = scheme;
            CurrentContext = context;
            return Task.CompletedTask;
        }

        #region Token有效性验证

        /// <summary>
        /// Token有效性验证
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task<(bool isSuccess, ClaimsPrincipal? clAdmin)> VerifyToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token)) return (false, null);
            return await _tokenService.DecryptToken(token);
        }

        #endregion
    }
}
