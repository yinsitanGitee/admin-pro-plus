﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel;
using System.Reflection;
using Admin.Common.SysStatic;

namespace Admin.Core.Filter
{
    /// <summary>
    /// SwaggerParaFilter
    /// </summary>
    public class SwaggerParaFilter : IOperationFilter
    {
        /// <summary>
        /// Apply
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.Parameters = new List<OpenApiParameter>
                 {
                       new OpenApiParameter
                       {
                           Name = "Authorization",
                           In = ParameterLocation.Header,
                           Description = "访问令牌",
                           Required = false,
                           Schema = new OpenApiSchema
                           {
                               Type = "string"
                           }
                       },
                       new OpenApiParameter
                       {
                           Name = "Tenant",
                           In = ParameterLocation.Header,
                           Description = "租户",
                           Required = false,
                           Schema = new OpenApiSchema
                           {
                               Type = "string"
                           }
                       }
                 };
        }
    }

    /// <summary>
    /// swagger文档生成过滤器，用于枚举描述的生成
    /// </summary>
    public class SwaggerEnumFilter : IDocumentFilter
    {
        /// <summary>
        /// 实现IDocumentFilter接口的Apply函数
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            Dictionary<string, Type> dict = GetAllEnum();

            foreach (var item in swaggerDoc.Components.Schemas)
            {
                var property = item.Value;
                var typeName = item.Key;
                Type? itemType;
                if (property.Enum != null && property.Enum.Count > 0)
                {
                    if (dict.TryGetValue(typeName, out Type? value))
                        itemType = value;
                    else
                        itemType = null;

                    List<OpenApiInteger> list = [];
                    foreach (var val in property.Enum)
                    {
                        list.Add((OpenApiInteger)val);
                    }
                    if (itemType != null)
                        property.Description += DescribeEnum(itemType, list);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, Type> GetAllEnum()
        {
            Assembly ass1 = Assembly.Load(SystemConfig.SystemName + ".Common");
            Type[] types1 = ass1.GetTypes();
            Dictionary<string, Type> dict = [];

            foreach (Type item in types1)
            {
                if (item.IsEnum)
                {
                    dict.Add(item.Name, item);
                }
            }

            Assembly ass2 = Assembly.Load(SystemConfig.SystemName + ".Repository");
            Type[] types2 = ass2.GetTypes();

            foreach (Type item in types2)
            {
                if (item.IsEnum)
                {
                    dict.Add(item.Name, item);
                }
            }
            return dict;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="enums"></param>
        /// <returns></returns>
        private static string DescribeEnum(Type type, List<OpenApiInteger> enums)
        {
            var enumDescriptions = new List<string>();
            foreach (var item in enums)
            {
                if (type == null) continue;
                var value = Enum.Parse(type, item.Value.ToString());
                var desc = GetDescription(type, value);

                if (string.IsNullOrEmpty(desc))
                    enumDescriptions.Add($"{item.Value}：{Enum.GetName(type, value)}；");
                else
                    enumDescriptions.Add($"{item.Value}：{Enum.GetName(type, value)}，{desc}；");
            }
            return $"<div>{string.Join("", enumDescriptions)}</div>";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetDescription(Type t, object value)
        {
            foreach (MemberInfo mInfo in t.GetMembers())
            {
                if (mInfo.Name == t.GetEnumName(value))
                {
                    foreach (Attribute attr in Attribute.GetCustomAttributes(mInfo))
                    {
                        if (attr.GetType() == typeof(DescriptionAttribute))
                        {
                            return ((DescriptionAttribute)attr).Description;
                        }
                    }
                }
            }
            return string.Empty;
        }
    }
}
