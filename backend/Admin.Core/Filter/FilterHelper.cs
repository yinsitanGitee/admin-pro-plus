﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Admin.Core.Filter
{
    /// <summary>
    /// 拦截帮助类
    /// </summary>
    public static class FilterHelper
    {
        /// <summary>
        /// 判断类和方法头上的特性是否要进行Action拦截
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsActionDescriptor(ActionExecutingContext context, Type type)
        {
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (controllerActionDescriptor == null) return false;
            return controllerActionDescriptor.ControllerTypeInfo.GetCustomAttributes(type, false).FirstOrDefault() != null
                || controllerActionDescriptor.MethodInfo.GetCustomAttributes(type, false).FirstOrDefault() != null ? true : false;
        }

        /// <summary>
        /// 获取Ip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetIP(HttpRequest request)
        {
            try
            {
                string? ip = request.Headers["X-Real-IP"].FirstOrDefault();
                if (string.IsNullOrWhiteSpace(ip)) ip = request.Headers["X-Forwarded-For"].FirstOrDefault();
                if (string.IsNullOrWhiteSpace(ip)) ip = request.HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var a = ip?.Split(":");
                if (a == null) return string.Empty;
                return a[^1];
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
