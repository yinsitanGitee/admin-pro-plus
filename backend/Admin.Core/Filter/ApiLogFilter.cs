﻿using Admin.Common.Ioc;
using Admin.Common.Json;
using Admin.Core.Auth;
using Admin.Core.Service.Queue;
using Admin.Mq;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Admin.Repository.Tenant;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using System.Text;

namespace Admin.Core.Filter
{
    /// <summary>
    /// api日志拦截器
    /// </summary>
    public class ApiLogFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 方法执行
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (FilterHelper.IsActionDescriptor(context, typeof(NoApiLogAttribute)))
                await next();
            else
            {
                Stopwatch sw = new();
                sw.Start();
                var response = await next();
                sw.Stop();
                StringBuilder ex = new();
                string? result = null;
                if (response.Exception != null)
                {
                    Exception exception = response.Exception;
                    ex.AppendLine(exception.Message);
                    while (exception.InnerException != null)
                    {
                        ex.AppendLine(exception.InnerException.Message);
                        exception = exception.InnerException;
                    }
                    ex.AppendLine(response.Exception.StackTrace);
                    result = "调用失败: " + ex.ToString();
                }
                var user = IocManager.Instance.GetService<ICurrentUser>();

                await IocManager.Instance.GetService<IQueueService>().AddSimpleQueueAsync(SysMqQueueSetting.ApiLogQueue,
                    JsonHelper.SerializeObject(new SysApiLogEntity
                    {
                        Tenant = TenantManager.Current,
                        IP = FilterHelper.GetIP(context.HttpContext.Request),
                        Api = (context.HttpContext.Request.Path != null ? context.HttpContext.Request.Path.Value : "") ?? "无路径",
                        Request = context.HttpContext.Request.Method.ToUpper() switch
                        {
                            "GET" => context.HttpContext.Request.QueryString.Value,
                            "POST" => JsonHelper.SerializeObject(context?.ActionArguments),
                            _ => context.HttpContext.Request.QueryString.Value,
                        },
                        ExecuteTime = sw.ElapsedMilliseconds.ToString(),
                        RequestTime = DateTime.Now,
                        Response = JsonHelper.SerializeObject(response.Result),
                        RequestUserId = user.UserId,
                        Error = result,
                        Status = result == null ? SuccessStateEnum.Success : SuccessStateEnum.Fail
                    }));
            }
        }
    }

    /// <summary>
    /// 拦截不需要记录日志，[NoApiLog]
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true)]
    public class NoApiLogAttribute : Attribute
    {

    }
}
