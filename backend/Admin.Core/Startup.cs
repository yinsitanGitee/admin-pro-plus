﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using FreeSql;
using IGeekFan.AspNetCore.Knife4jUI;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Admin.Core.Auth;
using Admin.Core.Config;
using Admin.Core.Filter;
using Admin.Core.HttpHelper;
using Admin.Core.Init;
using Admin.Core.Service.AutoCode;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Flow;
using Admin.Core.Service.MsgTask;
using Admin.Core.Tasks;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System.Reflection;
using Admin.Common.Ioc;
using Admin.Common.Json;
using Admin.Common.Files;
using Admin.Common.SysStatic;
using Admin.Common.SysEnum;
using Admin.Core.Service.Reminder;

namespace Admin.Core
{
    /// <summary>
    /// 系统初始化
    /// </summary>
    public static class Startup
    {
        /// <summary>
        /// 定义初始化dto
        /// </summary>
        public class InitDto
        {
            /// <summary>
            /// Freesql
            /// </summary>
            public FreeSqlCloud<string> FreeSql { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public static AppsettingConfig appSettingConfig;

        /// <summary>
        /// Load
        /// </summary>
        public static AppsettingConfig ConfigureServices(this WebApplicationBuilder builder, InitDto dto)
        {
            SystemConfig.SystemName = "Admin";

            var services = builder.Services;

            builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

            appSettingConfig = JsonHelper.GetFileJson<AppsettingConfig>(AppContext.BaseDirectory + "appsettings.json") ?? throw new Exception("请检查appsetting.json配置");
            if (appSettingConfig.Kestrel == null) throw new Exception("请检查appsetting.json/Kestrel配置");
            if (appSettingConfig.RedisConfig == null) throw new Exception("请检查appsetting.json/RedisConfig配置");
            if (appSettingConfig.AuthTokenConfig == null) throw new Exception("请检查appsetting.json/AuthTokenConfig配置");
            if (appSettingConfig.CorsConfig == null) throw new Exception("请检查appsetting.json/CorsConfig配置");
            if (appSettingConfig.MqConfig == null) throw new Exception("请检查appsetting.json/MqConfig配置");
            if (appSettingConfig.OrmConfig == null || appSettingConfig.OrmConfig.Count == 0) throw new Exception("请检查appsetting.json/OrmConfig配置");

            SystemConfig.SystemKestrelHttpUrl = appSettingConfig.SystemUrl;
            if (appSettingConfig.FileUploadConfig == null) appSettingConfig.FileUploadConfig = new FileUploadConfigDto { Path = Directory.GetCurrentDirectory() };
            else if (string.IsNullOrWhiteSpace(appSettingConfig.FileUploadConfig.Path)) appSettingConfig.FileUploadConfig.Path = Directory.GetCurrentDirectory();
            builder.Services.AddSingleton<ICacheService, CacheService>();
            builder.Services.AddSingleton<IMsgTaskService, MsgTaskService>();
            builder.Services.AddSingleton<IReminderService, ReminderService>();
            builder.Services.AddSingleton<IFlowService, FlowService>();
            builder.Services.AddSingleton<IAutoCodeService, AutoCodeService>();
            builder.Services.AddSingleton<IProcService, ProcService>();

            #region Mapster

            var config = new TypeAdapterConfig();
            config.Scan(Assembly.Load(SystemConfig.SystemName + ".Application"));
            var mapper = new Mapper(config);
            builder.Services.AddSingleton<IMapper>(mapper);

            #endregion

            #region 配置Autofac容器

            builder.Host.ConfigureContainer<ContainerBuilder>(builder =>
            {
                builder.RegisterModule(new ServiceStartup(SystemConfig.SystemName + ".Application"));
                builder.RegisterModule(new RepositoryStartup(SystemConfig.SystemName + ".Repository"));
                builder.RegisterModule(new CacheStartup(appSettingConfig.RedisConfig));
            });

            #endregion

            #region Client

            builder.Services.AddTransient<IHttpService, HttpService>();

            builder.Services.AddHttpClient();

            builder.Services.AddHttpContextAccessor();

            #endregion

            services.AddAuthentication(options =>
            {
                options.AddScheme<TokenAuthenticationFilter>(TokenAuthConfig.TokenAuthentication, "Token");
                options.DefaultAuthenticateScheme = TokenAuthConfig.TokenAuthentication;
                options.DefaultChallengeScheme = TokenAuthConfig.TokenAuthentication;
            });

            builder.Services.AddScoped<ICurrentUser, CurrentUser>();
            builder.Services.AddTransient<ITokenService, TokenService>();
            builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            if (SystemConfig.SystemPlatformType == PlatformEnum.BServe)
            {
                #region swagger

                builder.Services.AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo { Title = SystemConfig.SystemName + ".API", Version = "v1" });
                    var basePath = AppContext.BaseDirectory;
                    DirectoryInfo d = new(basePath);
                    FileInfo[] files = d.GetFiles("*.xml");
                    var xmls = files.Select(a => Path.Combine(basePath, a.FullName)).ToList();

                    foreach (var item in xmls)
                    {
                        options.IncludeXmlComments(item, true);
                    }

                    options.OperationFilter<SwaggerParaFilter>();
                    options.DocumentFilter<SwaggerEnumFilter>();
                });

                #endregion

                #region 控制器-拦截器

                builder.Services.AddCors(options =>
                {
                    options.AddPolicy(name: "Policy",
                            builder =>
                            {
                                builder
                                .WithOrigins(appSettingConfig.CorsConfig.Hosts)
                                .AllowCredentials()
                                .AllowAnyMethod()
                                .AllowAnyHeader();
                            });
                });

                builder.Services.AddControllers(options =>
                {
                    #region 拦截器

                    options.Filters.Add<ExceptionFilter>();
                    options.Filters.Add<ApiLogFilter>();

                    #endregion

                    #region 非null校验

                    options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;

                    #endregion

                }).AddNewtonsoftJson();

                #endregion

                #region Quzrtz

                services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
                services.AddSingleton<IJobFactory, IOCJobFactory>();
                services.AddTransient<HttpJob>();

                #endregion

                #region Mq

                builder.Services.MqProducerIn(appSettingConfig.MqConfig);

                #endregion
            }

            #region Orm注入

            builder.Services.OrmIn(appSettingConfig.OrmConfig, dto.FreeSql);

            #endregion

            return appSettingConfig;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="dto"></param>
        public static void Configure(this IApplicationBuilder app, InitDto dto)
        {
            if (SystemConfig.SystemPlatformType == PlatformEnum.BServe)
            {
                app.UseSwagger();

                app.UseQuartz(dto.FreeSql, appSettingConfig.OrmConfig);

                app.UseKnife4UI(c =>
                {
                    c.RoutePrefix = string.Empty;
                    c.SwaggerEndpoint($"/swagger/v1/swagger.json", "h.swagger.webapi v1");
                });

                FileHelper.CreatePath(Directory.GetCurrentDirectory() + "/Upload");

                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Upload")),
                    RequestPath = "/upload"
                }); ;

                app.UseCors("Policy");
            }

            IocManager.Instance.Container = app.UseHostFiltering().ApplicationServices.GetAutofacRoot();
        }
    }
}
