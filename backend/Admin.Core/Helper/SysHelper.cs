﻿using Admin.Common.Security;
using System.Text.RegularExpressions;

namespace Admin.Core.Helper
{
    /// <summary>
    /// SysHelper
    /// </summary>
    public class SysHelper
    {
        /// <summary>
        /// 验证密码策略
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static string? ExistPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password)) return null;
            if (!Regex.IsMatch(password, "^(?![A-Za-z]+$)(?![A-Z\\d]+$)(?![A-Z\\W]+$)(?![a-z\\d]+$)(?![a-z\\W]+$)(?![\\d\\W]+$)\\S{8,}")) return "密码必须包含小写字母、大写字母和数字，并且长度不能少于8位数";
            return null;
        }

        /// <summary>
        /// 获取账户密码
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string GetPassword(string password)
        {
            return password.AESEncrypt();
        }
    }
}
