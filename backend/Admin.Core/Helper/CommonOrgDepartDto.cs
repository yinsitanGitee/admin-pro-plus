﻿namespace Admin.Core.Helper
{
    /// <summary>
    /// 定义系统通用机构部门树形返回数据格式
    /// </summary>
    public class CommonOrgDepartDto
    {

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }


        /// <summary>
        /// 1 机构 2 部门
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public List<CommonOrgDepartDto>? Children { get; set; }
    }
}
