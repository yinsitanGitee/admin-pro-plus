﻿namespace Admin.Core.Helper
{
    /// <summary>
    /// 树节点信息
    /// </summary> 
    public class TreeNode
    {
        /// <summary>
        /// 节点唯一标识
        /// 不允许为null
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Attr1 { get; set; }
        /// <summary>
        /// 节点名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 节点类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 节点编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 节点图标
        /// 预留属性,图标样式请使用 IconSkin
        /// </summary>
        [Obsolete("预留属性,图标样式请使用 IconSkin")]
        public string Icon { get; set; }

        /// <summary>
        /// 节点图标样式
        /// 如果使用系统图标样式 如:fa fa-sitemap  请使用此属性
        /// </summary>
        public string IconSkin { get; set; }
        /// <summary>
        /// 节点地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 是否展开
        /// </summary>
        public bool Open { get; set; } = true;

        /// <summary>
        /// 子节点集合
        /// </summary>
        public List<TreeNode>? Children { get; set; }

        /// <summary>
        /// 递归获取机构
        /// </summary>
        /// <param name="list"></param>
        /// <param name="proIds"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static List<TreeNode>? ForeachListDto(List<TreeNode>? list, List<string>? proIds,string pid)
        {
            if (list != null && list.Count > 0)
            {
                var a = list.FindAll(p => p.ParentId == pid);
                if (a.Count > 0)
                {
                    List<TreeNode> ds = new();
                    foreach (var item in a)
                    {
                        item.Children = ForeachListDto(list, proIds, item.Id);
                        ds.Add(item);
                        proIds?.Add(item.Id);
                    }
                    return ds;
                }
            }

            return null;
        }
    }
}