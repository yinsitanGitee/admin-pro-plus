﻿namespace Admin.Core.Helper
{
    /// <summary>
    /// 定义系统通用下拉框返回数据格式
    /// </summary>
    public class CommonSelectDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Label { get; set; }
    }
}
