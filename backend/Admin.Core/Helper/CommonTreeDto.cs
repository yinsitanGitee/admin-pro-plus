﻿namespace Admin.Core.Helper
{
    /// <summary>
    /// 定义系统通用树形返回数据格式
    /// </summary>
    public class CommonTreeDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 拓展属性1
        /// </summary>
        public string Attr1 { get; set; }

        /// <summary>
        /// 拓展属性2
        /// </summary>
        public string Attr2 { get; set; }

        /// <summary>
        /// 拓展属性3
        /// </summary>
        public string Attr3 { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public List<CommonTreeDto>? Children { get; set; }
    }
}
