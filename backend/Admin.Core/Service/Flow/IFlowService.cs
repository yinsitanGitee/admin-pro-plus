﻿using Admin.Core.Service.Flow.Dto;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFlowService
    {
        /// <summary>
        /// 保存流程
        /// 会判断当前流程是否已自动通过
        /// 如果自动通过, 则返回 Status = Skip
        /// 如果没有,则流程正常执行, 返回 Status = Common
        /// </summary>
        /// <param name="app"></param>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        Task<FlowCommonResult<string>> SubmitFlow(FlowAppAPIVM app, FlowInfoAPIVM flowInfo);
    }
}
