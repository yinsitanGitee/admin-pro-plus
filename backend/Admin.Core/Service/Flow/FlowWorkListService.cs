﻿using Admin.Common.Ioc;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Flow.Dto;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Mapster;
using System.Data;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    public static class FlowWorkListService
    {
        /// <summary>
        /// 获取下级节点执行人
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetCurrentProcIdByUserId(IFreeSql freeSql, string flowId, string accId)
        {
            var cc = freeSql.Select<WorkListEntity>().Where(a => a.ProcId == freeSql.Select<ProcEntity>().Where(b => b.FlowId == flowId && b.ProcStatus == EnumProcStatus.Handing).ToOne(a => a.Id) && a.IsNeed && !a.IsHandled && a.AccountId == accId).ToSql();
            var procId = await freeSql.Select<WorkListEntity>().Where(a => a.ProcId == freeSql.Select<ProcEntity>().Where(b => b.FlowId == flowId && b.ProcStatus == EnumProcStatus.Handing).ToOne(a => a.Id) && a.IsNeed && !a.IsHandled && a.AccountId == accId).ToOneAsync(a => a.ProcId);
            if (string.IsNullOrWhiteSpace(procId)) throw new Exception("您没有权限，当前节点可能已经审批，请刷新后重试");
            return procId;
        }

        /// <summary>
        /// 验证用户是否有审批权限
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flowId"></param>
        /// <param name="accId"></param>
        /// <returns></returns>
        public static async Task<bool> VerificationCurrentProc(IFreeSql freeSql, string flowId, string accId)
        {
            return await freeSql.Select<WorkListEntity>().Where(a => a.ProcId == freeSql.Select<ProcEntity>().Where(b => b.FlowId == flowId && b.ProcStatus == EnumProcStatus.Handing).ToOne(a => a.Id) && a.IsNeed && !a.IsHandled && a.AccountId == accId).AnyAsync();
        }

        /// <summary>
        /// 获取下级节点执行人
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ApprovalUser>> GetNextProcExecutors(IFreeSql freeSql, string flowId)
        {
            return await freeSql.Select<WorkListEntity, ProcEntity>().InnerJoin(a => a.t1.ProcId == a.t2.Id).Where(a => a.t2.FlowId == flowId && a.t2.Type == EnumProcType.Normal
            && a.t2.ProcStatus == EnumProcStatus.Handing).ToListAsync(a => new ApprovalUser { ProcName = a.t2.Name });
        }

        /// <summary>
        /// 构建工作列表
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <param name="executors">该流程所有的执行人</param>
        /// <param name="allExecutors"></param>
        /// <param name="opeateRecordAPIVMs"></param>
        /// <param name="isSelectExecutor"></param>
        /// <returns></returns>
        public static async Task<bool> AddWorkerIntoList(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc, List<ExecutorEntity> executors, List<string> allExecutors, List<OpeateRecordAPIVM> opeateRecordAPIVMs, bool isSelectExecutor)
        {
            if (executors != null && executors.Count > 0)
            {
                var li = await CreateWorkerList(freeSql, para, proc, executors);
                if (li.Count != 0)
                {
                    if (proc.IsSelectExecutor && isSelectExecutor && para.NextProcExecutor != null && para.NextProcExecutor.Count > 0)
                    {
                        li = li.Where(a => para.NextProcExecutor.Contains(a.AccountId)).ToList();
                    }

                    if (proc.IsSelectNextProc)
                    {
                        await freeSql.Insert(li).ExecuteAffrowsAsync();
                        return false;
                    }
                    else
                    {
                        if (proc.ProcApprovalType == EnumProcApprovalType.AllApproval && li.Count > 1)
                        {
                            var currentExecutors = li.Where(a => allExecutors.Contains(a.AccountId)).ToList();

                            foreach (var currentExecutor in currentExecutors)
                            {
                                #region 下级节点执行人已经审批过该流程,节点被系统自动通过

                                currentExecutor.IsNeed = true;
                                currentExecutor.ApprovalStatus = EnumProcApprovalStatus.SystemSkip;
                                currentExecutor.IsHandled = true;
                                currentExecutor.CheckOpinion = "系统自动通过";
                                currentExecutor.ComplateTime = DateTime.Now;
                                currentExecutor.StartTime = DateTime.Now;

                                #endregion
                            }

                            await freeSql.Insert(li).ExecuteAffrowsAsync();
                            return currentExecutors.Count == li.Count;
                        }
                        else
                        {
                            var currentExecutor = li.Where(a => allExecutors.Contains(a.AccountId)).FirstOrDefault();
                            if (currentExecutor != null)
                            {
                                #region 下级节点执行人已经审批过该流程,节点被系统自动通过

                                currentExecutor.IsNeed = true;
                                currentExecutor.ApprovalStatus = EnumProcApprovalStatus.SystemSkip;
                                currentExecutor.IsHandled = true;
                                currentExecutor.CheckOpinion = "系统自动通过";
                                currentExecutor.ComplateTime = DateTime.Now;
                                currentExecutor.StartTime = DateTime.Now;

                                await freeSql.Insert(currentExecutor).ExecuteAffrowsAsync();


                                opeateRecordAPIVMs.Add(new OpeateRecordAPIVM
                                {
                                    Content = "系统自动通过",
                                    OperateUserId = currentExecutor.AccountId,
                                    OperateUserName = currentExecutor.AccountName,
                                    DataId = para.Flow.AppId,
                                    OperateDate = DateTime.Now,
                                    Description = "系统自动通过",
                                    ProcId = proc.Id,
                                    State = EnumOperateState.Skip,
                                    Title = proc.Name,
                                    Type = "审批"
                                });

                                return true;

                                #endregion
                            }
                            else
                            {
                                #region 正常处理流程,添加下级节点执行人信息

                                await freeSql.Insert(li).ExecuteAffrowsAsync();
                                return false;

                                #endregion
                            }
                        }
                    }
                }
                else
                {
                    #region 未计算出下级节点执行人,系统将会自动通过

                    await freeSql.Insert(new WorkListEntity
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        AccountId = "gssystem",
                        ExecutorId = "gssystem",
                        ApprovalStatus = EnumProcApprovalStatus.SystemSkip,
                        CheckOpinion = "系统自动通过",
                        ActionCode = para.Flow.ActionCode,
                        AppId = para.Flow.AppId,
                        ComplateTime = DateTime.Now,
                        ExecutorRefType = EnumExecutorRefType.Account,
                        FlowId = para.Flow.Id,
                        IsHandled = true,
                        IsNeed = true,
                        ProcId = proc.Id,
                        StartTime = DateTime.Now
                    }).ExecuteAffrowsAsync();

                    opeateRecordAPIVMs.Add(new OpeateRecordAPIVM
                    {
                        Content = "系统自动通过",
                        OperateUserId = "gssystem",
                        OperateUserName = "系统管理员",
                        DataId = para.Flow.AppId,
                        OperateDate = DateTime.Now,
                        Description = "系统自动通过",
                        ProcId = proc.Id,
                        State = EnumOperateState.Skip,
                        Title = proc.Name,
                        Type = "审批"
                    });

                    return true;
                    #endregion
                }
            }
            else
            {
                #region 未计算出下级节点执行人,系统将会自动通过

                await freeSql.Insert(new WorkListEntity
                {
                    Id = Guid.NewGuid().ToString("N"),
                    AccountId = "gssystem",
                    ExecutorId = "gssystem",
                    ApprovalStatus = EnumProcApprovalStatus.SystemSkip,
                    CheckOpinion = "系统自动通过",
                    ActionCode = para.Flow.ActionCode,
                    AppId = para.Flow.AppId,
                    ComplateTime = DateTime.Now,
                    ExecutorRefType = EnumExecutorRefType.Account,
                    FlowId = para.Flow.Id,
                    IsHandled = true,
                    IsNeed = true,
                    ProcId = proc.Id,
                    StartTime = DateTime.Now
                }).ExecuteAffrowsAsync();

                opeateRecordAPIVMs.Add(new OpeateRecordAPIVM
                {
                    Content = "系统自动通过",
                    OperateUserId = "gssystem",
                    OperateUserName = "系统管理员",
                    OperateDate = DateTime.Now,
                    DataId = para.Flow.AppId,
                    Description = "系统自动通过",
                    ProcId = proc.Id,
                    State = EnumOperateState.Skip,
                    Title = proc.Name,
                    Type = "审批"
                });

                return true;

                #endregion
            }
        }


        #region 计算执行人

        /// <summary>
        /// 构建工作列表
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <param name="executors"></param>
        /// <returns></returns>
        public static async Task<List<WorkListEntity>> AddWorkerIntoList(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc, List<ExecutorEntity> executors)
        {
            var li = await CreateWorkerList(freeSql, para, proc, executors);
            if (li.Count != 0)
            {
                if (proc.IsSelectExecutor)
                {
                    if (para.NextProcExecutor != null && para.NextProcExecutor.Count > 0)
                        li = li.Where(a => para.NextProcExecutor.Contains(a.AccountId)).ToList();
                }

                await freeSql.Insert(li).ExecuteAffrowsAsync();
            }

            return li;
        }

        /// <summary>
        /// 根据账号信息获取工作列表
        /// </summary>
        /// <param name="para">必要参数</param>
        /// <param name="proc">环节实体</param>
        /// <param name="executor">执行人实体</param>
        /// <param name="wlPara">工作列表参数</param>
        /// <returns></returns>
        public static async Task<WorkListEntity?> CreateWLByAccount(ExecutProcPara para, ProcEntity proc, ExecutorEntity executor, WorkListHandParaVM wlPara)
        {
            try
            {
                var user = await IocManager.Instance.GetService<ICacheService>().GetUser(executor.RefId);

                var ww = new WorkListEntity()
                {
                    Id = Guid.NewGuid().ToString("N"),
                    ExecutorId = executor.Id,
                    ExecutorRefType = executor.RefType,
                    AccountId = executor.RefId,
                    ProcId = proc.Id,
                    ApprovalStatus = wlPara.ApprovalStatus,
                    CheckOpinion = string.Empty,
                    FlowId = para.Flow.Id,
                    IsHandled = wlPara.IsHanded,
                    IsNeed = wlPara.IsNeed,
                    StartTime = DateTime.Now,
                    ActionCode = para.Flow.ActionCode,
                    AppId = para.Flow.AppId,
                    AccountName = user.Name
                };
                if (proc.LimtTime.HasValue && proc.LimtTime != 0 && proc.BeginTime.HasValue)
                    ww.LimtTime = proc.BeginTime.Value.AddDays(proc.LimtTime.Value);
                return ww;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 根据角色信息获取工作列表
        /// </summary>
        /// <param name="freeSql">必要参数</param>
        /// <param name="para">必要参数</param>
        /// <param name="proc">环节实体</param>
        /// <param name="executor">执行人实体</param>
        /// <param name="wlPara">工作列表参数</param>
        /// <returns></returns>
        public static async Task<List<WorkListEntity>?> CreateWLByRole(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc, ExecutorEntity executor, WorkListHandParaVM wlPara)
        {
            try
            {
                var list = new List<WorkListEntity>();
                var us = await freeSql.Select<SysAccountRoleEntity, SysAccountEntity>().LeftJoin(a => a.t1.AccountId == a.t2.Id).Where(a => a.t1.RoleId == executor.RefId)
                     .ToListAsync(a => new
                     {
                         a.t2.Name,
                         a.t2.Id,
                     });

                foreach (var item in us)
                {
                    var ww = new WorkListEntity()
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        ExecutorId = executor.Id,
                        ExecutorRefType = executor.RefType,
                        AccountId = item.Id,
                        ProcId = proc.Id,
                        ApprovalStatus = wlPara.ApprovalStatus,
                        CheckOpinion = string.Empty,
                        FlowId = para.Flow.Id,
                        IsHandled = wlPara.IsHanded,
                        IsNeed = wlPara.IsNeed,
                        StartTime = DateTime.Now,
                        ActionCode = para.Flow.ActionCode,
                        AppId = para.Flow.AppId,
                        AccountName = item.Name
                    };
                    if (proc.LimtTime.HasValue && proc.LimtTime != 0 && proc.BeginTime.HasValue)
                        ww.LimtTime = proc.BeginTime.Value.AddDays(proc.LimtTime.Value);

                    list.Add(ww);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 获取工作列表
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <param name="executors"></param>
        /// <returns></returns>
        public static async Task<List<WorkListEntity>> CreateWorkerList(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc, List<ExecutorEntity> executors)
        {
            var li = new List<WorkListEntity>();
            var wlPara = new WorkListHandParaVM()
            {
                IsNeed = true,
                IsHanded = false,
                ApprovalStatus = EnumProcApprovalStatus.None
            };
            foreach (var item in executors)
            {
                var ss = new List<WorkListEntity>();
                switch (item.RefType)
                {
                    case EnumExecutorRefType.Account:
                        var u = await CreateWLByAccount(para, proc, item, wlPara);
                        if (u != null) li.Add(u);
                        break;
                    case EnumExecutorRefType.Role:
                        var r = await CreateWLByRole(freeSql, para, proc, item, wlPara);
                        if (r != null && r.Count > 0) li.AddRange(r);
                        break;
                }
            }
            var nli = new List<WorkListEntity>();
            //需要去重
            foreach (var item in li)
            {
                if (nli.Any(t => t.ProcId == item.ProcId && t.AccountId == item.AccountId)) continue;
                nli.Add(item);
            }
            return nli;
        }

        #endregion

        #region 计算流程审批人 下级节点使用，选择执行人使用

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="vm"></param>
        /// <param name="executors"></param>
        /// <returns></returns>
        public static async Task<List<ExcutorWorkListAPIVM>> CreateFlowProcWorkerList(IFreeSql freeSql, GetNextProcExecutorVM vm, List<ExecutorTempListVM> executors)
        {
            var ss = new List<ExcutorWorkListAPIVM>();

            foreach (var item in executors)
            {
                switch (item.RefType)
                {
                    case EnumExecutorRefType.Account:
                        var u = await CreateWorkListByAccount(item);
                        if (u != null) ss.Add(u);
                        break;

                    case EnumExecutorRefType.Role:
                        var r = await CreateWorkListByRole(freeSql, item);
                        if (r != null) ss.AddRange(r);
                        break;
                }
            }

            var nli = new List<ExcutorWorkListAPIVM>();
            //需要去重
            foreach (var item in ss)
            {
                if (nli.Any(t => t.Id == item.Id)) continue;
                nli.Add(item);
            }
            return nli;
        }


        /// <summary>
        /// 根据账号信息获取工作列表
        /// </summary>
        /// <param name="executor">执行人实体</param>
        /// <returns></returns>
        private static async Task<ExcutorWorkListAPIVM> CreateWorkListByAccount(ExecutorTempListVM executor)
        {
            try
            {
                var user = await IocManager.Instance.GetService<ICacheService>().GetUser(executor.RefId);
                return user.Adapt<ExcutorWorkListAPIVM>();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 根据账号信息获取工作列表
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="executor">执行人实体</param>
        /// <returns></returns>
        private static async Task<List<ExcutorWorkListAPIVM>> CreateWorkListByRole(IFreeSql freeSql, ExecutorTempListVM executor)
        {
            try
            {
                var list = new List<ExcutorWorkListAPIVM>();
                var us = await freeSql.Select<SysAccountRoleEntity, SysAccountEntity>().LeftJoin(a => a.t1.AccountId == a.t2.Id).Where(a => a.t1.RoleId == executor.RefId)
                     .ToListAsync(a => new
                     {
                         a.t2.Name,
                         a.t2.Id,
                         a.t2.Phone,
                     });

                foreach (var item in us)
                {
                    var ww = new ExcutorWorkListAPIVM()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Phone = item.Phone,
                    };

                    list.Add(ww);
                }

                return list;
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
