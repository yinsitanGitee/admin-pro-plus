﻿namespace Admin.Core.Service.Flow
{
    public enum EnumFieldType
    {
        //
        // 摘要:
        //     字符串类型
        String,
        //
        // 摘要:
        //     int类型
        Int32,
        //
        // 摘要:
        //     数字
        Decimal,
        //
        // 摘要:
        //     时间类型
        DateTime,
        //
        // 摘要:
        //     列表
        List
    }
}
