﻿using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    public static class CondService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flow"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public static async Task<bool> CheckCustLineCond(IFreeSql freeSql, FlowEntity flow, LineEntity line)
        {
            var cond = await freeSql.Select<CondEntity>().Where(t => t.FlowId == flow.Id && t.RefId == line.Id && t.RefType == EnumFlowEle.Line).ToOneAsync();
            if (cond == null) return false;

            return await FlowProviderService.TrialCalcula(freeSql, flow.ActionCode, cond.Expression, new Dto.FlowDataProviderDto()
            {
                AppCode = flow.AppCode,
                AppId = flow.AppId
            });
        }
    }
}
