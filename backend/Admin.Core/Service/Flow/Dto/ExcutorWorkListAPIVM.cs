﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 预测执行人信息
    /// </summary>
    public class ExcutorWorkListAPIVM
    {
        public string Name { get; set; }

        /// <summary>
        ///账号Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPic { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        public EnumProcApprovalStatus ApprovalStatus { get; set; }

        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime? ComplateTime { get; set; }

        #region 用于流程当前节点

        /// <summary>
        /// 审批意见
        /// </summary>
        public string CheckOpinion { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        public int State { get; set; }

        #endregion
    }
}
