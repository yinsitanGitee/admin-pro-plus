﻿using Admin.Repository.Entities.Wfs;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class FlowTempVM
    {
        /// <summary>
        /// 
        /// </summary>
        public FlowTempVM()
        {
            ProcTemps = [];
            ExecutorTemps = [];
            LineTemps = [];
            CondTemps = [];
        }

        /// <summary>
        /// 模板
        /// </summary>
        public FlowTempEntity Entity { get; set; }

        /// <summary>
        /// 环节
        /// </summary>
        public List<ProcTempEntity> ProcTemps { get; set; }
        /// <summary>
        /// 线
        /// </summary>
        public List<LineTempEntity> LineTemps { get; set; }
        /// <summary>
        /// 条件
        /// </summary>
        public List<CondTempEntity> CondTemps { get; set; }
        /// <summary>
        /// 执行人
        /// </summary>
        public List<ExecutorTempEntity> ExecutorTemps { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 业务名称
        /// </summary>
        public string ActionName { get; set; }
    }
}