﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    public class FlowTempAPIVM
    {
        /// <summary>
        ///主键
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        ///业务标识编号
        /// </summary>
        public string ActionCode { get; set; }
        /// <summary>
        ///编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        ///名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        ///表单编号
        /// </summary>
        public string FormCode { get; set; }
        /// <summary>
        ///所属机构
        /// </summary>
        public string OrgId { get; set; }
        /// <summary>
        ///是否可用
        /// </summary>
        public bool Enable { get; set; }

        /// <summary>
        ///公司标识
        /// </summary>
        public string CmpCode { get; set; }
        /// <summary>
        ///限定完成时间(天)
        /// </summary>
        public int LimtTime { get; set; }
        /// <summary>
        ///备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        ///是否发送所有消息给执行人
        /// </summary>
        public bool EnableSendMessageToAllExcuter { get; set; }

        /// <summary>
        ///归档后发消息类型
        /// </summary>
        public EnumSendMessageToExcuterType SendMessageToExcuterType { get; set; }

        /// <summary>
        ///归档后发消息指定人员
        /// </summary>
        public string SendMessageSelectUsers { get; set; }

        /// <summary>
        /// 是否自动通过
        /// </summary>
        public bool IsApprovalPass { get; set; }
    }
}
