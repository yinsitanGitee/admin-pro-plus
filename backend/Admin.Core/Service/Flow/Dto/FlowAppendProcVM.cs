﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class FlowOperateProcVM
    {
        /// <summary>
        /// 用户
        /// </summary>
        public FlowProcAccountVM Account { get; set; }

        /// <summary>
        /// 流程id
        /// </summary>
        public string FlowId { get; set; }
    }

    public class FlowProcAccountVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class FlowProcInformerVM
    {
        public List<string> InformerIds { get; set; }

        public List<string> InformerNames { get; set; }

        public string FlowId { get; set; }
    }
}
