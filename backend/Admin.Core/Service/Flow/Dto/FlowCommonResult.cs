﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 流程结果
    /// </summary>
    public class FlowCommonResult<T>
    {
        /// <summary>
        /// 流程状态
        /// </summary>
        public EnumFlowCommonResult Status { get; set; } = EnumFlowCommonResult.Common;

        /// <summary>
        /// 结果
        /// </summary>
        public T Result { get; set; }
    }
}
