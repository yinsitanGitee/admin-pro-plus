﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// FlowNextBaseProcsVM
    /// </summary>
    public class FlowNextBaseProcsVM
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string ProcName { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string ProcId { get; set; }

        /// <summary>
        /// 是否选择下级节点执行人
        /// </summary>
        public bool IsSelectNextExecutor { get; set; }
    }

    /// <summary>
    /// FlowNextProcsVM
    /// </summary>
    public class FlowNextProcsVM
    {
        /// <summary>
        /// Lists
        /// </summary>
        public List<FlowNextBaseProcsVM> Lists { get; set; }

        /// <summary>
        /// FlowTempId
        /// </summary>
        public string FlowTempId { get; set; }

        /// <summary>
        /// 机构Id
        /// </summary>
        public string OrgId { get; set; }
    }
}
