﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// FlowTempProcInfoVM
    /// </summary>
    public class FlowTempProcInfoVM : FlowTempProcBaseInfoVM
    {
        /// <summary>
        /// 模板数量
        /// </summary>
        public int Count { get; set; } = 0;

        /// <summary>
        /// 流程模板
        /// </summary>
        public List<FlowTempListVM>? Temps { get; set; }
    }

    /// <summary>
    /// FlowProcsVM
    /// </summary>
    public class FlowProcsVM
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OriginalSchemeProcId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SchemeProcId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public EnumProcStatus ProcStatus { get; set; }
    }

    /// <summary>
    /// FlowProcsInfoVM
    /// </summary>
    public class FlowProcsInfoVM
    {
        /// <summary>
        /// Procs
        /// </summary>
        public List<FlowProcsVM> Procs { get; set; }

        /// <summary>
        /// 当前节点
        /// </summary>
        public string CurrentProcName { get; set; }
    }


    /// <summary>
    /// FlowTempProcBaseInfoVM
    /// </summary>
    public class FlowTempProcBaseInfoVM
    {
        #region 节点基础属性

        /// <summary>
        /// 下一级节点
        /// </summary>
        public string NextProcId { get; set; }

        /// <summary>
        /// 下一个驳回模板节点
        /// </summary>
        public string RejectNextProcTempId { get; set; }

        /// <summary>
        /// 当前节点，提交即为 流程第一个节点
        /// </summary>
        public string CurrentProcSchemeId { get; set; }

        /// <summary>
        /// 流程模板Id
        /// </summary>
        public string FlowTempId { get; set; }

        #endregion

        #region 节点执行属性，会根据优先级顺序执行

        /// <summary>
        /// 是否选择下级节点(优先级第一)
        /// </summary>
        public bool IsSelectNextProc { get; set; }

        /// <summary>
        /// 是否选择下级节点执行人(优先级第二)
        /// </summary>
        public bool IsSelectNextExecutor { get; set; }

        #endregion
    }
}
