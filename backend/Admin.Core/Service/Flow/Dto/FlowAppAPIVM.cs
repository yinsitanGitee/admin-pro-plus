﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// FlowAppAPIVM
    /// </summary>
    public class FlowAppAPIVM
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 业务编号
        /// </summary>
        public string AppCode { get; set; }

        /// <summary>
        /// 单据名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 待办任务内容
        /// (数据需保持一行)
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 待办任务摘要
        /// (数据无限制)
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 待办来源标识
        /// </summary>
        public MsgTaskTypeEnum SourceTypeValue { get; set; }
    }
}
