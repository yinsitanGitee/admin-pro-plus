﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class FlowTaskContentVM
    {
        /// <summary>
        ///来源Id/单据Id
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        ///标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///摘要
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 机构Id
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        ///来源编号/待办跳转类型编号
        /// </summary>
        public string SourceTypeCode { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
