﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 定义选择人dto
    /// </summary>
    internal class SelectExcuterDto
    {
        /// <summary>
        /// 账号Id
        /// 对应员工账号Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 账号名称
        /// </summary>
        public string UserName { get; set; }
    }
}
