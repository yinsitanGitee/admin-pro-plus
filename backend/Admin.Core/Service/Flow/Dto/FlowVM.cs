﻿using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class FlowVM
    {
        /// <summary>
        /// 
        /// </summary>
        public FlowVM()
        {
            Procs = [];
            Executors = [];
            Lines = [];
            Conds = [];
        }

        /// <summary>
        /// 
        /// </summary>
        public FlowEntity Entity { get; set; }

        /// <summary>
        /// 环节
        /// </summary>
        public List<ProcEntity> Procs { get; set; }
        /// <summary>
        /// 线
        /// </summary>
        public List<LineEntity> Lines { get; set; }
        /// <summary>
        /// 条件
        /// </summary>
        public List<CondEntity> Conds { get; set; }
        /// <summary>
        /// 执行人
        /// </summary>
        public List<ExecutorEntity> Executors { get; set; }

        /// <summary>
        /// SpecialProcId
        /// </summary>
        public string RejectNextProcTempId { get; set; }

        /// <summary>
        /// 选择的下级节点
        /// </summary>
        public string NextProcTempId { get; set; }

        /// <summary>
        /// 下级节点执行人
        /// </summary>
        public List<string> NextProcExecutor { get; set; }
    }

    public class WorkListHandParaVM
    {
        /// <summary>
        /// 是否已经处理
        /// </summary>
        public bool IsHanded { get; set; }
        /// <summary>
        /// 是否需要处理
        /// </summary>
        public bool IsNeed { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public EnumProcApprovalStatus ApprovalStatus { get; set; }
    }
}