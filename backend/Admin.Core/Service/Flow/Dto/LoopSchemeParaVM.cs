﻿using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    public class LoopSchemeParaVM
    {
        /// <summary>
        /// 当前Id
        /// </summary>
        public string CurrentId { get; set; }

        /// <summary>
        /// 当前类型
        /// </summary>
        public EnumFlowEle CurrentType { get; set; }

        /// <summary>
        /// 业务编号
        /// </summary>
        public string AppCode { get; set; }

        /// <summary>
        /// 单据Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 流程主体Id
        /// </summary>
        public string FlowId { get; set; }

        /// <summary>
        /// 下级节点Id
        /// </summary>
        public string NextProcTempId { get; set; }
    }

    public class ExecutProcPara
    {
        /// <summary>
        /// 流程主体
        /// </summary>
        public FlowEntity Flow { get; set; }

        /// <summary>
        /// 当前环节处理状态
        /// </summary>
        public ProcHandStatus CurrentProcHandStatus { get; set; }

        /// <summary>
        /// 下一环节审批人
        /// </summary>
        public List<string> NextProcExecutor { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ProcHandStatus
    {
        /// <summary>
        /// 环节审批状态
        /// </summary>
        public EnumProcApprovalStatus ApprovalStatus { get; set; }
    }
}
