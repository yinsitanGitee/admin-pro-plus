﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class FlowDataProviderDto
    {
        /// <summary>
        /// 单据id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string AppCode { get; set; }
    }
}
