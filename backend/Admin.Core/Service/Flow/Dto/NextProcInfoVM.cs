﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// NextProcInfoVM
    /// </summary>
    public class NextProcInfoVM
    {
        #region 节点基础属性

        /// <summary>
        /// 当前需要处理节点
        /// </summary>
        public string CurrentProcId { get; set; }

        /// <summary>
        /// 当前需要处理模板节点
        /// </summary>
        public string CurrentProcSchemeId { get; set; }

        /// <summary>
        /// 下一节点
        /// </summary>
        public string NextProcId { get; set; }

        /// <summary>
        /// 流程模板Id
        /// </summary>
        public string FlowTempId { get; set; }

        #endregion


        #region 节点执行属性，会根据优先级顺序执行

        /// <summary>
        /// 是否选择下级节点(优先级第一)
        /// </summary>
        public bool IsSelectNextProc { get; set; }

        /// <summary>
        /// 是否选择下级节点执行人(优先级第二)
        /// </summary>
        public bool IsSelectNextExecutor { get; set; }

        #endregion

        #region 当IsSelectNextProc == false && ProcApprovalType != 2的时候才会对以下进行赋值,用户审批确认弹窗展示

        /// <summary>
        /// 下级节点名称
        /// </summary>
        public string NextProcName { get; set; }

        /// <summary>
        /// 下级节点执行人
        /// </summary>
        public string NextProcExecutors { get; set; }

        #endregion
    }

    /// <summary>
    /// GetFlowProcExecutorDto
    /// </summary>
    public class GetFlowProcExecutorDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string ProcId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FlowId { get; set; }
    }

    /// <summary>
    /// GetNextProcExecutorVM
    /// </summary>
    public class GetNextProcExecutorVM
    {
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        /// 机构Id
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 流程模板Id
        /// </summary>
        public string FlowTempId { get; set; }

        /// <summary>
        /// 节点模板Id
        /// </summary>
        public string ProcTempId { get; set; }

        /// <summary>
        /// 核算机构Id
        /// </summary>
        public string UnitId { get; set; }

        /// <summary>
        /// AppCode
        /// </summary>
        public string AppCode { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// FlowDataProvider
        /// </summary>
        public string FlowDataProvider { get; set; }


        /// <summary>
        /// 楼盘片区
        /// </summary>
        public string ProZoneId { get; set; }


        /// <summary>
        ///楼栋片区
        /// </summary>
        public string PavZoneId { get; set; }
    }
}
