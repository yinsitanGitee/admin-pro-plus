﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// WFS审批日志添加
    /// 单执行人
    /// </summary>
    public class OpeateRecordAPIVM
    {
        /// <summary>
        /// 当前用户Id
        /// </summary>
        public string OperateUserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime OperateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 当前用户名称
        /// </summary>
        public string OperateUserName { get; set; }

        /// <summary>
        /// 数据源Id
        /// </summary>
        public string DataId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public EnumOperateState State { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 日志标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 日志内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 日志备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///节点Id
        /// </summary>
        public string ProcId { get; set; }
    }

    /// <summary>
    /// WFS审批日志添加
    /// 多执行人
    /// </summary>
    public class OpeateRecordMultiExecutorAPIVM
    {
        /// <summary>
        /// 当前用户Id
        /// </summary>
        public string OperateUserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime OperateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 当前用户名称
        /// </summary>
        public string OperateUserName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 数据源Id
        /// </summary>
        public string DataId { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 日志标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///节点Id
        /// </summary>
        public string ProcId { get; set; }

        /// <summary>
        /// 审批记录执行人
        /// </summary>
        public List<OpeateRecordExecutorAPIVM> Executors { get; set; }
    }

    /// <summary>
    /// 审批记录执行人
    /// </summary>
    public class OpeateRecordExecutorAPIVM
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        public EnumOperateState State { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 日期(字符串格式) => (yyyy-MM-dd HH:mm:ss)
        /// </summary>
        public DateTime ComplateTime { get; set; }

        /// <summary>
        /// 审批意见
        /// </summary>
        public string CheckOpinion { get; set; }
    }
}
