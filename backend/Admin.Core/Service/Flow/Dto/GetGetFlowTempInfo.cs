﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class GetFlowTempInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string ActionCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrgId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetFlowApproveNextProcsDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string FlowId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FlowTempId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentProcSchemeId { get; set; }
    }
}
