﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// ExecutorTempListVM
    /// </summary>
    public class ExecutorTempListVM
    {
        /// <summary>
        ///关联人Id
        /// </summary>
        public string RefId { get; set; }

        /// <summary>
        ///关联类型
        /// </summary>
        public EnumExecutorRefType RefType { get; set; }

        /// <summary>
        /// 是否限定和提交人同机构
        /// </summary>
        public bool IsLimtOrgan { get; set; }

        /// <summary>
        /// 节点Id
        /// </summary>
        public string ProcId { get; set; }

        /// <summary>
        /// 节点Id
        /// </summary>
        public string Name { get; set; }
    }
}
