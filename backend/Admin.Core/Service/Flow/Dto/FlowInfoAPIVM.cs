﻿namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class FlowInfoAPIVM
    {
        /// <summary>
        /// 业务标识
        /// </summary>
        public string ActionCode { get; set; }

        /// <summary>
        /// 机构Id
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 核算机构Id
        /// </summary>
        public string UnitId { get; set; }

        /// <summary>
        /// 流程模板Id
        /// </summary>
        public string FlowTempId { get; set; }

        /// <summary>
        /// 下级节点执行人
        /// </summary>
        public List<string> NextProcExecutor { get; set; }

        /// <summary>
        /// 下级驳回节点
        /// </summary>
        public string RejectNextProcTempId { get; set; }

        /// <summary>
        /// 选择的下级模板节点
        /// </summary>
        public string NextProcTempId { get; set; }

        /// <summary>
        /// 下级执行节点Id
        /// </summary>
        public string NextProcId { get; set; }
    }
}
