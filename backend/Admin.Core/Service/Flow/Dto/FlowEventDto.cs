﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class EndFlowEventDto : FlowDataProviderDto
    {
        /// <summary>
        /// 审批状态
        /// </summary>
        public EnumProcApprovalStatus State { get; set; }

        /// <summary>
        /// 审批意见
        /// </summary>
        public string CheckOpinion { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AfterNextProcCreatedDto : FlowDataProviderDto
    {
        /// <summary>
        /// 审批状态
        /// </summary>
        public EnumProcApprovalStatus State { get; set; }

        /// <summary>
        /// 审批意见
        /// </summary>
        public string CheckOpinion { get; set; }

        /// <summary>
        /// 下级环节处理信息
        /// </summary>
        public List<ApprovalUser> NextProcApprovalInfo { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ApprovalUser
    {
        /// <summary>
        ///账号Id
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        ///账号名称
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 节点名称
        /// </summary>
        public string ProcName { get; set; }
    }
}
