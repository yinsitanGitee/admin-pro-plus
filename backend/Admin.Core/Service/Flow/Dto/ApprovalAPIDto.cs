﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow.Dto
{
    /// <summary>
    /// ApprovalAPIVM
    /// </summary>
    public class ApprovalAPIDto
    {
        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        public EnumProcApprovalStatus Status { get; set; }

        /// <summary>
        /// 审批内容
        /// </summary>
        public string CheckOpinion { get; set; }

        /// <summary>
        /// AuditMemo
        /// </summary>
        public string AuditMemo { get; set; }

        /// <summary>
        /// 流程Id
        /// </summary>
        public string FlowId { get; set; }

        /// <summary>
        /// 环节Id
        /// </summary>
        public string ProcId { get; set; }


        #region 全新的节点属性

        /// <summary>
        /// 0重走流程
        /// 1直接提交本环节
        /// 2直接提交指定环节
        /// </summary> 
        public int RejectType { get; set; }

        /// <summary>
        /// 驳回的指定节点
        /// 审批页面中-驳回- 驳回指定节点 || 驳回至当前节点 有效
        /// </summary>
        public string RejectProcId { get; set; }

        /// <summary>
        /// 下一节点执行人，由当前节点选择的下级节点审批人
        /// 在模板节点属性中，当前节点由上级节点用户选择 有效
        /// </summary>
        public List<string> NextProcExecutor { get; set; }

        /// <summary>
        /// 选择的下级节点
        /// 在模板节点属性中 是否选择下级节点有效
        /// </summary>
        public string NextProcTempId { get; set; }

        /// <summary>
        /// 下级节点，用于在节点审批中验证节点有效性
        /// </summary>
        public string NextProcId { get; set; }

        #endregion
    }
}
