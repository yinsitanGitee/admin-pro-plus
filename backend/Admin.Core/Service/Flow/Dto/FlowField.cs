﻿using Admin.Common.Helper;
using System.ComponentModel;
using System.Reflection;

namespace Admin.Core.Service.Flow.Dto
{
    public class CalculateVal
    {
        /// <summary>
        /// 解析字段
        /// </summary>
        public List<FlowField> AnalyFields { get; set; }
        /// <summary>
        /// 解析后表达式
        /// </summary>
        public string AnalysExpstr { get; set; }
        /// <summary>
        /// 表达式计算结果
        /// </summary>
        public string Result { get; set; }
    }

    /// <summary>
    /// 流程数据源字段
    /// </summary>
    public class FlowField
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName { get; set; }
        /// <summary>
        /// 字段描述
        /// </summary>
        public string FieldDesc { get; set; }
        /// <summary>
        /// 字段类型
        /// </summary>
        public EnumFieldType FieldType { get; set; }
        /// <summary>
        /// 字段值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 转换对象属性列表
        /// </summary>
        /// <typeparam name="T">来源类型</typeparam>
        /// <param name="source">对象</param>
        /// <returns></returns>
        public static List<FlowField> GetFlowFields<T>(T source)
        {
            var li = new List<FlowField>();
            var pros = source.GetType().GetProperties();
            var liFieldTypes = EnumItemHelper.GetEnumFieldList(typeof(EnumFieldType)).Select(t => t.EnumName).ToList();
            foreach (var item in pros)
            {
                if (!liFieldTypes.Contains(item.PropertyType.Name.ToString())) continue;
                var fType = (EnumFieldType)Enum.Parse(typeof(EnumFieldType), item.PropertyType.Name.ToString());
                var val = GetPropValueByObj(item, fType, source);
                var desc = GetPropDesc(item);
                if (string.IsNullOrWhiteSpace(desc)) continue;
                li.Add(new FlowField()
                {
                    FieldType = fType,
                    FieldDesc = desc,
                    FieldName = item.Name,
                    Value = val
                });
            }
            return li;
        }
        /// <summary>
        /// 获取属性描述
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static string GetPropDesc(PropertyInfo prop)
        {
            try
            {
                var desObj = prop.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault();
                if (desObj == null) return null;
                var des = desObj as DescriptionAttribute;
                return des.Description;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 获取属性值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prop"></param>
        /// <param name="fType"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string GetPropValueByObj<T>(PropertyInfo prop, EnumFieldType fType, T source)
        {
            var oVal = prop.GetValue(source);
            if (oVal == null) return null;
            return Convert.ChangeType(oVal, Type.GetType("System." + fType.ToString(), true, true)).ToString();
        }
    }
}
