﻿using Admin.Common.Ioc;
using Admin.Core.Service.Flow.Dto;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    ///  定义流程事件
    /// </summary>
    public class FlowEvent
    {
        /// <summary>
        /// 流程中止事件
        /// </summary>
        /// <param name="flowEvent"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static async Task TriggerEndFlowEvent(string flowEvent, EndFlowEventDto e)
        {
            if (string.IsNullOrWhiteSpace(flowEvent)) return;
            await IocManager.Instance.GerSerciceByOptiona<IFlowEvent>(flowEvent).EndFlowEvent(e);
        }

        /// <summary>
        /// 流程数据源获取接口 注意: 1. 不能返回空值,不能抛异常 2. 如果id为空,必须返回数据源的所有字段 3.工作流高度依赖此接口,请务必注意执行性能
        /// </summary>
        /// <param name="flowEvent"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static async Task TriggerGetDetailForWorkFlow(string flowEvent, FlowDataProviderDto e)
        {
            if (string.IsNullOrWhiteSpace(flowEvent)) return;
            await IocManager.Instance.GerSerciceByOptiona<IFlowEvent>(flowEvent).GetDetailForWorkFlow(e);
        }

        /// <summary>
        ///  下一环节创建完成后触发接口 事件执行顺序:IProcEvent.AfterCurrentProcExecuted>IProcEvent.AfterNextProcCreated>IFlowEvent.EndFlowEvent
        /// </summary>
        /// <param name="flowEvent"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static async Task TriggerAfterNextProcCreated(string flowEvent, AfterNextProcCreatedDto e)
        {
            if (string.IsNullOrWhiteSpace(flowEvent)) return;
            await IocManager.Instance.GerSerciceByOptiona<IFlowEvent>(flowEvent).AfterNextProcCreated(e);
        }

        /// <summary>
        /// 流程主动终止
        /// </summary>
        /// <param name="flowEvent"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static async Task TriggerInitiativeAbandonFlow(string flowEvent, FlowDataProviderDto e)
        {
            if (string.IsNullOrWhiteSpace(flowEvent)) return;
            await IocManager.Instance.GerSerciceByOptiona<IFlowEvent>(flowEvent).InitiativeAbandonFlow(e);
        }
    }
}