﻿using Admin.Common.Ioc;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Flow.Dto;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Mapster;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 定义流程服务
    /// </summary>
    /// <param name="freeSql"></param>
    public class FlowService(IFreeSql freeSql) : IFlowService
    {
        /// <summary>
        /// 保存流程
        /// 会判断当前流程是否已自动通过
        /// 如果自动通过, 则返回 Status = Skip
        /// 如果没有,则流程正常执行, 返回 Status = Common
        /// </summary>
        /// <param name="app"></param>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<FlowCommonResult<string>> SubmitFlow(FlowAppAPIVM app, FlowInfoAPIVM flowInfo)
        {
            var result = new FlowCommonResult<string>();
            var flowId = string.Empty;

            var acc = await IocManager.Instance.GetService<ICacheService>().GetUser();

            #region 构建流程实例

            var flowVM = await CreateFlow(app, flowInfo);
            flowId = flowVM.Entity.Id;
            flowVM.Entity.AppId = app.AppId;
            flowVM.Entity.AppCode = app.AppCode;
            flowVM.Entity.TaskContent = app.Content;
            flowVM.Entity.TaskTitle = app.Title;
            flowVM.Entity.TaskDescription = app.Description;
            flowVM.Entity.SourceTypeValue = app.SourceTypeValue;

            #endregion

            #region 执行环节第一步

            var beginProc = flowVM.Procs.Where(t => t.Type == EnumProcType.Start).FirstOrDefault();
            if (beginProc == null) throw new Exception("不存在开始环节");
            beginProc.ComplateTime = beginProc.BeginTime = flowVM.Entity.CreateDateTime;

            #endregion

            #region 更新开始环节开始时间

            var createdProcIds = new List<string>();

            #endregion

            #region 构建处理参数

            var para = new ExecutProcPara()
            {
                Flow = flowVM.Entity,
                CurrentProcHandStatus = new ProcHandStatus()
                {
                    ApprovalStatus = EnumProcApprovalStatus.Agree
                },
            };

            #endregion

            #region 开启事务，保存流程实例

            await freeSql.Insert(flowVM.Entity).ExecuteAffrowsAsync();
            await freeSql.Insert(flowVM.Procs).ExecuteAffrowsAsync();
            await freeSql.Insert(flowVM.Lines).ExecuteAffrowsAsync();
            await freeSql.Insert(flowVM.Conds).ExecuteAffrowsAsync();
            await freeSql.Insert(flowVM.Executors).ExecuteAffrowsAsync();

            if (!string.IsNullOrWhiteSpace(flowInfo.RejectNextProcTempId))
                await freeSql.Delete<FlowSpecialProcEntity>().Where(a => a.FlowSpecialProcId == flowInfo.RejectNextProcTempId && a.SourceId == app.AppId).ExecuteAffrowsAsync();

            #region 创建下级节点, 流程跳过, 流程选人, 插入流程日志 都在此处理

            var opeateRecordAPIVMs = new List<OpeateRecordAPIVM> { new OpeateRecordAPIVM
                {
                    OperateUserId = acc.Id,
                    OperateUserName = acc.Name,
                    OperateDate = DateTime.Now.AddSeconds(-1),
                    ProcId = beginProc.Id,
                    Title = "提交",
                    DataId = app.AppId,
                    State = EnumOperateState.Agree,
                    Type = "提交"
                }};

            if (flowVM.Entity.IsApprovalPass)
            {
                var skipProcIds = new List<string>(); // 本次审批所跳过的环节, 不包括本环节和最后一级未跳过处理中环节

                // 本次审批插入的自动跳过日志
                await FlowProcService.NewExecutProc(freeSql, para, createdProcIds, beginProc, new List<string> { acc.Id }, opeateRecordAPIVMs, skipProcIds);

                if (skipProcIds.Count > 0)
                {
                    await freeSql.Update<ProcEntity>().Set(a => new ProcEntity
                    {
                        ApprovalStatus = EnumProcApprovalStatus.Agree,
                        ComplateTime = DateTime.Now,
                        ProcStatus = EnumProcStatus.Complete
                    }).Where(a => skipProcIds.Contains(a.Id)).ExecuteAffrowsAsync();
                }
            }
            else
                await FlowProcService.ExecutProc(freeSql, para, createdProcIds, beginProc);

            await FlowBusinessService.BatchAddOperateRecord(freeSql, opeateRecordAPIVMs);

            #endregion

            var flow = await freeSql.Select<FlowEntity>().Where(a => a.Id == flowVM.Entity.Id).ToOneAsync();

            if (flow.FlowStatus == EnumFlowStatus.Complete)
            {
                await FlowEvent.TriggerEndFlowEvent(flowVM.Entity.FlowEvent, new EndFlowEventDto
                {
                    AppCode = flow.AppCode,
                    AppId = flow.AppId,
                    State = EnumProcApprovalStatus.Agree
                });
                result.Status = EnumFlowCommonResult.Skip;

                await FlowBusinessService.CloseTask(new ExecuteTaskDto
                {
                    Status = 3,
                    ExecuteUserId = acc.Id,
                    ExecuteUserName = acc.Name,
                    SourceId = app.AppId
                });

                // 发送消息
                await FlowBusinessService.ExecutAfterFlowComplete(freeSql, flow, EnumProcApprovalStatus.Agree);
            }
            else
            {
                #region 流程、节点后置完成 ，发送待办

                var nextInfo = await FlowWorkListService.GetNextProcExecutors(freeSql, flowVM.Entity.Id);

                await FlowEvent.TriggerAfterNextProcCreated(para.Flow.FlowEvent, new AfterNextProcCreatedDto
                {
                    AppCode = para.Flow.AppCode,
                    AppId = para.Flow.AppId,
                    CheckOpinion = "提交",
                    State = EnumProcApprovalStatus.Agree,
                    NextProcApprovalInfo = nextInfo,
                });

                await FlowBusinessService.CreateTask(new MsgTaskCreateDto
                {
                    ReceiveAccIds = nextInfo.Select(a => a.AccountId).ToList(),
                    SourceId = app.AppId,
                    SourceTypeValue = app.SourceTypeValue,
                    Description = app.Description,
                    ExecuteUserId = acc.Id,
                    ExecuteUserName = acc.Name,
                    TaskContent = app.Content,
                    Title = app.Title,
                    OrgId = flowInfo.OrgId
                });

                #endregion
            }

            #endregion
            result.Result = flowId;
            return result;
        }

        /// <summary>
        /// 创建流程
        /// </summary>
        /// <param name="app"></param>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        public async Task<FlowVM> CreateFlow(FlowAppAPIVM app, FlowInfoAPIVM flowInfo)
        {
            var flowTemp = await GetFlowTempVM(freeSql, flowInfo.FlowTempId);
            var flow = flowTemp.Adapt<FlowVM>();
            flow.Entity.FlowTempId = flowTemp.Entity.Id;
            flow.Entity.FlowEvent = await freeSql.Select<ActionEntity>().Where(a => a.Code == flow.Entity.ActionCode).ToOneAsync(a => a.FlowEvent);
            flow.Entity.Id = Guid.NewGuid().ToString("N");
            flow.RejectNextProcTempId = flowInfo.RejectNextProcTempId;
            flow.NextProcTempId = flowInfo.NextProcTempId;
            flow.NextProcExecutor = flowInfo.NextProcExecutor;
            flow = await CreateApprovalFlowInstance(flowTemp, flow, app);
            return flow;
        }

        /// <summary>
        ///  审批流程实例化
        /// </summary>
        /// <param name="flowTemp"></param>
        /// <param name="flow"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        public async Task<FlowVM> CreateApprovalFlowInstance(FlowTempVM flowTemp, FlowVM flow, FlowAppAPIVM app)
        {
            //审批流程,需要动态计算参与流程的所有环节
            var vm = await GetApprovalProcLInes(flowTemp, flow, app);
            //构建流程实例信息
            //实例化流程线,条件 
            foreach (var line in vm.LineTemps)
            {
                var ll = new LineEntity()
                {
                    Id = Guid.NewGuid().ToString("N"),
                    ActionCode = flowTemp.Entity.ActionCode,
                    SchemeLineId = line.SchemeLineId,
                    Code = line.Code,
                    FlowId = flow.Entity.Id,
                    FromSchemeProcId = line.FromSchemeProcId,
                    ToSchemeProcId = line.ToSchemeProcId,
                    Name = line.Name,
                    LineCondType = line.LineCondType,
                    SchemeLineInfo = line.SchemeLineInfo,
                };
                flow.Lines.Add(ll);
                //实例化流程条件
                var condTemp = flowTemp.CondTemps.Where(t => t.RefType == EnumFlowEle.Line && t.RefId == line.Id).FirstOrDefault();
                if (condTemp != null)
                {
                    var cond = new CondEntity()
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        Expression = condTemp.Expression,
                        FlowId = flow.Entity.Id,
                        RefId = ll.Id,
                        RefType = EnumFlowEle.Line
                    };
                    flow.Conds.Add(cond);
                }
            }

            //实例化环节
            foreach (var item in vm.ProcTemps)
            {
                var p = new ProcEntity()
                {
                    Id = Guid.NewGuid().ToString("N"),
                    FlowId = flow.Entity.Id,
                    SchemeProcId = item.SchemeProcId,
                    ActionCode = flow.Entity.ActionCode,
                    ApprovalStatus = EnumProcApprovalStatus.None,
                    Code = item.Code,
                    Name = item.Name,
                    LimtTimeType = item.LimtTimeType,
                    ProcStatus = EnumProcStatus.Wait,
                    LimtTime = item.LimtTime ?? 0,
                    Type = item.Type,
                    SchemeProcInfo = item.SchemeProcInfo,
                    Sort = item.Sort,
                    OriginalSchemeProcId = item.OriginalSchemeProcId,
                    ProcApprovalType = item.ProcApprovalType,
                    ProcExecutorType = item.ProcExecutorType,
                    IsSelectNextProc = item.IsSelectNextProc,
                    IsSelectExecutor = item.IsSelectExecutor
                };
                var exeTemps = flowTemp.ExecutorTemps.Where(t => t.ProcId == item.Id).ToList();
                if (exeTemps != null && exeTemps.Count() != 0)
                {
                    foreach (var exe in exeTemps)
                    {
                        //实例化执行人信息
                        var ee = new ExecutorEntity()
                        {
                            Id = Guid.NewGuid().ToString("N"),
                            FlowId = flow.Entity.Id,
                            ProcId = p.Id,
                            RefId = exe.RefId,
                            RefType = exe.RefType,
                            RefName = exe.RefName,
                            IsLimtOrgan = exe.IsLimtOrgan,
                        };
                        flow.Executors.Add(ee);
                    }
                }
                flow.Procs.Add(p);
            }
            return flow;
        }

        /// <summary>
        /// 构建审批流程实例
        /// </summary>
        /// <param name="flowTemp"></param>
        /// <param name="flow"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        public async Task<FlowTempVM> GetApprovalProcLInes(FlowTempVM flowTemp, FlowVM flow, FlowAppAPIVM app)
        {
            var beginProc = flowTemp.ProcTemps.Where(t => t.Type == EnumProcType.Start).FirstOrDefault();

            var nFlowTempVM = new FlowTempVM();
            var para = new LoopSchemeParaVM()
            {
                FlowId = flow.Entity.Id,
                AppCode = app.AppCode,
                AppId = app.AppId,
                CurrentId = beginProc.Id,
                CurrentType = EnumFlowEle.Proc
            };
            var hasLoopProcs = new List<ProcTempEntity>();

            if (!string.IsNullOrWhiteSpace(flow.RejectNextProcTempId) && flowTemp.ProcTemps.FirstOrDefault(a => a.SchemeProcId == flow.RejectNextProcTempId) != null)
            {
                var first = flowTemp.ProcTemps.FirstOrDefault(t => t.Id == para.CurrentId);
                if (first != null) first.IsSelectNextProc = false;
                foreach (var item in flowTemp.LineTemps.Where(a => a.FromSchemeProcId == beginProc.SchemeProcId))
                {
                    item.ToSchemeProcId = flow.RejectNextProcTempId;
                }
            }
            else
            {
                var next = flowTemp.ProcTemps.FirstOrDefault(a => a.Id == flow.NextProcTempId)?.SchemeProcId;
                para.NextProcTempId = next;
            }

            await LoopScheme(para, flowTemp, nFlowTempVM, hasLoopProcs);

            var toProc = flowTemp.ProcTemps.Where(t => t.Type == EnumProcType.End).FirstOrDefault();
            toProc.Sort = nFlowTempVM.ProcTemps.Count();
            nFlowTempVM.ProcTemps.Add(toProc);

            var line = nFlowTempVM.LineTemps.FirstOrDefault(a => a.FromSchemeProcId == beginProc.SchemeProcId);
            if (line != null)
            {
                var firstProc = nFlowTempVM.ProcTemps.FirstOrDefault(a => a.SchemeProcId == line.ToSchemeProcId);

                if (firstProc != null)
                {
                    if (firstProc.Type == EnumProcType.Normal)
                    {
                        if (firstProc.IsSelectExecutor)
                        {
                            if (flow.NextProcExecutor == null || flow.NextProcExecutor.Count <= 0)
                            {
                                throw new Exception("流程模板可能已被改动，请重新提交!");
                            }

                            flowTemp.ExecutorTemps = flowTemp.ExecutorTemps.Where(a => a.ProcId != firstProc.Id).ToList();
                            if (firstProc.ProcExecutorType == EnumProcExecutorType.EveryOneProc)
                            {
                                firstProc.IsSelectExecutor = false;
                                var firstProcExecutor = flow.NextProcExecutor[0];
                                flow.NextProcExecutor.Remove(firstProcExecutor);

                                flowTemp.ExecutorTemps.Add(new ExecutorTempEntity
                                {
                                    Id = Guid.NewGuid().ToString("N"),
                                    ProcId = firstProc.Id,
                                    RefId = firstProcExecutor,
                                    RefType = EnumExecutorRefType.Account,
                                    IsLimtOrgan = false,
                                    FlowTempId = flowTemp.Entity.Id
                                });

                                var lastSchemeProcId = firstProc.SchemeProcId;
                                var atLastSchemeProcId = Guid.NewGuid().ToString();

                                if (flow.NextProcExecutor.Count > 0)
                                {
                                    int sort = firstProc.Sort;

                                    foreach (var item in nFlowTempVM.ProcTemps.Where(a => a.Sort > firstProc.Sort))
                                    {
                                        item.Sort += flow.NextProcExecutor.Count;
                                    }

                                    foreach (var item in nFlowTempVM.LineTemps.Where(a => a.FromSchemeProcId == firstProc.SchemeProcId && (a.LineCondType == EnumLineCondType.Agree || a.LineCondType == EnumLineCondType.None)))
                                    {
                                        item.FromSchemeProcId = atLastSchemeProcId;
                                    }

                                    int length = flow.NextProcExecutor.Count - 1;
                                    for (int i = 0; i <= length; i++)
                                    {
                                        var currentSchemeProcId = i == length ? atLastSchemeProcId : Guid.NewGuid().ToString("N");
                                        var isSelectNextProc = i == length && firstProc.IsSelectNextProc;
                                        var procId = Guid.NewGuid().ToString("N");

                                        nFlowTempVM.ProcTemps.Add(new ProcTempEntity
                                        {
                                            Id = procId,
                                            Code = "AppendProc",
                                            SchemeProcId = currentSchemeProcId,
                                            ActionCode = flow.Entity.ActionCode,
                                            Name = firstProc.Name,
                                            LimtTime = firstProc.LimtTime,
                                            Type = firstProc.Type,
                                            SchemeProcInfo = firstProc.SchemeProcInfo,
                                            OriginalSchemeProcId = firstProc.SchemeProcId,
                                            ProcApprovalType = EnumProcApprovalType.OneApproval,
                                            ProcExecutorType = EnumProcExecutorType.OneProc,
                                            IsSelectExecutor = false,
                                            IsSelectNextProc = isSelectNextProc,
                                            Sort = ++sort,
                                            FlowTempId = flowTemp.Entity.Id
                                        });

                                        nFlowTempVM.LineTemps.Add(new LineTempEntity
                                        {
                                            Id = Guid.NewGuid().ToString("N"),
                                            ActionCode = flowTemp.Entity.ActionCode,
                                            SchemeLineId = Guid.NewGuid().ToString("N"),
                                            Code = "AppendLine",
                                            FromSchemeProcId = lastSchemeProcId,
                                            ToSchemeProcId = currentSchemeProcId,
                                            Name = "同意",
                                            LineCondType = EnumLineCondType.Agree,
                                            FlowTempId = flowTemp.Entity.Id
                                        });

                                        flowTemp.ExecutorTemps.Add(new ExecutorTempEntity
                                        {
                                            Id = Guid.NewGuid().ToString("N"),
                                            ProcId = procId,
                                            RefId = flow.NextProcExecutor[i],
                                            RefType = EnumExecutorRefType.Account,
                                            IsLimtOrgan = false,
                                            FlowTempId = flowTemp.Entity.Id
                                        });

                                        lastSchemeProcId = currentSchemeProcId; // 上一个节点模板Id，用于连接线数据拼接
                                    }

                                    firstProc.IsSelectNextProc = false;
                                }
                            }
                            else
                            {
                                for (int i = 0; i < flow.NextProcExecutor.Count; i++)
                                {
                                    flowTemp.ExecutorTemps.Add(new ExecutorTempEntity
                                    {
                                        Id = Guid.NewGuid().ToString("N"),
                                        ProcId = firstProc.Id,
                                        RefId = flow.NextProcExecutor[i],
                                        RefType = EnumExecutorRefType.Account,
                                        IsLimtOrgan = false,
                                        FlowTempId = flowTemp.Entity.Id
                                    });
                                }
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("流程模板可能已被改动，请重新提交!");
                }
            }

            return nFlowTempVM;
        }

        /// <summary>
        /// 递归遍历所有节点,线;根据流程线上条件加载对应条件
        /// </summary>
        /// <param name="para"></param>
        /// <param name="flowTemp"></param>
        /// <param name="nFlowTempVM"></param>
        /// <param name="hasLoopProcs"></param>
        public async Task LoopScheme(LoopSchemeParaVM para, FlowTempVM flowTemp, FlowTempVM nFlowTempVM, List<ProcTempEntity> hasLoopProcs)
        {

            if (para.CurrentType == EnumFlowEle.Proc)
            {
                var currentProc = flowTemp.ProcTemps.Where(t => t.Id == para.CurrentId).FirstOrDefault();

                if (!nFlowTempVM.ProcTemps.Any(t => t.Id == currentProc.Id))
                {
                    currentProc.Sort = nFlowTempVM.ProcTemps == null ? 0 : nFlowTempVM.ProcTemps.Count();
                    nFlowTempVM.ProcTemps.Add(currentProc);
                }
                var lines = flowTemp.LineTemps.Where(t => t.FromSchemeProcId == currentProc.SchemeProcId).ToList();
                if (lines != null && lines.Count != 0)
                {
                    foreach (var line in lines)
                    {
                        if (currentProc.IsSelectNextProc)
                        {
                            if (line.LineCondType == EnumLineCondType.Agree || line.LineCondType == EnumLineCondType.None)
                            {
                                if (string.IsNullOrWhiteSpace(para.NextProcTempId))
                                {
                                    continue;
                                }
                                else
                                {
                                    if (line.ToSchemeProcId != para.NextProcTempId)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }

                        //日志需要,如果当前线条件类型属于自定义表达式,则先获取条件
                        CondTempEntity cond = null;
                        string condLogStr = string.Empty;
                        if (line.LineCondType == EnumLineCondType.CustDefine)
                        {
                            cond = await freeSql.Select<CondTempEntity>().Where(t => t.RefId == line.Id && t.FlowTempId == line.FlowTempId && t.RefType == EnumFlowEle.Line).ToOneAsync();
                            if (cond == null) throw new Exception("流程配置中自定义条件异常，请检查");
                            condLogStr = cond == null ? string.Empty : ",表达式为:" + cond.Expression;
                        }
                        //无论同意不同意还是不设置条件,都需要将线纳入并且进入递归
                        if (new List<EnumLineCondType>() { EnumLineCondType.Agree, EnumLineCondType.None, EnumLineCondType.DisAgree }.Contains(line.LineCondType))
                        {
                            if (!nFlowTempVM.LineTemps.Any(t => t.Id == line.Id)) nFlowTempVM.LineTemps.Add(line);
                        }
                        else if (line.LineCondType == EnumLineCondType.CustDefine)
                        {
                            if (!await FlowProviderService.TrialCalcula(freeSql, flowTemp.Entity.ActionCode, cond.Expression, new FlowDataProviderDto
                            {
                                AppCode = para.AppCode,
                                AppId = para.AppId
                            })) continue;

                            if (!nFlowTempVM.LineTemps.Any(t => t.Id == line.Id)) nFlowTempVM.LineTemps.Add(line);
                        }

                        var toProc = flowTemp.ProcTemps.Where(t => t.SchemeProcId == line.ToSchemeProcId).FirstOrDefault();
                        //收集已经递归过节点,防止死循环
                        if (hasLoopProcs.Any(t => t.Id == toProc.Id)) continue;
                        else hasLoopProcs.Add(toProc);

                        if (toProc.Type == EnumProcType.End)
                        {
                            continue;
                        }

                        var nPara = new LoopSchemeParaVM()
                        {
                            AppCode = para.AppCode,
                            CurrentId = toProc.Id,
                            AppId = para.AppId,
                            CurrentType = EnumFlowEle.Proc,
                            FlowId = para.FlowId
                        };

                        await LoopScheme(nPara, flowTemp, nFlowTempVM, hasLoopProcs);
                    }
                }
            }
        }

        /// <summary>
        /// 获取流程模板主体信息
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flowTempId"></param>
        /// <returns></returns>
        public static async Task<FlowTempVM> GetFlowTempVM(IFreeSql freeSql, string flowTempId)
        {
            var flowTemp = await GetFlowTempById(freeSql, flowTempId);
            if (flowTemp == null) throw new Exception("未找到对应的流程模板");
            await GetFlowTempVM(freeSql, flowTemp);
            return flowTemp;
        }

        /// <summary>
        /// FlowTempVM
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static async Task GetFlowTempVM(IFreeSql freeSql, FlowTempVM dto)
        {
            //获取全部关联元素
            dto.ProcTemps = await freeSql.Select<ProcTempEntity>().Where(t => t.FlowTempId == dto.Entity.Id).ToListAsync();
            if (dto.ProcTemps.Count == 0) throw new Exception("未在编号为(" + dto.Entity.Code + ")的流程模板中找到任何环节");
            dto.LineTemps = await freeSql.Select<LineTempEntity>().Where(t => t.FlowTempId == dto.Entity.Id).ToListAsync();
            if (dto.LineTemps.Count == 0) throw new Exception("未在编号为(" + dto.Entity.Code + ")的流程模板中找到任何线");
            dto.CondTemps = await freeSql.Select<CondTempEntity>().Where(t => t.FlowTempId == dto.Entity.Id).ToListAsync();
            dto.ExecutorTemps = await freeSql.Select<ExecutorTempEntity>().Where(t => t.FlowTempId == dto.Entity.Id).ToListAsync();
        }

        /// <summary>
        /// 根据模板Id获取模板数据
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<FlowTempVM> GetFlowTempById(IFreeSql freeSql, string id)
        {
            return new FlowTempVM { Entity = await freeSql.Select<FlowTempEntity>().Where(a => a.Id == id).ToOneAsync() };
        }
    }
}
