﻿using Admin.Core.Service.Flow.Dto;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    public interface IProcService
    {
        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="approvalInfo"></param>
        Task ApprovalProc(ApprovalAPIDto approvalInfo);
    }
}
