﻿using Admin.Cache;
using Admin.Common.Ioc;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Service.Flow.Dto;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Repository.Entities.Msg;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using System.Data;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="freeSql"></param>
    public class ProcService(IFreeSql freeSql) : IProcService
    {
        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="approvalInfo"></param>
        public async Task ApprovalProc(ApprovalAPIDto approvalInfo)
        {
            var flow = await freeSql.Select<FlowEntity>().Where(t => t.Id == approvalInfo.FlowId).ToOneAsync() ?? throw new Exception("流程不存在，请重新操作");

            #region 一系列前置验证

            var acc = await IocManager.Instance.GetService<ICacheService>().GetUser();

            if (flow.FlowStatus == EnumFlowStatus.Termination) throw new Exception("流程已经终止,审核环节失败");

            if (string.IsNullOrWhiteSpace(approvalInfo.ProcId))
                approvalInfo.ProcId = await FlowWorkListService.GetCurrentProcIdByUserId(freeSql, approvalInfo.FlowId, acc.Id);

            //throw new Exception("必须传入环节Id(ProcId),审核环节失败");

            var currentProc = await freeSql.Select<ProcEntity>().Where(t => t.FlowId == approvalInfo.FlowId && t.Id == approvalInfo.ProcId).ToOneAsync();
            if (currentProc.Type == EnumProcType.OA) throw new Exception("OA环节只能从第三方系统操作,审核环节失败");

            if (currentProc.ProcStatus != EnumProcStatus.Handing) throw new Exception("当前节点已被审批，请刷新查看！");

            if (currentProc.IsAppendProc && approvalInfo.RejectType == 1 && approvalInfo.Status == EnumProcApprovalStatus.DisAgree) throw new Exception("当前节点为后续追加的处理节点，无法重新提交至本流程！");

            #endregion

            var lockKey = RedisKeyDto.FlowProcApprovalKey + approvalInfo.FlowId;
            bool isCreateNextProc = true; // 是否可以构建下级环节
            var allExecutors = new List<string> { acc.Id };
            try
            {
                if (await RedisManage.LockAsync(lockKey, 60, 60))
                {
                    if (approvalInfo.Status == EnumProcApprovalStatus.Agree || approvalInfo.Status == EnumProcApprovalStatus.BusinessDisAgree)
                    {
                        #region 获取当前流程所有执行人

                        if (flow.IsApprovalPass)
                            allExecutors = await freeSql.Select<WorkListEntity>().Where(a => a.FlowId == approvalInfo.FlowId && a.IsNeed && a.IsHandled &&
                            new EnumProcApprovalStatus[2] { EnumProcApprovalStatus.None, EnumProcApprovalStatus.Agree }.Contains(a.ApprovalStatus)).ToListAsync(a => a.AccountId);

                        #endregion

                        #region 节点属性 - 节点需执行人全部通过才通过

                        if (currentProc.ProcApprovalType == EnumProcApprovalType.AllApproval)
                        {
                            #region 验证多审批节点是否有人未审批

                            if (await freeSql.Select<WorkListEntity>().Where(a => a.AccountId != acc.Id && a.ProcId == approvalInfo.ProcId && !a.IsHandled && a.IsNeed).AnyAsync())
                                isCreateNextProc = false; // 多人审批节点，还有其它审批人未审批，不进行下级节点构建

                            #endregion
                        }

                        #endregion
                    }

                    if (isCreateNextProc) // 是否所有执行人已经审批
                    {
                        if (approvalInfo.Status == EnumProcApprovalStatus.Agree || approvalInfo.Status == EnumProcApprovalStatus.BusinessDisAgree)
                        {
                            if (currentProc.IsSelectNextProc)
                            {
                                #region 构建下级节点、直到遇到需要选择节点的节点

                                await CreateNextProcRealTime(approvalInfo, flow, currentProc);

                                #endregion
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(approvalInfo.NextProcId))
                                {
                                    var nextNormalProc = await freeSql.Select<ProcEntity>().Where(a => a.Id == approvalInfo.NextProcId).ToOneAsync();
                                    if (nextNormalProc == null || nextNormalProc.ProcStatus != EnumProcStatus.Wait)
                                        throw new Exception("当前节点可能已经审批，请刷新重试！");
                                    if (nextNormalProc.ProcExecutorType == EnumProcExecutorType.EveryOneProc && nextNormalProc.IsSelectExecutor)
                                    {
                                        if (approvalInfo.NextProcExecutor != null && approvalInfo.NextProcExecutor.Count > 0)
                                        {
                                            #region 节点属性 - 每人一个节点

                                            await CreateEveryOneProc(approvalInfo, flow, nextNormalProc);

                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #region 下级所有线条

                    var nextLines = await freeSql.Select<LineEntity>().Where(t => t.FromSchemeProcId == currentProc.SchemeProcId && t.FlowId == approvalInfo.FlowId).OrderBy(t => t.Name).ToListAsync();

                    #endregion

                    #region 下级所有节点

                    var lineIds = nextLines.Select(a => a.ToSchemeProcId).ToList();

                    var nextProcs = lineIds.Count > 0 ? await freeSql.Select<ProcEntity>().Where(t => t.FlowId == approvalInfo.FlowId && lineIds.Contains(t.SchemeProcId)).ToListAsync() : null;

                    #endregion

                    #region 构建环节审批参数

                    var apprPara = new ExecutProcPara()
                    {
                        Flow = flow,
                        NextProcExecutor = approvalInfo.NextProcExecutor,
                        CurrentProcHandStatus = new ProcHandStatus()
                        {
                            ApprovalStatus = approvalInfo.Status,
                        }
                    };
                    #endregion

                    #region 构建执行人数据、事务中进行提交

                    var wls = await freeSql.Select<WorkListEntity>().Where(t => t.FlowId == approvalInfo.FlowId &&
                   t.ProcId == approvalInfo.ProcId && !t.IsHandled && t.IsNeed).ToListAsync();

                    if (wls == null || wls.Count == 0) throw new Exception("未找到任何审批人信息");
                    var wl = wls.Where(t => t.AccountId == acc.Id).FirstOrDefault() ?? wls.FirstOrDefault();

                    //更新局部对象信息
                    wl.ComplateTime = DateTime.Now;
                    wl.ApprovalStatus = approvalInfo.Status;
                    wl.CheckOpinion = approvalInfo.CheckOpinion;
                    wl.AccountId = acc.Id;
                    wl.IsHandled = true;

                    #endregion

                    #region 构建驳回指定节点数据

                    FlowSpecialProcEntity specialProc = null;
                    if (approvalInfo.Status == EnumProcApprovalStatus.DisAgree)
                    {
                        if (approvalInfo.RejectType > 0)
                        {
                            string flowSpecialProcId;
                            if (approvalInfo.RejectType == 2)
                            {
                                flowSpecialProcId = approvalInfo.RejectProcId;
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(currentProc.OriginalSchemeProcId))
                                {
                                    flowSpecialProcId = currentProc.SchemeProcId;
                                }
                                else
                                {
                                    flowSpecialProcId = currentProc.OriginalSchemeProcId;
                                }
                            }
                            specialProc = new FlowSpecialProcEntity
                            {
                                Type = approvalInfo.RejectType,
                                FlowSpecialProcId = flowSpecialProcId,
                                Id = Guid.NewGuid().ToString("N"),
                                SourceId = flow.AppId,
                                FlowTempId = flow.FlowTempId
                            };
                        }
                    }


                    #endregion

                    #region 基础变量

                    EnumFlowStatus flowStatus = flow.FlowStatus;
                    var createdProcIds = new List<string>();

                    #endregion

                    #region 流程审批事务提交、包括下级节点构建、节点审批回调、流程结束回调、第三方OA接口处理等等

                    if (specialProc != null)
                        await freeSql.Insert(specialProc).ExecuteAffrowsAsync();

                    //环节属于任何人的时候,更新执行人审批信息
                    await freeSql.Update<WorkListEntity>().SetSource(wl).ExecuteAffrowsAsync();

                    if (approvalInfo.Status == EnumProcApprovalStatus.DisAgree)
                        await freeSql.Update<FlowEntity>().Set(a => a.AbandonReason == approvalInfo.CheckOpinion).Where(a => a.Id == approvalInfo.FlowId).ExecuteAffrowsAsync();

                    if (isCreateNextProc)
                    {
                        #region 创建下级节点, 流程跳过, 流程选人, 插入流程日志 都在此处理
                        var opeateRecordAPIVMs = new List<OpeateRecordAPIVM>(); // 本次审批插入的自动跳过日志
                        var skipProcIds = new List<string>(); // 本次审批所跳过的环节, 不包括本环节和最后一级未跳过处理中环节
                        if (currentProc.ProcApprovalType == EnumProcApprovalType.AllApproval)
                        {
                            #region 插入多执行人日志

                            var executors = await freeSql.Select<WorkListEntity, SysAccountEntity>().LeftJoin(a => a.t1.AccountId == a.t2.Id).Where(a => a.t1.ProcId == currentProc.Id && a.t1.IsNeed && a.t1.IsHandled
                            && a.t1.ApprovalStatus != EnumProcApprovalStatus.None).ToListAsync(a => new OpeateRecordExecutorAPIVM
                            {
                                State = (EnumOperateState)a.t1.ApprovalStatus,
                                Name = a.t2.Name
                            });

                            await FlowBusinessService.BatchAddOperateExecutorRecord(freeSql, new OpeateRecordMultiExecutorAPIVM
                            {
                                OperateUserId = acc.Id,
                                OperateUserName = acc.Name,
                                ProcId = currentProc.Id,
                                OperateDate = DateTime.Now,
                                DataId = flow.AppId,
                                Description = approvalInfo.CheckOpinion,
                                Title = currentProc.Name,
                                Type = "审批",
                                Executors = executors
                            });

                            #endregion
                        }
                        else
                        {
                            opeateRecordAPIVMs.Add(new OpeateRecordAPIVM
                            {
                                OperateUserId = acc.Id,
                                OperateUserName = acc.Name,
                                ProcId = currentProc.Id,
                                Title = currentProc.Name,
                                DataId = flow.AppId,
                                State = (EnumOperateState)approvalInfo.Status,
                                Content = approvalInfo.CheckOpinion,
                                OperateDate = DateTime.Now,
                                Description = approvalInfo.CheckOpinion,
                                Type = "审批",
                            });
                        }
                        await FlowProcService.CreateNextProcs(freeSql, apprPara, currentProc.SchemeProcId, approvalInfo, createdProcIds, nextLines, nextProcs, allExecutors, opeateRecordAPIVMs, skipProcIds);

                        await FlowBusinessService.BatchAddOperateRecord(freeSql, opeateRecordAPIVMs);

                        // 当前节点数据修改
                        currentProc.ComplateTime = DateTime.Now;
                        currentProc.ProcStatus = EnumProcStatus.Complete;
                        currentProc.ApprovalStatus = approvalInfo.Status;

                        //更新环节信息
                        await freeSql.Update<ProcEntity>().SetSource(currentProc).ExecuteAffrowsAsync();
                        if (skipProcIds.Count > 0)
                        {
                            await freeSql.Update<ProcEntity>().Set(a => new ProcEntity
                            {
                                ApprovalStatus = EnumProcApprovalStatus.Agree,
                                ComplateTime = DateTime.Now,
                                ProcStatus = EnumProcStatus.Complete
                            }).Where(a => skipProcIds.Contains(a.Id)).ExecuteAffrowsAsync();
                        }

                        #endregion

                        flowStatus = await freeSql.Select<FlowEntity>().Where(t => t.Id == flow.Id).ToOneAsync(a => a.FlowStatus);

                        #region 日志、待办处理
                        if (flowStatus == EnumFlowStatus.Complete)
                        {
                            #region 流程完成，关闭当前节点产生的待办

                            await FlowEvent.TriggerEndFlowEvent(apprPara.Flow.FlowEvent, new EndFlowEventDto
                            {
                                AppCode = apprPara.Flow.AppCode,
                                AppId = apprPara.Flow.AppId,
                                CheckOpinion = approvalInfo.CheckOpinion,
                                State = approvalInfo.Status
                            });

                            await FlowBusinessService.CloseTask(new ExecuteTaskDto
                            {
                                Type = EnumTaskTypeEunm.Audit,
                                SourceId = apprPara.Flow.AppId,
                                ExecuteUserId = acc.Id,
                                ExecuteUserName = acc.Name
                            });

                            #endregion

                            // 发送消息
                            await FlowBusinessService.ExecutAfterFlowComplete(freeSql, flow, approvalInfo.Status);
                        }
                        else if (flowStatus == EnumFlowStatus.Termination)
                        {
                            #region 流程终止 ，关闭当前节点产生的待办

                            await FlowBusinessService.CloseTask(new ExecuteTaskDto
                            {
                                Type = EnumTaskTypeEunm.Audit,
                                SourceId = apprPara.Flow.AppId,
                                ExecuteUserId = acc.Id,
                                ExecuteUserName = acc.Name
                            });

                            #endregion
                        }
                        else
                        {
                            #region 流程继续,创建新的审批待办并关闭当前节点待办

                            var nextInfo = await FlowWorkListService.GetNextProcExecutors(freeSql, apprPara.Flow.Id);

                            #region 下一环节完成事件触发

                            await FlowEvent.TriggerAfterNextProcCreated(apprPara.Flow.FlowEvent, new AfterNextProcCreatedDto
                            {
                                AppCode = apprPara.Flow.AppCode,
                                AppId = apprPara.Flow.AppId,
                                CheckOpinion = approvalInfo.CheckOpinion,
                                State = approvalInfo.Status,
                                NextProcApprovalInfo = nextInfo
                            });

                            #endregion

                            await FlowBusinessService.CloseAndCreateTask(new MsgTaskCreateDto
                            {
                                ReceiveAccIds = nextInfo.Select(a => a.AccountId).ToList(),
                                SourceId = apprPara.Flow.AppId,
                                SourceTypeValue = apprPara.Flow.SourceTypeValue,
                                Description = apprPara.Flow.TaskDescription,
                                ExecuteUserId = acc.Id,
                                Type = EnumTaskTypeEunm.Audit,
                                ExecuteUserName = acc.Name,
                                TaskContent = apprPara.Flow.TaskContent,
                                Title = apprPara.Flow.TaskTitle,
                                OrgId = apprPara.Flow.OrgId
                            }, new ExecuteTaskDto
                            {
                                SourceId = apprPara.Flow.AppId,
                                ExecuteUserId = acc.Id,
                                ExecuteUserName = acc.Name
                            });

                            #endregion
                        }

                        #endregion
                    }
                    else await FlowBusinessService.CompleteTaskByUserId(new DoneUserTaskDto
                    {
                        SourceId = apprPara.Flow.AppId,
                        CurrentUserId = acc.Id
                    });

                    #endregion
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                await RedisManage.DelLockAsync(lockKey);
            }
        }


        /// <summary>
        /// 创建节点数据 -》单人一个节点
        /// </summary>
        /// <param name="approvalInfo"></param>
        /// <param name="flow"></param>
        /// <param name="nextNormalProc"></param>
        private async Task CreateEveryOneProc(ApprovalAPIDto approvalInfo, FlowEntity flow, ProcEntity nextNormalProc)
        {
            string atLastSchemeProcId = string.Empty;
            var flowExecutors = new List<ExecutorEntity>();
            var flowProcs = new List<ProcEntity>();
            var flowLines = new List<LineEntity>();
            var firstProcExecutor = approvalInfo.NextProcExecutor[0];
            approvalInfo.NextProcExecutor.Remove(firstProcExecutor);

            flowExecutors.Add(new ExecutorEntity
            {
                Id = Guid.NewGuid().ToString("N"),
                ProcId = nextNormalProc.Id,
                FlowId = flow.Id,
                IsLimtOrgan = false,
                RefId = firstProcExecutor,
                RefType = EnumExecutorRefType.Account
            });

            if (approvalInfo.NextProcExecutor.Count > 0)
            {
                // 修改排序
                await freeSql.Update<ProcEntity>().Set(a => a.Sort == a.Sort + approvalInfo.NextProcExecutor.Count).Where(a => a.Sort > nextNormalProc.Sort && a.FlowId == flow.Id).ExecuteAffrowsAsync();

                string lastSchemeProcId = nextNormalProc.SchemeProcId;

                int sort = nextNormalProc.Sort;

                int length = approvalInfo.NextProcExecutor.Count - 1;

                atLastSchemeProcId = Guid.NewGuid().ToString();

                for (int i = 0; i <= length; i++)
                {
                    var currentSchemeProcId = i == length ? atLastSchemeProcId : Guid.NewGuid().ToString("N");
                    var isSelectNextProc = i == length && nextNormalProc.IsSelectNextProc;
                    var procId = Guid.NewGuid().ToString("N");

                    flowProcs.Add(new ProcEntity
                    {
                        Id = procId,
                        Code = "AppendProc",
                        SchemeProcId = currentSchemeProcId,
                        ActionCode = flow.ActionCode,
                        Name = nextNormalProc.Name,
                        LimtTime = nextNormalProc.LimtTime,
                        Type = nextNormalProc.Type,
                        SchemeProcInfo = nextNormalProc.SchemeProcInfo,
                        ProcApprovalType = EnumProcApprovalType.OneApproval,
                        ProcExecutorType = EnumProcExecutorType.OneProc,
                        IsSelectExecutor = false,
                        IsSelectNextProc = isSelectNextProc,
                        ApprovalStatus = EnumProcApprovalStatus.None,
                        ProcStatus = EnumProcStatus.Wait,
                        OriginalSchemeProcId = nextNormalProc.SchemeProcId,
                        Sort = ++sort,
                        FlowId = flow.Id
                    });

                    flowLines.Add(new LineEntity
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        ActionCode = flow.ActionCode,
                        SchemeLineId = Guid.NewGuid().ToString("N"),
                        Code = "AppendLine",
                        FromSchemeProcId = lastSchemeProcId,
                        ToSchemeProcId = currentSchemeProcId,
                        Name = "同意",
                        LineCondType = EnumLineCondType.Agree,
                        FlowId = flow.Id
                    });

                    flowExecutors.Add(new ExecutorEntity
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        ProcId = procId,
                        RefId = approvalInfo.NextProcExecutor[i],
                        RefType = EnumExecutorRefType.Account,
                        IsLimtOrgan = false,
                        FlowId = flow.Id,
                    });

                    lastSchemeProcId = currentSchemeProcId; // 上一个节点模板Id，用于连接线数据拼接
                }

                nextNormalProc.IsSelectExecutor = false;
                nextNormalProc.IsSelectNextProc = false;
            }

            await freeSql.Delete<ExecutorEntity>().Where(a => a.ProcId == nextNormalProc.Id).ExecuteAffrowsAsync();
            await freeSql.Update<ProcEntity>().SetSource(nextNormalProc).ExecuteAffrowsAsync();
            if (!string.IsNullOrWhiteSpace(atLastSchemeProcId))
                await freeSql.Update<LineEntity>().Set(a => new LineEntity { FromSchemeProcId = atLastSchemeProcId }).Where(a => a.FromSchemeProcId == nextNormalProc.SchemeProcId && a.LineCondType == EnumLineCondType.Agree && a.FlowId == flow.Id).ExecuteAffrowsAsync();

            await freeSql.Insert(flowProcs).ExecuteAffrowsAsync();
            await freeSql.Insert(flowLines).ExecuteAffrowsAsync();
            await freeSql.Insert(flowExecutors).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 构建下级节点、直到遇到需要选择节点的节点
        /// </summary>
        /// <param name="approvalInfo"></param>
        /// <param name="flow"></param>
        /// <param name="currentProc"></param>
        private async Task CreateNextProcRealTime(ApprovalAPIDto approvalInfo, FlowEntity flow, ProcEntity currentProc)
        {
            if (string.IsNullOrWhiteSpace(approvalInfo.NextProcTempId))
            {
                if (currentProc.ProcApprovalType == EnumProcApprovalType.AllApproval)
                    throw new Exception("当前节点需要实时构建下级节点，您是最后审批人，请选择下级节点！");

                throw new Exception("当前节点需要实时构建下级节点，请选择下级节点！");
            }

            var para = new LoopSchemeParaVM()
            {
                FlowId = flow.Id,
                AppCode = flow.AppCode,
                AppId = flow.AppId,
                CurrentId = approvalInfo.NextProcTempId,
                CurrentType = EnumFlowEle.Proc
            };

            var nFlowTempVM = new FlowTempVM();
            var flowTemp = await FlowService.GetFlowTempById(freeSql, flow.FlowTempId);
            var tempInfo = await FlowService.GetFlowTempVM(freeSql, flow.FlowTempId);
            var hasLoopProcs = new List<ProcTempEntity>();

            var nextProc = tempInfo.ProcTemps.FirstOrDefault(t => t.Id == approvalInfo.NextProcTempId);
            if (nextProc == null)
                throw new Exception("选择的节点不存在，请刷新重试!!!");

            //构建流程实例信息
            //实例化流程线,条件 
            var flowLines = new List<LineEntity> {new LineEntity()
                            {
                                Id = Guid.NewGuid().ToString("N"),
                                ActionCode = flowTemp.Entity.ActionCode,
                                SchemeLineId = Guid.NewGuid().ToString("N"),
                                Code = "AppendLine",
                                FlowId = flow.Id,
                                FromSchemeProcId = currentProc.SchemeProcId,
                                ToSchemeProcId = nextProc.SchemeProcId,
                                Name = "AppendLine",
                                LineCondType = EnumLineCondType.Agree,
                            } };
            var flowConds = new List<CondEntity>();
            var flowExecutors = new List<ExecutorEntity>();
            var flowProcs = new List<ProcEntity>();

            await LoopScheme(para, tempInfo, nFlowTempVM, hasLoopProcs, currentProc.Sort + 1);

            foreach (var line in nFlowTempVM.LineTemps)
            {
                var ll = new LineEntity()
                {
                    Id = Guid.NewGuid().ToString("N"),
                    ActionCode = flowTemp.Entity.ActionCode,
                    SchemeLineId = line.SchemeLineId,
                    Code = line.Code,
                    FlowId = flow.Id,
                    FromSchemeProcId = line.FromSchemeProcId,
                    ToSchemeProcId = line.ToSchemeProcId,
                    Name = line.Name,
                    LineCondType = line.LineCondType,
                    SchemeLineInfo = line.SchemeLineInfo,
                };
                flowLines.Add(ll);
                //实例化流程条件
                var condTemp = tempInfo.CondTemps.Where(t => t.RefType == EnumFlowEle.Line && t.RefId == line.Id).FirstOrDefault();
                if (condTemp != null)
                {
                    var cond = new CondEntity()
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        Expression = condTemp.Expression,
                        FlowId = flow.Id,
                        RefId = ll.Id,
                        RefType = EnumFlowEle.Line,
                    };
                    flowConds.Add(cond);
                }
            }
            //实例化环节
            foreach (var item in nFlowTempVM.ProcTemps)
            {
                var p = new ProcEntity()
                {
                    Id = Guid.NewGuid().ToString("N"),
                    FlowId = flow.Id,
                    SchemeProcId = item.SchemeProcId,
                    ActionCode = flow.ActionCode,
                    ApprovalStatus = EnumProcApprovalStatus.None,
                    Code = item.Code,
                    Name = item.Name,
                    ProcStatus = EnumProcStatus.Wait,
                    LimtTime = item.LimtTime,
                    Type = item.Type,
                    SchemeProcInfo = item.SchemeProcInfo,
                    Sort = item.Sort,
                    ProcApprovalType = item.ProcApprovalType,
                    ProcExecutorType = item.ProcExecutorType,
                    IsSelectExecutor = item.IsSelectExecutor,
                    IsSelectNextProc = item.IsSelectNextProc,
                };
                var exeTemps = tempInfo.ExecutorTemps.Where(t => t.ProcId == item.Id).ToList();
                if (exeTemps != null && exeTemps.Count() != 0)
                {
                    foreach (var exe in exeTemps)
                    {
                        //实例化执行人信息
                        var ee = new ExecutorEntity()
                        {
                            Id = Guid.NewGuid().ToString("N"),
                            FlowId = flow.Id,
                            ProcId = p.Id,
                            RefId = exe.RefId,
                            RefType = exe.RefType,
                            RefName = exe.RefName,
                            IsLimtOrgan = exe.IsLimtOrgan,
                        };
                        flowExecutors.Add(ee);
                    }
                }
                flowProcs.Add(p);
            }

            if (nextProc.ProcExecutorType == EnumProcExecutorType.EveryOneProc && nextProc.IsSelectExecutor)
            {
                if (approvalInfo.NextProcExecutor != null && approvalInfo.NextProcExecutor.Count > 0)
                {
                    int sort = nextProc.Sort;
                    var flowProc = flowProcs.FirstOrDefault(a => a.SchemeProcId == nextProc.SchemeProcId);
                    if (flowProc != null)
                    {
                        flowExecutors = flowExecutors.Where(a => a.ProcId != flowProc.Id).ToList();
                        flowProc.IsSelectExecutor = false;
                        var firstProcExecutor = approvalInfo.NextProcExecutor[0];
                        approvalInfo.NextProcExecutor.Remove(firstProcExecutor);
                        flowExecutors.Add(new ExecutorEntity
                        {
                            Id = Guid.NewGuid().ToString("N"),
                            ProcId = flowProc.Id,
                            RefId = firstProcExecutor,
                            RefType = EnumExecutorRefType.Account,
                            IsLimtOrgan = false,
                            FlowId = flow.Id,
                        });

                        if (approvalInfo.NextProcExecutor.Count > 0)
                        {
                            foreach (var item in flowProcs.Where(a => a.Sort > nextProc.Sort))
                            {
                                item.Sort += approvalInfo.NextProcExecutor.Count;
                            }

                            var lastSchemeProcId = nextProc.SchemeProcId;
                            var atLastSchemeProcId = Guid.NewGuid().ToString();

                            foreach (var item in flowLines.Where(a => a.FromSchemeProcId == nextProc.SchemeProcId && a.LineCondType == EnumLineCondType.Agree))
                            {
                                item.FromSchemeProcId = atLastSchemeProcId;
                            }

                            int length = approvalInfo.NextProcExecutor.Count - 1;
                            // 构建节点   
                            for (int i = 0; i <= length; i++)
                            {
                                var currentSchemeProcId = i == length ? atLastSchemeProcId : Guid.NewGuid().ToString("N");
                                var isSelectNextProc = i == length && nextProc.IsSelectNextProc;
                                var procId = Guid.NewGuid().ToString("N");

                                flowProcs.Add(new ProcEntity
                                {
                                    Id = procId,
                                    Code = "AppendProc",
                                    SchemeProcId = currentSchemeProcId,
                                    ActionCode = flow.ActionCode,
                                    Name = nextProc.Name,
                                    LimtTime = nextProc.LimtTime,
                                    Type = nextProc.Type,
                                    SchemeProcInfo = nextProc.SchemeProcInfo,
                                    ProcApprovalType = EnumProcApprovalType.OneApproval,
                                    ProcExecutorType = EnumProcExecutorType.OneProc,
                                    IsSelectExecutor = false,
                                    IsSelectNextProc = isSelectNextProc,
                                    ApprovalStatus = EnumProcApprovalStatus.None,
                                    ProcStatus = EnumProcStatus.Wait,
                                    OriginalSchemeProcId = nextProc.SchemeProcId,
                                    Sort = ++sort,
                                    FlowId = flow.Id
                                });

                                flowLines.Add(new LineEntity
                                {
                                    Id = Guid.NewGuid().ToString("N"),
                                    ActionCode = flowTemp.Entity.ActionCode,
                                    SchemeLineId = Guid.NewGuid().ToString("N"),
                                    Code = "AppendLine",
                                    FromSchemeProcId = lastSchemeProcId,
                                    ToSchemeProcId = currentSchemeProcId,
                                    Name = "同意",
                                    LineCondType = EnumLineCondType.Agree,
                                    FlowId = flow.Id
                                });

                                flowExecutors.Add(new ExecutorEntity
                                {
                                    Id = Guid.NewGuid().ToString("N"),
                                    ProcId = procId,
                                    RefId = approvalInfo.NextProcExecutor[i],
                                    RefType = EnumExecutorRefType.Account,
                                    IsLimtOrgan = false,
                                    FlowId = flow.Id,
                                });

                                lastSchemeProcId = currentSchemeProcId; // 上一个节点模板Id，用于连接线数据拼接
                            }

                            flowProc.IsSelectNextProc = false;
                        }
                    }
                }
            }

            await freeSql.Update<ProcEntity>().Set(a => a.IsSelectNextProc == false).Where(a => a.Id == currentProc.Id).ExecuteAffrowsAsync();
            await freeSql.Insert(flowProcs).ExecuteAffrowsAsync();
            await freeSql.Insert(flowLines).ExecuteAffrowsAsync();
            await freeSql.Insert(flowExecutors).ExecuteAffrowsAsync();
            await freeSql.Insert(flowConds).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 递归遍历所有节点,线;根据流程线上条件加载对应条件
        /// </summary>
        /// <param name="para"></param>
        /// <param name="flowTemp"></param>
        /// <param name="nFlowTempVM"></param>
        /// <param name="hasLoopProcs"></param>
        public async Task LoopScheme(LoopSchemeParaVM para, FlowTempVM flowTemp, FlowTempVM nFlowTempVM, List<ProcTempEntity> hasLoopProcs, int count)
        {
            if (para.CurrentType == EnumFlowEle.Proc)
            {
                var currentProc = flowTemp.ProcTemps.Where(t => t.Id == para.CurrentId).FirstOrDefault();
                if (!nFlowTempVM.ProcTemps.Any(t => t.Id == currentProc.Id))
                {
                    currentProc.Sort = count++;
                    nFlowTempVM.ProcTemps.Add(currentProc);
                }

                var lines = flowTemp.LineTemps.Where(t => t.FromSchemeProcId == currentProc.SchemeProcId).ToList();
                if (lines != null && lines.Count != 0)
                {
                    foreach (var line in lines)
                    {
                        if (currentProc.IsSelectNextProc)
                        {
                            if (line.LineCondType == EnumLineCondType.Agree)
                            {
                                continue;
                            }
                        }

                        //日志需要,如果当前线条件类型属于自定义表达式,则先获取条件
                        CondTempEntity cond = null;
                        string condLogStr = string.Empty;
                        if (line.LineCondType == EnumLineCondType.CustDefine)
                        {
                            cond = await freeSql.Select<CondTempEntity>().Where(t => t.RefId == line.Id && t.FlowTempId == line.FlowTempId && t.RefType == EnumFlowEle.Line).ToOneAsync();
                            if (cond == null) throw new Exception("流程配置中自定义条件异常，请检查");
                            condLogStr = cond == null ? string.Empty : ",表达式为:" + cond.Expression;
                        }
                        //无论同意不同意还是不设置条件,都需要将线纳入并且进入递归
                        if (new List<EnumLineCondType>() { EnumLineCondType.Agree, EnumLineCondType.None, EnumLineCondType.DisAgree }.Contains(line.LineCondType))
                        {
                            if (!nFlowTempVM.LineTemps.Any(t => t.Id == line.Id)) nFlowTempVM.LineTemps.Add(line);
                        }

                        var toProc = flowTemp.ProcTemps.Where(t => t.SchemeProcId == line.ToSchemeProcId).FirstOrDefault();
                        //收集已经递归过节点,防止死循环
                        if (hasLoopProcs.Any(t => t.Id == toProc.Id)) continue;
                        else hasLoopProcs.Add(toProc);

                        if (toProc.Type == EnumProcType.End)
                        {
                            continue;
                        }
                        var nPara = new LoopSchemeParaVM()
                        {
                            AppCode = para.AppCode,
                            CurrentId = toProc.Id,
                            AppId = para.AppId,
                            CurrentType = EnumFlowEle.Proc,
                            FlowId = para.FlowId
                        };

                        await LoopScheme(nPara, flowTemp, nFlowTempVM, hasLoopProcs, count);
                    }
                }
            }
        }
    }
}
