﻿using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Ioc;
using Admin.Core.Auth;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Service.Flow.Dto;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Repository.Entities.Msg;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    public static class FlowProcService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="schemeProcId"></param>
        /// <param name="approvalInfo"></param>
        /// <param name="procIds"></param>
        /// <param name="lines"></param>
        /// <param name="procs"></param>
        /// <param name="allExecutors"></param>
        /// <param name="opeateRecordAPIVMs"></param>
        /// <param name="skipProcIds"></param>
        /// <returns></returns>
        public static async Task CreateNextProcs(IFreeSql freeSql, ExecutProcPara para, string schemeProcId, ApprovalAPIDto approvalInfo, List<string> procIds, List<LineEntity> lines, List<ProcEntity> procs,
         List<string> allExecutors, List<OpeateRecordAPIVM> opeateRecordAPIVMs, List<string> skipProcIds)
        {
            #region 针对于流程没有不同意线处理 , 审批不同意并且当前操作节点没有指向不同意节点的线

            if (lines == null || lines.Count == 0)
            {
                await ExecutEndProc(freeSql, approvalInfo.FlowId);
                return;
            }

            if (approvalInfo.Status == EnumProcApprovalStatus.DisAgree)
            {
                var endProc = procs.FirstOrDefault(a => a.Type == EnumProcType.End);
                if (endProc == null)
                {
                    await ExecutEndProc(freeSql, approvalInfo.FlowId);
                    return;
                }
                else
                {
                    if (!lines.Any(a => a.ToSchemeProcId == endProc.SchemeProcId && a.FromSchemeProcId == schemeProcId))
                    {
                        await ExecutEndProc(freeSql, approvalInfo.FlowId);
                        return;
                    }
                }
            }

            #endregion

            if (para.Flow.IsApprovalPass)
            {
                foreach (var line in lines)
                {
                    var procToNode = procs.FirstOrDefault(a => a.SchemeProcId == line.ToSchemeProcId);
                    if (procToNode == null)
                    {
                        #region 未找到下级节点, 异常处理直接结束流程

                        await ExecutEndProc(freeSql, approvalInfo.FlowId);
                        return;

                        #endregion
                    }
                    else
                    {
                        if (line.LineCondType == EnumLineCondType.CustDefine)//特殊处理自定义条件
                        {
                            if (await CondService.CheckCustLineCond(freeSql, para.Flow, line))//只递归条件通过的节点
                            {
                                await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                            }
                        }
                        else if (line.LineCondType == EnumLineCondType.None)
                        {
                            await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                        }
                        else
                        {
                            EnumLineCondType condType = EnumLineCondType.None;
                            switch (approvalInfo.Status)
                            {
                                case EnumProcApprovalStatus.None:
                                    condType = EnumLineCondType.None;
                                    break;
                                case EnumProcApprovalStatus.Agree:
                                    condType = EnumLineCondType.Agree;
                                    break;
                                case EnumProcApprovalStatus.DisAgree:
                                    condType = EnumLineCondType.DisAgree;
                                    break;
                                case EnumProcApprovalStatus.Termination:
                                    break;
                                case EnumProcApprovalStatus.BusinessDisAgree:
                                    condType = EnumLineCondType.Agree;
                                    break;
                            }
                            if (condType != line.LineCondType) continue;
                            await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds, true);
                        }
                    }
                }
            }
            else
            {
                foreach (var line in lines)
                {
                    var procToNode = procs.FirstOrDefault(a => a.SchemeProcId == line.ToSchemeProcId);
                    if (procToNode == null)
                    {
                        #region 未找到下级节点, 异常处理直接结束流程

                        await ExecutEndProc(freeSql, approvalInfo.FlowId);
                        return;

                        #endregion
                    }
                    else
                    {
                        if (line.LineCondType == EnumLineCondType.CustDefine)//特殊处理自定义条件
                        {
                            if (await CondService.CheckCustLineCond(freeSql, para.Flow, line))//只递归条件通过的节点
                            {
                                await ExecutProc(freeSql, para, procIds, procToNode);
                            }
                        }
                        else if (line.LineCondType == EnumLineCondType.None)
                        {
                            await ExecutProc(freeSql, para, procIds, procToNode);
                        }
                        else
                        {
                            EnumLineCondType condType = EnumLineCondType.None;
                            switch (approvalInfo.Status)
                            {
                                case EnumProcApprovalStatus.None:
                                    condType = EnumLineCondType.None;
                                    break;
                                case EnumProcApprovalStatus.Agree:
                                    condType = EnumLineCondType.Agree;
                                    break;
                                case EnumProcApprovalStatus.DisAgree:
                                    condType = EnumLineCondType.DisAgree;
                                    break;
                                case EnumProcApprovalStatus.Termination:
                                    break;
                                case EnumProcApprovalStatus.BusinessDisAgree:
                                    condType = EnumLineCondType.Agree;
                                    break;
                            }
                            if (condType != line.LineCondType) continue;
                            await ExecutProc(freeSql, para, procIds, procToNode);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        private static async Task ExecProc(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc)
        {
            switch (proc.Type)
            {
                case EnumProcType.Normal:
                    await ExecutNormalProc(freeSql, para, proc);
                    break;
                case EnumProcType.Start:
                    await ExecutStartProc(freeSql, para, proc);
                    break;
                case EnumProcType.End:
                    await ExecutEndProc(freeSql, para, proc);
                    break;
                case EnumProcType.Branch:
                    await ExecutNotExecutedProc(freeSql, para, proc);
                    break;
            }
        }

        /// <summary>
        /// 执行下一环节入口
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="procIds"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static async Task ExecutProc(IFreeSql freeSql, ExecutProcPara para, List<string> procIds, ProcEntity proc)
        {
            //记录递归环节,如果已经递归过,则跳出
            if (procIds.Contains(proc.SchemeProcId)) return;
            else { procIds.Add(proc.SchemeProcId); }

            await ExecProc(freeSql, para, proc);
            //特定的停止环节
            if (new EnumProcType[] { EnumProcType.Normal, EnumProcType.End, EnumProcType.OA }.Contains(proc.Type)) return;
            //只有重新开始得流程才停止
            if (proc.Type == EnumProcType.Start && para.Flow.FlowStatus == EnumFlowStatus.ReStarted) return;

            var lines = await freeSql.Select<LineEntity>().Where(t => t.FlowId == para.Flow.Id && t.FromSchemeProcId == proc.SchemeProcId).ToListAsync();
            foreach (var line in lines)
            {
                var procToNode = await freeSql.Select<ProcEntity>().Where(t => t.FlowId == para.Flow.Id && t.SchemeProcId == line.ToSchemeProcId).ToOneAsync();
                if (procToNode == null) continue;
                //进入递归
                switch (line.LineCondType)
                {
                    case EnumLineCondType.None:
                        await ExecutProc(freeSql, para, procIds, procToNode);
                        break;
                    case EnumLineCondType.CustDefine:
                        if (await CondService.CheckCustLineCond(freeSql, para.Flow, line))//只递归条件通过的节点
                        {
                            await ExecutProc(freeSql, para, procIds, procToNode);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// 执行下一环节入口
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para">审批参数</param>
        /// <param name="procIds">已处理的所有节点Id,防止重复处理</param>
        /// <param name="proc">当前节点</param>
        /// <param name="allExecutors">该流程所有执行人</param>
        /// <returns></returns>
        public static async Task<bool> NewExecutProc(IFreeSql freeSql, ExecutProcPara para, List<string> procIds, ProcEntity proc, List<string> allExecutors, List<OpeateRecordAPIVM> opeateRecordAPIVMs, List<string> skipProcIds, bool isSelectExecutor = false)
        {
            #region 记录递归环节,如果已经递归过,则跳出

            if (procIds.Contains(proc.SchemeProcId)) return true;
            else { procIds.Add(proc.SchemeProcId); }

            #endregion

            #region 处理节点业务

            if (proc.Type == EnumProcType.Normal)
            {
                var isSkip = await FlowWorkListService.AddWorkerIntoList(freeSql, para, proc, await freeSql.Select<ExecutorEntity>().Where(t => t.ProcId == proc.Id && t.FlowId == para.Flow.Id).ToListAsync(),
                        allExecutors, opeateRecordAPIVMs, isSelectExecutor);
                if (isSkip)
                    skipProcIds.Add(proc.Id);
                else
                {
                    DateTime? endTime = null;
                    if (proc.LimtTime.HasValue && proc.LimtTime != 0) endTime = DateTime.Now.AddDays(proc.LimtTime.Value);
                    await freeSql.Update<ProcEntity>().Set(t => new ProcEntity()
                    {
                        ApprovalStatus = EnumProcApprovalStatus.None,
                        ProcStatus = EnumProcStatus.Handing,
                        BeginTime = DateTime.Now,
                        EndTime = endTime
                    }).Where(w => w.Id == proc.Id).ExecuteAffrowsAsync();
                    return true;
                }

                //特定的停止环节
                if (new EnumProcType[] { EnumProcType.End, EnumProcType.OA }.Contains(proc.Type)) return true;

                //只有重新开始得流程才停止
                if (proc.Type == EnumProcType.Start && para.Flow.FlowStatus == EnumFlowStatus.ReStarted) return true;

                var lines = await freeSql.Select<LineEntity>().Where(t => t.FlowId == para.Flow.Id && t.FromSchemeProcId == proc.SchemeProcId && t.LineCondType != EnumLineCondType.DisAgree).ToListAsync();
                foreach (var line in lines)
                {
                    var procToNode = await freeSql.Select<ProcEntity>().Where(t => t.FlowId == para.Flow.Id && t.SchemeProcId == line.ToSchemeProcId).ToOneAsync();
                    if (procToNode == null) continue;
                    //进入递归
                    switch (line.LineCondType)
                    {
                        case EnumLineCondType.None:
                            await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                            break;
                        case EnumLineCondType.CustDefine:
                            if (await CondService.CheckCustLineCond(freeSql, para.Flow, line))//只递归条件通过的节点
                            {
                                await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                            }
                            break;
                        case EnumLineCondType.Agree:
                            {
                                await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                            }
                            break;
                    }
                }
            }
            else
            {
                switch (proc.Type)
                {
                    case EnumProcType.Start:
                        await ExecutStartProc(freeSql, para, proc);
                        break;
                    case EnumProcType.End:
                        await ExecutEndProc(freeSql, para, proc);
                        break;
                    case EnumProcType.Branch:
                        await ExecutNotExecutedProc(freeSql, para, proc);
                        break;
                        //case EnumProcType.OA:
                        //    ExecutThirdOaProc(para, proc);
                        //    break;
                }

                //特定的停止环节
                if (new EnumProcType[] { EnumProcType.End, EnumProcType.OA }.Contains(proc.Type)) return true;

                //只有重新开始得流程才停止
                if (proc.Type == EnumProcType.Start && para.Flow.FlowStatus == EnumFlowStatus.ReStarted) return true;

                var lines = await freeSql.Select<LineEntity>().Where(t => t.FlowId == para.Flow.Id && t.FromSchemeProcId == proc.SchemeProcId && t.LineCondType != EnumLineCondType.Agree && t.LineCondType != EnumLineCondType.DisAgree).ToListAsync();

                foreach (var line in lines)
                {
                    var procToNode = await freeSql.Select<ProcEntity>().Where(t => t.FlowId == para.Flow.Id && t.SchemeProcId == line.ToSchemeProcId).ToOneAsync();
                    if (procToNode == null) continue;
                    //进入递归
                    switch (line.LineCondType)
                    {
                        case EnumLineCondType.None:
                            await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                            break;
                        case EnumLineCondType.CustDefine:
                            if (await CondService.CheckCustLineCond(freeSql, para.Flow, line))//只递归条件通过的节点
                            {
                                await NewExecutProc(freeSql, para, procIds, procToNode, allExecutors, opeateRecordAPIVMs, skipProcIds);
                            }
                            break;
                    }
                }
            }

            #endregion

            return true;
        }

        /// <summary>
        /// 执行开始环节
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static async Task ExecutStartProc(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc)
        {
            try
            {
                if (await IsRestartFlow(freeSql, para.Flow.Id)) //如果是重复指向开始环节
                {
                    para.Flow.FlowStatus = EnumFlowStatus.ReStarted;
                    await freeSql.Update<FlowEntity>().Set(a => new FlowEntity()
                    {
                        FlowStatus = EnumFlowStatus.ReStarted
                    }).Where(w => w.Id == para.Flow.Id).ExecuteAffrowsAsync();

                    await freeSql.Update<ProcEntity>().Set(a => new ProcEntity()
                    {
                        ApprovalStatus = EnumProcApprovalStatus.None,
                        ProcStatus = EnumProcStatus.Handing,
                        BeginTime = DateTime.Now,
                        ComplateTime = null,
                    }).Where(w => w.Id == proc.Id).ExecuteAffrowsAsync();

                    var exeCutors = await freeSql.Select<ExecutorEntity>().Where(t => t.ProcId == proc.Id && t.FlowId == para.Flow.Id).ToListAsync();
                    if (exeCutors == null || exeCutors.Count == 0) return;
                    await FlowWorkListService.AddWorkerIntoList(freeSql, para, proc, exeCutors);
                }
                else
                {
                    await freeSql.Update<ProcEntity>().Set(t => new ProcEntity()
                    {
                        ProcStatus = EnumProcStatus.Complete,
                        BeginTime = DateTime.Now,
                    }).Where(w => w.Id == proc.Id).ExecuteAffrowsAsync();

                    var wwUser = await IocManager.Instance.GetService<ICacheService>().GetUser(para.Flow.CreateUserId);
                    wwUser ??= new SysAccountEntity();

                    var exeId = Guid.NewGuid().ToString("N");
                    var liExes = new List<ExecutorEntity>() { new ExecutorEntity() {
                    Id=exeId,
                    FlowId=para.Flow.Id,
                    ProcId=proc.Id,
                    RefId=para.Flow.CreateUserId,
                    RefType=EnumExecutorRefType.Account,
                    RefName=para.Flow.CreateUserName,
                    Tenant=para.Flow.                    Tenant=para.Flow.Tenant,

                }};
                    var liWrokList = new List<WorkListEntity>() { new WorkListEntity() {
                    Id=Guid.NewGuid().ToString("N"),
                    AccountId=para.Flow.CreateUserId,
                    ActionCode=para.Flow.ActionCode,
                    AppId=para.Flow.AppId,
                    Tenant=para.Flow.Tenant,
                    ExecutorId=exeId,
                    ExecutorRefType=EnumExecutorRefType.Account,
                    FlowId=para.Flow.Id,
                    ProcId=proc.Id,
                    IsHandled=true,
                    IsNeed=true,
                    ApprovalStatus=EnumProcApprovalStatus.None,
                    ComplateTime=para.Flow.CreateDateTime,
                    StartTime=para.Flow.CreateDateTime
                }};
                    //构建开始环节执行人
                    await freeSql.Insert(liExes).ExecuteAffrowsAsync();
                    await freeSql.Insert(liWrokList).ExecuteAffrowsAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        ///  插入执行人并且推送消息
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static async Task ExecutNormalProc(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc)
        {
            //变更当前环节状态为待处理
            DateTime? endTime = null;
            if (proc.LimtTime.HasValue && proc.LimtTime != 0) endTime = DateTime.Now.AddDays(proc.LimtTime.Value);

            await freeSql.Update<FlowEntity>().Set(t => new FlowEntity()
            {
                FlowStatus = EnumFlowStatus.Executing
            }).Where(w => w.Id == para.Flow.Id).ExecuteAffrowsAsync();

            await freeSql.Update<ProcEntity>().Set(t => new ProcEntity()
            {
                ApprovalStatus = EnumProcApprovalStatus.None,
                ProcStatus = EnumProcStatus.Handing,
                BeginTime = DateTime.Now,
                ComplateTime = null,
                EndTime = endTime
            }).Where(w => w.Id == proc.Id).ExecuteAffrowsAsync();

            var exeCutors = await freeSql.Select<ExecutorEntity>().Where(t => t.ProcId == proc.Id && t.FlowId == para.Flow.Id).ToListAsync();
            if (exeCutors == null || exeCutors.Count == 0) return;
            await FlowWorkListService.AddWorkerIntoList(freeSql, para, proc, exeCutors);
        }

        /// <summary>
        /// 执行不带执行人环节
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static async Task ExecutNotExecutedProc(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc)
        {
            await freeSql.Update<FlowEntity>().Set(t => new FlowEntity()
            {
                FlowStatus = EnumFlowStatus.Executing
            }).Where(w => w.Id == para.Flow.Id).ExecuteAffrowsAsync();

            await freeSql.Update<ProcEntity>().Set(t => new ProcEntity()
            {
                ProcStatus = EnumProcStatus.Complete,
                BeginTime = DateTime.Now,
            }).Where(w => w.Id == proc.Id).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 执行结束环节
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flowId"></param>
        /// <returns></returns>
        public static async Task ExecutEndProc(IFreeSql freeSql, string flowId)
        {
            //流程结束触发环节状态完成
            await freeSql.Update<ProcEntity>().Set(t => new ProcEntity()
            {
                ProcStatus = EnumProcStatus.Complete
            }).Where(w => w.FlowId == flowId && new EnumProcStatus[2] { EnumProcStatus.Wait, EnumProcStatus.Handing }.Contains(w.ProcStatus)).ExecuteAffrowsAsync();
            //流程结束触发待执行工作完成
            await freeSql.Update<WorkListEntity>().Set(t => new WorkListEntity()
            {
                ComplateTime = DateTime.Now,
                IsHandled = true,
                CheckOpinion = "流程结束触发工作完成"
            }).Where(w => w.FlowId == flowId && w.IsNeed && !w.IsHandled).ExecuteAffrowsAsync();

            //流程归档
            await freeSql.Update<FlowEntity>().Set(t => new FlowEntity()
            {
                FlowStatus = EnumFlowStatus.Complete,
                EndTime = DateTime.Now
            }).Where(w => w.Id == flowId).ExecuteAffrowsAsync();

        }

        /// <summary>
        /// 执行结束环节
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="para"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static async Task ExecutEndProc(IFreeSql freeSql, ExecutProcPara para, ProcEntity proc)
        {
            //流程结束触发环节状态完成
            await freeSql.Update<ProcEntity>().Set(t => new ProcEntity()
            {
                ProcStatus = EnumProcStatus.Complete
            }).Where(w => w.FlowId == proc.FlowId && (w.ProcStatus == EnumProcStatus.Wait || w.ProcStatus == EnumProcStatus.Handing)).ExecuteAffrowsAsync();

            //流程结束触发待执行工作完成
            await freeSql.Update<WorkListEntity>().Set(t => new WorkListEntity()
            {
                ComplateTime = DateTime.Now,
                IsHandled = true,
                CheckOpinion = "流程结束触发工作完成"
            }).Where(w => w.FlowId == proc.FlowId && w.IsNeed == true && w.IsHandled == false).ExecuteAffrowsAsync();

            //流程归档
            await freeSql.Update<FlowEntity>().Set(t => new FlowEntity()
            {
                FlowStatus = EnumFlowStatus.Complete,
                EndTime = DateTime.Now
            }).Where(w => w.Id == proc.FlowId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 判断流程是否是重新开始
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flowId"></param>
        /// <returns></returns>
        private static async Task<bool> IsRestartFlow(IFreeSql freeSql, string flowId)
        {
            return await freeSql.Select<WorkListEntity>().Where(t => t.FlowId == flowId && freeSql.Select<ProcEntity>().Where(a => a.Type == EnumProcType.Start && a.Id == t.ProcId).Any()).CountAsync() > 1;
        }

        /// <summary>
        /// 知会
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="vm"></param>
        /// <returns></returns>
        public static async Task<BusinessResult> FlowInformer(IFreeSql freeSql, FlowProcInformerVM vm)
        {
            var lockKey = RedisKeyDto.FlowProcApprovalKey + vm.FlowId;
            try
            {
                if (await RedisManage.LockAsync(lockKey, 60, 60))
                {
                    var user = IocManager.Instance.GetService<ICurrentUser>();
                    var procId = await FlowWorkListService.GetCurrentProcIdByUserId(freeSql, vm.FlowId, user.UserId);
                    if (string.IsNullOrWhiteSpace(procId)) return BusinessResponse.Error(msg: "您没有权限，当前节点可能已经审批，请刷新后重试");

                    var flowEntity = await freeSql.Select<FlowEntity>().Where(a => a.Id == vm.FlowId).ToOneAsync();
                    if (flowEntity == null) return BusinessResponse.Error(msg: "流程不存在，请刷新重试");
                    var proc = await freeSql.Select<ProcEntity>().Where(a => a.Id == procId && a.FlowId == vm.FlowId).ToOneAsync();
                    if (proc == null) throw new Exception("当前节点不存在，请刷新重试");

                    await FlowBusinessService.CreateTask(new MsgTaskCreateDto
                    {
                        ReceiveAccIds = vm.InformerIds,
                        SourceId = flowEntity.AppId,
                        SourceTypeValue = flowEntity.SourceTypeValue,
                        Description = flowEntity.TaskDescription,
                        ExecuteUserId = user.UserId,
                        Type = EnumTaskTypeEunm.Audit,
                        SysType = 2,
                        ExecuteUserName = user.UserName,
                        TaskContent = flowEntity.TaskContent,
                        Title = flowEntity.TaskTitle,
                        OrgId = flowEntity.OrgId
                    });

                    var entity = await freeSql.Select<FlowInformerRecordEntity>().Where(a => a.ProcId == procId).ToOneAsync();
                    if (entity != null) await freeSql.Update<FlowInformerRecordEntity>().Set(a => new FlowInformerRecordEntity
                    {
                        Names = a.Names + "、" + string.Join(",", vm.InformerNames),
                        Ids = a.Ids + "、" + string.Join(",", vm.InformerIds),
                    }).Where(a => a.Id == entity.Id).ExecuteAffrowsAsync();
                    else await freeSql.Insert(new FlowInformerRecordEntity
                    {
                        FlowId = vm.FlowId,
                        ProcId = procId,
                        Names = string.Join(",", vm.InformerNames),
                        Ids = string.Join(",", vm.InformerIds),
                        SourceId = flowEntity.AppId,
                        SourceCode = flowEntity.ActionCode
                    }).ExecuteAffrowsAsync();
                }
            }
            finally
            {
                await RedisManage.DelLockAsync(lockKey);
            }

            return BusinessResponse.Success();
        }

        /// <summary>
        /// 更改处理人
        /// </summary>
        public static async Task<BusinessResult> FlowChangeProcExecutor(IFreeSql freeSql, FlowOperateProcVM vm)
        {
            var lockKey = RedisKeyDto.FlowProcApprovalKey + vm.FlowId;
            try
            {
                if (await RedisManage.LockAsync(lockKey, 60, 60))
                {
                    var user = IocManager.Instance.GetService<ICurrentUser>();
                    var procId = await FlowWorkListService.GetCurrentProcIdByUserId(freeSql, vm.FlowId, user.UserId);
                    if (string.IsNullOrWhiteSpace(procId)) return BusinessResponse.Error(msg: "您没有权限，当前节点可能已经审批，请刷新后重试");

                    var flowEntity = await freeSql.Select<FlowEntity>().Where(a => a.Id == vm.FlowId).ToOneAsync();
                    if (flowEntity == null) return BusinessResponse.Error(msg: "流程不存在，请刷新重试");
                    var proc = await freeSql.Select<ProcEntity>().Where(a => a.Id == procId && a.FlowId == vm.FlowId).ToOneAsync() ?? throw new Exception("当前节点不存在，请刷新重试");
                    var workListId = await freeSql.Select<WorkListEntity>().Where(t => t.ProcId == proc.Id && t.AccountId == user.UserId && t.FlowId == vm.FlowId && t.IsNeed && !t.IsHandled).ToOneAsync(a => a.Id);
                    if (string.IsNullOrWhiteSpace(workListId)) { throw new Exception("没有查询到相关执行人信息"); }

                    await freeSql.Update<WorkListEntity>().Set(a => a.AccountId == vm.Account.Id).Where(a => a.Id == workListId).ExecuteAffrowsAsync();
                    string str = user.UserName + "转处理给了" + vm.Account.Name;

                    await FlowBusinessService.AddOperateRecord(freeSql, new OpeateRecordAPIVM
                    {
                        Content = str,
                        Description = str,
                        OperateUserId = user.UserId,
                        OperateUserName = user.UserName,
                        DataId = flowEntity.AppId,
                        Type = "审批",
                        ProcId = proc.Id,
                        State = EnumOperateState.ChangeExecutor,
                        Title = "转处理"
                    });

                    await FlowBusinessService.ChangeTaskUser(new ChangeUserTaskDto
                    {
                        NewUserId = vm.Account.Id,
                        OldUserId = user.UserId,
                        SourceId = flowEntity.AppId,
                    });
                }
            }
            finally
            {
                await RedisManage.DelLockAsync(lockKey);
            }

            return BusinessResponse.Success();
        }

        /// <summary>
        /// 追加节点
        /// </summary>
        public static async Task<BusinessResult> AppendProc(IFreeSql freeSql, FlowOperateProcVM vm)
        {
            var lockKey = RedisKeyDto.FlowProcApprovalKey + vm.FlowId;
            try
            {
                if (await RedisManage.LockAsync(lockKey, 60, 60))
                {
                    var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;

                    var flowEntity = await freeSql.Select<FlowEntity>().Where(a => a.Id == vm.FlowId).ToOneAsync();
                    if (flowEntity == null) return BusinessResponse.Error(msg: "流程不存在，请刷新重试");

                    var procId = await FlowWorkListService.GetCurrentProcIdByUserId(freeSql, vm.FlowId, userId);
                    if (string.IsNullOrWhiteSpace(procId)) return BusinessResponse.Error(msg: "您没有权限，当前节点可能已经审批，请刷新后重试");

                    // 添加处理节点，节点连接线，修改排序，节点执行人
                    var currentProc = await freeSql.Select<ProcEntity>().Where(a => a.Id == procId).ToOneAsync();
                    if (currentProc != null)
                    {
                        if (currentProc.Type == EnumProcType.End) throw new Exception("【" + currentProc.Name + "】节点是结束节点，无法追加处理人!");
                    }
                    else throw new Exception("当前节点不存在!请重试!");

                    if (currentProc.IsSelectNextProc)
                        throw new Exception("【" + currentProc.Name + "】节点需要选择下级节点，暂时无法加在此节点后面!");

                    var newProcId = Guid.NewGuid().ToString("N");

                    var newLines = new List<LineEntity>
                      {
                          new LineEntity
                          {
                                 Id = Guid.NewGuid().ToString("N"),
                                 ActionCode = currentProc.ActionCode,
                                 Code = "AppendLine",
                                 FlowId = vm.FlowId,
                                 FromSchemeProcId = currentProc.SchemeProcId,
                                 ToSchemeProcId = newProcId,
                                 LineCondType = EnumLineCondType.Agree,
                                 Name = "同意"
                         }
                      };

                    var endProc = await freeSql.Select<ProcEntity>().Where(a => a.Type == EnumProcType.End && a.FlowId == vm.FlowId).ToOneAsync();

                    if (endProc != null)
                    {
                        newLines.Add(new LineEntity
                        {
                            Id = Guid.NewGuid().ToString("N"),
                            ActionCode = currentProc.ActionCode,
                            Code = "AppendLine",
                            FlowId = vm.FlowId,
                            FromSchemeProcId = newProcId,
                            ToSchemeProcId = endProc.SchemeProcId,
                            LineCondType = EnumLineCondType.DisAgree,
                            Name = "不同意"
                        });
                    }
                    else throw new Exception("当前流程不存在结束节点!属于异常流程，请联系管理员!");

                    var newLine = new LineEntity
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        ActionCode = currentProc.ActionCode,
                        Code = "AppendLine",
                        FlowId = vm.FlowId,
                        FromSchemeProcId = currentProc.SchemeProcId,
                        ToSchemeProcId = newProcId,
                        LineCondType = EnumLineCondType.Agree,
                        Name = "同意"
                    };

                    var newProc = new ProcEntity
                    {
                        Id = newProcId,
                        SchemeProcId = newProcId,
                        ActionCode = currentProc.ActionCode,
                        ApprovalStatus = EnumProcApprovalStatus.None,
                        Code = "AppendProc",
                        FlowId = vm.FlowId,
                        Sort = currentProc.Sort + 1,
                        IsAppendProc = true,
                        Name = vm.Account.Name + "【追加】",
                        ProcApprovalType = EnumProcApprovalType.OneApproval,
                        ProcExecutorType = EnumProcExecutorType.OneProc
                    };

                    var newExecutor = new ExecutorEntity
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        FlowId = vm.FlowId,
                        IsLimtOrgan = false,
                        ProcId = newProcId,
                        RefId = vm.Account.Id,
                        RefName = vm.Account.Name,
                        RefType = EnumExecutorRefType.Account
                    };

                    await freeSql.Update<ProcEntity>().Set(a => a.Sort == a.Sort + 1).Where(a => a.FlowId == vm.FlowId && a.Sort > currentProc.Sort).ExecuteAffrowsAsync();
                    await freeSql.Insert(newProc).ExecuteAffrowsAsync();
                    await freeSql.Update<LineEntity>().Set(a => a.FromSchemeProcId == newProc.SchemeProcId).Where(a => a.FromSchemeProcId == currentProc.SchemeProcId && a.FlowId == vm.FlowId
                         && a.LineCondType != EnumLineCondType.DisAgree).ExecuteAffrowsAsync();
                    await freeSql.Insert(newLines).ExecuteAffrowsAsync();
                    await freeSql.Insert(newExecutor).ExecuteAffrowsAsync();
                }
            }
            finally
            {
                await RedisManage.DelLockAsync(lockKey);
            }

            return BusinessResponse.Success();
        }
    }
}
