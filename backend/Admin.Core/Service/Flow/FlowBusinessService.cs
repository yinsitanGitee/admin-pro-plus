﻿using Admin.Common.Ioc;
using Admin.Common.Json;
using Admin.Core.Service.Flow.Dto;
using Admin.Core.Service.MsgTask;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Core.Service.Reminder;
using Admin.Core.Service.Reminder.Dto;
using Admin.Repository.Entities.Msg;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 
    /// </summary>
    public static class FlowBusinessService
    {
        /// <summary>
        /// 关闭系统代办
        /// </summary>
        /// <param name=""></param>
        /// <param name="vm"></param>
        public static async Task CloseTask(ExecuteTaskDto vm)
        {
            vm.Type = EnumTaskTypeEunm.Audit;
            await IocManager.Instance.GetService<IMsgTaskService>().CloseTask(vm);
        }

        /// <summary>
        /// 转处理人，修改待办
        /// </summary>
        /// <param name=""></param>
        /// <param name="vm"></param>
        public static async Task ChangeTaskUser(ChangeUserTaskDto vm)
        {
            vm.Type = EnumTaskTypeEunm.Audit;
            await IocManager.Instance.GetService<IMsgTaskService>().ChangeTaskUser(vm);
        }

        /// <summary>
        /// 创建待办
        /// </summary>
        /// <param name="vm"></param>
        public static async Task CreateTask(MsgTaskCreateDto vm)
        {
            vm.Type = EnumTaskTypeEunm.Audit;
            await IocManager.Instance.GetService<IMsgTaskService>().CreateTask(vm);
        }

        /// <summary>
        /// 关闭并创建待办
        /// </summary>
        /// <param name="taskCreateDto"></param>
        /// <param name="executeTaskDto"></param>
        public static async Task CloseAndCreateTask(MsgTaskCreateDto taskCreateDto, ExecuteTaskDto executeTaskDto)
        {
            taskCreateDto.Type = EnumTaskTypeEunm.Audit;
            executeTaskDto.Type = EnumTaskTypeEunm.Audit;
            await IocManager.Instance.GetService<IMsgTaskService>().CloseAndCreateTask(taskCreateDto, executeTaskDto);
        }

        /// <summary>
        /// 创建待阅任务
        /// </summary>
        /// <param name="taskCreateDto"></param>
        /// <param name="executeTaskDto"></param>
        public static async Task CreateReadTask(MsgTaskCreateDto taskCreateDto, ExecuteTaskDto executeTaskDto)
        {
            await IocManager.Instance.GetService<IMsgTaskService>().CloseAndCreateTask(taskCreateDto, executeTaskDto);
        }

        /// <summary>
        /// 关闭单人待办
        /// </summary>
        /// <param name="dto"></param>
        public static async Task CompleteTaskByUserId(DoneUserTaskDto dto)
        {
            await IocManager.Instance.GetService<IMsgTaskService>().CompleteTaskByUserId(dto);
        }

        /// <summary>
        /// 批量添加业务审批日志(单人执行)
        /// 如新增、提交、签约、变更、续签等等
        /// 无返回值，报错会直接抛出
        /// </summary>
        /// <param name="freeSql">cmpCode</param>
        /// <param name="vms">操作日志详细信息</param>
        /// <returns></returns>
        public static async Task BatchAddOperateRecord(IFreeSql freeSql, List<OpeateRecordAPIVM> vms)
        {
            if (vms == null || vms.Count == 0) return;

            var operateRecords = new List<OperateRecordEntity>();
            var operateRecordexecutors = new List<OperateRecordExecutorEntity>();
            foreach (var vm in vms)
            {
                var id = Guid.NewGuid().ToString("N");
                operateRecords.Add(new OperateRecordEntity
                {
                    Id = id,
                    DataId = vm.DataId,
                    Description = vm.Description,
                    ProcId = vm.ProcId,
                    State = vm.State,
                    Type = vm.Type,
                    CreateDateTime = vm.OperateDate,
                    Title = vm.Title
                });

                operateRecordexecutors.Add(new OperateRecordExecutorEntity
                {
                    OperateDate = vm.OperateDate,
                    OperateUserId = vm.OperateUserId,
                    OperateUserName = vm.OperateUserName,
                    Content = vm.Content,
                    State = vm.State,
                    DataId = vm.DataId,
                    RecordId = id
                });
            }

            await freeSql.Insert(operateRecords).ExecuteAffrowsAsync();
            await freeSql.Insert(operateRecordexecutors).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 如新增、提交、签约、变更、续签等等
        /// 无返回值，报错会直接抛出
        /// </summary>
        /// <param name="freeSql">cmpCode</param>
        /// <param name="vms">操作日志详细信息</param>
        /// <returns></returns>
        public static async Task AddOperateRecord(IFreeSql freeSql, OpeateRecordAPIVM vm)
        {
            var id = Guid.NewGuid().ToString("N");
            var operateRecord = new OperateRecordEntity
            {
                Id = id,
                DataId = vm.DataId,
                Description = vm.Description,
                CreateDateTime = vm.OperateDate,
                ProcId = vm.ProcId,
                State = vm.State,
                Type = vm.Type,
                Title = vm.Title
            };
            var operateRecordexecutor = new OperateRecordExecutorEntity
            {
                OperateDate = vm.OperateDate,
                OperateUserId = vm.OperateUserId,
                OperateUserName = vm.OperateUserName,
                Content = vm.Content,
                State = vm.State,
                DataId = vm.DataId,
                RecordId = id
            };

            await freeSql.Insert(operateRecord).ExecuteAffrowsAsync();
            await freeSql.Insert(operateRecordexecutor).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 添加数据(多人执行)
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="vm"></param>
        public static async Task BatchAddOperateExecutorRecord(IFreeSql freeSql, OpeateRecordMultiExecutorAPIVM vm)
        {
            if (vm.Executors == null || vm.Executors.Count == 0)
            {
                throw new Exception("执行人不能为空!");
            }

            var operateRecord = new OperateRecordEntity
            {
                Id = Guid.NewGuid().ToString("N"),
                Description = vm.Description,
                CreateDateTime = vm.OperateDate,
                DataId = vm.DataId,
                ProcId = vm.ProcId,
                Type = vm.Type,
                Title = vm.Title
            };
            var executors = new List<OperateRecordExecutorEntity>();
            foreach (var item in vm.Executors)
            {
                executors.Add(new OperateRecordExecutorEntity
                {
                    Content = item.CheckOpinion,
                    OperateDate = item.ComplateTime,
                    OperateUserId = item.AccountId,
                    OperateUserName = item.Name,
                    RecordId = operateRecord.Id,
                    State = item.State,
                    DataId = vm.DataId,
                });
            }

            #region 处理记录主表 State

            operateRecord.State = executors.Where(a => a.State == EnumOperateState.DisAgree).Any() ? EnumOperateState.DisAgree :
                executors.Where(a => a.State == EnumOperateState.BusinessDisAgree).Any() ? EnumOperateState.BusinessDisAgree : EnumOperateState.Agree;

            #endregion

            await freeSql.Insert(operateRecord).ExecuteAffrowsAsync();
            await freeSql.Insert(executors).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 执行发送归档消息
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flow"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static async Task ExecutAfterFlowComplete(IFreeSql freeSql, FlowEntity flow, EnumProcApprovalStatus state)
        {
            if (flow.SendMessageToExcuterType == EnumSendMessageToExcuterType.None) return;
            var users = await GetSendMsgUserIds(freeSql, flow);
            if (users == null || users.Count == 0) return;
            var userIds = users.Select(t => t.UserId).ToList();
            var remain = new ReminderDto()
            {
                SourceTypeValue = flow.SourceTypeValue,
                ContentType = EnumReminderContentType.Mesage,
                Id = Guid.NewGuid().ToString("N"),
                MsgId = Guid.NewGuid().ToString("N"),
                OpenType = EnumReminderOpenType.Detail,
                RemainType = EnumReminderType.Normal,
                Title = string.Empty,
                ActionCode = flow.ActionCode,
                AppId = flow.AppId,
            };

            remain.Title = @$"{flow.TaskTitle}:{flow.AppCode}{(state == EnumProcApprovalStatus.Agree ? "已通过" : "未通过") + "审批  "}";
            remain.Content = flow.TaskDescription;
            await IocManager.Instance.GetService<IReminderService>().CreateReminder(remain, userIds);
        }

        /// <summary>
        /// 获取发送配置
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="flow"></param>
        /// <returns></returns>
        private static async Task<List<SelectExcuterDto>?> GetSendMsgUserIds(IFreeSql freeSql, FlowEntity flow)
        {
            switch (flow.SendMessageToExcuterType)
            {
                case EnumSendMessageToExcuterType.SelectUser:
                    return JsonHelper.DeserializeObject<List<SelectExcuterDto>>(flow.SendMessageSelectUsers);
                case EnumSendMessageToExcuterType.FlowExcutor:
                    var wls = await freeSql.Select<WorkListEntity>().Where(t => t.FlowId == flow.Id && new List<EnumProcApprovalStatus> { EnumProcApprovalStatus.Agree, EnumProcApprovalStatus.BusinessDisAgree, EnumProcApprovalStatus.DisAgree }.Contains(t.ApprovalStatus))
                        .ToListAsync();
                    if (wls == null || wls.Count == 0) return new List<SelectExcuterDto>();
                    var li2 = wls.Select(t => new SelectExcuterDto()
                    {
                        UserId = t.AccountId
                    }).ToList();
                    return li2;
                case EnumSendMessageToExcuterType.FlowAllExcutor:
                    return await freeSql.Select<WorkListEntity>().Where(t => t.FlowId == flow.Id).ToListAsync(a => new SelectExcuterDto
                    {
                        UserId = a.AccountId
                    });
                case EnumSendMessageToExcuterType.None:
                default:
                    return null;
            }
        }
    }
}
