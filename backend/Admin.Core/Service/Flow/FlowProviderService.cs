﻿using Admin.Common.Ioc;
using Admin.Common.Log;
using Admin.Core.Service.Flow.Dto;
using Admin.Repository.Entities.Wfs;
using DynamicExpresso;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// ProviderService
    /// </summary>
    public static class FlowProviderService
    {
        /// <summary>
        ///  获取数据源
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="actCode"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static async Task<List<FlowField>?> GetFlowFields(IFreeSql freeSql, string actCode, FlowDataProviderDto arg)
        {
            try
            {
                var act = await freeSql.Select<ActionEntity>().Where(t => t.Code == actCode).ToOneAsync() ?? throw new Exception("未找到编号为[" + actCode + "]的业务标识");
                if (string.IsNullOrWhiteSpace(act.FlowEvent)) return new List<FlowField>();
                return await IocManager.Instance.GerSerciceByOptiona<IFlowEvent>(act.FlowEvent).GetDetailForWorkFlow(arg);
            }
            catch (Exception ex)
            {
                if (ex is NotImplementedException) return new List<FlowField>();
                throw;
            }
        }

        /// <summary>
        /// 计算表达式
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="actionCode">接口标识</param>
        /// <param name="expStr">表达式</param>
        /// <param name="arg">表达式</param>
        /// <returns></returns>
        public static async Task<bool> TrialCalcula(IFreeSql freeSql, string actionCode, string expStr, FlowDataProviderDto arg)
        {
            var liFields = await GetFlowFields(freeSql, actionCode, arg);
            if (liFields == null) return false;

            foreach (var item in liFields)
                expStr = expStr.Replace(item.FieldName, item.Value);

            try
            {
                return new Interpreter().ParseAsDelegate<Func<bool>>(expStr)();
            }
            catch (Exception ex)
            {
                $"审批流表达式计算异常，ex:{ex}".Loger("FlowError");
                return false;
            }
        }
    }
}
