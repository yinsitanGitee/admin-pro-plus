﻿using Admin.Core.Service.Flow.Dto;

namespace Admin.Core.Service.Flow
{
    /// <summary>
    /// 流程事件
    /// </summary>
    public interface IFlowEvent
    {
        /// <summary>
        /// 流程数据源获取接口 注意: 1. 不能返回空值,不能抛异常 2. 如果id为空,必须返回数据源的所有字段 3.工作流高度依赖此接口,请务必注意执行性能
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        Task<List<FlowField>?>? GetDetailForWorkFlow(FlowDataProviderDto e);

        /// <summary>
        ///  下一环节创建完成后触发接口 事件执行顺序:IProcEvent.AfterCurrentProcExecuted>IProcEvent.AfterNextProcCreated>IFlowEvent.EndFlowEvent
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        Task AfterNextProcCreated(AfterNextProcCreatedDto e);

        /// <summary>
        ///  流程执行完成
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        Task EndFlowEvent(EndFlowEventDto e);

        /// <summary>
        /// 流程主动终止
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        Task InitiativeAbandonFlow(FlowDataProviderDto e);
    }
}
