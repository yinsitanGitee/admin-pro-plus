﻿namespace Admin.Core.Service.AutoCode
{
    /// <summary>
    /// 单据编号服务
    /// </summary>
    public interface IAutoCodeService
    {
        /// <summary>
        /// 获取编号缓存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<string> GetBillCode(string code);
    }
}
