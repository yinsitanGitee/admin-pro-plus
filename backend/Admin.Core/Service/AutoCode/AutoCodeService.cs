﻿using Admin.Common.Ioc;
using Admin.Repository.Entities.Sys;
using Admin.Cache;
using Admin.Repository.Enum;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using System.Text;

namespace Admin.Core.Service.AutoCode
{
    /// <summary>
    /// 单据编号服务
    /// </summary>
    public class AutoCodeService : IAutoCodeService
    {
        /// <summary>
        /// 获取编号缓存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<string> GetBillCode(string code)
        {
            var lockKey = RedisKeyDto.BillCodeLockKey + code;
            try
            {
                if (await RedisManage.LockAsync(lockKey, 60, 60))
                {
                    var frsql = IocManager.Instance.GetService<IFreeSql>();
                    var e = await frsql.Select<SysBillCodeEntity>().Where(a => a.Code == code)
                         .ToOneAsync();
                    if (e == null) return string.Empty;

                    var d = await IocManager.Instance.GetService<ICacheService>().GetBillCodeDetails(e.Id);
                    if (d == null || d.Count == 0) return string.Empty;

                    StringBuilder value = new();
                    foreach (var item in d)
                    {
                        switch (item.Type)
                        {
                            case BillCodeDetailTypeEnum.Cust:
                                value.Append(item.Value);
                                break;
                            case BillCodeDetailTypeEnum.Date:
                                value.Append(DateTime.Now.ToString(item.Value));
                                break;
                        }
                    }

                    var v = value.ToString() + e.SeedValue;
                    if (e.Prefix != v)
                    {
                        e.Prefix = v;
                        if (e.CurrentNum != 0) e.CurrentNum = 0;
                    }

                    ++e.CurrentNum;
                    value.Append(e.CurrentNum.ToString(e.SeedValue));

                    await frsql.Update<SysBillCodeEntity>().Set(a => new SysBillCodeEntity
                    {
                        CurrentNum = e.CurrentNum,
                        Prefix = e.Prefix
                    }).Where(a => a.Id == e.Id).ExecuteAffrowsAsync();

                    return value.ToString();
                }

                return string.Empty;
            }
            catch
            {
                throw;
            }
            finally
            {
                await RedisManage.DelLockAsync(lockKey);
            }
        }
    }
}
