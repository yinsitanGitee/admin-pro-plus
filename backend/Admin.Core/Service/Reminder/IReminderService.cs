﻿using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.Reminder.Dto;
using Admin.Repository.Page;

namespace Admin.Core.Service.Reminder
{
    /// <summary>
    /// 消息通知
    /// </summary>
    public interface IReminderService
    {
        /// <summary>
        /// 创建消息提醒
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="userIds"></param>
        /// <returns></returns>
        Task CreateReminder(ReminderDto dto, List<string> userIds);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ReminderListDto> Get(InputDto dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<ReminderListDto>> Query(GetReminderInputDto dto);

        /// <summary>
        /// 设置已读
        /// </summary>
        /// <returns></returns>
        Task<BusinessResult> SetRead(InputDto dto);
    }
}
