﻿using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core.Auth;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Service.Reminder.Dto;
using Admin.Repository.Entities.Msg;
using Admin.Repository.Page;
using Mapster;

namespace Admin.Core.Service.Reminder
{
    /// <summary>
    /// 消息提醒
    /// </summary>
    /// <param name="freeSql"></param>
    public class ReminderService(IFreeSql freeSql) : IReminderService
    {
        /// <summary>
        /// 创建消息提醒
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public async Task CreateReminder(ReminderDto dto, List<string> userIds)
        {
            if (userIds == null || userIds.Count == 0) return;
            userIds = userIds.Distinct().ToList();
            var li = userIds.Select(t =>
            {
                var re = dto.Adapt<ReminderEntity>();
                re.AccountId = t;
                re.Id = Guid.NewGuid().ToString("N");
                re.CreateTime = DateTime.Now;
                return re;
            }).ToList();

            await freeSql.Insert(li).ExecuteAffrowsAsync();
            await RedisManage.DelAsync(userIds.Select(a => RedisKeyDto.MsgCountKey + a).ToArray());
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ReminderListDto> Get(InputDto dto)
        {
            return await freeSql.Select<ReminderEntity>()
                  .Where(a => a.Id == dto.Id).ToOneAsync<ReminderListDto>();
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<ReminderListDto>> Query(GetReminderInputDto dto)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            var r = freeSql.Select<ReminderEntity>()
                .Where(a => a.AccountId == userId)
                .WhereIf(dto.IsRead.HasValue, t => t.IsRead == dto.IsRead)
                .WhereIf(dto.ContentType.HasValue, t => t.ContentType == dto.ContentType)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.Content), t => t.Content.Contains(dto.Content));

            return new CommonPageOutputDto<ReminderListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderBy(x => x.CreateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<ReminderListDto>()
            };
        }

        /// <summary>
        /// 设置已读
        /// </summary>
        /// <returns></returns>
        public async Task<BusinessResult> SetRead(InputDto dto)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;

            await freeSql.Update<ReminderEntity>().Set(a => new ReminderEntity
            {
                IsRead = true,
                ReadTime = DateTime.Now
            }).Where(a => a.Id == dto.Id).ExecuteAffrowsAsync();

            await RedisManage.DelAsync(RedisKeyDto.ReminderKey + userId);
            return BusinessResponse.Success();
        }
    }
}