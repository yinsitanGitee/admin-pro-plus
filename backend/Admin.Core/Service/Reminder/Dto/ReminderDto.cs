﻿using Admin.Repository.Entities.Msg;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Reminder.Dto
{
    public class ReminderDto
    {
        /// <summary>
        /// 任务来源类别  
        /// </summary>
        public MsgTaskTypeEnum? SourceTypeValue { get; set; }
        //
        // 摘要:
        //     主键标识(Guid) 非必传
        public string Id { get; set; }

        //
        // 摘要:
        //     标题
        public string Title { get; set; }

        //
        // 摘要:
        //     提醒内容
        public string Content { get; set; }

        //
        // 摘要:
        //     关联链接
        public string Url { get; set; }

        //
        // 摘要:
        //     消息Id
        public string MsgId { get; set; }

        //
        // 摘要:
        //     打开方式
        public EnumReminderOpenType OpenType { get; set; }

        //
        // 摘要:
        //     提醒类型
        public EnumReminderType RemainType { get; set; }

        //
        // 摘要:
        //     内容类型
        public EnumReminderContentType ContentType { get; set; }

        public string ActionCode { get; set; }

        public string AppId { get; set; }
    }
}
