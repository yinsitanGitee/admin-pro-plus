﻿using Admin.Repository.Dto;
using Admin.Repository.Entities.Msg;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Reminder.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class ReminderListDto : BaseDto
    {
        /// <summary>
        /// 任务来源类别  
        /// </summary>
        public MsgTaskTypeEnum? SourceTypeValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 打开类型
        /// </summary>
        public EnumReminderOpenType OpenType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumReminderType RemainType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumReminderContentType ContentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ReadTime { get; set; }

        /// <summary>
        ///  消息Id
        ///  允许为空,业务消息关联字段
        /// </summary>
        public string MsgId { get; set; }

        /// <summary>
        ///  SourceId
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        ///  1是待办的过期提醒 
        /// </summary>
        public int SourceType { get; set; }

        /// <summary>
        /// 业务编号
        /// </summary>
        public string ActionCode { get; set; }

        /// <summary>
        ///业务Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
