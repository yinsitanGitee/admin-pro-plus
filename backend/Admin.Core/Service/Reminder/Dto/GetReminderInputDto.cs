﻿using Admin.Repository.Entities.Msg;
using Admin.Repository.Page;

namespace Admin.Core.Service.Reminder.Dto
{
    /// <summary>
    /// 定义获取列表输入dto
    /// </summary>
    public class GetReminderInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 是否已读
        /// </summary>
        public bool? IsRead { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public EnumReminderContentType? ContentType { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
    }
}
