﻿using Admin.Mq;
using Admin.Mq.Integration;
using Admin.Repository.Tenant;

namespace Admin.Core.Service.Queue
{
    /// <summary>
    /// 定义队列服务
    /// </summary>
    /// <param name="rabbitProducerFactory"></param>
    public class QueueService(IRabbitProducerFactory rabbitProducerFactory) : IQueueService
    {
        /// <summary>
        /// AddSimpleQueue
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        public void AddSimpleQueue(string queueName, string[] message, Dictionary<string, object>? headers = null, bool Persistent = true)
        {
            var producer = rabbitProducerFactory.Create("SimplePattern");
            headers = GetHeaderBase(headers);
            producer.Publish(message, queueName, new MessageOptions() { Headers = headers, Persistent = Persistent });
        }

        /// <summary>
        /// AddSimpleQueue
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        public void AddSimpleQueue(string queueName, string message, Dictionary<string, object>? headers = null, bool Persistent = true)
        {
            var producer = rabbitProducerFactory.Create("SimplePattern");
            headers = GetHeaderBase(headers);
            producer.Publish(message, queueName, new MessageOptions() { Headers = headers, Persistent = Persistent });
        }

        /// <summary>
        /// AddSimpleQueueAsync
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        public async Task AddSimpleQueueAsync(string queueName, string[] message, Dictionary<string, object>? headers = null, bool Persistent = true)
        {
            var producer = rabbitProducerFactory.Create("SimplePattern");
            headers = GetHeaderBase(headers);
            await producer.PublishAsync(message, queueName, new MessageOptions() { Headers = headers, Persistent = Persistent });
        }

        /// <summary>
        /// AddSimpleQueueAsync
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        public async Task AddSimpleQueueAsync(string queueName, string message, Dictionary<string, object>? headers = null, bool Persistent = true)
        {
            var producer = rabbitProducerFactory.Create("SimplePattern");
            headers = GetHeaderBase(headers);
            await producer.PublishAsync(message, queueName, new MessageOptions() { Headers = headers, Persistent = Persistent });
        }

        /// <summary>
        /// 设置请求头当前租户
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        private Dictionary<string, object>? GetHeaderBase(Dictionary<string, object>? headers = null)
        {
            headers ??= [];
            headers.Add("Tenant", TenantManager.Current);
            return headers;
        }
    }
}
