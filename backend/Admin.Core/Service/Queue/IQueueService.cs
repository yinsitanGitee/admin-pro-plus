﻿namespace Admin.Core.Service.Queue
{
    /// <summary>
    /// 定义队列服务
    /// </summary>
    public interface IQueueService
    {
        /// <summary>
        /// 添加简单消息
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        void AddSimpleQueue(string queueName, string[] message, Dictionary<string, object>? headers = null, bool Persistent = true);

        /// <summary>
        /// 添加简单消息
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        void AddSimpleQueue(string queueName, string message, Dictionary<string, object>? headers = null, bool Persistent = true);

        /// <summary>
        /// 添加简单消息
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        Task AddSimpleQueueAsync(string queueName, string[] message, Dictionary<string, object>? headers = null, bool Persistent = true);

        /// <summary>
        /// 添加简单消息
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <param name="headers"></param>
        /// <param name="Persistent"></param>
        Task AddSimpleQueueAsync(string queueName, string message, Dictionary<string, object>? headers = null, bool Persistent = true);
    }
}
