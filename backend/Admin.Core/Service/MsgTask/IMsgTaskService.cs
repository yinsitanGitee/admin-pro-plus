﻿using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Repository.Page;

namespace Admin.Core.Service.MsgTask
{
    /// <summary>
    /// 待办任务
    /// </summary>
    public interface IMsgTaskService
    {
        /// <summary>
        /// 创建待办
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task CreateTask(MsgTaskCreateDto dto);

        /// <summary>
        /// 关闭待办
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task CloseTask(ExecuteTaskDto dto);

        /// <summary>
        /// 关闭并创建待办
        /// </summary>
        /// <param name="taskCreateDto"></param>
        /// <param name="executeTaskDto"></param>
        /// <returns></returns>
        Task CloseAndCreateTask(MsgTaskCreateDto taskCreateDto, ExecuteTaskDto executeTaskDto);

        /// <summary>
        /// 关闭单人待办
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task CompleteTaskByUserId(DoneUserTaskDto dto);

        /// <summary>
        /// 更改待办处理人
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task ChangeTaskUser(ChangeUserTaskDto dto);

        /// <summary>
        /// 查询我的待办
        /// </summary>
        /// <returns></returns>
        Task<List<MsgTaskListDto>?> QueryMyWaitTask(int count);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<MsgTaskListDto>> Query(GetMsgTaskInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<MsgTaskListDto>> Get(InputDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 设置已阅
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> SetRead(InputDto dto);
    }
}
