﻿using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core.Auth;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Core.SysSignalR.BPro;
using Admin.Repository.Entities.Msg;
using Admin.Repository.Page;
using Microsoft.AspNetCore.SignalR;

namespace Admin.Core.Service.MsgTask
{
    /// <summary>
    /// 待办任务
    /// </summary>
    /// <param name="freeSql"></param>
    public class MsgTaskService(IFreeSql freeSql) : IMsgTaskService
    {
        /// <summary>
        /// 创建待办
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task CreateTask(MsgTaskCreateDto dto)
        {
            if (dto.ReceiveAccIds == null || dto.ReceiveAccIds.Count == 0) return;
            var msgTask = new MsgTaskEntity()
            {
                Id = Guid.NewGuid().ToString("N"),
                TaskContent = dto.TaskContent,
                Description = dto.Description,
                OrgId = dto.OrgId,
                SendDate = dto.SendDate ?? DateTime.Now,
                Type = dto.Type,
                Title = dto.Title,
                SysType = dto.SysType,
                State = 0,
                SourceId = dto.SourceId,
                SourceTypeValue = dto.SourceTypeValue,
                SendUserName = dto.ExecuteUserName,
                SendUserId = dto.ExecuteUserId,
            };

            List<MsgTaskUserEntity> msgTaskUsers = [];

            foreach (var item in dto.ReceiveAccIds)
            {
                msgTaskUsers.Add(new MsgTaskUserEntity
                {
                    TaskId = msgTask.Id,
                    ReceiveDate = DateTime.Now,
                    State = 0,
                    UserId = item,
                });
            }
            await freeSql.Insert(msgTask).ExecuteAffrowsAsync();
            await freeSql.Insert(msgTaskUsers).ExecuteAffrowsAsync();
            if (dto.SysType == 1)
            {
                var user = BHubState.Users.Where(a => dto.ReceiveAccIds.Contains(a.UserId)).Select(a => a.ConnectionID).ToList();
                if (user.Count > 0)
                    await IocManager.Instance.GetService<IHubContext<BMessageHub>>().Clients.Clients(user).SendAsync("msgTaskCreate", dto);
            }

            await RedisManage.DelAsync(dto.ReceiveAccIds.Select(a => RedisKeyDto.MsgCountKey + a).ToArray());
        }

        /// <summary>
        /// 关闭待办
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task CloseTask(ExecuteTaskDto dto)
        {
            List<string> taskIds = [];
            if (dto.Type.HasValue && dto.Type == EnumTaskTypeEunm.Audit)
                taskIds = await freeSql.Select<MsgTaskEntity>().Where(a => a.SourceId == dto.SourceId && a.SysType == 1 && a.Type == dto.Type).ToListAsync(a => a.Id);
            else
                taskIds = await freeSql.Select<MsgTaskEntity>().Where(a => a.SourceId == dto.SourceId && a.SysType == 1).ToListAsync(a => a.Id);

            if (taskIds.Count == 0) return;
            var users = await freeSql.Select<MsgTaskUserEntity>().Where(a => taskIds.Contains(a.TaskId) && a.State == 0).ToListAsync(a => new { a.UserId, a.Id });

            await freeSql.Update<MsgTaskEntity>().Set(a => new
            {
                State = dto.Status,
                CompleteUserId = dto.ExecuteUserId,
                CompleteTime = DateTime.Now,
                CompleteUserName = dto.ExecuteUserName
            }).Where(a => taskIds.Contains(a.Id)).ExecuteAffrowsAsync();

            var curr = users.Where(a => a.UserId == dto.ExecuteUserId).Select(a => a.Id).Distinct().ToList();
            await freeSql.Update<MsgTaskUserEntity>().Set(a => new
            {
                State = 3,
                CompleteTime = DateTime.Now
            }).Where(a => curr.Contains(a.Id)).ExecuteAffrowsAsync();

            var noCurr = users.Where(a => a.UserId != dto.ExecuteUserId).Select(a => a.Id).Distinct().ToList();
            await freeSql.Update<MsgTaskUserEntity>().Set(a => new
            {
                State = 4,
                CompleteTime = DateTime.Now
            }).Where(a => noCurr.Contains(a.Id)).ExecuteAffrowsAsync();

            await RedisManage.DelAsync(users.Select(a => RedisKeyDto.MsgCountKey + a.UserId).ToArray());
        }

        /// <summary>
        /// 关闭并创建待办
        /// </summary>
        /// <param name="taskCreateDto"></param>
        /// <param name="executeTaskDto"></param>
        /// <returns></returns>
        public async Task CloseAndCreateTask(MsgTaskCreateDto taskCreateDto, ExecuteTaskDto executeTaskDto)
        {
            await CloseTask(executeTaskDto);
            await CreateTask(taskCreateDto);
        }

        /// <summary>
        /// 关闭单人待办
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task CompleteTaskByUserId(DoneUserTaskDto dto)
        {
            var tsids = await freeSql.Select<MsgTaskEntity>().Where(b => b.SourceId == dto.SourceId && b.State == 0).ToListAsync(a => a.Id);
            if (tsids.Count > 0)
                await freeSql.Update<MsgTaskUserEntity>().Set(a => new
                {
                    State = 3,
                    CompleteTime = DateTime.Now
                }).Where(a => tsids.Contains(a.TaskId) && a.UserId == dto.CurrentUserId).ExecuteAffrowsAsync();
            await RedisManage.DelAsync(RedisKeyDto.MsgCountKey + dto.CurrentUserId);
        }

        /// <summary>
        /// 更改待办处理人
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task ChangeTaskUser(ChangeUserTaskDto dto)
        {
            List<string> taskIds = [];
            if (dto.Type == EnumTaskTypeEunm.Audit)
                taskIds = await freeSql.Select<MsgTaskEntity>().Where(a => a.SourceId == dto.SourceId && a.SysType == 1 && a.Type == dto.Type).ToListAsync(a => a.Id);
            else
                taskIds = await freeSql.Select<MsgTaskEntity>().Where(a => a.SourceId == dto.SourceId && a.SysType == 1).ToListAsync(a => a.Id);

            await freeSql.Update<MsgTaskUserEntity>().Set(a => new
            {
                UserId = dto.NewUserId,
            }).Where(a => taskIds.Contains(a.TaskId) && a.UserId == dto.OldUserId).ExecuteAffrowsAsync();

            await RedisManage.DelAsync([RedisKeyDto.MsgCountKey + dto.OldUserId, RedisKeyDto.MsgCountKey + dto.NewUserId]);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<MsgTaskListDto>> Query(GetMsgTaskInputDto dto)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            var r = freeSql.Select<MsgTaskEntity, MsgTaskUserEntity>().InnerJoin(a => a.t1.Id == a.t2.TaskId)
                .Where(a => a.t2.UserId == userId)
                .WhereIf(dto.State.HasValue, t => t.t2.State == dto.State)
                .WhereIf(dto.SysType.HasValue, t => t.t1.SysType == dto.SysType)
                .WhereIf(!string.IsNullOrEmpty(dto.Title), t => t.t1.Title.Contains(dto.Title))
                .WhereIf(!string.IsNullOrEmpty(dto.TaskContent), t => t.t1.TaskContent.Contains(dto.TaskContent))
                .WhereIf(!string.IsNullOrEmpty(dto.Description), t => t.t1.Description.Contains(dto.Description));

            return new CommonPageOutputDto<MsgTaskListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderBy(x => x.t2.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new MsgTaskListDto
                {
                    CompleteTime = a.t2.CompleteTime,
                    State = a.t2.State
                })
            };
        }

        /// <summary>
        /// 查询我的待办
        /// </summary>
        /// <returns></returns>
        public async Task<List<MsgTaskListDto>?> QueryMyWaitTask(int count)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            return await freeSql.Select<MsgTaskEntity, MsgTaskUserEntity>().LeftJoin(a => a.t1.Id == a.t2.TaskId).Where(a => a.t2.UserId == userId && a.t2.State == 0 && a.t1.State == 0).OrderByDescending(a => a.t1.SendDate).Page(1, count).ToListAsync<MsgTaskListDto>();
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<MsgTaskListDto>> Get(InputDto dto)
        {
            return BusinessResponse<MsgTaskListDto>.Success(data: await freeSql.Select<MsgTaskEntity>().Where(a => a.Id == dto.Id).ToOneAsync<MsgTaskListDto>());
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await freeSql.Delete<MsgTaskEntity>().Where(a => dto.Ids.Contains(a.Id)).ExecuteAffrowsAsync();
            await freeSql.Delete<MsgTaskUserEntity>().Where(a => dto.Ids.Contains(a.TaskId)).ExecuteAffrowsAsync();
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 设置已阅
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> SetRead(InputDto dto)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            await freeSql.Update<MsgTaskUserEntity>().Set(a => new MsgTaskUserEntity { State = 3, CompleteTime = DateTime.Now }).Where(a => a.TaskId == dto.Id && a.UserId == userId).ExecuteAffrowsAsync();
            return BusinessResponse.Success();
        }
    }
}
