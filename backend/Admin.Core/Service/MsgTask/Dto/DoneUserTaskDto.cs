﻿using Admin.Repository.Entities.Msg;

namespace Admin.Core.Service.MsgTask.Dto
{
    /// <summary>
    /// DoneUserTaskDto
    /// </summary>
    public class DoneUserTaskDto
    {
        /// <summary>
        /// 代办来源Id
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 执行人名称
        /// </summary>
        public string CurrentUserId { get; set; }
    }

    /// <summary>
    /// 转处理人
    /// </summary>
    public class ChangeUserTaskDto
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public string OldUserId { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public string NewUserId { get; set; }

        /// <summary>
        /// SourceId
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public EnumTaskTypeEunm Type { get; set; }
    }
}
