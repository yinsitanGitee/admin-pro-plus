﻿using Admin.Repository.Entities.Msg;
using Admin.Repository.Enum;

namespace Admin.Core.Service.MsgTask.Dto
{
    /// <summary>
    /// MsgTaskCreateDto
    /// </summary>
    public class MsgTaskCreateDto
    {
        /// <summary>
        /// 发送人
        /// </summary>
        public string ExecuteUserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumTaskTypeEunm Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SysType { get; set; } = 1;

        /// <summary>
        /// 发送人
        /// </summary>
        public string ExecuteUserId { get; set; }

        /// <summary>
        /// 接收人
        /// </summary>
        public List<string> ReceiveAccIds { get; set; }

        /// <summary>
        /// 来源Id
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 来源类型编号
        /// </summary>
        public MsgTaskTypeEnum SourceTypeValue { get; set; }

        /// <summary>
        /// 所属机构Id
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 代办内容
        /// </summary>
        public string TaskContent { get; set; }

        /// <summary>
        /// 单据创建日期
        /// </summary>
        public DateTime? SendDate { get; set; }

        /// <summary>
        /// 代办标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 代办备注
        /// </summary>
        public string Description { get; set; }
    }
}
