﻿using Admin.Repository.Page;

namespace Admin.Core.Service.MsgTask.Dto
{
    /// <summary>
    /// 定义获取分页输入dto
    /// </summary>
    public class GetMsgTaskInputDto : CommonPageInputDto
    {
        /// <summary>
        ///状态
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        ///类型
        /// </summary>
        public int? SysType { get; set; }

        /// <summary>
        ///名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 任务内容
        /// </summary>
        public string TaskContent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }
}
