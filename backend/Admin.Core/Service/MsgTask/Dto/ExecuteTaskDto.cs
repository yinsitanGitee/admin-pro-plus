﻿using Admin.Repository.Entities.Msg;

namespace Admin.Core.Service.MsgTask.Dto
{
    /// <summary>
    /// ExecuteTaskDto
    /// </summary>
    public class ExecuteTaskDto
    {
        /// <summary>
        /// 任务状态 0-新创建 1-任务接受中（多人完成时）2-进行中 3-已完成 4-已取消
        /// 默认 0-新创建
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 代办来源Id
        /// </summary>
        public string SourceId { get; set; }


        public EnumTaskTypeEunm? Type { get; set; }

        /// <summary>
        /// 节点名称
        /// </summary>
        public string NodeName { get; set; }

        //
        // 摘要:
        //     待办跳转来源数值
        public int Code
        {
            get;
            set;
        }

        /// <summary>
        /// 执行人名称
        /// </summary>
        public string ExecuteUserName { get; set; }

        /// <summary>
        /// 执行人用户Id
        /// </summary>
        public string ExecuteUserId { get; set; }
    }
}
