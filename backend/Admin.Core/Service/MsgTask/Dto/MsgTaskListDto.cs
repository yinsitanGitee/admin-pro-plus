﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Core.Service.MsgTask.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class MsgTaskListDto : BaseDto
    {
        /// <summary>
        /// 代办标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 任务内容
        /// </summary>
        public string TaskContent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 任务发起人
        /// </summary>
        public string SendUserName { get; set; }

        /// <summary>
        /// 发起时间
        /// </summary>
        public DateTime SendDate { get; set; }

        /// <summary>
        /// 类型  1 待办  2 待阅
        /// </summary>
        public int SysType { get; set; }

        /// <summary>
        /// 任务来源类别  
        /// </summary>
        public MsgTaskTypeEnum SourceTypeValue { get; set; }

        /// <summary>
        /// 来源id  
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        public DateTime? CompleteTime { get; set; }

        /// <summary>
        /// 状态
        /// 任务状态 0-新创建 1-任务接受中（多人完成时）2-进行中 3-已完成 4-已取消
        /// </summary>
        public int State { get; set; }
    }
}