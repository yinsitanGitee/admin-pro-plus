﻿using Admin.Common.Ioc;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Admin.Cache;
using Admin.Core.Auth;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;

namespace Admin.Core.Service.Cache
{
    /// <summary>
    /// 缓存服务
    /// </summary>
    public class CacheService : ICacheService
    {
        /// <summary>
        /// 获取编号缓存
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<SysBillCodeDetailEntity>?> GetBillCodeDetails(string id)
        {
            var key = RedisKeyDto.BillCodeKey + id;
            var cache = await RedisManage.GetAsync<List<SysBillCodeDetailEntity>>(key);
            if (cache != null) return cache;

            cache = await IocManager.Instance.GetService<IFreeSql>().Select<SysBillCodeDetailEntity>()
                .Where(a => a.BillId == id).OrderBy(a => a.Sort).ToListAsync();

            if (cache != null)
            {
                await RedisManage.SetAsync(key, cache, TimeSpan.FromMinutes(RedisKeyDto.CommonLargeExpiresIn));
                return cache;
            }

            return null;
        }

        /// <summary>
        /// 获取用户缓存
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SysAccountEntity?> GetUser(string? id = null)
        {
            id ??= IocManager.Instance.GetService<ICurrentUser>().UserId;
            var key = RedisKeyDto.UserKey + id;
            var cache = await RedisManage.GetAsync<SysAccountEntity>(key);
            if (cache != null) return cache;

            cache = await IocManager.Instance.GetService<IFreeSql>().Select<SysAccountEntity>().DisableGlobalFilter(FilterSetting.DataAuth).
                Where(a => a.Id == id && a.State == CommonStateEnum.Effective).ToOneAsync();

            if (cache != null)
            {
                await RedisManage.SetAsync(key, cache, TimeSpan.FromMinutes(RedisKeyDto.CommonExpiresIn));
                return cache;
            }

            return null;
        }

        /// <summary>
        /// 获取用户权限缓存
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<AccountRolePermissionsDto> GetUserPermissionCache(string? userId = null)
        {
            userId ??= IocManager.Instance.GetService<ICurrentUser>().UserId;
            var key = RedisKeyDto.UserPermissionsKey + userId;

            var cache = await RedisManage.GetAsync<AccountRolePermissionsDto>(key);
            if (cache != null) return cache;

            var user = await GetUser(userId) ?? throw new Exception("用户不存在");

            var pers = await IocManager.Instance.GetService<IFreeSql>().Select<SysAccountRoleEntity, SysRoleEntity>().LeftJoin(a => a.t1.RoleId == a.t2.Id).Where(a => a.t1.AccountId == userId)
                  .ToListAsync(a => new AccountRolePermissionDto { HalfViews = a.t2.HalfViews, Views = a.t2.Views });

            List<string> halfCodes = [];
            List<string> codes = [];

            if (!string.IsNullOrWhiteSpace(user.HalfViews))
                halfCodes.AddRange(user.HalfViews.Split(','));

            if (!string.IsNullOrWhiteSpace(user.Views))
                codes.AddRange(user.Views.Split(','));

            if (pers.Count > 0)
            {
                foreach (var item in pers)
                {
                    if (!string.IsNullOrWhiteSpace(item.HalfViews))
                        halfCodes.AddRange(item.HalfViews.Split(','));

                    if (!string.IsNullOrWhiteSpace(item.Views))
                        codes.AddRange(item.Views.Split(','));
                }
            }

            var p = new AccountRolePermissionsDto
            {
                Views = codes.Distinct().ToList(),
                HalfViews = halfCodes.Distinct().ToList()
            };

            await RedisManage.SetAsync(key, p, TimeSpan.FromMinutes(RedisKeyDto.CommonExpiresIn));

            return p;
        }

        /// <summary>
        /// 获取部门缓存
        /// </summary>
        /// <returns></returns>
        public async Task<List<DepartmentListDto>?> GetDepartmentCache()
        {
            var key = RedisKeyDto.DepartmentKey;
            var cache = await RedisManage.GetAsync<List<DepartmentListDto>>(key);

            if (cache == null)
            {
                var freeSql = IocManager.Instance.GetService<IFreeSql>();
                cache = await freeSql.Select<SysDepartmentEntity, SysOrganizeEntity>().LeftJoin(a => a.t1.OrganizeId == a.t2.Id)
                    .OrderBy(a => a.t1.Sort).ToListAsync(a => new DepartmentListDto
                    {
                        OrganizeName = a.t2.Name
                    });

                if (cache.Count > 0) await RedisManage.SetAsync(key, cache, TimeSpan.FromMinutes(RedisKeyDto.CommonExpiresIn));
            }

            return cache;
        }

        /// <summary>
        /// 获取机构缓存
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<OrganizeListDto>?> GetOrganizeCache(string? userId = null)
        {
            var currentUser = IocManager.Instance.GetService<ICurrentUser>() ?? throw new Exception("登录失效，请重新登录");
            userId ??= currentUser.UserId;

            var key = RedisKeyDto.OrganizeKey + userId;
            var cache = await RedisManage.GetAsync<List<OrganizeListDto>>(key);

            if (cache == null)
            {
                var freeSql = IocManager.Instance.GetService<IFreeSql>();

                cache = await freeSql.Select<SysOrganizeEntity>()
                       .WhereIf(currentUser.DataAuth == EnumDataAuth.OwnUser, a => a.CreateUserId == currentUser.UserId)
                       .WhereIf(currentUser.DataAuth == EnumDataAuth.CustOrganize || currentUser.DataAuth == EnumDataAuth.OwnOrganize, a => currentUser.CustDataAuth.Contains(a.Id))
                       .OrderBy(a => a.Sort).ToListAsync<OrganizeListDto>();

                if (cache.Count > 0) await RedisManage.SetAsync(key, cache, TimeSpan.FromMinutes(RedisKeyDto.CommonExpiresIn));
            }

            return cache;
        }
    }
}
