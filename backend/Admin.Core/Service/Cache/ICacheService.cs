﻿using Admin.Repository.Entities.Sys;
using Admin.Core.Service.Cache.Dto;

namespace Admin.Core.Service.Cache
{
    /// <summary>
    /// ICacheService
    /// </summary>
    public interface ICacheService
    {
        /// <summary>
        /// 获取编号缓存
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<List<SysBillCodeDetailEntity>?> GetBillCodeDetails(string id);

        /// <summary>
        /// 获取用户缓存
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SysAccountEntity?> GetUser(string? id = null);

        /// <summary>
        /// 获取用户权限缓存
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<AccountRolePermissionsDto> GetUserPermissionCache(string? userId = null);

        /// <summary>
        /// 获取部门缓存
        /// </summary>
        /// <returns></returns>
        Task<List<DepartmentListDto>?> GetDepartmentCache();

        /// <summary>
        /// 获取机构缓存
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<OrganizeListDto>?> GetOrganizeCache(string? userId = null);
    }
}
