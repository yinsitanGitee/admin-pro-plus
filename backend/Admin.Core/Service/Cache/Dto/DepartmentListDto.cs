﻿using Admin.Repository.Enum;
using Admin.Repository.Enum;

namespace Admin.Core.Service.Cache.Dto
{
    /// <summary>
    /// 定义列表Dto
    /// </summary>
    public class DepartmentListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrganizeName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public List<DepartmentListDto>? Children { get; set; }
    }
}
