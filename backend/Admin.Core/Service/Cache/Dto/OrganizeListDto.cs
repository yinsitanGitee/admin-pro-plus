﻿using Admin.Repository.Enum;

namespace Admin.Core.Service.Cache.Dto
{
    /// <summary>
    /// 定义列表Dto
    /// </summary>
    public class OrganizeListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumBool State { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public string Remark { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public List<OrganizeListDto>? Children { get; set; }
    }
}
