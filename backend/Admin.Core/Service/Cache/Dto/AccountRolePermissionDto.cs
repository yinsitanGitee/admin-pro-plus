﻿namespace Admin.Core.Service.Cache.Dto
{
    /// <summary>
    /// 定义用户角色权限dto(string)
    /// </summary>
    public class AccountRolePermissionDto
    {
        /// <summary>
        /// 权限编号集合
        /// </summary>
        public string HalfViews { get; set; }

        /// <summary>
        /// 授权的权限编号集合
        /// </summary>
        public string Views { get; set; }
    }

    /// <summary>
    /// 定义用户角色权限dto(List)
    /// </summary>
    public class AccountRolePermissionsDto
    {
        /// <summary>
        /// 权限编号集合
        /// </summary>
        public List<string> HalfViews { get; set; }

        /// <summary>
        /// 授权的权限编号集合
        /// </summary>
        public List<string> Views { get; set; }
    }
}