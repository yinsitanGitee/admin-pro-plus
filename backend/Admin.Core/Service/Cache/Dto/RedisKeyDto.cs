﻿using Admin.Repository.Tenant;

namespace Admin.Core.Service.Cache.Dto
{
    /// <summary>
    /// 定义redis配置项目
    /// </summary>
    public class RedisKeyDto
    {
        /// <summary>
        /// 定义用户缓存过期时间
        /// </summary>
        public static double TenRedisExpiresIn = 10;

        /// <summary>
        /// 
        /// </summary>
        public static double CommonLargeExpiresIn = 1440;

        /// <summary>
        /// 
        /// </summary>
        public static double CommonExpiresIn = 60;

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string ViewKey => TenantManager.Current + "_View";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string DictionaryKey => TenantManager.Current + "_Dict";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string FlowContentKey => TenantManager.Current + "_FlowContent";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string CityKey => TenantManager.Current + "_City";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string UserRegisterCodeKey => TenantManager.Current + "_UserRegisterCode";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string UseResetPassCodeKey => TenantManager.Current + "_UseResetPassCode";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string UserKey => TenantManager.Current + "_User";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string ReminderKey => TenantManager.Current + "_Reminder";  
        
        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string MsgCountKey => TenantManager.Current + "_MsgCount";

        /// <summary>
        /// 定义缓存前缀
        /// </summary>
        public static string CustomerKey => TenantManager.Current + "_Customer";

        /// <summary>
        /// 
        /// </summary>
        public static string BiddingBuykey => TenantManager.Current + "_BiddingBuy";

        /// <summary>
        /// 
        /// </summary>
        public static string BillCodeKey => TenantManager.Current + "_BillCode";

        /// <summary>
        /// 
        /// </summary>
        public static string DepartmentKey => TenantManager.Current + "_Department";

        /// <summary>
        /// 
        /// </summary>
        public static string OrganizeKey => TenantManager.Current + "_Organize";

        /// <summary>
        /// 
        /// </summary>
        public static string RoleKey => TenantManager.Current + "_Role";

        /// <summary>
        /// 
        /// </summary>
        public static string UserPermissionsKey => TenantManager.Current + "_UserPermission";

        /// <summary>
        /// 
        /// </summary>
        public static string CorpKey => TenantManager.Current + "_Corp";

        /// <summary>
        /// 
        /// </summary>
        public static string ConfigItemKey => TenantManager.Current + "_ConfigItem";

        /// <summary>
        /// 
        /// </summary>
        public static string BillCodeLockKey => TenantManager.Current + "_BillCodeLock";

        /// <summary>
        /// 
        /// </summary>
        public static string VersonExecLockKey => TenantManager.Current + "_VersonExecLock";

        /// <summary>
        /// 
        /// </summary>
        public static string BiddingTaskLockKey => TenantManager.Current + "_BiddingTaskLock";

        /// <summary>
        /// 
        /// </summary>
        public static string FlowProcApprovalKey => TenantManager.Current + "_FlowProcApprovalLock";

        /// <summary>
        /// 
        /// </summary>
        public static string BiddingBuyLockKey => TenantManager.Current + "_BiddingBuyLock";
    }
}
