﻿namespace Admin.Repository
{
    public interface IEntityAdd
    {
        /// <summary>
        /// 创建者用户Id
        /// </summary>
        string CreateUserId { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime CreateDateTime { get; set; }
    }
}
