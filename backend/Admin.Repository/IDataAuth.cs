﻿namespace Admin.Repository
{
    /// <summary>
    /// 数据权限
    /// </summary>
    public interface IDataAuth
    {
        /// <summary>
        /// 机构id
        /// </summary>
        string OrganizeId { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        string CreateUserId { get; set; }
    }
}
