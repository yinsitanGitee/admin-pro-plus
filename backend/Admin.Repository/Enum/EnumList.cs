﻿using System.ComponentModel;

namespace Admin.Repository.Enum
{
    /// <summary>
    /// 通过状态
    /// </summary>
    public enum SuccessStateEnum
    {
        /// <summary>
        /// 失败
        /// </summary>
        Fail = 0,

        /// <summary>
        /// 成功
        /// </summary>
        Success = 1
    }

    /// <summary>
    /// 通过状态
    /// </summary>
    public enum CommonStateEnum
    {
        /// <summary>
        /// 无效
        /// </summary>
        Ineffective = 0,

        /// <summary>
        /// 有效
        /// </summary>
        Effective = 1
    }

    /// <summary>
    /// 菜单类型
    /// </summary>
    public enum ViewTypeEnum
    {
        /// <summary>
        /// 分组
        /// </summary>
        Group = 1,

        /// <summary>
        /// 菜单
        /// </summary>
        Menu = 2,

        /// <summary>
        /// 权限点
        /// </summary>
        Permission = 3
    }

    /// <summary>
    /// 任务请求方式
    /// </summary>
    public enum APIRequestTypeEnum
    {
        /// <summary>
        /// Post
        /// </summary>
        [Description("Post请求")]
        Post = 0,
        /// <summary>
        /// Get
        /// </summary>
        [Description("Get请求")]
        Get = 1
    }

    /// <summary>
    /// 任务状态
    /// </summary>
    public enum APIStatusEnum
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 暂停
        /// </summary>
        Parse = 1
    }

    /// <summary>
    /// 任务类型
    /// </summary>
    public enum APITaskType
    {
        /// <summary>
        /// 系统任务
        /// </summary>
        System = 0,
        /// <summary>
        /// 第三方任务
        /// </summary>
        Third = 1
    }

    /// <summary>
    /// 任务操作类型
    /// </summary>
    public enum JobOperateEnum
    {
        /// <summary>
        /// 新增
        /// </summary>
        Add = 1,
        /// <summary>
        /// 删除
        /// </summary>
        Delete = 2,
        /// <summary>
        /// 修改
        /// </summary>
        Update = 3,
        /// <summary>
        /// 暂停
        /// </summary>
        Parse = 4,
        /// <summary>
        /// 开启
        /// </summary>
        Start = 5,
        /// <summary>
        /// 立即执行
        /// </summary>
        ExecuteImmediately = 6
    }

    /// <summary>
    /// 单据编号明细类型
    /// </summary>
    public enum BillCodeDetailTypeEnum
    {
        /// <summary>
        /// 自定义前缀
        /// </summary>
        Cust = 0,

        /// <summary>
        /// 日期
        /// </summary>
        Date = 1
    }

    /// <summary>
    /// 
    /// </summary>
    public enum EnumBool
    {

        /// <summary>
        /// 
        /// </summary>
        None = -1,
        /// <summary>
        /// 
        /// </summary>
        TRUE = 1,
        /// <summary>
        /// 
        /// </summary>
        FALSE = 0
    }

    /// <summary>
    /// 数据授权
    /// </summary>
    public enum EnumDataAuth
    {
        /// <summary>
        /// 全部机构
        /// </summary>
        [Description("全部机构")]
        All = 0,

        /// <summary>
        /// 本人机构
        /// </summary>
        [Description("本人机构")]
        OwnOrganize = 1,

        /// <summary>
        /// 指定机构
        /// </summary>
        [Description("指定机构")]
        CustOrganize = 2,

        /// <summary>
        /// 本人数据
        /// </summary>
        [Description("本人数据")]
        OwnUser = 3
    }
}
