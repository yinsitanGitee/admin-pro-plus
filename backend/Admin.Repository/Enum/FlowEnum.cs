﻿using System.ComponentModel;

namespace Admin.Repository.Enum
{
    public enum EnumFlowEle
    {
        /// <summary>
        /// 无
        /// </summary>
        None = -1,
        /// <summary>
        /// 流程
        /// </summary>
        Flow = 0,
        /// <summary>
        /// 环节
        /// </summary>
        Proc = 1,
        /// <summary>
        /// 线
        /// </summary>
        Line = 2,
        /// <summary>
        /// 执行人列表
        /// </summary>
        WorkList = 3
    }


    /// <summary>
    /// 执行人关联类型
    /// </summary>
    public enum EnumExecutorRefType
    {
        /// <summary>
        /// 指定账号
        /// </summary>
        [Description("指定账号")]
        Account = 0,

        /// <summary>
        /// 角色
        /// </summary>
        [Description("角色")]
        Role = 3
    }


    /// <summary>
    /// 流程状态
    /// </summary>
    public enum EnumFlowStatus
    {
        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        Executing = 0,
        /// <summary>
        /// 已归档
        /// </summary>
        [Description("已归档")]
        Complete = 1,
        /// <summary>
        /// 已终止
        /// </summary>
        [Description("已终止")]
        Termination = 2,
        /// <summary>
        /// 重新开始
        /// </summary>
        [Description("重新开始")]
        ReStarted = 3
    }

    public enum EnumSendMessageToExcuterType
    {
        /// <summary>
        /// 不发
        /// </summary>
        [Description("不发")]
        None = 0,
        /// <summary>
        /// 指定人员
        /// </summary>
        [Description("指定人员")]
        SelectUser = 1,
        /// <summary>
        /// 流程中的执行人
        /// </summary>
        [Description("流程中的执行人")]
        FlowExcutor = 2,
        /// <summary>
        /// 流程中的所有执行人
        /// </summary>
        [Description("流程中所有执行人")]
        FlowAllExcutor = 3
    }

    /// <summary>
    /// 环节类型
    /// </summary>
    public enum EnumProcType
    {
        /// <summary>
        /// 流程
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 开始
        /// </summary>
        Start = 1,
        /// <summary>
        /// 结束
        /// </summary>
        End = 2,
        /// <summary>
        /// 分支
        /// </summary>
        Branch = 4,
        /// <summary>
        /// OA环节
        /// </summary>
        OA = 5,
    }


    /// <summary>
    /// 环节审批状态
    /// </summary>
    public enum EnumProcApprovalStatus
    {
        /// <summary>
        /// 无
        /// </summary> 
        [Description("无")]
        None = 0,
        /// <summary>
        /// 同意
        /// </summary> 
        [Description("同意")]
        Agree = 1,
        /// <summary>
        /// 不同意
        /// </summary> 
        [Description("不同意")]
        DisAgree = 2,
        /// <summary>
        /// 终止
        /// 针对流程主动或者被动终止时候状态
        /// </summary>
        [Description("终止")]
        Termination = 3,
        /// <summary>
        /// 业务不同意(与同意功能一直，仅标识不一致)
        /// </summary> 
        [Description("不同意")]
        BusinessDisAgree = 4,
        /// <summary>
        /// 系统自动跳过
        /// </summary> 
        [Description("系统自动跳过")]
        SystemSkip = 5,
    }

    /// <summary>
    /// 环节状态
    /// </summary>
    public enum EnumProcStatus
    {
        /// <summary>
        /// 待处理
        /// </summary> 
        [Description("待处理")]
        Wait = 0,
        /// <summary>
        /// 处理中
        /// </summary> 
        [Description("处理中")]
        Handing = 1,
        /// <summary>
        /// 已处理
        /// 流程正常终止
        /// </summary> 
        [Description("已处理")]
        Complete = 2,
        /// <summary>
        /// 已完结
        /// 流程非正常终止
        /// </summary> 
        [Description("已完结")]
        Finished = 3,

        /// <summary>
        /// 环节异常
        /// </summary> 
        [Description("异常")]
        ProcException = 4
    }

    /// <summary>
    /// 环节节点审批类型
    /// </summary>
    public enum EnumProcApprovalType
    {
        /// <summary>
        /// 一个用户通过节点通过
        /// </summary>
        OneApproval = 1,

        /// <summary>
        /// 所有用户通过节点通过
        /// </summary>
        AllApproval = 2
    }

    /// <summary>
    /// 环节节点执行人审批类型
    /// </summary>
    public enum EnumProcExecutorType
    {
        /// <summary>
        /// 选择的用户都在当前节点
        /// </summary>
        OneProc = 1,

        /// <summary>
        /// 每人一个节点
        /// </summary>
        EveryOneProc = 2
    }

    /// <summary>
    /// 操作日志状态
    /// </summary>
    public enum EnumOperateState
    {
        /// <summary>
        /// 同意
        /// </summary>
        Agree = 1,

        /// <summary>
        /// 不同意
        /// </summary>
        DisAgree = 2,

        /// <summary>
        /// 转处理
        /// </summary>
        ChangeExecutor = 3,

        /// <summary>
        /// 业务不同意
        /// </summary>
        BusinessDisAgree = 4,

        /// <summary>
        /// 单据撤回
        /// </summary>
        BillBack = 5,

        /// <summary>
        /// 自动跳过
        /// </summary>
        Skip = 6
    }


    public enum EnumLineCondType
    {
        /// <summary>
        /// 无
        /// </summary>
        [Description("无")]
        None = 0,
        /// <summary>
        /// 同意
        /// </summary>
        [Description("同意")]
        Agree = 1,
        /// <summary>
        /// 不同意
        /// </summary>
        [Description("不同意")]
        DisAgree = 2,
        /// <summary>
        /// 自定义
        /// </summary>
        [Description("自定义")]
        CustDefine = 3,
    }

    public enum EnumFieldType
    {
        //
        // 摘要:
        //     字符串类型
        String,
        //
        // 摘要:
        //     int类型
        Int32,
        //
        // 摘要:
        //     数字
        Decimal,
        //
        // 摘要:
        //     时间类型
        DateTime,
        //
        // 摘要:
        //     列表
        List
    }

    /// <summary>
    /// 结果
    /// </summary>
    public enum EnumFlowCommonResult
    {
        /// <summary>
        /// 流程已通过
        /// </summary>
        Skip = 0,

        /// <summary>
        /// 流程正常执行
        /// </summary>
        Common = 1
    }

    /// <summary>
    /// 审批通用状态
    /// </summary>
    public enum EnumFlowState
    {
        /// <summary>
        /// 待提交 
        /// </summary> 
        [Description("待提交")]
        NoSubmit = 0,

        /// <summary>
        /// 审批中 
        /// </summary> 
        [Description("审批中")]
        Approval = 1,

        /// <summary>
        /// 审批通过
        /// </summary> 
        [Description("审批通过")]
        Through = 2,

        /// <summary>
        /// 审批不通过
        /// </summary> 
        [Description("审批不通过")]
        NotThrough = 3,

        /// <summary>
        /// 作废
        /// </summary> 
        [Description("作废")]
        Invalid = 4,
    }

}
