﻿namespace Admin.Repository.Enum
{
    /// <summary>
    /// 待办来源
    /// </summary>
    public enum MsgTaskTypeEnum
    {
        /// <summary>
        /// 无
        /// </summary>
        None = 1,

        /// <summary>
        /// 自定义表单
        /// </summary>
        Form = 2
    }
}
