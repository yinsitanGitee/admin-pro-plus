﻿using FreeSql;
using System.Linq.Expressions;

namespace Admin.Repository
{
    public interface IRepository<TEntity, TKey> : IBaseRepository<TEntity, TKey> where TEntity : class
    {
        /// <summary>
        /// 根据条件获取Dto
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        Task<TDto> GetAsync<TDto>(Expression<Func<TEntity, bool>> exp, params string[]? disableNames);

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> exp, params string[]? disableNames);

        /// <summary>
        /// 软删除
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="disableGlobalFilterNames">禁用全局过滤器名</param>
        /// <returns></returns>
        Task<bool> SoftDeleteAsync(Expression<Func<TEntity, bool>> exp, params string[]? disableNames);

        /// <summary>
        /// 通过表达式修改实体
        /// </summary>
        /// <param name="update"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        Task UpdateAsync(Expression<Func<TEntity, TEntity>> update, Expression<Func<TEntity, bool>> exp, params string[]? disableNames);

        /// <summary>
        /// 通过表达式修改实体,忽略列
        /// </summary>
        /// <param name="update"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        Task UpdateAsync(TEntity update, Expression<Func<TEntity, object>> exp, params string[]? disableNames);
    }
}