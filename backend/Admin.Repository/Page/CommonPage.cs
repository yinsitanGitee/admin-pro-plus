﻿namespace Admin.Repository.Page
{
    /// <summary>
    /// 定义分页通用返回dto
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommonPageOutputDto<T>
    {
        /// <summary>
        /// CommonPageOutputDto
        /// </summary>
        /// <param name="curPage"></param>
        /// <param name="pageSize"></param>
        public CommonPageOutputDto(int curPage, int pageSize)
        {
            CurPage = curPage;
            PageSize = pageSize;
        }

        /// <summary>
        /// 页数
        /// </summary>
        public int CurPage { get; set; }

        /// <summary>
        /// 条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public long Total { get; set; }

        /// <summary>
        /// 数据集
        /// </summary>
        public List<T> Info { get; set; }
    }

    /// <summary>
    /// 定义分页通用dto
    /// </summary>
    public class CommonPageInputDto
    {
        /// <summary>
        /// 页数
        /// </summary>
        public int CurPage { get; set; }

        /// <summary>
        /// 条数
        /// </summary>
        public int PageSize { get; set; }
    }
}
