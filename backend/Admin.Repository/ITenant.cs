﻿namespace Admin.Repository
{
    /// <summary>
    /// 租户
    /// </summary>
    public interface ITenant
    {
        /// <summary>
        /// 公司Id
        /// </summary>
        string Tenant { get; set; }
    }
}
