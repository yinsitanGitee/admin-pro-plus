﻿using FreeSql;
using System.Linq.Expressions;

namespace Admin.Repository
{
    /// <summary>
    /// Repository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class Repository<TEntity, TKey> : DefaultRepository<TEntity, TKey>, IRepository<TEntity, TKey> where TEntity : class
    {
        /// <summary>
        /// UnitOfWorkManager
        /// </summary>
        /// <param name="uowm"></param>
        public Repository(UnitOfWorkManager uowm) : base(uowm?.Orm, uowm) { }

        /// <summary>
        /// GetAsync
        /// </summary>
        /// <typeparam name="TDto"></typeparam>
        /// <param name="exp"></param>
        /// <param name="disableNames"></param>
        /// <returns></returns>
        public virtual Task<TDto> GetAsync<TDto>(Expression<Func<TEntity, bool>> exp, params string[]? disableNames)
        {
            if (disableNames != null && disableNames.Length > 0) return Select.DisableGlobalFilter(disableNames).Where(exp).ToOneAsync<TDto>();
            else return Select.Where(exp).ToOneAsync<TDto>();
        }

        /// <summary>
        /// GetAsync
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="disableNames"></param>
        /// <returns></returns>
        public virtual Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> exp, params string[]? disableNames)
        {
            if (disableNames != null && disableNames.Length > 0) return Select.DisableGlobalFilter(disableNames).Where(exp).ToOneAsync();
            else return Select.Where(exp).ToOneAsync();
        }

        /// <summary>
        /// 软删除
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="disableNames"></param>
        /// <returns></returns>
        public virtual async Task<bool> SoftDeleteAsync(Expression<Func<TEntity, bool>> exp, params string[]? disableNames)
        {
            if (disableNames != null && disableNames.Length > 0) await UpdateDiy.DisableGlobalFilter(disableNames).SetDto(new { IsDelete = true }).Where(exp).ExecuteAffrowsAsync();
            else await UpdateDiy.SetDto(new { IsDelete = true }).Where(exp).ExecuteAffrowsAsync();
            return true;
        }

        /// <summary>
        /// 通过表达式修改实体
        /// </summary>
        /// <param name="update"></param>
        /// <param name="exp"></param>
        /// <param name="disableNames"></param>
        /// <returns></returns>
        public async Task UpdateAsync(Expression<Func<TEntity, TEntity>> update, Expression<Func<TEntity, bool>> exp, params string[]? disableNames)
        {
            if (disableNames != null && disableNames.Length > 0) await UpdateDiy.DisableGlobalFilter(disableNames).Set(update).Where(exp).ExecuteAffrowsAsync();
            else await UpdateDiy.Set(update).Where(exp).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 修改实体，忽略列
        /// </summary>
        /// <param name="update"></param>
        /// <param name="exp"></param>
        /// <param name="disableNames"></param>
        /// <returns></returns>
        public async Task UpdateAsync(TEntity update, Expression<Func<TEntity, object>> exp, params string[]? disableNames)
        {
            if (disableNames != null && disableNames.Length > 0) await UpdateDiy.DisableGlobalFilter(disableNames).SetSource(update).IgnoreColumns(exp).ExecuteAffrowsAsync();
            else await UpdateDiy.SetSource(update).IgnoreColumns(exp).ExecuteAffrowsAsync();
        }
    }
}