﻿namespace Admin.Repository.Dto
{
    public class BaseDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
    }
}
