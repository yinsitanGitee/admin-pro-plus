﻿using FreeSql;

namespace Admin.Repository.Dto
{
    /// <summary>
    /// 定义orm输入dto
    /// </summary>
    public class OrmConfigOutDto
    {
        /// <summary>
        /// TenantName
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// TenantKey
        /// </summary>
        public string TenantKey { get; set; }
    }

    /// <summary>
    /// OrmConfigDto
    /// </summary>
    public class OrmConfigDto : OrmConfigOutDto
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public DataType DbType { get; set; }

        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 是否 codefirst 同步表结构
        /// </summary>
        public bool SyncStructureSql { get; set; }

        /// <summary>
        /// 从库连接字符串
        /// </summary>
        public string[] SlaveConnectionStrings { get; set; }
    }
}
