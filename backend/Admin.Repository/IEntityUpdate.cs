﻿namespace Admin.Repository
{
    public interface IEntityUpdate
    {
        /// <summary>
        /// 修改人id
        /// </summary>
        string? ModifyUserId { get; set; }

        /// <summary>
        /// 修改人名称
        /// </summary>
        string? ModifyUserName { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        DateTime? ModifyDateTime { get; set; }
    }
}
