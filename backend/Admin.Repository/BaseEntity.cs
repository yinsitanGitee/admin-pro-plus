﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Admin.Repository
{
    /// <summary>
    /// 实体租户基类
    /// 无创建时间等等
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TenantEntityBase<T> : EntityBase<T>, ITenant
    {
        /// <summary>
        /// 公司Id
        /// </summary>
        [Description("租户信息")]
        [Column(StringLength = 64, IsNullable = true)]
        public string Tenant { get; set; }
    }

    /// <summary>
    /// 实体租户基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseTenantEntity<T> : BaseEntity<T>, ITenant
    {
        /// <summary>
        /// 公司Id
        /// </summary>
        [Description("租户信息")]
        [Column(StringLength = 64, IsNullable = true)]
        public string Tenant { get; set; }
    }

    /// <summary>
    /// 实体基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseEntity<T> : EntityBase<T>, IEntityAdd, IEntityUpdate
    {
        /// <summary>
        /// 创建者Id
        /// </summary>
        [Description("创建者Id")]
        [Column(StringLength = 64, IsNullable = true)]
        public string CreateUserId { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        [Description("创建者名称")]
        [Column(StringLength = 64, IsNullable = true)]
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Column(IsNullable = true, CanUpdate = false, ServerTime = DateTimeKind.Local)]
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// 修改者Id
        /// </summary>
        [Description("修改者Id")]
        [Column(StringLength = 64, IsNullable = true)]
        public string? ModifyUserId { get; set; }

        /// <summary>
        /// 修改者名称
        /// </summary>
        [Description("修改者名称")]
        [Column(StringLength = 64, IsNullable = true)]
        public string? ModifyUserName { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Column(IsNullable = true, ServerTime = DateTimeKind.Local)]
        public DateTime? ModifyDateTime { get; set; }
    }

    /// <summary>
    /// 实体基类
    /// </summary>
    public class EntityBase<T> : IIsDelete
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Column(IsPrimary = true, StringLength = 64)]
        public virtual T Id { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [Description("是否删除")]
        public bool IsDelete { get; set; }
    }
}