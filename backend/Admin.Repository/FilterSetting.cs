﻿namespace Admin.Repository
{
    /// <summary>
    /// 定义freesql 过滤器配置
    /// </summary>
    public class FilterSetting
    {
        /// <summary>
        /// 软删除
        /// </summary>
        public const string IsDelete = "IsDelete";

        /// <summary>
        /// 租户
        /// </summary>
        public const string Tenant = "Tenant";  
        
        /// <summary>
        /// 数据权限
        /// </summary>
        public const string DataAuth = "DataAuth";
    }
}
