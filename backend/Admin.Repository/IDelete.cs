﻿namespace Admin.Repository
{
    public interface IIsDelete
    {
        /// <summary>
        /// 是否删除
        /// </summary>
        bool IsDelete { get; set; }
    }
}
