﻿namespace Admin.Repository.Tenant
{
    /// <summary>
    /// 租户管理器
    /// 线程安全、支持异步
    /// </summary>
    public class TenantManager
    {
        /// <summary>
        /// 租户
        /// </summary>
        static AsyncLocal<string> _asyncLocal = new AsyncLocal<string>();

        /// <summary>
        /// 当前租户
        /// </summary>
        public static string Current
        {
            get => _asyncLocal.Value;
            set => _asyncLocal.Value = value;
        }
    }
}
