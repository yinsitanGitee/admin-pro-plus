﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities
{
    /// <summary>
    /// 定义系统通用长度
    /// </summary>
    [Table(DisableSyncStructure = true)]
    public class BaseStringLength
    {
        /// <summary>
        /// 通用长度(max)
        /// </summary>
        public const int CommonMaxlength = -1;

        /// <summary>
        /// 通用长度
        /// </summary>
        public const int CommonMinlength = 50;

        /// <summary>
        /// 通用长度
        /// </summary>
        public const int CommonMidlength = 200;
    }
}
