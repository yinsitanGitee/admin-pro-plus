﻿using Admin.Repository.Enum;
using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Base
{
    /// <summary>
    /// 表单表
    /// </summary>
    [Table(Name = "Base_Form")]
    public class BaseFormEntity : BaseTenantEntity<string>, IDataAuth
    {
        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string OrganizeId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = true)]
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Type { get; set; }

        /// <summary>
        /// 表单详情
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string FormDetail { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }
    }
}
