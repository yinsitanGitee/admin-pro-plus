﻿using Admin.Repository.Enum;
using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Base
{
    /// <summary>
    /// 表单单据表
    /// </summary>
    [Table(Name = "Base_FormBill")]
    public class BaseFormBillEntity : BaseTenantEntity<string>, IDataAuth
    {
        /// <summary>
        /// 表单id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string FormId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string OrganizeId { get; set; }

        /// <summary>
        /// 表单详情
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string FormDetail { get; set; }

        /// <summary>
        /// 表单值详情
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string FormDetailData { get; set; }

        /// <summary>
        ///流程id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string ProcessId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public EnumFlowState State { get; set; }

        /// <summary>
        /// 下级节点处理人
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string? NextApprovalName { get; set; }

        /// <summary>
        /// 下级节点名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string? NextApprovalProcName { get; set; }
    }
}
