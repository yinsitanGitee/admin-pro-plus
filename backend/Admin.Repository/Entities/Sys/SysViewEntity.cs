﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 视图
    /// </summary>
    [Table(Name = "Sys_View")]
    public class SysViewEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 上级菜单
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string ParentId { get; set; }

        /// <summary>
        /// 组件
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string Component { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        /// 菜单类型
        /// </summary>
        [Column(IsNullable = false)]
        public ViewTypeEnum Type { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [Column(StringLength = 256, IsNullable = true)]
        public string Path { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string Icon { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(StringLength = 256, IsNullable = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Column(IsNullable = false)]
        public bool? IsHide { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(IsNullable = true)]
        public int? Sort { get; set; }

        /// <summary>
        /// 重定向
        /// </summary>
        [Column(StringLength = 256, IsNullable = true)]
        public string Redirect { get; set; }

        /// <summary>
        /// 页面是否缓存
        /// </summary>
        [Column(IsNullable = true)]
        public bool? IsKeepAlive { get; set; }

        /// <summary>
        /// 页面是否固定，不能关闭
        /// </summary>
        [Column(IsNullable = true)]
        public bool? IsAffix { get; set; }

        /// <summary>
        /// 是否内嵌
        /// </summary>
        [Column(IsNullable = true)]
        public bool? IsIframe { get; set; }

        /// <summary>
        /// 外链地址
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string Link { get; set; }
    }
}

