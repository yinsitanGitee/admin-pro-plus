﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 角色
    /// </summary>
    [Table(Name = "Sys_Role")]
    public class SysRoleEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 全选权限
        /// </summary>
        [Column(StringLength = -2, IsNullable = true)]
        public string Views { get; set; }

        /// <summary>
        /// 半选权限
        /// </summary>
        [Column(StringLength = -2, IsNullable = true)]
        public string HalfViews { get; set; }
    }
}
