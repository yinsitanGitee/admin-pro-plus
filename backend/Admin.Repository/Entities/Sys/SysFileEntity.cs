﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 文件上传表
    /// </summary>
    [Table(Name = "Sys_File")]
    public class SysFileEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 路径
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = false)]
        public string Url { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        ///关联Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string RefId { get; set; }

        /// <summary>
        ///业务自定义
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public int? RefType { get; set; }
    }
}
