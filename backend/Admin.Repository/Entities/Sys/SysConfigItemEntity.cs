﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 参数表
    /// </summary>
    [Table(Name = "Sys_ConfigItem")]
    public class SysConfigItemEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 键
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string ConfigKey { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [Column(StringLength = -1, IsNullable = false)]
        public string ConfigValue { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string Remark { get; set; }
    }
}
