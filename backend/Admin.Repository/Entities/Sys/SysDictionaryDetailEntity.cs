﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 词典
    /// </summary>
    [Table(Name = "Sys_DictionaryDetail")]
    public class SysDictionaryDetailEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 值
        /// </summary>
        [Column(StringLength = 512, IsNullable = false)]
        public string Value { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string ParentId { get; set; }

        /// <summary>
        /// 单据id
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string BillId { get; set; }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool IsEffective { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
