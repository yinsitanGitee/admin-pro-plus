﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 单据编号配置
    /// </summary>
    [Table(Name = "Sys_BillCode")]
    public class SysBillCodeEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        /// 流水长度
        /// </summary>
        [Column(IsNullable = false)]
        public int Length { get; set; }

        /// <summary>
        /// 当前下标
        /// </summary>
        [Column(IsNullable = false)]
        public long CurrentNum { get; set; }

        /// <summary>
        /// 初始数
        /// </summary>
        [Column(StringLength = 10, IsNullable = false)]
        public string SeedValue { get; set; }

        /// <summary>
        /// 前缀
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Prefix { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }
    }
}

