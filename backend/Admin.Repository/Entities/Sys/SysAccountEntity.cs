﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 用户表
    /// </summary>
    [Table(Name = "Sys_Account")]
    public class SysAccountEntity : BaseTenantEntity<string>, IDataAuth
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        /// 账户
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Account { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Column(StringLength = 11, IsNullable = false)]
        public string Phone { get; set; }

        /// <summary>
        /// OrganizeId
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string OrganizeId { get; set; }

        /// <summary>
        /// 部门
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string DepartmentId { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column(StringLength = 512, IsNullable = false)]
        public string Password { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string HeadPic { get; set; }

        /// <summary>
        /// 全选权限
        /// </summary>
        [Column(StringLength = -2, IsNullable = true)]
        public string Views { get; set; }

        /// <summary>
        /// 半选权限
        /// </summary>
        [Column(StringLength = -2, IsNullable = true)]
        public string HalfViews { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 数据权限
        /// </summary>
        [Column(IsNullable = false)]
        public EnumDataAuth DataAuth { get; set; } = EnumDataAuth.All;

        /// <summary>
        /// 自定义权限
        /// </summary>
        [Column(StringLength = -2, IsNullable = true)]
        public string CustDataAuth { get; set; }
    }
}
