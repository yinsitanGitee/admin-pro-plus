﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 用户关联角色表
    /// </summary>
    [Table(Name = "Sys_AccountRole")]
    public class SysAccountRoleEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string AccountId { get; set; }

        /// <summary>
        /// 角色Id
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string RoleId { get; set; }
    }
}
