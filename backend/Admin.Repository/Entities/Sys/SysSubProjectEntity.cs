﻿using Admin.Repository;
using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 升级脚本表
    /// </summary>
    [Table(Name = "Sys_SubProject")]
    public class SysSubProjectEntity : ITenant
    {
        /// <summary>
        /// 版本
        /// </summary>
        [Column(IsNullable = false)]
        public int Verson { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Column(IsPrimary = true, StringLength = 64, IsNullable = true)]
        public string Tenant { get; set; }
    }
}
