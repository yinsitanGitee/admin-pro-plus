﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 省市区表
    /// </summary>
    [Table(Name = "Sys_Area")]
    public class SysAreaEntity : BaseTenantEntity<string>
    {

        /// <summary>
        /// 父节点编码
        /// </summary>
        [Description("父级")]
        [Column(StringLength = 64, IsNullable = false)]
        public string ParentId { get; set; }

        /// <summary>
        /// 地域编码
        /// </summary>
        [Description("编号")]
        [Column(StringLength = 64, IsNullable = true)]
        public string AreaCode { get; set; }

        /// <summary>
        /// 地域名称
        /// </summary>
        [Description("地域名称")]
        [Column(StringLength = 64, IsNullable = true)]
        public string AreaName { get; set; }

        /// <summary>
        /// SimpleSpelling
        /// </summary>
        [Description("SimpleSpelling")]
        [Column(StringLength = 64, IsNullable = true)]
        public string SimpleSpelling { get; set; }

        /// <summary>
        /// 快记符
        /// </summary>
        [Description("快记符")]
        [Column(StringLength = 64, IsNullable = true)]
        public string QuickQuery { get; set; }

        /// <summary>
        /// 层级
        /// </summary>
        [Description("Layer")]
        [Column(StringLength = 64, IsNullable = true)]
        public int Layer { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Description("排序")]
        [Column(StringLength = 64, IsNullable = true)]
        public string SortCode { get; set; }

        /// <summary>
        /// DataId
        /// </summary>
        [Description("DataId")]
        [Column(StringLength = 64, IsNullable = true)]
        public string DataId { get; set; }

        /// <summary>
        /// Lon
        /// </summary>
        [Description("Lon")]
        [Column(StringLength = 64, IsNullable = true)]
        public decimal Lon { get; set; }

        /// <summary>
        /// Lat
        /// </summary>
        [Description("Lat")]
        [Column(StringLength = 64, IsNullable = true)]
        public decimal Lat { get; set; }

        /// <summary>
        /// 附加说明
        /// </summary>
        [Description("附加说明")]
        [Column(StringLength = 500, IsNullable = true)]
        public string Description { get; set; }

    }
}

