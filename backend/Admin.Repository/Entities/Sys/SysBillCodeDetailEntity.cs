﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 单据编号明细配置
    /// </summary>
    [Table(Name = "Sys_BillCodeDetail")]
    public class SysBillCodeDetailEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 主表id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string BillId { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Column(IsNullable = false)]
        public BillCodeDetailTypeEnum Type { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [Column(StringLength = 20, IsNullable = false)]
        public string Value { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(IsNullable = false)]
        public int Sort { get; set; }
    }
}