﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 任务日志
    /// </summary>
    [Table(Name = "Sys_TaskLog")]
    public class SysTaskLogEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string TaskId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Response { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        [Column(IsNullable = false)]
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public SuccessStateEnum Status { get; set; }

        /// <summary>
        /// 请求头信息
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string RequestHeader { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string RequestBody { get; set; }
    }
}
