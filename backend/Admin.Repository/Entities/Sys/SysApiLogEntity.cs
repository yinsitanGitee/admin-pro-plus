﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 接口日志
    /// </summary>
    [Table(Name = "Sys_ApiLog")]
    public class SysApiLogEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// 接口地址
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string Api { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [Column(StringLength = 15, IsNullable = true)]
        public string IP { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string? Request { get; set; }

        /// <summary>
        /// 响应参数
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string Response { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string? Error { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        [Column(IsNullable = false)]
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 请求人
        /// </summary>
        [Column(StringLength = 50, IsNullable = true)]
        public string RequestUserId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public SuccessStateEnum Status { get; set; }
    }
}
