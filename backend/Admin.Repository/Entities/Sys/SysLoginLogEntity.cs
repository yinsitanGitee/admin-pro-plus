﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 登录日志
    /// </summary>
    [Table(Name = "Sys_LoginLog")]
    public class SysLoginLogEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// IP
        /// </summary>
        [Column(StringLength = 15, IsNullable = true)]
        public string IP { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Account { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string Request { get; set; }

        /// <summary>
        /// 响应参数
        /// </summary>
        [Column(StringLength = -1, IsNullable = true)]
        public string Response { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        [Column(IsNullable = false)]
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public SuccessStateEnum Status { get; set; }
    }
}
