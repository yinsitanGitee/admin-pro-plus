﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 机构表
    /// </summary>
    [Table(Name = "Sys_Organize")]
    public class SysOrganizeEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(IsNullable = true)]
        public int Sort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(IsNullable = true)]
        public EnumBool State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Remark { get; set; }
    }
}
