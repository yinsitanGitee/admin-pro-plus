﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 词典
    /// </summary>
    [Table(Name = "Sys_Dictionary")]
    public class SysDictionaryEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        /// 是否树级
        /// </summary>
        public bool IsTree { get; set; }

        /// <summary>
        /// 树层级，最多有几层
        /// </summary>
        public int Level { get; set; }
    }
}
