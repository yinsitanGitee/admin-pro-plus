﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 部门表
    /// </summary>
    [Table(Name = "Sys_Department")]
    public class SysDepartmentEntity : BaseTenantEntity<string>, IDataAuth
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(StringLength = 64, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// OrganizeId
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string OrganizeId { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = 512, IsNullable = false)]
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(IsNullable = false)]
        public int Sort { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsNullable = false)]
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string ParentId { get; set; }
    }
}
