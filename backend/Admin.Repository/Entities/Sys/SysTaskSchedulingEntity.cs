﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Sys
{
    /// <summary>
    /// 定时任务
    /// </summary>
    [Table(Name = "Sys_TaskScheduling")]
    public class SysTaskSchedulingEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string TaskName { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        /// 执行内容
        /// </summary>
        [Column(StringLength = -1, IsNullable = false)]
        public string Content { get; set; }

        /// <summary>
        /// 间隔Cron表达式
        /// </summary>
        [Column(StringLength = 64, IsNullable = false)]
        public string Interval { get; set; }

        /// <summary>
        /// RequestType
        /// </summary>
        [Column(IsNullable = false)]
        public APIRequestTypeEnum RequestType { get; set; }

        /// <summary>
        /// 任务类型
        /// 系统任务会自动附加租户请求头
        /// 第三方任务自定义
        /// </summary>
        [Column(IsNullable = false)]
        public APITaskType Type { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [Column(IsNullable = false)]
        public APIStatusEnum Status { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(IsNullable = true)]
        public DateTime? LastRunTime { get; set; }

        /// <summary>
        /// 请求头信息
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string RequestHeader { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [Column(StringLength = 512, IsNullable = true)]
        public string RequestBody { get; set; }
    }
}
