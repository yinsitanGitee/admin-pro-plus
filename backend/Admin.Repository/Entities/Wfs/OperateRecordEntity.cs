﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;
using System.ComponentModel;
using Admin.Repository;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    /// <summary>
    /// WFS审批日志表
    /// </summary>

    [Table(Name = "WFS_OperateRecord")]
    public class OperateRecordEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// 数据Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string DataId { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
		[Column(IsNullable = false)]
        public EnumOperateState State { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Type { get; set; }

        /// <summary>
        /// 日志标题
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Title { get; set; }

        /// <summary>
        /// 日志备注
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        ///节点Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ProcId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Column(IsNullable = true, CanUpdate = false, ServerTime = DateTimeKind.Local)]
        public DateTime CreateDateTime { get; set; }
    }
}
