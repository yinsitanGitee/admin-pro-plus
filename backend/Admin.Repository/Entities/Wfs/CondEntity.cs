﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_Cond")]
    public class CondEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// 流程模板Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowId { get; set; }

        /// <summary>
        ///关联Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string RefId { get; set; }

        /// <summary>
        ///关联类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumFlowEle RefType { get; set; }

        /// <summary>
        ///表达式
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Expression { get; set; }
    }
}

