﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_ProcTemp")]
    public class ProcTempEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///流程图中环节Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SchemeProcId { get; set; }

        /// <summary>
        ///流程模板Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowTempId { get; set; }

        /// <summary>
        ///业务标识编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ActionCode { get; set; }

        /// <summary>
        ///环节名称
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        ///环节编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        ///环节类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumProcType Type { get; set; }

        /// <summary>
        ///限定完成时间
        /// </summary>
		[Column(IsNullable = false)]
        public int? LimtTime { get; set; }

        /// <summary>
        ///限定完成类型
        /// </summary>
		[Column(IsNullable = true)]
        public string LimtTimeType { get; set; }

        /// <summary>
        ///图形图形其他信息
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = false)]
        public string SchemeProcInfo { get; set; }

        /// <summary>
        ///环节节点审批类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumProcApprovalType ProcApprovalType { get; set; }

        /// <summary>
        ///环节节点执行人审批类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumProcExecutorType ProcExecutorType { get; set; }

        /// <summary>
        /// 是否选择下级节点
        /// </summary>
		[Column(IsNullable = false)]
        public bool IsSelectNextProc { get; set; }

        /// <summary>
        /// 审批用户是否由前一个节点选择
        /// </summary>
		[Column(IsNullable = false)]
        public bool IsSelectExecutor { get; set; }

        /// <summary>
        /// 原始模板节点ID
        /// 用于节点属性、 当前节点用户一人一个节点
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string OriginalSchemeProcId { get; set; }

        /// <summary>
        /// 排序
        /// 从递归中按照遍历顺序排
        /// </summary>
        [Column(IsNullable = false)]
        public int Sort { get; set; }
    }
}
