﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 流程待办消息表
    /// </summary>
    [Table(Name = "Wfs_FlowTaskContent")]
    public class FlowTaskContentEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///来源Id/单据Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SourceId { get; set; }

        /// <summary>
        ///流程Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowId { get; set; }

        /// <summary>
        ///标题
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Title { get; set; }

        /// <summary>
        ///内容
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        ///来源编号/待办跳转类型编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string SourceTypeCode { get; set; }

        /// <summary>
        ///来源编号/待办跳转类型数值
        /// </summary>
		[Column(IsNullable = true)]
        public int SourceTypeValue { get; set; }
    }
}
