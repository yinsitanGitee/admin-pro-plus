﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_OperateRecordExecutor")]
    public class OperateRecordExecutorEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// 审批记录外键
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string RecordId { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
		[Column(IsNullable = false)]
        public EnumOperateState State { get; set; }

        /// <summary>
        /// 单据Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string DataId { get; set; }

        /// <summary>
        /// 审批内容
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = false)]
        public string Content { get; set; }

        /// <summary>
        /// 审批人Accountid
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string OperateUserId { get; set; }

        /// <summary>
        /// 审批日期(字符串格式) => (yyyy-MM-dd HH:mm:ss)
        /// </summary>
		[Column(IsNullable = false)]
        public DateTime OperateDate { get; set; }

        /// <summary>
        /// 审批人名称
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string OperateUserName { get; set; }
    }
}
