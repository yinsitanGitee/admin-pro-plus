﻿using Admin.Repository.Enum;
using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_FlowTemp")]
    public class FlowTempEntity : BaseTenantEntity<string>
    {
        /// <summary>
        ///业务标识编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ActionCode { get; set; }

        /// <summary>
        ///编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        ///名称
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        ///所属机构
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string OrgId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(IsNullable = true)]
        public EnumBool EnabledMark { get; set; }

        /// <summary>
        ///限定完成时间(天)
        /// </summary>
        [Column(IsNullable = true)]
        public int LimtTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        ///是否发送所有消息给执行人
        /// </summary>
        [Column(IsNullable = false)]
        public bool EnableSendMessageToAllExcuter { get; set; }

        /// <summary>
        ///归档后发消息类型
        /// </summary>
        [Column(IsNullable = true)]
        public EnumSendMessageToExcuterType SendMessageToExcuterType { get; set; }

        /// <summary>
        ///归档后发消息指定人员
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string SendMessageSelectUsers { get; set; }

        /// <summary>
        /// 是否自动通过
        /// </summary>
        [Column(IsNullable = true)]
        public bool IsApprovalPass { get; set; }
    }
}

