﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_ExecutorTemp")]
    public class ExecutorTempEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///流程模板Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowTempId { get; set; }

        /// <summary>
        ///关联人Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string RefId { get; set; }

        /// <summary>
        ///关联人名称
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string RefName { get; set; }

        /// <summary>
        ///关联类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumExecutorRefType RefType { get; set; }

        /// <summary>
        ///环节编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ProcId { get; set; }

        /// <summary>
        /// 是否限定和提交人同机构
        /// </summary>
		[Column(IsNullable = false)]
        public bool IsLimtOrgan { get; set; }
    }
}

