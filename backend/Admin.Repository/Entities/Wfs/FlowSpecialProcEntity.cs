﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_FlowSpecialProc")]
    public class FlowSpecialProcEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 来源Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SourceId { get; set; }

        /// <summary>
        /// 流程模板Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowTempId { get; set; }

        /// <summary>
        ///流程节点Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowSpecialProcId { get; set; }

        /// <summary>
        /// 0重走流程
        /// 1直接提交本环节
        /// 2直接提交指定环节
        /// </summary> 
		[Column(IsNullable = false)]
        public int Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(IsNullable = false)]
        public int State { get; set; } = 0;
    }
}