﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_Action")]
    public class ActionEntity : BaseTenantEntity<string>
    {
        /// <summary>
        ///编号
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Code { get; set; }
        /// <summary>
        ///名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        ///流程事件
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = false)]
        public string FlowEvent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(IsNullable = true)]
        public EnumBool EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }
    }
}