﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;
using Admin.Repository;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_Executor")]
    public class ExecutorEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///流程Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowId { get; set; }

        /// <summary>
        ///关联Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string RefId { get; set; }

        /// <summary>
        ///关联类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumExecutorRefType RefType { get; set; }

        /// <summary>
        ///关联名称
        /// </summary>
		[Column(IsNullable = true)]
        public string RefName { get; set; }

        /// <summary>
        ///环节编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ProcId { get; set; }

        /// <summary>
        /// 是否限定和提交人同机构
        /// </summary>
		[Column(IsNullable = false)]
        public bool IsLimtOrgan { get; set; }
    }
}

