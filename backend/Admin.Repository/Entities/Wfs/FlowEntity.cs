﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_Flow")]
    public class FlowEntity : BaseTenantEntity<string>
    {
        /// <summary>
        ///业务标识编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ActionCode { get; set; }

        /// <summary>
        ///流程模板编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowTempId { get; set; }

        /// <summary>
        ///业务Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string AppId { get; set; }

        /// <summary>
        ///业务编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string AppCode { get; set; }

        /// <summary>
        ///业务摘要
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string AppSummary { get; set; }

        /// <summary>
        ///流程事件
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = false)]
        public string FlowEvent { get; set; }

        /// <summary>
        ///所属机构
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string OrgId { get; set; }

        /// <summary>
        ///流程状态
        /// </summary>
		[Column(IsNullable = false)]
        public EnumFlowStatus FlowStatus { get; set; }

        /// <summary>
        ///开始时间
        /// </summary>
		[Column(IsNullable = false)]
        public DateTime BeginTime { get; set; }

        /// <summary>
        ///限定完成时间
        /// </summary>
		[Column(IsNullable = true)]
        public int? LimtTime { get; set; }

        /// <summary>
        ///结束时间
        /// </summary>
        [Column(IsNullable = true)]
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        ///终止理由
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = false)]
        public string AbandonReason { get; set; }

        /// <summary>
        ///是否发送所有消息给执行人
        /// </summary>
		[Column(IsNullable = false)]
        public bool EnableSendMessageToAllExcuter { get; set; }

        /// <summary>
        ///归档后发消息类型
        /// </summary>
		[Column(IsNullable = true)]
        public EnumSendMessageToExcuterType SendMessageToExcuterType { get; set; }

        /// <summary>
        ///归档后发消息指定人员
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string SendMessageSelectUsers { get; set; }

        /// <summary>
        ///核算机构Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string UnitId { get; set; }

        /// <summary>
        /// 是否自动通过
        /// </summary>
        [Column(IsNullable = false)]
        public bool IsApprovalPass { get; set; }

        /// <summary>
        /// <summary>
        ///楼盘片区负责人
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string ProZoneId { get; set; }

        /// <summary>
        ///楼栋片区负责人
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string PavZoneId { get; set; }

        /// <summary>
        ///待办标题
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string TaskTitle { get; set; }

        /// <summary>
        ///待办内容
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string TaskContent { get; set; }

        /// <summary>
        ///待办描述
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string TaskDescription { get; set; }

        /// <summary>
        ///来源值
        /// </summary>
        [Column(IsNullable = true)]
        public MsgTaskTypeEnum SourceTypeValue { get; set; }
    }
}