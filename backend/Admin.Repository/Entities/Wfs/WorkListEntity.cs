﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_WorkList")]
    public class WorkListEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///流程Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowId { get; set; }

        /// <summary>
        ///环节Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ProcId { get; set; }

        /// <summary>
        /// 执行人Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ExecutorId { get; set; }

        /// <summary>
        /// 执行人类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumExecutorRefType ExecutorRefType { get; set; }

        /// <summary>
        ///账号Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string AccountId { get; set; }

        /// <summary>
        ///账号名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string AccountName { get; set; }

        /// <summary>
        ///需要处理
        /// </summary>
		[Column(IsNullable = false)]
        public bool IsNeed { get; set; }

        /// <summary>
        ///是否已经处理
        /// </summary>
		[Column(IsNullable = false)]
        public bool IsHandled { get; set; }

        /// <summary>
        ///审批状态
        /// </summary>
		[Column(IsNullable = false)]
        public EnumProcApprovalStatus ApprovalStatus { get; set; }

        /// <summary>
        ///处理意见
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string CheckOpinion { get; set; }

        /// <summary>
        ///开始时间
        /// </summary>
		[Column(IsNullable = true)]
        public DateTime? StartTime { get; set; }

        /// <summary>
        ///完成时间
        /// </summary>
		[Column(IsNullable = true)]
        public DateTime? ComplateTime { get; set; }

        /// <summary>
        ///完成期限
        /// </summary>
		[Column(IsNullable = true)]
        public DateTime? LimtTime { get; set; }

        /// <summary>
        ///业务标识
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ActionCode { get; set; }

        /// <summary>
        /// 业务Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string AppId { get; set; }
    }
}

