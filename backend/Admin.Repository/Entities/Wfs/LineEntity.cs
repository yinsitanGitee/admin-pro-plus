﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_Line")]
    public class LineEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///流程Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowId { get; set; }

        /// <summary>
        ///流程图中线Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SchemeLineId { get; set; }

        /// <summary>
        ///业务标识编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ActionCode { get; set; }

        /// <summary>
        ///名称
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Name { get; set; }

        /// <summary>
        ///编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Code { get; set; }

        /// <summary>
        ///来源环节
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FromSchemeProcId { get; set; }

        /// <summary>
        ///目标环节
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string ToSchemeProcId { get; set; }

        /// <summary>
        ///线条件类型
        /// </summary>
		[Column(IsNullable = false)]
        public EnumLineCondType LineCondType { get; set; }

        /// <summary>
        ///流程线图形其他信息
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string SchemeLineInfo { get; set; }
    }
}

