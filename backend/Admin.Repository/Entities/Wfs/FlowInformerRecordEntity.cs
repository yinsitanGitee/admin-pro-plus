﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Wfs
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "WFS_FlowInformerRecord")]
    public class FlowInformerRecordEntity : TenantEntityBase<string>
    {
        /// <summary>
        ///知会人名称
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string Names { get; set; }

        /// <summary>
        ///知会人Ids
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string Ids { get; set; }

        /// <summary>
        /// 流程Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string FlowId { get; set; }

        /// <summary>
        ///来源Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SourceId { get; set; }

        /// <summary>
        /// 节点Id
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string ProcId { get; set; }

        /// <summary>
        /// 来源编号
        /// </summary>
		[Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string SourceCode { get; set; }
    }
}

