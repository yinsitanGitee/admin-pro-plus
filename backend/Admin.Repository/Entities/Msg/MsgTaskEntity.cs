﻿using FreeSql.DataAnnotations;
using Admin.Repository.Enum;

namespace Admin.Repository.Entities.Msg
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "Msg_Task")]
    public class MsgTaskEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 任务来源类别  
        /// </summary>
        [Column(IsNullable = false)]
        public MsgTaskTypeEnum SourceTypeValue { get; set; }

        /// <summary>
        /// 任务来源Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SourceId { get; set; }

        /// <summary>
        /// 任务类别 1-处理任务 2-派单任务 3-回访任务 4-立项任务 5-审核任务
        /// </summary>
        [Column(IsNullable = false)]
        public EnumTaskTypeEunm Type { get; set; }

        /// <summary>
        /// 代办标题
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string Title { get; set; }

        /// <summary>
        /// 任务内容
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = false)]
        public string TaskContent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMidlength, IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 任务发起人
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SendUserName { get; set; }

        /// <summary>
        /// 任务发起人ID
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string SendUserId { get; set; }

        /// <summary>
        /// 发起时间
        /// </summary>
        [Column(IsNullable = false)]
        public DateTime SendDate { get; set; }

        /// <summary>
        /// 任务状态 0-新创建 1-任务接受中（多人完成时）2-进行中 3-已完成 4-已取消
        /// 默认 0-新创建
        /// </summary>
        [Column(IsNullable = false)]
        public int State { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string OrgId { get; set; }

        /// <summary>
        /// 完成人用户Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string CompleteUserId { get; set; }

        /// <summary>
        /// 完成人用户名称
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string CompleteUserName { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [Column(IsNullable = true)]
        public DateTime? CompleteTime { get; set; }

        /// <summary>
        /// 类型  1 待办  2 待阅
        /// </summary>
        [Column(IsNullable = false)]
        public int SysType { get; set; } = 1;
    }

    /// <summary>
    /// 系统待办-任务类别
    /// </summary>
    public enum EnumTaskTypeEunm
    {
        /// <summary>
        /// 处理任务
        /// </summary>
        Deal = 1,
        /// <summary>
        /// 派单任务
        /// </summary>
        Send = 2,
        /// <summary>
        /// 回访任务
        /// </summary>
        Visit = 3,
        /// <summary>
        /// 立项任务
        /// </summary> 
        Project = 4,
        /// <summary>
        /// 审核任务
        /// </summary> 
        Audit = 5,
    }
}