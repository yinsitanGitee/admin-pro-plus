﻿using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Msg
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "Msg_TaskUser")]
    public class MsgTaskUserEntity : BaseTenantEntity<string>
    {
        /// <summary>
        /// 所属任务Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string TaskId { get; set; }

        /// <summary>
        /// 计划接受人Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = false)]
        public string UserId { get; set; }

        /// <summary>
        /// 接收时间
        /// </summary>
        [Column(IsNullable = false)]
        public DateTime ReceiveDate { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [Column(IsNullable = true)]
        public DateTime? CompleteTime { get; set; }

        /// <summary>
        /// 状态
        /// 任务状态 0-新创建 1-任务接受中（多人完成时）2-进行中 3-已完成 4-已取消
        /// </summary>
        [Column(IsNullable = false)]
        public int State { get; set; }
    }
}