﻿using Admin.Repository.Enum;
using FreeSql.DataAnnotations;

namespace Admin.Repository.Entities.Msg
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "Msg_Reminder")]
    public class ReminderEntity : TenantEntityBase<string>
    {
        /// <summary>
        /// 任务来源类别  
        /// </summary>
        [Column(IsNullable = true)]
        public MsgTaskTypeEnum? SourceTypeValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMaxlength, IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string Url { get; set; }

        /// <summary>
        /// 打开类型
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public EnumReminderOpenType OpenType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public EnumReminderType RemainType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public EnumReminderContentType ContentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public bool IsRead { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public DateTime? ReadTime { get; set; }

        /// <summary>
        ///  消息Id
        ///  允许为空,业务消息关联字段
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string MsgId { get; set; }

        /// <summary>
        ///  SourceId
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string SourceId { get; set; }

        /// <summary>
        ///  1是待办的过期提醒 
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public int SourceType { get; set; }

        /// <summary>
        /// 业务编号
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string ActionCode { get; set; }

        /// <summary>
        ///业务Id
        /// </summary>
        [Column(StringLength = BaseStringLength.CommonMinlength, IsNullable = true)]
        public string AppId { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        [Column(IsNullable = false)]
        public DateTime CreateTime { get; set; }
    }

    //
    // 摘要:
    //     提醒内容类型
    public enum EnumReminderOpenType
    {
        //
        // 摘要:
        //     详情
        Detail = 0,
        //
        // 摘要:
        //     弹窗
        Dialog = 1,
        //
        // 摘要:
        Tab = 2,
        //
        // 摘要:
        //     新浏览器选项卡
        NewTarget = 3
    }

    //
    // 摘要:
    //     提醒方式
    public enum EnumReminderType
    {
        //
        // 摘要:
        //     正常消息
        Normal = 0,
        //
        // 摘要:
        //     强提醒
        Strong = 1
    }

    //
    // 摘要:
    //     提醒内容类型
    public enum EnumReminderContentType
    {
        //
        // 摘要:
        //     系统预警
        SystemRemin = 0,
        //
        // 摘要:
        //     消息
        Mesage = 1,
        //
        // 摘要:
        //     新业务预警
        BusinessWarn = 2
    }
}