﻿namespace Admin.Cache.Setting
{
    /// <summary>
    /// Redis配置信息
    /// </summary>
    public class RedisConfigDto
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
