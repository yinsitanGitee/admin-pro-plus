﻿namespace Admin.Cache
{
    /// <summary>
    /// Redis管理器
    /// </summary>
    public class RedisManage
    {
        #region 异步

        /// <summary>
        /// 用于在 key 存在时删除 key
        /// </summary>
        /// <param name="key">键</param>
        /// <returns></returns>
        public static async Task DelAsync(params string[] key)
        {
            await RedisHelper.DelAsync(key);
        }

        /// <summary>
        /// 模糊删除key
        /// </summary>
        /// <returns></returns>
        public static async Task DelAllAsync(string key)
        {
            key += "*";
            var keys = RedisHelper.Keys(key);
            foreach (var item in keys)
                await RedisHelper.DelAsync(item);
        }

        /// <summary>
        /// 获取指定 key 的值
        /// </summary>
        /// <param name="key">键</param>
        /// <returns></returns>
        public static async Task<string> GetAsync(string key)
        {
            return await RedisHelper.GetAsync(key);
        }

        /// <summary>
        /// 获取指定 key 的值
        /// </summary>
        /// <typeparam name="T">数据类型</typeparam>
        /// <param name="key">键</param>
        /// <returns></returns>
        public static async Task<T> GetAsync<T>(string key)
        {
            return await RedisHelper.GetAsync<T>(key);
        }

        /// <summary>
        /// 设置指定 key 的值，所有写入参数object都支持string | byte[] | 数值 | 对象
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static async Task SetAsync(string key, object value)
        {
            await RedisHelper.SetAsync(key, value);
        }

        /// <summary>
        /// 设置指定 key 的值，所有写入参数object都支持string | byte[] | 数值 | 对象
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="expire">有效期</param>
        /// <returns></returns>
        public static async Task SetAsync(string key, object value, TimeSpan expire)
        {
            await RedisHelper.SetAsync(key, value, expire);
        }

        #endregion

        #region 锁

        /// <summary>
        /// 分布式锁
        /// </summary>
        /// <param name="key">锁key</param>
        /// <param name="lockExpirySeconds">锁自动超时时间(秒)</param>
        /// <param name="waitLockSeconds">等待锁时间(秒)</param>
        /// <returns></returns>
        public static async Task<bool> LockAsync(string key, int lockExpirySeconds = 10, double waitLockSeconds = 0)
        {
            string lockKey = "CsRedisLocked:" + key;

            DateTime begin = DateTime.Now;
            while (true)
            {
                //循环获取锁
                if (await RedisHelper.SetNxAsync(lockKey, 1))
                {
                    //设置锁的过期时间
                    await RedisHelper.ExpireAsync(lockKey, lockExpirySeconds);
                    return true;
                }

                //不等待锁则返回
                if (waitLockSeconds == 0) break;

                //超过等待时间，则不再等待
                if ((DateTime.Now - begin).TotalSeconds >= waitLockSeconds) break;

                Thread.Sleep(50);
            }
            return false;

        }

        /// <summary>
        /// 删除锁 执行完代码以后调用释放锁
        /// </summary>
        /// <param name="key"></param>
        public static async Task DelLockAsync(string key)
        {
            string lockKey = "CsRedisLocked:" + key;
            await RedisHelper.DelAsync(lockKey);
        }

        #endregion
    }
}