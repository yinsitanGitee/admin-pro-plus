﻿using Admin.Common.Ioc;
using Admin.Common.Json;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Tenant;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository.Enum;

namespace Admin.Verson
{
    /// <summary>
    /// 升级脚本
    /// </summary>
    public class UpdateDB202408 : BaseVersonService
    {
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="isExcute"></param>
        /// <returns></returns>
        public string ExcuteSql2024081201(bool isExcute)
        {
            var msg = string.Empty;
            if (isExcute)
            {
                try
                {
                    AddConfigItem(new SysConfigItemEntity
                    {
                        ConfigKey = "Watermark.Show",
                        ConfigValue = "1",
                        Remark = "水印是否显示 => 1显示，0隐藏"
                    });
                    msg = "执行成功";
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                }
            }
            return "参数初始化：" + msg;
        }

        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="isExcute"></param>
        /// <returns></returns>
        public string ExcuteSql2024081202(bool isExcute)
        {
            var msg = string.Empty;
            if (isExcute)
            {
                try
                {
                    AddDictionary(new SysDictionaryEntity
                    {
                        Code = "DepartmentType",
                        Name = "部门类型"
                    });
                    msg = "执行成功";
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                }
            }
            return "词典初始化：" + msg;
        }

        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="isExcute"></param>
        /// <returns></returns>
        public string ExcuteSql2024081203(bool isExcute)
        {
            var msg = string.Empty;
            if (isExcute)
            {
                try
                {
                    var t = JsonHelper.GetFileJson<List<SysViewEntity>>(AppContext.BaseDirectory + "InitData/SysViewEntity.json");
                    if (t != null && t.Count > 0)
                    {
                        t.ForEach(a => a.Tenant = TenantManager.Current);
                        IocManager.Instance.GetService<IFreeSql>().InsertOrUpdate<SysViewEntity>().SetSource(t).ExecuteAffrows();
                        RedisHelper.Del(RedisKeyDto.ViewKey);
                    }

                    msg = "执行成功";
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                }
            }
            return "菜单初始化：" + msg;
        }

        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="isExcute"></param>
        /// <returns></returns>
        public string ExcuteSql2024081204(bool isExcute)
        {
            var msg = string.Empty;
            if (isExcute)
            {
                try
                {
                    AddBillCode(new SysBillCodeEntity
                    {
                        Name = "自定义单据编号",
                        Code = "Ls.Form.Code",
                        Length = 5
                    },[
                        new SysBillCodeDetailEntity
                        {
                            Type = BillCodeDetailTypeEnum.Cust,
                            Value = "Cus_",
                            Sort = 1
                        },
                         new SysBillCodeDetailEntity
                        {
                            Type = BillCodeDetailTypeEnum.Date,
                            Value = "yyyyMMdd",
                            Sort = 2
                        }
                    ]);

                    msg = "执行成功";
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                }
            }
            return "菜单初始化：" + msg;
        }
    }
}
