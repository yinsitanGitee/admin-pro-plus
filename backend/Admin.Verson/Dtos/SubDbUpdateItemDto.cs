﻿namespace Admin.Verson.Dtos
{
    /// <summary>
    /// 定义升级dto
    /// </summary>
    public class SubDbUpdateItemDto
    {
        /// <summary>
        /// 执行信息
        /// </summary>
        public string? ExcuteMsg { get; set; }

        /// <summary>
        /// 方法签名
        /// </summary>
        public string MethodName { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        public string AssemFullName { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public decimal Ver { get; set; }
    }
}
