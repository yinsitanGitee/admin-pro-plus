﻿using Admin.Common.Ioc;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Tenant;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Tasks;
using Quartz;
using Quartz.Spi;

namespace Admin.Verson
{
    /// <summary>
    /// 定义升级脚本基础服务
    /// </summary>
    public class BaseVersonService
    {
        /// <summary>
        /// 添加任务
        /// </summary>
        /// <param name="entity"></param>
        protected async void AddTask(SysTaskSchedulingEntity entity)
        {
            if (!IocManager.Instance.GetService<IFreeSql>().Select<SysTaskSchedulingEntity>().Any(a => a.Code == entity.Code))
            {
                entity.Id = Guid.NewGuid().ToString("N");
                entity.Tenant = TenantManager.Current;
                entity.CreateDateTime = DateTime.Now;
                entity.CreateUserId = "system";
                entity.CreateUserName = "system";
                IocManager.Instance.GetService<IFreeSql>().Insert(entity).ExecuteAffrows();
                await IocManager.Instance.GetService<ISchedulerFactory>().AddJob(entity, true, IocManager.Instance.GetService<IJobFactory>());
            }
        }

        /// <summary>
        /// 添加词典
        /// </summary>
        /// <param name="entity"></param>
        protected void AddDictionary(SysDictionaryEntity entity)
        {
            if (!IocManager.Instance.GetService<IFreeSql>().Select<SysDictionaryEntity>().Any(a => a.Code == entity.Code))
            {
                entity.Id = Guid.NewGuid().ToString("N");
                entity.Tenant = TenantManager.Current;
                entity.CreateDateTime = DateTime.Now;
                entity.CreateUserId = "system";
                entity.CreateUserName = "system";
                IocManager.Instance.GetService<IFreeSql>().Insert(entity).ExecuteAffrows();
            }
        }

        /// <summary>
        /// 添加参数
        /// </summary>
        /// <param name="entity"></param>
        protected void AddConfigItem(SysConfigItemEntity entity)
        {
            if (!IocManager.Instance.GetService<IFreeSql>().Select<SysConfigItemEntity>().Any(a => a.ConfigKey == entity.ConfigKey))
            {
                entity.Id = Guid.NewGuid().ToString("N");
                entity.Tenant = TenantManager.Current;
                entity.CreateDateTime = DateTime.Now;
                entity.CreateUserId = "system";
                entity.CreateUserName = "system";
                IocManager.Instance.GetService<IFreeSql>().Insert(entity).ExecuteAffrows();
            }
        }

        /// <summary>
        /// 添加流程定义
        /// </summary>
        /// <param name="entity"></param>
        protected void AddFlowAction(ActionEntity entity)
        {
            if (!IocManager.Instance.GetService<IFreeSql>().Select<ActionEntity>().Any(a => a.Code == entity.Code))
            {
                entity.Id = Guid.NewGuid().ToString("N");
                entity.Tenant = TenantManager.Current;
                entity.CreateDateTime = DateTime.Now;
                entity.CreateUserId = "system";
                entity.CreateUserName = "system";
                IocManager.Instance.GetService<IFreeSql>().Insert(entity).ExecuteAffrows();
            }
        }

        /// <summary>
        /// 添加单据编号
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="detailEntities"></param>
        protected void AddBillCode(SysBillCodeEntity entity, List<SysBillCodeDetailEntity> detailEntities)
        {
            if (!IocManager.Instance.GetService<IFreeSql>().Select<SysBillCodeEntity>().Any(a => a.Code == entity.Code))
            {
                if (string.IsNullOrWhiteSpace(entity.Id))
                    entity.Id = Guid.NewGuid().ToString("N");
                entity.Tenant = TenantManager.Current;
                entity.CreateDateTime = DateTime.Now;
                entity.CreateUserId = "system";
                entity.CreateUserName = "system";
                IocManager.Instance.GetService<IFreeSql>().Insert(entity).ExecuteAffrows();

                foreach (var item in detailEntities)
                {
                    item.BillId = entity.Id;
                    item.Tenant = TenantManager.Current;
                    item.CreateDateTime = DateTime.Now;
                    item.CreateUserId = "system";
                    item.CreateUserName = "system";
                }
                IocManager.Instance.GetService<IFreeSql>().Insert(detailEntities).ExecuteAffrows();
            }
        }

        /// <summary>
        /// 添加菜单、权限
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="detailEntities"></param>
        protected void AddView(List<SysViewEntity> entities)
        {
            var ids = entities.Select(a => a.Id).ToList();
            if (!IocManager.Instance.GetService<IFreeSql>().Select<SysViewEntity>().Any(a => ids.Contains(a.Id)))
            {
                IocManager.Instance.GetService<IFreeSql>().Insert(entities).ExecuteAffrows();
                RedisHelper.Del(RedisKeyDto.ViewKey);
            }
        }
    }
}
