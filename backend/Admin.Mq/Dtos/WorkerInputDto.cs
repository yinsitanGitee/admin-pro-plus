﻿namespace Admin.Mq.Dtos
{
    /// <summary>
    /// 定义QueneBaseInputDto
    /// </summary>
    public class QueneBaseInputDto
    {
        /// <summary>
        /// 是否持久化
        /// </summary>
        public bool Persistent { get; set; } = true;
    }

    /// <summary>
    /// 定义QueneInputDto
    /// </summary>
    public class QueneInputSingDto : QueneBaseInputDto
    {
        /// <summary>
        /// 队列名称
        /// </summary>
        public string Quene { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// 定义QueneInputDto
    /// </summary>
    public class QueneInputDto : QueneBaseInputDto
    {
        /// <summary>
        /// 队列名称
        /// </summary>
        public string Quene { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string[] Message { get; set; }
    }

    /// <summary>
    /// 定义RouteInputDto
    /// </summary>
    public class RouteInputDto : QueneInputDto
    {
        /// <summary>
        /// 路由
        /// </summary>
        public string Route { get; set; }
    }
}
