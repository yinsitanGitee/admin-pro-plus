﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Admin.Mq.Logger
{
    public class RabbitLoggerProvider : BaseProducerPool, ILoggerProvider
    {
        RabbitLoggerOptions loggerOptions;

        public RabbitLoggerProvider(IOptionsMonitor<RabbitLoggerOptions> options) : base(options.CurrentValue)
        {
            loggerOptions = options.CurrentValue;
        }

        protected override int InitializeCount => loggerOptions.InitializeCount;

        /// <summary>
        /// 创建Logger对象
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public ILogger CreateLogger(string categoryName)
        {
            return new RabbitLogger(categoryName, loggerOptions, this);
        }
    }
}
