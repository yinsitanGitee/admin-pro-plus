﻿using Admin.Mq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Collections.Concurrent;

namespace Admin.Mq.Producers
{
    public class RabbitProducerFactory : IRabbitProducerFactory
    {
        readonly ConcurrentDictionary<string, IRabbitClientProducer> clientProducers;
        readonly IServiceProvider serviceProvider;

        public RabbitProducerFactory(IServiceProvider serviceProvider)
        {
            clientProducers = new ConcurrentDictionary<string, IRabbitClientProducer>();
            this.serviceProvider = serviceProvider;
        }

        /// <summary>
        /// 创建客户端发送
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IRabbitClientProducer Create(string name)
        {
            lock (clientProducers)
            {
                if (!clientProducers.TryGetValue(name, out IRabbitClientProducer? rabbitClientProducer) || (rabbitClientProducer as RabbitClientProducer).Disposed)
                {
                    var optionsFactory = serviceProvider.GetService<IOptionsFactory<RabbitProducerOptions>>();
                    var rabbitProducerOptions = optionsFactory.Create(name);
                    if (rabbitProducerOptions.Hosts == null || rabbitProducerOptions.Hosts.Length == 0)
                    {
                        throw new InvalidOperationException($"{nameof(RabbitProducerOptions)} named '{name}' is not configured");
                    }

                    rabbitClientProducer = new RabbitClientProducer(rabbitProducerOptions);
                    clientProducers.AddOrUpdate(name, rabbitClientProducer, (n, b) => rabbitClientProducer);
                }
                return rabbitClientProducer;
            }
        }
    }
}
