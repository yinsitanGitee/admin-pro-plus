﻿namespace Admin.Mq
{
    /// <summary>
    /// 队列名称
    /// </summary>
    public class SysMqQueueSetting
    {
        /// <summary>
        /// LoginLog 登录日志队列
        /// </summary>
        public const string LoginLogQueue = "admin.login.log";

        /// <summary>
        /// ApiLog 接口日志队列
        /// </summary>
        public const string ApiLogQueue = "admin.api.log";
    }
}