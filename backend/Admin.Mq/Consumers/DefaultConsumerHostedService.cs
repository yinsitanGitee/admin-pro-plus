﻿using Microsoft.Extensions.Hosting;

namespace Admin.Mq.Consumers
{
    public class DefaultConsumerHostedService : IHostedService
    {
        IEnumerable<IRabbitConsumerProvider> _rabbitConsumerProviders;
        public DefaultConsumerHostedService(IEnumerable<IRabbitConsumerProvider> rabbitConsumerProviders)
        {
            _rabbitConsumerProviders = rabbitConsumerProviders;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            foreach (var provider in _rabbitConsumerProviders)
            {
                await provider.ListenAsync();
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            cancellationToken.Register(() =>
            {
                foreach (var provider in _rabbitConsumerProviders)
                {
                    provider.Dispose();
                }
            });
            await Task.CompletedTask;
        }
    }
}
