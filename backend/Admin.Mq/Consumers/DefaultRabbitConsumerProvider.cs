﻿using Admin.Mq.Integration;

namespace Admin.Mq.Consumers
{
    public class DefaultRabbitConsumerProvider : IRabbitConsumerProvider
    {
        RabbitConsumerOptions _rabbitConsumerOptions;
        string exchange;
        string queue;
        ListenResult listenResult;
        bool disposed = false;
        Action<RecieveResult> action;

        public DefaultRabbitConsumerProvider(string queue, RabbitConsumerOptions rabbitConsumerOptions, Action<RecieveResult> action) : this("", queue, rabbitConsumerOptions, action) { }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="exchange"></param>
        /// <param name="queue"></param>
        /// <param name="rabbitConsumerOptions"></param>
        /// <param name="action"></param>
        public DefaultRabbitConsumerProvider(string exchange, string queue, RabbitConsumerOptions rabbitConsumerOptions, Action<RecieveResult> action)
        {
            _rabbitConsumerOptions = rabbitConsumerOptions;
            this.exchange = exchange;
            this.queue = queue;
            this.action = action;

            Consumer = RabbitConsumer.Create(rabbitConsumerOptions);
        }

        public RabbitConsumer Consumer { get; }

        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                listenResult?.Stop();
                Consumer?.Dispose();
            }
        }


        public async Task ListenAsync()
        {
            if (disposed) throw new ObjectDisposedException(nameof(DefaultRabbitConsumerProvider));

            if (listenResult == null)
            {
                if (string.IsNullOrEmpty(exchange))
                {
                    listenResult = Consumer.Listen(queue, options =>
                    {
                        options.AutoDelete = _rabbitConsumerOptions.AutoDelete;
                        options.Durable = _rabbitConsumerOptions.Durable;
                        options.AutoAck = _rabbitConsumerOptions.AutoAck;
                        options.Arguments = _rabbitConsumerOptions.Arguments;
                        options.FetchCount = _rabbitConsumerOptions.FetchCount;
                    }, action);
                }
                else
                {
                    listenResult = Consumer.Listen(exchange, queue, options =>
                    {
                        options.AutoDelete = _rabbitConsumerOptions.AutoDelete;
                        options.Durable = _rabbitConsumerOptions.Durable;
                        options.AutoAck = _rabbitConsumerOptions.AutoAck;
                        options.Arguments = _rabbitConsumerOptions.Arguments;
                        options.FetchCount = _rabbitConsumerOptions.FetchCount;
                        options.Type = _rabbitConsumerOptions.Type;
                        options.RouteQueues = _rabbitConsumerOptions.RouteQueues;
                    }, action);
                }
            }

            await Task.CompletedTask;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(exchange))
            {
                return $"queue:{queue} from {Consumer}";
            }
            else
            {
                return $"queue:{queue}(exchange:{exchange}) from {Consumer}";
            }
        }
    }
}
