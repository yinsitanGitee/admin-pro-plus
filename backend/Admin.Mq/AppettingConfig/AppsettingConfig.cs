﻿namespace Admin.Mq.AppettingConfig
{
    /// <summary>
    /// 定义mqh配置类
    /// </summary>
    public class MqConfigDto
    {
        /// <summary>
        ///  消息生产者接口token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 服务器地址
        /// </summary>
        public string[] Hosts { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}
