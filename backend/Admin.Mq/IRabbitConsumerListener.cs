﻿using Admin.Mq.Integration;

namespace Admin.Mq
{
    public interface IRabbitConsumerListener
    {
        /// <summary>
        /// 消费消息
        /// </summary>
        /// <param name="recieveResult"></param>
        /// <returns></returns>
        Task ConsumeAsync(RecieveResult recieveResult);
    }
}
