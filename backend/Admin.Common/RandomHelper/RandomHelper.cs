﻿using System.Text;

namespace Admin.Common.RandomHelper
{
    public class RandomHelper
    {
        /// <summary>
        /// 生成6位随机数
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string GeneratesRandomNumber()
        {
            Random random = new Random();
            return random.Next(100000, 999999).ToString();
        }
        /// <summary>
        /// 生成随机的字符串
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetRandomString(int length)
        {
            string sbase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            Random random = new();

            StringBuilder sb = new();

            for (int i = 0; i < length; i++)
            {
                int number = random.Next(sbase.Length);
                sb.Append(sbase.AsSpan(number, 1));
            }

            return sb.ToString();
        }

        /// <summary>
        /// 随机生成编号
        /// </summary>
        /// <returns></returns>
        public static string CreateNo()
        {
            Random random = new Random();
            string strRandom = random.Next(1000, 10000).ToString(); //生成编号 
            string code = DateTime.Now.ToString("yyyyMMddHHmmss") + strRandom;//形如
            return code;
        }
    }
}
