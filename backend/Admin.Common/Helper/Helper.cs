﻿namespace Admin.Common.Helper
{
    public static class Helper
    {
        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <param name="bflag">是否秒</param>
        /// <returns></returns>
        public static string GetTimeStamp(bool bflag = true)
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            string ret;
            if (bflag)
                ret = Convert.ToInt64(ts.TotalSeconds).ToString();
            else
                ret = Convert.ToInt64(ts.TotalMilliseconds).ToString();

            return ret;
        }

        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <param name="bflag">是否秒</param>
        /// <returns></returns>
        public static long GetTimeStampLong(bool bflag = true)
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            long ret;
            if (bflag)
                ret = Convert.ToInt64(ts.TotalSeconds);
            else
                ret = Convert.ToInt64(ts.TotalMilliseconds);

            return ret;
        }
    }
}
