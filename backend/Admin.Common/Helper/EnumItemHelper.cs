﻿using System.ComponentModel;
using System.Reflection;

namespace Admin.Common.Helper
{
    //
    // 摘要:
    //     枚举项帮助类
    public static class EnumItemHelper
    {
        //
        // 摘要:
        //     获取指定枚举描述
        //
        // 参数:
        //   type:
        //
        //   nums:
        public static string GetEnumDesc(Type type, long nums)
        {
            if (!type.IsEnum)
            {
                throw new NotSupportedException("类型应为Enum");
            }

            List<EnumItem> enumFieldList = GetEnumFieldList(type);
            return enumFieldList.SingleOrDefault((a) => a.EnumValue == nums).EnumDes;
        }

        //
        // 摘要:
        //     枚举序列化成列表
        //
        // 参数:
        //   type:
        public static List<EnumItem> GetEnumFieldList(Type type)
        {
            if (!type.IsEnum)
            {
                throw new NotSupportedException("类型应为Enum");
            }

            Type typeofDescriptionAttribute = typeof(DescriptionAttribute);
            return (from a in (from a in type.GetFields(BindingFlags.Static | BindingFlags.Public)
                               where !a.Name.Equals("Unknown", StringComparison.OrdinalIgnoreCase)
                               select a).Select(delegate (FieldInfo a)
                               {
                                   DescriptionAttribute descriptionAttribute = a.GetCustomAttributes(typeofDescriptionAttribute, inherit: false).FirstOrDefault() as DescriptionAttribute;
                                   return new EnumItem
                                   {
                                       EnumDes = descriptionAttribute == null ? a.Name : descriptionAttribute.Description,
                                       EnumName = a.Name,
                                       EnumValue = Convert.ToInt64(a.GetRawConstantValue()),
                                       EnumValueString = Convert.ToString(a.GetRawConstantValue())
                                   };
                               })
                    orderby a.EnumValue
                    select a).ToList();
        }
    }

    //
    // 摘要:
    //     枚举字段对象
    public class EnumItem
    {
        //
        // 摘要:
        //     Enum的数字值
        public long EnumValue { get; set; }

        //
        // 摘要:
        //     前端所使用的EnumValue的字符串表现形式(前端对于数字型处理有BUG)
        public string EnumValueString { get; set; }

        //
        // 摘要:
        //     Enum的字符
        public string EnumName { get; set; }

        //
        // 摘要:
        //     说明
        public string EnumDes { get; set; }
    }
}
