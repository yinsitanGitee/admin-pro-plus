﻿using Autofac;

namespace Admin.Common.Ioc
{
    /// <summary>
    /// 定义Autofac Ioc管理者
    /// </summary> 
    public class IocManager
    {
        /// <summary>
        /// Instance
        /// </summary>
        public static IocManager Instance = new();

        /// <summary>
        /// Container
        /// </summary>
        public ILifetimeScope Container { get; set; }

        /// <summary>
        /// GetService
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetService<T>() where T : notnull
        {
            while (true)
            {
                if (Container != null) return Container.Resolve<T>();
            }
        }

        /// <summary>
        /// GetService
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceKey"></param>
        /// <returns></returns>
        public T GetService<T>(string serviceKey) where T : notnull
        {
            while (true)
            {
                if (Container != null) return Container.ResolveKeyed<T>(serviceKey);
            }
        }

        /// <summary>
        /// GerSerciceByOptiona
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceKey"></param>
        /// <returns></returns>
        public T GerSerciceByOptiona<T>(string serviceKey) where T : class
        {
            while (true)
            {
                if (Container != null)
                {
                    return Container.ResolveOptionalKeyed<T>(serviceKey) ?? throw new Exception($"{serviceKey}未获取到IOC实例,请检查是否成功注入"); ;
                }
            }
        }

        /// <summary>
        /// GetService
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            while (true)
            {
                if (Container != null) return Container.Resolve(serviceType);
            }
        }
    }
}
