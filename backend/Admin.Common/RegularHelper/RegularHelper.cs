﻿using System.Text.RegularExpressions;

namespace Admin.Common.Common.RegularHelper
{
    /// <summary>
    /// 定义正则表达式帮助类
    /// </summary>
    public partial class RegularHelper
    {
        /// <summary>
        /// 手机号校验
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static bool IsPhone(string phone)
        {
            return Regex.IsMatch(phone, @"^1[123456789]\d{9}$");
        }

        /// <summary>
        /// 手机号格式化
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="type">1代表手机号中间4位变为****</param>
        /// <returns></returns>
        public static string PhoneParse(string phone, int type)
        {
            return type switch
            {
                1 => PhoneRegex().Replace(phone, "$1****$2"),
                _ => "",
            };
        }

        /// <summary>
        /// 手机号格式化正则表达式
        /// </summary>
        /// <returns></returns>
        [GeneratedRegex("(\\d{3})\\d{4}(\\d{4})")]
        private static partial Regex PhoneRegex();
    }
}
