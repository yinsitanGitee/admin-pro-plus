﻿namespace Admin.Common.BusinessResult
{
    /// <summary>
    /// 统一返回业务结果数据
    /// </summary>
    public class BusinessResult<TResult> : BusinessBaseResult
    {
        /// <summary>
        /// 返回结果数据
        /// </summary>
        public TResult? ResultData { get; set; }
    }

    /// <summary>
    /// 统一返回结果(不带结果数据)
    /// </summary>
    public class BusinessResult : BusinessBaseResult
    {

    }

    public class BusinessBaseResult
    {
        /// <summary>
        /// 设置请求错误
        /// </summary>
        /// <param name="message"></param>
        public void BusinessError(string message = "Error")
        {
            Code = BusinessState.Fail;
            Message = message;
        }

        /// <summary>
        /// 设置请求错误
        /// </summary>
        /// <param name="message"></param>
        public void BusinessSuccess(string message = "Success")
        {
            Code = BusinessState.Success;
            Message = message;
        }

        /// <summary>
        /// 返回结果信息
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// 是否成功(业务层面)
        /// </summary>
        public BusinessState Code { get; set; } = BusinessState.Success;
    }

    public class BusinessResponse
    {
        /// <summary>
        /// Success
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static BusinessResult Success(BusinessState code = BusinessState.Success, string? msg = null) => new() { Code = code, Message = msg };

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static BusinessResult Error(BusinessState code = BusinessState.Fail, string? msg = null) => new() { Code = code, Message = msg };
    }

    public class BusinessResponse<T>
    {
        /// <summary>
        /// Success
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static BusinessResult<T> Success(T data, string? msg = null) => new() { Message = msg, ResultData = data };

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static BusinessResult<T> Error(string? msg = null, BusinessState code = BusinessState.Fail) => new() { Code = code, Message = msg };

    }
}
