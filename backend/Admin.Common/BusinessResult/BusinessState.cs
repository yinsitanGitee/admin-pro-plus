﻿namespace Admin.Common.BusinessResult
{
    public enum BusinessState
    {
        /// <summary>
        /// 200
        /// </summary>
        Success = 200,
        /// <summary>
        /// Fail
        /// </summary>
        Fail = 500
    }
}

