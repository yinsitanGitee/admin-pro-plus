﻿namespace Admin.Common.SysEnum
{
    /// <summary>
    /// 平台类型枚举
    /// </summary>
    public enum PlatformEnum
    {
        /// <summary>
        /// b端服务端
        /// </summary>
        BServe = 1,

        /// <summary>
        /// 消息队列消费者端
        /// </summary>
        Consumer = 3
    }
}
