﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace Admin.Common.Json
{
    /// <summary>
    /// Json帮助类
    /// </summary>
    public class JsonHelper
    {
        /// <summary>
        /// GetFileJson
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static T? GetFileJson<T>(string filepath)
        {
            using FileStream fs = new(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return System.Text.Json.JsonSerializer.Deserialize<T>(fs);
        }

        /// <summary>
        /// SerializeObject
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SerializeObject(object? value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// DeserializeObject
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T? DeserializeObject<T>(string value)
        {
            if (value == null) return default;
            return JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// DeserializeObject
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object? DeserializeObject(string value)
        {
            if (value == null) return default;
            return JsonConvert.DeserializeObject(value);
        }

        /// <summary>
        /// json转字典
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue>? DeserializeDictionary<TKey, TValue>(string value) where TKey : notnull
        {
            return JsonConvert.DeserializeObject<Dictionary<TKey, TValue>>(value);
        }

        /// <summary>
        /// ToJObject
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static JObject? ToJObject(object? source)
        {
            if (source == null) return null;
            var dictionary = new JObject();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
            {
                object? value = property.GetValue(source);
                if (value != null) dictionary.Add(property.Name, value.ToString());
            }
            return dictionary;
        }

        /// <summary>
        /// object转成词典
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Dictionary<string, string>? ToDictionary(object? source)
        {
            if (source == null) return null;
            var dictionary = new Dictionary<string, string>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
            {
                object? value = property.GetValue(source);
                if (value != null) dictionary.Add(property.Name, value.ToString());
            }
            return dictionary;
        }
    }
}