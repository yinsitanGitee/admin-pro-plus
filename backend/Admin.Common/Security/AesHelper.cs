﻿using Admin.Common.SysEnum;
using System.Security.Cryptography;
using System.Text;

namespace Admin.Common.Security
{
    /// <summary>
    /// SecurityHelper
    /// </summary>
    public static class SecurityHelper
    {
        #region 默认密钥

        public const string DefaultKey = "LesoftV916891016";

        #endregion

        #region AES加密

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="source"></param>
        /// <param name="key">密钥</param>
        /// <param name="iv">初始向量</param>
        /// <param name="padding">填充模式</param>
        /// <param name="mode">加密模式</param>
        /// <param name="ciphertextType">密文类型</param>
        /// <returns></returns>
        public static string AESEncrypt(this string source, string key = DefaultKey, string iv = DefaultKey, PaddingMode padding = PaddingMode.PKCS7, CipherMode mode = CipherMode.CBC, CiphertextType ciphertextType = CiphertextType.Base64)
        {
            return AESBaseEncrypt(source, key, iv, padding, mode, ciphertextType);
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="source"></param>
        /// <param name="key">密钥</param>
        /// <param name="iv">初始向量</param>
        /// <param name="padding">填充模式</param>
        /// <param name="mode">加密模式</param>
        /// <param name="ciphertextType">密文类型</param>
        /// <returns></returns>
        public static string AESEncrypt(this Guid source, string key = DefaultKey, string iv = DefaultKey, PaddingMode padding = PaddingMode.PKCS7, CipherMode mode = CipherMode.CBC, CiphertextType ciphertextType = CiphertextType.Base64)
        {
            return AESBaseEncrypt(source.ToString(), key, iv, padding, mode, ciphertextType);
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="source"></param>
        /// <param name="key">密钥</param>
        /// <param name="iv">初始向量</param>
        /// <param name="padding">填充模式</param>
        /// <param name="mode">加密模式</param>
        /// <param name="ciphertextType">密文类型</param>
        /// <returns></returns>
        private static string AESBaseEncrypt(string source, string key, string iv, PaddingMode padding, CipherMode mode, CiphertextType ciphertextType)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] textBytes = Encoding.UTF8.GetBytes(source);
            byte[] ivBytes = Encoding.UTF8.GetBytes(iv);

            byte[] useKeyBytes = new byte[16];
            byte[] useIvBytes = new byte[16];

            if (keyBytes.Length > useKeyBytes.Length)
            {
                Array.Copy(keyBytes, useKeyBytes, useKeyBytes.Length);
            }
            else
            {
                Array.Copy(keyBytes, useKeyBytes, keyBytes.Length);
            }
            if (ivBytes.Length > useIvBytes.Length)
            {
                Array.Copy(ivBytes, useIvBytes, useIvBytes.Length);
            }
            else
            {
                Array.Copy(ivBytes, useIvBytes, ivBytes.Length);
            }

            Aes aes = Aes.Create();
            aes.KeySize = 256;//秘钥的大小，以位为单位,128,256等
            aes.BlockSize = 128;//支持的块大小
            aes.Padding = padding;//填充模式
            aes.Mode = mode;
            aes.Key = useKeyBytes;
            aes.IV = useIvBytes;//初始化向量，如果没有设置默认的16个0

            ICryptoTransform cryptoTransform = aes.CreateEncryptor();
            byte[] resultBytes = cryptoTransform.TransformFinalBlock(textBytes, 0, textBytes.Length);
            return CipherByteArrayToString(resultBytes, ciphertextType);
        }

        #endregion

        #region AES解密

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="source"></param>
        /// <param name="key">密钥</param>
        /// <param name="iv">初始向量</param>
        /// <param name="padding">填充模式</param>
        /// <param name="mode">加密模式</param>
        /// <param name="ciphertextType">密文类型</param>
        /// <returns></returns>
        public static (bool isSuccess, string text) AESDecrypt(this string source, string key = DefaultKey, string iv = DefaultKey, PaddingMode padding = PaddingMode.PKCS7, CipherMode mode = CipherMode.CBC, CiphertextType ciphertextType = CiphertextType.Base64)
        {
            try
            {
                byte[] keyBytes = Encoding.UTF8.GetBytes(key);
                byte[] textBytes = CiphertextStringToByteArray(source, ciphertextType);
                byte[] ivBytes = Encoding.UTF8.GetBytes(iv);

                byte[] useKeyBytes = new byte[16];
                byte[] useIvBytes = new byte[16];

                if (keyBytes.Length > useKeyBytes.Length)
                {
                    Array.Copy(keyBytes, useKeyBytes, useKeyBytes.Length);
                }
                else
                {
                    Array.Copy(keyBytes, useKeyBytes, keyBytes.Length);
                }

                if (ivBytes.Length > useIvBytes.Length)
                {
                    Array.Copy(ivBytes, useIvBytes, useIvBytes.Length);
                }
                else
                {
                    Array.Copy(ivBytes, useIvBytes, ivBytes.Length);
                }

                Aes aes = Aes.Create();
                aes.KeySize = 256;//秘钥的大小，以位为单位,128,256等
                aes.BlockSize = 128;//支持的块大小
                aes.Padding = padding;//填充模式
                aes.Mode = mode;
                aes.Key = useKeyBytes;
                aes.IV = useIvBytes;//初始化向量，如果没有设置默认的16个0

                ICryptoTransform decryptoTransform = aes.CreateDecryptor();
                byte[] resultBytes = decryptoTransform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                return (true, Encoding.UTF8.GetString(resultBytes));
            }
            catch (Exception ex)
            {
                return (false, ex.Message);
            }
        }

        /// <summary>
        /// 通过密文返回加密byte数组
        /// </summary>
        /// <param name="ciphertext"></param>
        /// <param name="ciphertextType"></param>
        /// <returns></returns>
        public static byte[] CiphertextStringToByteArray(string ciphertext, CiphertextType ciphertextType)
        {
            byte[] cipherByte = ciphertextType switch
            {
                CiphertextType.Base64 => Convert.FromBase64String(ciphertext),
                CiphertextType.Hex => HexStringToByteArray(ciphertext),
                _ => Convert.FromBase64String(ciphertext),
            };
            return cipherByte;
        }

        /// <summary>
        /// 通过加密的Byte数组返回加密后的密文
        /// </summary>
        /// <param name="cipherByte"></param>
        /// <param name="ciphertextType"></param>
        /// <returns></returns>
        public static string CipherByteArrayToString(byte[] cipherByte, CiphertextType ciphertextType)
        {
            string ciphertext = ciphertextType switch
            {
                CiphertextType.Base64 => Convert.ToBase64String(cipherByte),
                CiphertextType.Hex => ByteArrayToHexString(cipherByte),
                _ => Convert.ToBase64String(cipherByte),
            };
            return ciphertext;
        }

        /// <summary>
        /// Hex字符串转Byte
        /// </summary>
        /// <param name="hexContent"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string hexContent)
        {
            hexContent = hexContent.Replace(" ", "");
            byte[] buffer = new byte[hexContent.Length / 2];
            for (int i = 0; i < hexContent.Length; i += 2)
            {
                buffer[i / 2] = Convert.ToByte(hexContent.Substring(i, 2), 16);
            }
            return buffer;
        }

        /// <summary>
        /// Byte转Hex字符串
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new(data.Length * 3);
            foreach (byte b in data)
            {
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            }
            return sb.ToString().ToUpper();
        }
        #endregion
    }
}
