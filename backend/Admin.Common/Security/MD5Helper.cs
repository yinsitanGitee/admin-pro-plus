﻿using System.Text;

namespace Admin.Common.Security
{
    public class MD5Helper
    {
        /// <summary>
        /// MD5加密字符串（32位大写）
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <param name="charset">编码格式</param>
        /// <returns>加密后的字符串</returns>
        public static string Encrypt(string source, string charset = "UTF-8")
        {
            using var md5 = System.Security.Cryptography.MD5.Create();
            var result = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(source));
            var strResult = BitConverter.ToString(result);
            strResult = strResult.Replace("-", "").ToUpper();
            return strResult;
        }

        /// <summary>
        /// MD5加密字符串（32位小写）
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <param name="charset">编码格式</param>
        /// <returns>加密后的字符串</returns>
        public static string EncryptLower(string source, string charset = "UTF-8")
        {
            using var md5 = System.Security.Cryptography.MD5.Create();
            var result = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(source));
            var strResult = BitConverter.ToString(result);
            strResult = strResult.Replace("-", "").ToLower();
            return strResult;
        }
    }
}
