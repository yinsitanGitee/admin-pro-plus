﻿namespace Admin.Common.Dto
{
    /// <summary>
    /// 定义输入dto
    /// </summary>
    public class InputDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Ids
        /// </summary>
        public List<string> Ids { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        public List<string> BillCode { get; set; }
    }
}
