﻿namespace Admin.Common.Log.Dtos
{
    public class LogDto
    {
        /// <summary>
        /// 内容
        /// </summary>
        public string? Content { get; set; }

        /// <summary>
        /// 日志文件名称
        /// </summary>
        public string? FullName { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public string? DateTime { get; set; }
    }
}
