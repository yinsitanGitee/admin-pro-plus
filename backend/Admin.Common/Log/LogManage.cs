﻿using Admin.Common.Log.Dtos;
using System.Threading.Channels;

namespace Admin.Common.Log
{
    /// <summary>
    /// 日志管理器
    /// </summary>
    public static class LogManage
    {
        /// <summary>
        ///锁
        /// </summary>
        private readonly static ReaderWriterLockSlim LogWriteLock = new();

        /// <summary>
        /// 队列
        /// </summary>
        private readonly static Channel<LogDto> LogChannel = Channel.CreateUnbounded<LogDto>();

        /// <summary>
        /// 文件路径
        /// </summary>
        private static string FilePath = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// LogManage
        /// </summary>
        static LogManage()
        {
            Task.Factory.StartNew(async () =>
            {
                while (await LogChannel.Reader.WaitToReadAsync())
                {
                    if (LogChannel.Reader.TryRead(out LogDto? log))
                    {
                        if (log == null) continue;
                        try
                        {
                            string filePath = Path.Combine(FilePath, $"log\\{DateTime.Now:yyyy-MM-dd}");

                            if (!Directory.Exists(filePath)) { Directory.CreateDirectory(filePath); }

                            LogWriteLock.EnterWriteLock();

                            using StreamWriter write = File.AppendText(Path.Combine(filePath, $"{DateTime.Now:yyyy-MM-dd-HH}-{log.FullName}.log"));
                            write.WriteLine($"{log.DateTime}:{log.Content}");
                            write.Close();
                        }
                        catch
                        {

                        }
                        finally
                        {
                            LogWriteLock.ExitWriteLock();
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
            });
        }

        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fullName"></param>
        private static void Log(string content, string fullName)
        {
            LogChannel.Writer.TryWrite(new LogDto()
            {
                Content = content,
                DateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm"),
                FullName = fullName
            });
        }

        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fullName"></param>
        public static void Loger(this string content, string fullName = "LogInfo") => Log(content, fullName);
    }
}