﻿using Admin.Common.SysEnum;

namespace Admin.Common.SysStatic
{
    /// <summary>
    /// 定义系统底层配置
    /// </summary>
    public static class SystemConfig
    {
        /// <summary>
        /// 系统项目Kestrel路径
        /// </summary>
        public static string? SystemKestrelHttpUrl = string.Empty;

        /// <summary>
        /// 系统目标类型
        /// </summary>
        public static PlatformEnum? SystemPlatformType = PlatformEnum.BServe;

        /// <summary>
        /// 系统解决方案名称
        /// </summary>
        public static string? SystemName = string.Empty;
    }
}
