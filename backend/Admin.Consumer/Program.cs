using Admin.Application.Sys.ApiLog;
using Admin.Application.Sys.LoginLog;
using Admin.Common.Ioc;
using Admin.Common.Log;
using Admin.Common.SysEnum;
using Admin.Common.SysStatic;
using Admin.Core;
using Admin.Mq;
using Admin.Mq.Integration;
using Admin.Repository.Tenant;
using FreeSql;

FreeSqlCloud<string> fsql = new();

SystemConfig.SystemPlatformType = PlatformEnum.Consumer;
var dto = new Startup.InitDto
{
    FreeSql = fsql
};

var builder = WebApplication.CreateBuilder(args);
var appSettingConfig = builder.ConfigureServices(dto);

#region Mq

var arguments = new Dictionary<string, object>() { { "x-queue-type", "classic" } };

#region 普通模式  

builder.Services.AddRabbitConsumer(options =>
{
    options.Hosts = appSettingConfig.MqConfig.Hosts;
    options.Password = appSettingConfig.MqConfig.Password;
    options.Port = appSettingConfig.MqConfig.Port;
    options.UserName = appSettingConfig.MqConfig.UserName;
    options.VirtualHost = "/";
    options.Arguments = arguments;
    options.Durable = true;
    options.AutoDelete = true;
    options.FetchCount = 1;
    options.AutoAck = false;
}).AddListener(SysMqQueueSetting.LoginLogQueue, result => ChangeCurrenTenant(result, async result =>
{
    await IocManager.Instance.GetService<ILoginLogService>().Add(result.Body);
})).AddListener(SysMqQueueSetting.ApiLogQueue, result => ChangeCurrenTenant(result, async result =>
{
    await IocManager.Instance.GetService<IApiLogService>().Add(result.Body);
}));

#endregion

#endregion

var app = builder.Build();

app.Configure(dto);

app.UseRouting();

app.Run();

#region 修改当前租户

void ChangeCurrenTenant(RecieveResult data, Action<RecieveResult> result)
{
    try
    {
        if (data.Options.Headers != null)
        {
            object? value = null;
            data.Options.Headers?.TryGetValue("Tenant", out value);
            if (value != null)
            {
                var tenant = System.Text.Encoding.UTF8.GetString((byte[])value);
                TenantManager.Current = tenant;
                fsql.Change(tenant);
            }
            else
            {
                $"异常信息：租户为null，队列内容：{data}".Loger(data.RoutingKey);
                data.RollBack();
                return;
            }
        }
        result(data);
    }
    catch (Exception ex)
    {
        $"异常信息：{ex}，队列内容：{data}".Loger(data.RoutingKey);
        data.RollBack();
    }
}

#endregion