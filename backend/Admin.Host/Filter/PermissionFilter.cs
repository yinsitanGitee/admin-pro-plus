﻿using Microsoft.AspNetCore.Mvc.Filters;
using Admin.Core.Auth;
using Admin.Core.Service.Cache;
using Admin.Common.Ioc;

namespace Admin.Host.Filter
{
    /// <summary>
    /// 权限拦截器
    /// </summary>
    public class PermissionFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 权限编码
        /// </summary>
        public string[] _permissionCodes { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="permissionCodes"></param>
        public PermissionFilter(params string[] permissionCodes)
        {
            _permissionCodes = permissionCodes;
        }

        /// <summary>
        /// OnActionExecutionAsync
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (_permissionCodes != null)
            {
                var u = IocManager.Instance.GetService<ICurrentUser>();

                if (u != null)
                {
                    var ps = await IocManager.Instance.GetService<ICacheService>().GetUserPermissionCache(u.UserId);

                    foreach (var item in _permissionCodes)
                    {
                        if (!ps.Views.Contains(item))
                        {
                            context.HttpContext.Response.StatusCode = 403;
                            return;
                        }
                    }
                }
            }

            await next();
        }
    }
}
