﻿using Microsoft.AspNetCore.Mvc;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Tenant;
using Admin.Cache;
using Admin.Core.Filter;
using Admin.Core.Service.Cache.Dto;
using Admin.Host.Filter;
using Admin.Verson.Dtos;
using System.Reflection;

namespace Admin.Host.Controllers.Verson
{
    /// <summary>
    /// 版本管理
    /// </summary>
    /// <param name="freeSql"></param>
    [ApiController]
    [NoApiLog]
    [Route("api/[controller]/[action]")]
    public class VersonController(IFreeSql freeSql) : BaseControllerFilter
    {
        /// <summary>
        ///判断是否需要升级
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string> IsNeedSubUpdate()
        {
            DirectoryInfo root = new(AppContext.BaseDirectory);
            FileInfo[] files = root.GetFiles();
            if (files.Length == 0) return "0";
            var versionFiles = files.ToList().Where(t => t.Name.Contains("Verson.dll")).ToList();
            if (versionFiles.Count == 0) return "0";
            var sub = await freeSql.Select<SysSubProjectEntity>().Where(t => t.Tenant == TenantManager.Current).ToOneAsync();

            for (int i = 0; i < versionFiles.Count; i++)
            {
                var versonFile = versionFiles[i];
                try
                {
                    Assembly assemble = Assembly.LoadFrom(versionFiles[i].FullName);
                    var ts = assemble.GetTypes();
                    var maxAssemble = ts.OrderByDescending(th => th.Name).FirstOrDefault();

                    if (maxAssemble != null)
                    {
                        var maxVer = maxAssemble.GetMethods().Where(th => th.Name.Contains("ExcuteSql")).OrderByDescending(a => a.Name).FirstOrDefault();
                        if (maxVer != null)
                        {
                            if (sub == null) return "1";
                            if (Convert.ToInt32(maxVer.Name.Replace("ExcuteSql", "")) > sub.Verson)
                                return "1";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("搜索文件:" + versonFile.Name + ex.Message);
                }
            }
            return "0";
        }

        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        public async Task<string> ExecuteDbUpdateMethod(SubDbUpdateItemDto dto)
        {
            try
            {
                if (await RedisManage.LockAsync(RedisKeyDto.VersonExecLockKey, 600, 600))
                {
                    var targetType = Type.GetType(dto.AssemFullName, true, true) ?? throw new Exception($"类型:{dto.AssemFullName}不存在");
                    object? obj = Activator.CreateInstance(targetType) ?? throw new Exception($"实例创建失败。类型:{dto.AssemFullName}");
                    var m = targetType.GetMethod(dto.MethodName);
                    var msg = m?.Invoke(obj, new object[] { true })?.ToString();
                    if (!string.IsNullOrWhiteSpace(msg) && msg.Contains("执行成功", StringComparison.CurrentCulture))
                    {
                        var sub = await freeSql.Select<SysSubProjectEntity>().Where(t => t.Tenant == TenantManager.Current).ToOneAsync();
                        var thisVer = Convert.ToInt32(dto.MethodName.Replace("ExcuteSql", ""));
                        if (sub == null)
                            await freeSql.Insert(new SysSubProjectEntity
                            {
                                Verson = thisVer,
                                Tenant = TenantManager.Current
                            }).ExecuteAffrowsAsync();
                        else if (thisVer > sub.Verson)
                            await freeSql.Update<SysSubProjectEntity>().Set(t => new { Verson = thisVer }).Where(a => a.Tenant == sub.Tenant).ExecuteAffrowsAsync();
                    }
                    return msg;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                await RedisManage.DelLockAsync(RedisKeyDto.VersonExecLockKey);
            }

            return string.Empty;
        }

        /// <summary>
        /// 获取升级脚本
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpGet]
        public async Task<List<SubDbUpdateItemDto>> GetUpdateInfoList()
        {
            var li = new List<SubDbUpdateItemDto>();
            DirectoryInfo root = new(AppContext.BaseDirectory);
            FileInfo[] files = root.GetFiles();
            if (files.Length == 0) return [];
            var versionFiles = files.Where(t => t.Name.Contains("Verson.dll")).ToList();
            if (versionFiles.Count == 0) return [];
            var sub = await freeSql.Select<SysSubProjectEntity>().Where(t => t.Tenant == TenantManager.Current).ToOneAsync();
            for (int i = 0; i < versionFiles.Count; i++)
            {
                try
                {
                    Assembly assemble = Assembly.LoadFrom(versionFiles[i].FullName);
                    Type[]? theassem = null;
                    var verson = -1;
                    if (sub == null) theassem = assemble.GetTypes().OrderBy(th => th.Name).Where(th => th.Name.IndexOf("UpdateDB") > -1).ToArray();
                    else
                    {
                        verson = sub.Verson;
                        var verDate = Convert.ToInt32(sub.Verson.ToString()[..6]);
                        theassem = assemble.GetTypes().OrderBy(th => th.Name).Where(th => th.Name.IndexOf("UpdateDB") > -1 && Convert.ToInt32(th.Name.Replace("UpdateDB", "")) >= verDate).ToArray();
                    }

                    foreach (Type t in theassem)
                    {
                        try
                        {
                            object? obj = Activator.CreateInstance(t);
                            MethodInfo[] me = t.GetMethods().OrderBy(th => th.Name).Where(th => th.Name.Contains("ExcuteSql") && Convert.ToInt32(th.Name.Replace("ExcuteSql", "")) > verson).ToArray();
                            var subUpdateItems = new List<SubDbUpdateItemDto>();
                            foreach (MethodInfo m in me)
                            {
                                try
                                {
                                    if (!m.Name.Contains("ExcuteSql")) continue;
                                    var temp = new SubDbUpdateItemDto
                                    {
                                        Ver = Convert.ToInt32(m.Name.Replace("ExcuteSql", ""))
                                    };
                                    if (temp.Ver < verson) continue;
                                    temp.ExcuteMsg = m.Invoke(obj, new object[] { false })?.ToString();
                                    temp.MethodName = m.Name;
                                    temp.AssemFullName = t.FullName + "," + t.Namespace;
                                    subUpdateItems.Add(temp);
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("获取方法" + m.Name + ex.Message);
                                }
                            }
                            li.AddRange(subUpdateItems);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("构建类" + t.Name + ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("遍历文件:" + versionFiles[i].Name + ex.Message);
                }
            }

            return [.. li.OrderBy(t => t.Ver)];
        }
    }
}
