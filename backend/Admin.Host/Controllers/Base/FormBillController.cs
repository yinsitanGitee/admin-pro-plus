﻿using Admin.Application.Base.FormBill;
using Admin.Application.Base.FormBill.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.Flow.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Web.Controllers.Base
{
    /// <summary>
    /// 表单管理
    /// </summary>
    /// <param name="formBillService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FormBillController(IFormBillService formBillService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<FormBillListDto>> Query(GetFormBillInputDto dto) => await formBillService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<FormBillEditDto>> Get(InputDto dto) => await formBillService.Get(dto);

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<FormBillDetailDto>> GetDetail(InputDto dto) => await formBillService.GetDetail(dto);

        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Approval(ApprovalAPIDto dto) => await formBillService.Approval(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Add(FormBillEditDto dto) => await formBillService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Edit(FormBillEditDto dto) => await formBillService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("formBill.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await formBillService.Delete(dto);
    }
}
