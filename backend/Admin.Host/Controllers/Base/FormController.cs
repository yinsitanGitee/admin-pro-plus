﻿using Admin.Application.Base.Form;
using Admin.Application.Base.Form.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Web.Controllers.Base
{
    /// <summary>
    /// 表单管理
    /// </summary>
    /// <param name="formService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FormController(IFormService formService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<FormListDto>> Query(GetFormInputDto dto) => await formService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<FormEditDto>> Get(InputDto dto) => await formService.Get(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("formIndex.list.add")]
        public async Task<BusinessResult> Add(FormEditDto dto) => await formService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("formIndex.list.edit")]
        public async Task<BusinessResult> Edit(FormEditDto dto) => await formService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("formIndex.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await formService.Delete(dto);

        /// <summary>
        /// 获取下拉框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CommonSelectDto>> GetSelect() => await formService.GetSelect();
    }
}
