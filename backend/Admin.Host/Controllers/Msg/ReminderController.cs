﻿using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.Reminder;
using Admin.Core.Service.Reminder.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Msg
{
    /// <summary>
    /// 消息通知服务
    /// </summary>
    /// <param name="reminderService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ReminderController(IReminderService reminderService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<ReminderListDto>> Query(GetReminderInputDto dto) => await reminderService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ReminderListDto> Get(InputDto dto) => await reminderService.Get(dto);

        /// <summary>
        /// 设置已阅
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> SetRead(InputDto dto) => await reminderService.SetRead(dto);
    }
}
