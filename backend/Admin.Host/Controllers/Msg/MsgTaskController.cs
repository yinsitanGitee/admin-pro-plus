﻿using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.MsgTask;
using Admin.Core.Service.MsgTask.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Msg
{
    /// <summary>
    /// 待办任务服务
    /// </summary>
    /// <param name="msgTaskService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class MsgTaskController(IMsgTaskService msgTaskService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询我的待办
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<MsgTaskListDto>?> QueryMyWaitTask() => await msgTaskService.QueryMyWaitTask(6);

        /// <summary>
        /// 查询我的待办
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<MsgTaskListDto>?> QueryMyTopRightWaitTask() => await msgTaskService.QueryMyWaitTask(3);

        /// <summary>
        /// 查询我的待办
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<MsgTaskListDto>> Query(GetMsgTaskInputDto dto) => await msgTaskService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<MsgTaskListDto>> Get(InputDto dto) => await msgTaskService.Get(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Delete(InputDto dto) => await msgTaskService.Delete(dto);

        /// <summary>
        /// 设置已阅
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> SetRead(InputDto dto) => await msgTaskService.SetRead(dto);
    }
}
