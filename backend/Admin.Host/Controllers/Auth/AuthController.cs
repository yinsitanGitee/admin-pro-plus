﻿using Admin.Application.Auth;
using Admin.Application.Auth.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Ioc;
using Admin.Common.Json;
using Admin.Core.Filter;
using Admin.Core.Service.Queue;
using Admin.Host.Filter;
using Admin.Mq;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Admin.Repository.Tenant;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Admin.Host.Controllers.Auth
{
    /// <summary>
    /// 登录
    /// </summary>
    /// <param name="authService"></param>
    [ApiController]
    [NoApiLog]
    [Route("api/[controller]/[action]")]
    public class AuthController(IAuthService authService) : BaseControllerFilter
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<AuthOutputDto>> Login(AuthInputDto input)
        {
            Stopwatch sw = new();
            sw.Start();
            (BusinessResult<AuthOutputDto> result, AuthUserDto? entity) = await authService.Login(input);
            sw.Stop();
            SysLoginLogEntity logDto = new()
            {
                Tenant = TenantManager.Current,
                IP = FilterHelper.GetIP(HttpContext.Request),
                Account = input.Account,
                Request = JsonHelper.SerializeObject(input),
                RequestTime = DateTime.Now,
                Response = JsonHelper.SerializeObject(result),
                ExecuteTime = sw.ElapsedMilliseconds.ToString()
            };
            if (result.Code == BusinessState.Success)
            {
                logDto.Name = entity?.Name ?? "";
                logDto.Status = SuccessStateEnum.Success;
            }
            else logDto.Status = SuccessStateEnum.Fail;

            await IocManager.Instance.GetService<IQueueService>().AddSimpleQueueAsync(SysMqQueueSetting.LoginLogQueue, JsonHelper.SerializeObject(logDto));

            return result;
        }
    }
}
