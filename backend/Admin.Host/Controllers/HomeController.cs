﻿using Admin.Application.Home;
using Admin.Core.Filter;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers
{
    /// <summary>
    /// 主页接口
    /// </summary>
    /// <param name="homeService"></param>
    [ApiController]
    [NoApiLog]
    [Route("api/[controller]/[action]")]
    public class HomeController(IHomeService homeService) : BaseControllerFilter
    {
        /// <summary>
        /// 获取当前用户待办、消息、预警数量
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<Dictionary<string, long>?> GetCurrentTaskCount() => await homeService.GetCurrentTaskCount();
    }
}
