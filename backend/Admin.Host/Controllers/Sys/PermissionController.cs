﻿using Admin.Application.Sys.Permission;
using Admin.Application.Sys.Permission.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 权限管理
    /// </summary>
    /// <param name="permissionService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class PermissionController(IPermissionService permissionService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询菜单授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PermissionListOutputDto> Query(GetPermissionInputDto dto) => await permissionService.Query(dto);

        /// <summary>
        /// 保存菜单授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("role.list.auth")]
        public async Task<BusinessResult> RoleSubmit(PermissionEditDto dto) => await permissionService.RoleSubmit(dto);

        /// <summary>
        /// 保存菜单授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("account.list.auth")]
        public async Task<BusinessResult> AccountSubmit(PermissionEditDto dto) => await permissionService.AccountSubmit(dto);

        /// <summary>
        /// 查询数据权限授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<DataPermissionEditDto> GetDataAuth(InputDto dto) => await permissionService.GetDataAuth(dto);

        /// <summary>
        /// 保存数据权限授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> DataAuthSubmit(DataPermissionEditDto dto) => await permissionService.DataAuthSubmit(dto);
    }
}
