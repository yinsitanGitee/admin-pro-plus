﻿using Admin.Application.Sys.BaseArea;
using Admin.Application.Sys.BaseArea.Dto;
using Admin.Common.BusinessResult;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 省市区管理
    /// </summary>
    /// <param name="baseAreaService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BaseAreaController(IBaseAreaService baseAreaService) : BaseControllerFilter
    {
        /// <summary>
        /// 获取省市区
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<List<BaseAreaListDto>>> Query(GetBaseAreaInputDto dto) => await baseAreaService.Query(dto);

    }
}
