﻿using Admin.Application.Auth.Dto;
using Admin.Application.Sys.Account;
using Admin.Application.Sys.Account.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 用户管理
    /// </summary>
    /// <param name="accountService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountController(IAccountService accountService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("account.list.select")]
        public async Task<CommonPageOutputDto<AccountListDto>> Query(GetAccountInputDto dto) => await accountService.Query(dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<AccountListDto>> QueryAll(GetAccountInputDto dto) => await accountService.QueryAll(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<AccountEditDto>> Get(InputDto dto) => await accountService.Get(dto);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("account.list.add")]
        public async Task<BusinessResult> Add(AccountEditDto dto) => await accountService.Add(dto);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("account.list.edit")]
        public async Task<BusinessResult> Edit(AccountEditDto dto) => await accountService.Edit(dto);

        /// <summary>
        /// 修改头像
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> EditAccountImage(AccountEditImageDto dto) => await accountService.EditAccountImage(dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("account.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await accountService.Delete(dto);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> ChangePassword(ChangePwdDto dto) => await accountService.ChangePassword(dto);

        /// <summary>
        /// 个人中心修改密码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> PersonCenterChangePassword(ChangePwdDto dto) => await accountService.ChangePassword(dto);

        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> EditSimpleInfo(AccountSimgpleDto dto) => await accountService.EditSimpleInfo(dto);

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<UserPermissionOutputDto?> GetCurrentUserInfo()
        {
            var res = await accountService.GetCurrentUserInfo();
            if (res == null) HttpContext.Response.StatusCode = 401;
            return res;
        }

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<UserInfoDto?> GetCurrentUser()
        {
            var res = await accountService.GetCurrentUser();
            if (res == null) HttpContext.Response.StatusCode = 401;
            return res;
        }
    }
}
