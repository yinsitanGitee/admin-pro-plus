﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Admin.Application.Sys.LoginLog;
using Admin.Application.Sys.LoginLog.Dto;
using Admin.Repository.Page;
using Admin.Core.Filter;
using Admin.Host.Filter;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 登录日志管理
    /// </summary>
    /// <param name="loginLogService"></param>
    [Authorize]
    [NoApiLog]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class LoginLogController(ILoginLogService loginLogService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<LoginLogListDto>> Query(GetLoginLogInputDto dto) => await loginLogService.Query(dto);
    }
}
