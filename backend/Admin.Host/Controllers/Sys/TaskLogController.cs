﻿using Admin.Application.Sys.TaskLog;
using Admin.Application.Sys.TaskLog.Dto;
using Admin.Core.Filter;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 任务日志管理
    /// </summary>
    /// <param name="loginLogService"></param>
    [Authorize]
    [NoApiLog]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TaskLogController(ITaskLogService loginLogService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<TaskLogListDto>> Query(GetTaskLogInputDto dto) => await loginLogService.Query(dto);
    }
}
