﻿using Microsoft.AspNetCore.Mvc;
using Admin.Repository.Dto;
using Admin.Core;
using Admin.Core.Filter;
using Admin.Host.Filter;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 租户管理
    /// </summary>
    [ApiController]
    [NoApiLog]
    [Route("api/[controller]/[action]")]
    public class TenantController : BaseControllerFilter
    {
        /// <summary>
        /// 获取租户
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<OrmConfigOutDto> Query() => Startup.appSettingConfig.OrmConfig.Select(a => new OrmConfigOutDto { TenantName = a.TenantName, TenantKey = a.TenantKey }).ToList();
    }
}
