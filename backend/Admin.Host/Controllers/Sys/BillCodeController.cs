﻿using Admin.Application.Sys.BillCode;
using Admin.Application.Sys.BillCode.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 自动编号管理
    /// </summary>
    /// <param name="billCodeService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BillCodeController(IBillCodeService billCodeService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<BillCodeListDto>> Query(GetBillCodeInputDto dto) => await billCodeService.Query(dto);

        /// <summary>
        /// 获取明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<List<SysBillCodeDetailEntity>?>> GetDetail(InputDto dto) => await billCodeService.GetDetail(dto);

        /// <summary>
        /// 获取明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<BillCodeEditDto>> Get(InputDto dto) => await billCodeService.Get(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("autoCode.list.add")]
        public async Task<BusinessResult> Add(BillCodeEditDto dto) => await billCodeService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("autoCode.list.edit")]
        public async Task<BusinessResult> Edit(BillCodeEditDto dto) => await billCodeService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("autoCode.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await billCodeService.Delete(dto);
    }
}
