﻿using Admin.Application.Sys.AccountRole;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 用户角色管理
    /// </summary>
    /// <param name="accountRoleService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountRoleController(IAccountRoleService accountRoleService) : BaseControllerFilter
    {
        /// <summary>
        /// 获取用户授权的角色
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<string>> GetAccountRole(InputDto dto) => await accountRoleService.GetAccountRole(dto);
    }
}
