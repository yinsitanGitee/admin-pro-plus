﻿using Microsoft.AspNetCore.Mvc;
using Admin.Core.Filter;
using Admin.Host.Filter;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 测试
    /// </summary>
    [ApiController]
    [NoApiLog]
    [Route("api/[controller]/[action]")]
    public class TestController : BaseControllerFilter
    {
        /// <summary>
        /// 测试定时任务
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string Index()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
