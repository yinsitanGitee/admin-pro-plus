﻿using Admin.Application.Sys.Dictionary;
using Admin.Application.Sys.Dictionary.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 词典管理
    /// </summary>
    /// <param name="dictionaryService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class DictionaryController(IDictionaryService dictionaryService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("dictionary.list.select")]
        public async Task<CommonPageOutputDto<DictionaryListDto>> Query(GetDictionaryInputDto dto) => await dictionaryService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<DictionaryEditDto>> Get(InputDto dto) => await dictionaryService.Get(dto);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("dictionary.list.add")]
        public async Task<BusinessResult> Add(DictionaryEditDto dto) => await dictionaryService.Add(dto);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("dictionary.list.edit")]
        public async Task<BusinessResult> Edit(DictionaryEditDto dto) => await dictionaryService.Edit(dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("dictionary.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await dictionaryService.Delete(dto);

        /// <summary>
        /// 查询明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("dictionary.list.detail")]
        public async Task<List<DictionaryDetailTreeListDto>?> QueryDetail(GetDictionaryDetailInputDto dto) => await dictionaryService.QueryDetail(dto);

        /// <summary>
        /// 获取明细单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<DictionaryDetailEditDto>> GetDetail(InputDto dto) => await dictionaryService.GetDetail(dto);

        /// <summary>
        /// 添加明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> AddDetail(DictionaryDetailEditDto dto) => await dictionaryService.AddDetail(dto);

        /// <summary>
        /// 编辑明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> EditDetail(DictionaryDetailEditDto dto) => await dictionaryService.EditDetail(dto);

        /// <summary>
        /// 删除明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> DeleteDetail(InputDto dto) => await dictionaryService.DeleteDetail(dto);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("{code}")]
        public async Task<List<DictionaryDetailTreeListDto>> GetCache(string code) => await dictionaryService.GetCache(code);
    }
}
