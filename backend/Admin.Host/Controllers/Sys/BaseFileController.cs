﻿using Admin.Application.Sys.File;
using Admin.Application.Sys.File.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 文件管理
    /// </summary>
    /// <param name="baseFileService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BaseFileController(IBaseFileService baseFileService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<FileListDto>> Query(InputDto dto) => await baseFileService.Query(dto);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns> 
        [HttpPost]
        public async Task<BusinessResult> Submit(List<FileEditDto> dto)
        {
            await baseFileService.Submit(dto);
            return BusinessResponse.Success();
        }
    }
}
