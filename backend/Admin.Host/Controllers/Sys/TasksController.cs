﻿using Admin.Application.Sys.Tasks;
using Admin.Application.Sys.Tasks.Dtos;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quartz;
using Quartz.Impl.Calendar;
using Quartz.Impl.Triggers;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 任务管理
    /// </summary>
    /// <param name="tasksService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TasksController(ITasksService tasksService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<TasksListDto>> Query(GetCommTasksInputDto dto) => await tasksService.Query(dto);

        /// <summary>
        /// 获取所有任务数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CommonTreeDto>> GetAll() => await tasksService.GetAll();

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<TasksEditDto>> Get(InputDto dto) => await tasksService.Get(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Add(TasksEditDto dto) => await tasksService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Edit(TasksEditDto dto) => await tasksService.Edit(dto);

        /// <summary>
        /// 数据操作
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> Operate(TasksOperateDto dto) => await tasksService.Operate(dto);

        /// <summary>
        /// 计算Cron表达式
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public BusinessResult<List<string>> GetCronResults(TasksCronExDto dto)
        {
            try
            {
                CronTriggerImpl cron = new("name", "group", dto.Interval);
                ICalendar calendar = new BaseCalendar(TimeZoneInfo.Local);
                var arrayList = TriggerUtils.ComputeFireTimes(cron, calendar, 5);
                List<string> times = new();
                foreach (var item in arrayList) times.Add(item.LocalDateTime.ToString("yyyy-MM-dd HH:mm:ss"));
                return BusinessResponse<List<string>>.Success(times);
            }
            catch (Exception ex)
            {
                return BusinessResponse<List<string>>.Error(msg: ex.Message);
            }
        }
    }
}
