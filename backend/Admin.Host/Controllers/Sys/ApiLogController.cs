﻿using Admin.Application.Sys.ApiLog;
using Admin.Application.Sys.ApiLog.Dto;
using Admin.Core.Filter;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// api日志管理
    /// </summary>
    /// <param name="apiLogService"></param>
    [Authorize]
    [NoApiLog]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ApiLogController(IApiLogService apiLogService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<ApiLogListDto>> Query(GetApiLogInputDto dto) => await apiLogService.Query(dto);
    }
}
