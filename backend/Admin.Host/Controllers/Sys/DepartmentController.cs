﻿using Admin.Application.Sys.Department;
using Admin.Application.Sys.Department.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.Cache.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 部门管理
    /// </summary>
    /// <param name="departmentService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class DepartmentController(IDepartmentService departmentService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("department.list.select")]
        public async Task<List<DepartmentListDto>?> Query(GetDepartmentInputDto input) => await departmentService.Query(input);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<DepartmentEditDto>> Get(InputDto dto) => await departmentService.Get(dto);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("department.list.add")]
        public async Task<BusinessResult> Add(DepartmentEditDto dto) => await departmentService.Add(dto);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("department.list.edit")]
        public async Task<BusinessResult> Edit(DepartmentEditDto dto) => await departmentService.Edit(dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("department.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await departmentService.Delete(dto.Id);
    }
}
