﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Admin.Application.Sys.File.Dto;
using Admin.Core;
using Admin.Host.Filter;
using Admin.Common.BusinessResult;
using Admin.Common.Files;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 文件上传服务
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FileController : BaseControllerFilter
    {
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<List<FileListDto>>> Upload()
        {
            var businessResult = new BusinessResult<List<FileListDto>>
            {
                ResultData = new()
            };
            var files = Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                FileHelper.CreatePath(Startup.appSettingConfig.FileUploadConfig.Path);
                foreach (var file in files)
                {
                    int count = file.FileName.Split('.').Length;//统计.将名字分为几个部分
                    string exp = file.FileName.Split('.')[count - 1];//最后一部分为后缀名
                    var fileName = "/" + Guid.NewGuid().ToString("N") + "." + exp;
                    var path = "Upload/" + DateTime.Now.ToString("yyyyMMdd");
                    var directoryPath = Startup.appSettingConfig.FileUploadConfig.Path + "/" + path;
                    FileHelper.CreatePath(directoryPath);
                    string filePath = directoryPath + fileName;
                    FileHelper.CreateFile(filePath);
                    using var stream = new FileStream(filePath, FileMode.Create);
                    await file.CopyToAsync(stream);
                    businessResult.ResultData.Add(new FileListDto
                    {
                        Url = path + fileName,
                        Name = file.FileName
                    });
                }
            }
            else businessResult.BusinessError("请选择要上传的文件");

            return businessResult;
        }
    }
}
