﻿using Admin.Application.Sys.Organize;
using Admin.Application.Sys.Organize.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Core.Service.Cache.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 机构管理
    /// </summary>
    /// <param name="organizeService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class OrganizeController(IOrganizeService organizeService) : BaseControllerFilter
    {
        /// <summary>
        /// 获取机构树
        /// V9使用
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<TreeNode>?> GetDepartmentTree() => await organizeService.GetDepartmentTree();

        /// <summary>
        /// 获取机构部门树形数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<CommonOrgDepartDto>?> GetOrgDeparts() => await organizeService.GetOrgDeparts();

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<OrganizeListDto>?> Query(GetOrganizeInputDto input) => await organizeService.Query(input);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<OrganizeEditDto?>> Get(InputDto dto) => await organizeService.Get(dto);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("organize.list.add")]
        public async Task<BusinessResult> Add(OrganizeEditDto dto) => await organizeService.Add(dto);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("organize.list.edit")]
        public async Task<BusinessResult> Edit(OrganizeEditDto dto) => await organizeService.Edit(dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("organize.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await organizeService.Delete(dto.Id);
    }
}
