﻿using Admin.Application.Sys.Role;
using Admin.Application.Sys.Role.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 角色管理
    /// </summary>
    /// <param name="roleService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class RoleController(IRoleService roleService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("role.list.select")]
        public async Task<CommonPageOutputDto<RoleListDto>> Query(GetRoleInputDto dto) => await roleService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<RoleEditDto>> Get(InputDto dto) => await roleService.Get(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("role.list.add")]
        public async Task<BusinessResult> Add(RoleEditDto dto) => await roleService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("role.list.edit")]
        public async Task<BusinessResult> Edit(RoleEditDto dto) => await roleService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("role.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await roleService.Delete(dto);

        /// <summary>
        /// 获取角色下拉框
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<CommonSelectDto>> GetRoleSelect() => await roleService.GetRoleSelect();
    }
}
