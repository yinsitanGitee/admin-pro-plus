﻿using Admin.Application.Sys.ConfigItem;
using Admin.Application.Sys.ConfigItem.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 系统参数管理
    /// </summary>
    /// <param name="sysConfigItemService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class SysConfigItemController(ISysConfigItemService sysConfigItemService) : BaseControllerFilter
    {

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("configItem.list.select")]
        public async Task<CommonPageOutputDto<SysConfigItemListDto>> Query(GetSysConfigItemInputDto dto) => await sysConfigItemService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<SysConfigItemEditDto>> Get(InputDto dto) => await sysConfigItemService.Get(dto);

        /// <summary>
        /// 获取词典值
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Dictionary<string, string>> GetConfigItemValues(GetConfigItemValueInputDto dto) => await sysConfigItemService.GetConfigItemValues(dto.ConfigKeys);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("configItem.list.add")]
        public async Task<BusinessResult> Add(SysConfigItemEditDto dto) => await sysConfigItemService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("configItem.list.edit")]
        public async Task<BusinessResult> Edit(SysConfigItemEditDto dto) => await sysConfigItemService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("configItem.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await sysConfigItemService.Delete(dto);
    }
}
