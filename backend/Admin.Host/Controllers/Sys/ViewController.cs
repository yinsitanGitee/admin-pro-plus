﻿using Admin.Application.Sys.View;
using Admin.Application.Sys.View.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Sys
{
    /// <summary>
    /// 视图管理
    /// </summary>
    /// <param name="viewService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ViewController(IViewService viewService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("view.list.select")]
        public async Task<List<ViewListDto>?> Query() => await viewService.Query();

        /// <summary>
        /// 查询单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<ViewEditDto>> Get(InputDto dto) => await viewService.Get(dto);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("view.list.add")]
        public async Task<BusinessResult> Add(ViewEditDto dto) => await viewService.Add(dto);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("view.list.edit")]
        public async Task<BusinessResult> Edit(ViewEditDto dto) => await viewService.Edit(dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("view.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await viewService.Delete(dto);
    }
}
