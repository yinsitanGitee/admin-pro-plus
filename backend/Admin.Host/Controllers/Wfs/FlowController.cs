﻿using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core.Auth;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Service.Flow;
using Admin.Core.Service.Flow.Dto;
using Admin.Host.Filter;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Wfs
{
    /// <summary>
    /// 流程配置服务
    /// </summary>
    /// <param name="freeSql"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FlowController(IFreeSql freeSql) : BaseControllerFilter
    {
        /// <summary>
        /// 验证是否有审批权限
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<bool> VerificationApproval(InputDto dto)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            return await FlowWorkListService.VerificationCurrentProc(freeSql, dto.Id, userId);
        }

        /// <summary>
        /// 获取当前流程节点信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<NextProcInfoVM>> GetCurrentFlowInfo(InputDto dto)
        {
            var flow = await freeSql.Select<FlowEntity>().Where(a => a.Id == dto.Id).ToOneAsync<GetNextProcExecutorVM>();

            if (flow == null) return BusinessResponse<NextProcInfoVM>.Error(msg: "当前实例流程不存在,请刷新重试");

            var query = await freeSql.Select<ProcEntity>().Where(a => a.FlowId == dto.Id)
               .ToListAsync(a => new { a.Id, a.SchemeProcId, a.ProcApprovalType, a.IsSelectExecutor, a.IsSelectNextProc, a.ProcStatus, a.Type, a.Sort, a.Name, a.OriginalSchemeProcId });

            if (!query.Any()) return BusinessResponse<NextProcInfoVM>.Error(msg: "当前实例流程不存在节点，请联系管理员");

            query = query.Where(a => (a.ProcStatus == EnumProcStatus.Handing || a.ProcStatus == EnumProcStatus.Wait) && (a.Type == EnumProcType.Normal || a.Type == EnumProcType.OA)).ToList();

            var currentProcInfo = query.FirstOrDefault(a => a.ProcStatus == EnumProcStatus.Handing);

            if (currentProcInfo == null) return BusinessResponse<NextProcInfoVM>.Error(msg: "当前节点可能已经审批，请刷新页面重新查看");

            #region 验证当前用户是否有处理节点的权限，并且验证当前节点是否要多人审批

            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;

            if (!await freeSql.Select<WorkListEntity>().Where(a => a.AccountId == userId && a.IsNeed && !a.IsHandled && a.ProcId == currentProcInfo.Id).AnyAsync())
                return BusinessResponse<NextProcInfoVM>.Error(msg: "您没有权限，可能当前节点可能已经审批，请刷新页面重新查看");

            var vm = new NextProcInfoVM
            {
                CurrentProcId = currentProcInfo.Id,
                CurrentProcSchemeId = string.IsNullOrWhiteSpace(currentProcInfo.OriginalSchemeProcId) ? currentProcInfo.SchemeProcId : currentProcInfo.OriginalSchemeProcId,
                FlowTempId = flow.FlowTempId
            };

            bool isHaveNotApproval = false;

            if (currentProcInfo.ProcApprovalType == EnumProcApprovalType.AllApproval)
                isHaveNotApproval = await freeSql.Select<WorkListEntity>().Where(a => a.AccountId != userId && a.IsNeed && !a.IsHandled && a.ProcId == currentProcInfo.Id).AnyAsync();

            if (!isHaveNotApproval)
            {
                if (currentProcInfo.IsSelectNextProc)
                    vm.IsSelectNextProc = currentProcInfo.IsSelectNextProc;
                else
                {
                    var nextProc = query.Where(a => a.ProcStatus == EnumProcStatus.Wait && a.Type != EnumProcType.Branch && a.Type != EnumProcType.End).OrderBy(a => a.Sort).FirstOrDefault();
                    if (nextProc != null)
                    {
                        vm.NextProcName = "下步流程节点: " + nextProc.Name;
                        vm.NextProcId = nextProc.Id;
                        vm.IsSelectNextExecutor = nextProc.IsSelectExecutor;

                        var executor = await freeSql.Select<ExecutorEntity>().Where(a => a.ProcId == nextProc.Id).ToListAsync(a => new ExecutorTempListVM
                        {
                            IsLimtOrgan = a.IsLimtOrgan,
                            RefId = a.RefId,
                            RefType = a.RefType
                        });

                        var allWorker = await FlowWorkListService.CreateFlowProcWorkerList(freeSql, flow, executor);
                        if (allWorker != null && allWorker.Count > 0)
                            vm.NextProcExecutors = "下步审批人: " + string.Join(",", allWorker.Select(a => a.Name));
                    }
                    else
                        vm.NextProcName = "当前是流程最后一步节点";
                }
            }

            #endregion

            return BusinessResponse<NextProcInfoVM>.Success(data: vm);
        }

        /// <summary>
        /// 获取流程模板
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<FlowTempProcInfoVM> GetFlowTempInfoByOrgId(GetFlowTempInfo dto)
        {
            var user = IocManager.Instance.GetService<ICurrentUser>();
            if (string.IsNullOrWhiteSpace(dto.OrgId)) dto.OrgId = user.OrganizeId;

            var vm = new FlowTempProcInfoVM();

            if (!string.IsNullOrWhiteSpace(dto.SourceId))
            {
                var oldFlowTemp = await freeSql.Select<FlowSpecialProcEntity, FlowTempEntity>().InnerJoin(a => a.t1.FlowTempId == a.t2.Id).Where(a => a.t1.SourceId == dto.SourceId && a.t2.EnabledMark == EnumBool.TRUE).ToOneAsync(a => new
                {
                    a.t1.FlowSpecialProcId,
                    a.t1.FlowTempId
                });

                if (oldFlowTemp != null)
                {
                    var rejectProc = await freeSql.Select<ProcTempEntity>().Where(a => a.SchemeProcId == oldFlowTemp.FlowSpecialProcId && a.FlowTempId == oldFlowTemp.FlowTempId).ToOneAsync(a => new { a.Id, a.IsSelectExecutor, a.SchemeProcId, a.IsSelectNextProc });
                    if (rejectProc != null)
                    {
                        vm.Count = 1; // Count = 1 代表 业务单据在提交的时候不会去选择模板
                        vm.FlowTempId = oldFlowTemp.FlowTempId; // 驳回节点默认就是记录的模板Id
                        vm.RejectNextProcTempId = rejectProc.SchemeProcId;// 驳回节点SchemeProcId
                        vm.NextProcId = rejectProc.Id; // 下一级节点Id
                        vm.CurrentProcSchemeId = rejectProc.SchemeProcId; // 当前模板节点Id
                        vm.IsSelectNextExecutor = rejectProc.IsSelectExecutor;
                        return vm;
                    }
                }
            }

            var liTemps = await freeSql.Select<FlowTempEntity>().Where(a => a.ActionCode == dto.ActionCode && a.EnabledMark == EnumBool.TRUE).ToListAsync<FlowTempListVM>();

            var result = LoopToOrgTopVM(await IocManager.Instance.GetService<ICacheService>().GetOrganizeCache(), liTemps, dto.OrgId, dto.ActionCode);
            if (result != null && result.Count > 0)
            {
                vm.Count = result.Count;
                if (result.Count == 1) // 如果流程模板就一个，则找到当前模板第一个普通节点的属性，如果为多个，则打开选择流程模板弹窗
                {
                    vm.FlowTempId = result[0].Id;
                    var procTemp = await freeSql.Select<ProcTempEntity>().Where(a => a.FlowTempId == result[0].Id).ToListAsync(a => new { a.Type, a.SchemeProcId, a.Id, a.IsSelectNextProc, a.IsSelectExecutor });
                    var startTemp = procTemp.FirstOrDefault(a => a.Type == EnumProcType.Start);
                    if (startTemp != null)
                    {
                        vm.IsSelectNextProc = startTemp.IsSelectNextProc; // 是否选择下级节点，优先级最高，if == true ，直接返回
                        vm.CurrentProcSchemeId = startTemp.SchemeProcId; // 当前节点的 模板节点Id
                        if (!vm.IsSelectNextProc)
                        {
                            var lineTemp = await freeSql.Select<LineTempEntity>().Where(a => a.FromSchemeProcId == startTemp.SchemeProcId && a.FlowTempId == result[0].Id).ToOneAsync();
                            if (lineTemp != null)
                            {
                                var nextProcTemp = procTemp.Where(a => a.SchemeProcId == lineTemp.ToSchemeProcId && a.Type == EnumProcType.Normal).FirstOrDefault();
                                if (nextProcTemp != null)
                                {
                                    vm.NextProcId = nextProcTemp.Id;
                                    vm.IsSelectNextExecutor = nextProcTemp.IsSelectExecutor;
                                }
                            }
                        }
                    }
                }
                else vm.Temps = result;
            }

            return vm;
        }

        /// <summary>
        ///递归查询部分
        /// </summary>
        /// <param name="liOrgs"></param>
        /// <param name="liTemps"></param>
        /// <param name="orgId"></param>
        /// <param name="actCode"></param>
        /// <returns></returns>
        private List<FlowTempListVM>? LoopToOrgTopVM(List<OrganizeListDto>? liOrgs, List<FlowTempListVM> liTemps, string orgId, string actCode)
        {
            if (liOrgs == null) return null;
            var temp = liTemps.Where(t => t.OrgId == orgId).ToList();
            if (temp != null && temp.Count > 0)
            {
                var orgName = liOrgs.FirstOrDefault(a => a.Id == orgId)?.Name;
                temp.ForEach(a => a.OrgName = orgName);
                return temp;
            }
            var org = liOrgs.Where(t => t.Id == orgId).FirstOrDefault();
            if (org == null) return null;
            if (org.ParentId == "") return null;
            return LoopToOrgTopVM(liOrgs, liTemps, org.ParentId, actCode);
        }

        /// <summary>
        /// 获取流程需要选择的下级节点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<FlowNextProcsVM>> GetFlowApproveNextProcs(GetFlowApproveNextProcsDto dto)
        {
            var vm = new FlowNextProcsVM();

            if (!string.IsNullOrWhiteSpace(dto.FlowId))
            {
                var org = await freeSql.Select<FlowEntity>().Where(a => a.Id == dto.FlowId).ToOneAsync(a => a.OrgId);
                vm.OrgId = org ?? throw new Exception("未找到当前实例流程对应的模板，可能模板已被删除，请联系管理员!!!");
            }

            var query = await freeSql.Select<ProcTempEntity>().Where(a => a.FlowTempId == dto.FlowTempId && a.Type != EnumProcType.End && a.Type != EnumProcType.Start).ToListAsync();

            var lines = await freeSql.Select<LineTempEntity>().Where(a => a.FlowTempId == dto.FlowTempId && a.FromSchemeProcId == dto.CurrentProcSchemeId
            && (a.LineCondType == EnumLineCondType.Agree || a.LineCondType == EnumLineCondType.None))
                .ToListAsync(a => a.ToSchemeProcId);

            if (lines == null)
                return BusinessResponse<FlowNextProcsVM>.Error(msg: "没有找到下级节点");

            vm.FlowTempId = dto.FlowTempId;
            vm.Lists = query.Where(a => lines.Contains(a.SchemeProcId)).Select(a => new FlowNextBaseProcsVM
            {
                ProcId = a.Id,
                ProcName = a.Name,
                IsSelectNextExecutor = a.IsSelectExecutor
            }).OrderBy(a => a.ProcName).ToList();

            return BusinessResponse<FlowNextProcsVM>.Success(vm);
        }

        /// <summary>
        /// 实时计算下个节点执行人
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<List<ExcutorWorkListAPIVM>>> GetFlowProcComputeExecutor(GetNextProcExecutorVM vm)
        {
            var query = await freeSql.Select<ExecutorTempEntity>().Where(a => a.FlowTempId == vm.FlowTempId && a.ProcId == vm.ProcTempId).ToListAsync<ExecutorTempListVM>();
            return BusinessResponse<List<ExcutorWorkListAPIVM>>.Success(await FlowWorkListService.CreateFlowProcWorkerList(freeSql, vm, query));
        }

        /// <summary>
        /// 实时计算下个节点执行人
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<List<ExcutorWorkListAPIVM>>> GetFlowProcExecutor(GetFlowProcExecutorDto vm)
        {
            var executor = await freeSql.Select<ExecutorEntity>().Where(a => a.ProcId == vm.ProcId).ToListAsync(a => new ExecutorTempListVM
            {
                IsLimtOrgan = a.IsLimtOrgan,
                RefId = a.RefId,
                RefType = a.RefType
            });
            var flow = await freeSql.Select<FlowEntity>().Where(a => a.Id == vm.FlowId)
                .ToOneAsync(a => new GetNextProcExecutorVM
                {
                    Id = a.Id,
                    FlowTempId = a.FlowTempId,
                    AppId = a.AppId,
                    AppCode = a.AppCode,
                    UnitId = a.UnitId,
                    ProZoneId = a.ProZoneId,
                    PavZoneId = a.PavZoneId,
                    OrgId = a.OrgId,
                    CreateUserId = a.CreateUserId
                });

            return BusinessResponse<List<ExcutorWorkListAPIVM>>.Success(await FlowWorkListService.CreateFlowProcWorkerList(freeSql, flow, executor));
        }

        /// <summary>
        /// 获取机构下流程模板信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<FlowTempProcBaseInfoVM> GetFlowTempProcBaseInfoVM(InputDto dto)
        {
            var vm = new FlowTempProcBaseInfoVM { FlowTempId = dto.Id };
            var procTemp = await freeSql.Select<ProcTempEntity>().Where(a => a.FlowTempId == dto.Id).ToListAsync(a => new { a.Type, a.SchemeProcId, a.Id, a.IsSelectExecutor, a.IsSelectNextProc });
            if (procTemp != null && procTemp.Count > 0)
            {
                var startTemp = procTemp.FirstOrDefault(a => a.Type == EnumProcType.Start);
                vm.IsSelectNextProc = startTemp.IsSelectNextProc;
                vm.CurrentProcSchemeId = startTemp.SchemeProcId;
                if (!vm.IsSelectNextProc)
                {
                    if (startTemp != null)
                    {
                        var toSchemeProcId = await freeSql.Select<LineTempEntity>().Where(a => a.FromSchemeProcId == startTemp.SchemeProcId && a.FlowTempId == dto.Id).ToOneAsync(a => a.ToSchemeProcId);
                        if (!string.IsNullOrWhiteSpace(toSchemeProcId))
                        {
                            var nextProcTemp = procTemp.Where(a => a.SchemeProcId == toSchemeProcId && a.Type == EnumProcType.Normal).FirstOrDefault();
                            if (nextProcTemp != null)
                            {
                                vm.IsSelectNextExecutor = nextProcTemp.IsSelectExecutor;
                                vm.NextProcId = nextProcTemp.Id;
                            }
                        }
                    }
                }
            }
            return vm;
        }

        /// <summary>
        /// 获取流程已经审批过的节点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<FlowProcsInfoVM> GetFlowApproveProcs(InputDto dto)
        {
            var vm = new FlowProcsInfoVM();
            var procs = await freeSql.Select<ProcEntity>().Where(a => a.FlowId == dto.Id && !a.IsAppendProc && a.ProcStatus != EnumProcStatus.Wait && a.Type == EnumProcType.Normal).OrderBy(a => a.Sort).ToListAsync<FlowProcsVM>();

            procs.ForEach(a => a.Id = string.IsNullOrWhiteSpace(a.OriginalSchemeProcId) ? a.SchemeProcId : a.OriginalSchemeProcId);

            procs = procs.GroupBy(a => new { a.Id, a.Name, a.ProcStatus }).Select(a => new FlowProcsVM { Id = a.Key.Id, Name = a.Key.Name, ProcStatus = a.Key.ProcStatus }).ToList();

            if (procs != null && procs.Count > 0)
                vm.Procs = procs.Where(a => a.ProcStatus != EnumProcStatus.Handing).ToList();

            vm.CurrentProcName = await freeSql.Select<ProcEntity>().Where(a => a.FlowId == dto.Id
                         && a.ProcStatus == EnumProcStatus.Handing
                         && (a.Type == EnumProcType.Normal)).ToOneAsync(a => a.Name);

            return vm;
        }

        /// <summary>
        /// 追加处理人
        /// </summary>
        [HttpPost]
        public async Task<BusinessResult> FlowAppendProc(FlowOperateProcVM vm)
        {
            return await FlowProcService.AppendProc(freeSql, vm);
        }

        /// <summary>
        /// 更改处理人
        /// </summary>
        [HttpPost]
        public async Task<BusinessResult> FlowChangeProcExecutor(FlowOperateProcVM vm)
        {
            return await FlowProcService.FlowChangeProcExecutor(freeSql, vm);
        }

        /// <summary>
        /// 知会
        /// </summary>
        [HttpPost]
        public async Task<BusinessResult> FlowInformer(FlowProcInformerVM vm)
        {
            return await FlowProcService.FlowInformer(freeSql, vm);
        }
    }
}
