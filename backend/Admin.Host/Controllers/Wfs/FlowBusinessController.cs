﻿using Admin.Application.Wfs.FlowBusiness;
using Admin.Application.Wfs.FlowBusiness.Dto;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Wfs
{
    /// <summary>
    /// 流程日志服务
    /// </summary>
    /// <param name="flowBusinessService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FlowBusinessController(IFlowBusinessService flowBusinessService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<FlowBusinessListDto>> QueryFlowOperateLog(InputDto dto) => await flowBusinessService.QueryFlowOperateLog(dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<FlowProcVM>> QueryFlowNotApprovalProc(GetFlowNotApprovalDto dto) => await flowBusinessService.QueryFlowNotApprovalProc(dto);
    }
}
