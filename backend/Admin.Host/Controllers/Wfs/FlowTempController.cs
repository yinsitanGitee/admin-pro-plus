﻿using Admin.Application.Wfs.FlowTemp;
using Admin.Application.Wfs.FlowTemp.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Wfs
{
    /// <summary>
    /// 流程配置服务
    /// </summary>
    /// <param name="flowTempService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FlowTempController(IFlowTempService flowTempService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<FlowTempListDto>> Query(GetFlowTempInputDto dto) => await flowTempService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<FlowTempEditDto>> Get(InputDto dto) => await flowTempService.Get(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("flowSetting.list.add")]
        public async Task<BusinessResult> Add(FlowTempEditDto dto) => await flowTempService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("flowSetting.list.edit")]
        public async Task<BusinessResult> Edit(FlowTempEditDto dto) => await flowTempService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("flowSetting.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await flowTempService.Delete(dto);

        /// <summary>
        /// 获取流程图
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<SchemeTempDto?> GetFlowScheme(InputDto dto) => await flowTempService.GetFlowScheme(dto);

        /// <summary>
        /// 保存流程图
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult> SaveFlowScheme(SchemeTempDto dto) => await flowTempService.SaveFlowScheme(dto);
    }
}
