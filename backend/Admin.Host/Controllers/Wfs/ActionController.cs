﻿using Admin.Application.Wfs.Action;
using Admin.Application.Wfs.Action.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Host.Filter;
using Admin.Repository.Page;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Host.Controllers.Wfs
{
    /// <summary>
    /// 流程定义服务
    /// </summary>
    /// <param name="actionService"></param>
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ActionController(IActionService actionService) : BaseControllerFilter
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonPageOutputDto<ActionListDto>> Query(GetActionInputDto dto) => await actionService.Query(dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<BusinessResult<ActionEditDto>> Get(InputDto dto) => await actionService.Get(dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("flowConfig.list.add")]
        public async Task<BusinessResult> Add(ActionEditDto dto) => await actionService.Add(dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("flowConfig.list.edit")]
        public async Task<BusinessResult> Edit(ActionEditDto dto) => await actionService.Edit(dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [PermissionFilter("flowConfig.list.delete")]
        public async Task<BusinessResult> Delete(InputDto dto) => await actionService.Delete(dto);

        /// <summary>
        /// 获取下拉框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CommonSelectDto>> GetSelect() => await actionService.GetSelect();
    }
}
