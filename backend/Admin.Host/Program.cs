using Autofac;
using FreeSql;
using Admin.Repository.Tenant;
using Admin.Core;
using Admin.Core.SysSignalR.BPro;
using Admin.Core.Transactional;
using Admin.Common.SysStatic;
using Admin.Common.SysEnum;

FreeSqlCloud<string> fsql = new();
string defaultTenant = string.Empty;
List<string> NoTenantWhites =
[
  "/api/Tenant/Query"
];

SystemConfig.SystemPlatformType = PlatformEnum.BServe;

var dto = new Startup.InitDto
{
    FreeSql = fsql
};

var builder = WebApplication.CreateBuilder(args);
var appSettingConfig = builder.ConfigureServices(dto);

defaultTenant = appSettingConfig.OrmConfig[0].TenantKey;

builder.Services.AddSignalR().AddJsonProtocol(options =>
{
    options.PayloadSerializerOptions.PropertyNamingPolicy = null;
});

var app = builder.Build();

app.Configure(dto);

app.Use(async (context, next) =>
{
    try
    {
        if (!string.IsNullOrWhiteSpace(context.Request.Path.Value) && NoTenantWhites.IndexOf(context.Request.Path.Value) == -1)
        {
            ChangeCurrenTenant(context);
            TransactionalAttribute.SetServiceProvider(context.RequestServices);
        }
        await next();
    }
    finally
    {
        TenantManager.Current = string.Empty;
        fsql.Change(defaultTenant);
    }
});

app.UseAuthentication();

app.UseRouting();

app.UseAuthorization();

app.MapControllers();

app.MapHub<BMessageHub>("/bmessageHub");

app.Run();

#region 修改当前租户
void ChangeCurrenTenant(HttpContext context)
{
    if (context.Request.Method == "OPTIONS") return;
    var accessToken = context.Request.Query["access_token"];
    if (!string.IsNullOrWhiteSpace(accessToken))
    {
        var arrs = accessToken.ToString().Split(',');
        if (arrs.Length == 2)
        {
            TenantManager.Current = arrs[1];
            context.Request.Headers.Authorization = arrs[0];
        }
    }
    else
    {
        var tenant = context.Request.Headers["Tenant"].ToString();
        TenantManager.Current = tenant;
    }
    if (string.IsNullOrWhiteSpace(TenantManager.Current)) throw new Exception("非法请求");
    if (!appSettingConfig.OrmConfig.Any(a => a.TenantKey == TenantManager.Current)) throw new Exception("租户无效，非法请求");
    fsql.Change(TenantManager.Current);
}

#endregion
