﻿namespace Admin.Application.Home
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IHomeService
    {
        /// <summary>
        /// 获取当前用户待办、消息、预警数量
        /// </summary>
        /// <returns></returns>
        Task<Dictionary<string, long>?> GetCurrentTaskCount();
    }
}
