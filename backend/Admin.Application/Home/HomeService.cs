﻿using Admin.Cache;
using Admin.Core;
using Admin.Core.Auth;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository.Entities.Msg;

namespace Admin.Application.Home
{
    /// <summary>
    /// 系统服务
    /// </summary>
    /// <param name="freesql"></param>
    /// <param name="user"></param>
    public class HomeService(IFreeSql freesql, ICurrentUser user) : BaseService, IHomeService
    {
        /// <summary>
        /// 获取当前用户待办、消息、预警数量
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<string, long>?> GetCurrentTaskCount()
        {
            var result = new Dictionary<string, long>();
            await GetMsgTaskCount(result);
            await GetReminderCount(result);
            return result;
        }

        /// <summary>
        /// 获取待办任务数量
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private async Task GetMsgTaskCount(Dictionary<string, long> result)
        {
            var key = RedisKeyDto.MsgCountKey + user.UserId;
            var count = await RedisManage.GetAsync<long?>(key);
            if (count == null)
            {
                count = await freesql.Select<MsgTaskUserEntity>().Where(a => a.UserId == user.UserId && a.State == 0).CountAsync();
                await RedisManage.SetAsync(key, count, TimeSpan.FromMinutes(10));
            }

            result.Add("msgTask", count ?? 0);
        }

        /// <summary>
        /// 获取消息通知数量
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private async Task GetReminderCount(Dictionary<string, long> result)
        {
            var key = RedisKeyDto.ReminderKey + user.UserId;
            var count = await RedisManage.GetAsync<long?>(key);
            if (count == null)
            {
                count = await freesql.Select<ReminderEntity>().Where(a => a.AccountId == user.UserId && !a.IsRead && a.ContentType == EnumReminderContentType.Mesage).CountAsync();
                await RedisManage.SetAsync(key, count, TimeSpan.FromMinutes(10));
            }

            result.Add("reminder", count ?? 0);
        }
    }
}