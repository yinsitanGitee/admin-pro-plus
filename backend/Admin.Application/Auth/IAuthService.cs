﻿using Admin.Application.Auth.Dto;
using Admin.Common.BusinessResult;

namespace Admin.Application.Auth
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<(BusinessResult<AuthOutputDto>, AuthUserDto?)> Login(AuthInputDto input);
    }
}
