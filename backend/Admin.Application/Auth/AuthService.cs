﻿using Admin.Application.Auth.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Ioc;
using Admin.Common.Json;
using Admin.Core;
using Admin.Core.Auth;
using Admin.Core.Helper;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;

namespace Admin.Application.Auth
{
    /// <summary>
    /// 鉴权服务
    /// </summary>
    /// <param name="freesql"></param>
    public class AuthService(IFreeSql freesql) : BaseService, IAuthService
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<(BusinessResult<AuthOutputDto>, AuthUserDto?)> Login(AuthInputDto input)
        {
            var result = new BusinessResult<AuthOutputDto>() { ResultData = new AuthOutputDto() };
            var account = await freesql.Select<SysAccountEntity, SysDepartmentEntity>().LeftJoin(a => a.t1.DepartmentId == a.t2.Id)
                .Where(a => a.t1.Account == input.Account).ToOneAsync(a => new AuthUserDto { DepartmentName = a.t2.Name });
            if (account != null)
            {
                if (SysHelper.GetPassword(input.Password) == account.Password)
                {
                    var r = ExistAccount(account);
                    if (string.IsNullOrWhiteSpace(r)) result.ResultData = LoginBase(account);
                    else result.BusinessError(r);
                }
                else result.BusinessError("用户名或密码输入错误");
            }
            else result.BusinessError("用户名或密码输入错误");

            return (result, account);
        }

        /// <summary>
        /// 验证用户有效性
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        private static string ExistAccount(AuthUserDto account)
        {
            if (account.State == CommonStateEnum.Ineffective) return "用户无效,请联系管理员";
            return string.Empty;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        private static AuthOutputDto LoginBase(AuthUserDto account)
        {
            return new AuthOutputDto
            {
                Token = IocManager.Instance.GetService<ITokenService>().GenerateToken(account.Id),
            };
        }
    }
}