﻿namespace Admin.Application.Auth.Dto
{
    /// <summary>
    /// UserInfoDto
    /// </summary>
    public class UserInfoDto
    {
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPic { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// zhangh
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
    }

    /// <summary>
    /// 定义用户登录鉴权输入dto
    /// </summary>
    public class AuthOutputDto
    {
        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; set; }
    }
}
