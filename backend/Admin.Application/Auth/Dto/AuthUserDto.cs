﻿using Admin.Repository.Enum;

namespace Admin.Application.Auth.Dto
{
    /// <summary>
    /// 定义用户鉴权输出dto
    /// </summary>
    public class AuthUserDto
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPic { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        public string HalfViews { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        public string Views { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }
    }
}
