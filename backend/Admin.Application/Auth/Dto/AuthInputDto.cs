﻿using System.ComponentModel.DataAnnotations;

namespace Admin.Application.Auth.Dto
{
    /// <summary>
    /// 登录输入Dto
    /// </summary>
    public class AuthInputDto
    {
        /// <summary>
        /// 账户
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }

    /// <summary>
    /// 定义修改密码dto
    /// </summary>
    public class ChangePwdDto
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public List<string> Ids { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        public string Password { get; set; }
    }
}
