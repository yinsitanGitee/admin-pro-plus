﻿using Admin.Application.Wfs.FlowBusiness.Dto;
using Admin.Common.Dto;

namespace Admin.Application.Wfs.FlowBusiness
{
    /// <summary>
    /// 定义接口服务
    /// </summary>
    public interface IFlowBusinessService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<List<FlowBusinessListDto>> QueryFlowOperateLog(InputDto dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<List<FlowProcVM>> QueryFlowNotApprovalProc(GetFlowNotApprovalDto dto);
    }
}
