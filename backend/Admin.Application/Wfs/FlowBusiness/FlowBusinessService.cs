﻿using Admin.Application.Wfs.FlowBusiness.Dto;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Service.Flow;
using Admin.Core.Service.Flow.Dto;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;

namespace Admin.Application.Wfs.FlowBusiness
{
    /// <summary>
    /// 定义接口服务实现
    /// </summary>
    /// <param name="freeSql"></param>
    public class FlowBusinessService(IFreeSql freeSql) : BaseService, IFlowBusinessService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<FlowBusinessListDto>> QueryFlowOperateLog(InputDto dto)
        {
            return await freeSql.Select<OperateRecordEntity, FlowInformerRecordEntity>()
                  .LeftJoin(a => a.t1.ProcId == a.t2.ProcId)
                  .Where(a => a.t1.DataId == dto.Id)
                  .OrderBy(a => a.t1.CreateDateTime)
                  .OrderBy(a => a.t1.Title)
                  .ToListAsync(a => new FlowBusinessListDto
                  {
                      OperateDate = a.t1.CreateDateTime,
                      Executors = freeSql.Select<OperateRecordExecutorEntity, SysAccountEntity>().LeftJoin(b => b.t1.OperateUserId == b.t2.Id)
                      .Where(b => b.t1.DataId == dto.Id && b.t1.RecordId == a.t1.Id)
                        .ToList(a => new FlowOperateRecordExecutors
                        {
                            HeadPic = a.t2.HeadPic,
                            OperateUserName = a.t2.Name
                        })
                  });
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<FlowProcVM>> QueryFlowNotApprovalProc(GetFlowNotApprovalDto dto)
        {
            var flow = await freeSql.Select<FlowEntity>().Where(a => a.Id == dto.FlowId).ToOneAsync(a => new GetNextProcExecutorVM
            {
                Id = a.Id,
                FlowTempId = a.FlowTempId,
                AppId = a.AppId,
                AppCode = a.AppCode,
                UnitId = a.UnitId,
                ProZoneId = a.ProZoneId,
                PavZoneId = a.PavZoneId,
                OrgId = a.OrgId,
                CreateUserId = a.CreateUserId,
            });

            if (flow == null) return new List<FlowProcVM>();

            var procs = await freeSql.Select<ProcEntity, FlowInformerRecordEntity>().LeftJoin(a => a.t1.Id == a.t2.ProcId)
                .Where(a => a.t1.FlowId == dto.FlowId && new EnumProcStatus[2] { EnumProcStatus.Wait, EnumProcStatus.Handing }.Contains(a.t1.ProcStatus) && new EnumProcType[2] { EnumProcType.Normal, EnumProcType.OA }.Contains(a.t1.Type))
                .OrderBy(a => a.t1.Sort)
                .ToListAsync(a => new FlowProcVM
                {
                    InformerNames = a.t2.Names
                });

            if (procs == null || procs.Count == 0) return new List<FlowProcVM>();

            var ids = procs.Select(b => b.Id).ToList();
            var executorTempList = await freeSql.Select<ExecutorEntity>().Where(a => ids.Contains(a.ProcId)).ToListAsync(a => new ExecutorTempListVM
            {
                Name = a.RefName
            });

            List<Task> tasks = new();

            var hangProc = procs.FirstOrDefault(a => a.ProcStatus == EnumProcStatus.Handing);

            if (hangProc != null)
            {
                var result = await freeSql.Select<WorkListEntity, SysAccountEntity>().LeftJoin(a => a.t1.AccountId == a.t2.Id).Where(a => a.t1.ProcId == hangProc.Id).ToListAsync(a => new ExcutorWorkListAPIVM
                {
                    Name = a.t2.Name
                });

                if (hangProc.ProcApprovalType == EnumProcApprovalType.AllApproval || dto.IsHandlePro) hangProc.Executors = result.OrderBy(a => a.ComplateTime == null).ThenBy(a => a.ComplateTime).ToList();
                else hangProc.Executors = new List<ExcutorWorkListAPIVM> { new ExcutorWorkListAPIVM { Name = string.Join(",", result.Select(a => a.Name)) } };

                if (hangProc.ProcType == EnumProcType.OA) hangProc.Description = string.Format("<a class='ThrAuditlog' data-proid='{0}' data-flowid='{1}' click=approval() style='text-decoration: underline; color: #0da697;'>审批记录</a>", hangProc.Id, flow.Id);
            }

            foreach (var item in procs.Where(a => a.ProcStatus == EnumProcStatus.Wait))
            {
                tasks.Add(Task.Run(async () =>
                {
                    if (item.ProcType == EnumProcType.OA) item.Description = string.Format("<a class='ThrAuditlog' data-proid='{0}' data-flowid='{1}' click=approval() style='text-decoration: underline; color: #0da697;'>审批记录</a>", item.Id, flow.Id);
                    var lists = executorTempList.Where(a => a.ProcId == item.Id).ToList();
                    var result = await FlowWorkListService.CreateFlowProcWorkerList(freeSql, flow, lists);
                    if (item.ProcApprovalType == EnumProcApprovalType.AllApproval) item.Executors = result;
                    else item.Executors = [new ExcutorWorkListAPIVM { Name = string.Join(",", result.Select(a => a.Name)) }];
                }));
            }

            Task.WaitAll(tasks.ToArray());

            return procs;
        }
    }
}
