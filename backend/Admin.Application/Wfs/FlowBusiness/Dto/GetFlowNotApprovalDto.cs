﻿namespace Admin.Application.Wfs.FlowBusiness.Dto
{
    /// <summary>
    /// 获取流程未审批节点dto
    /// </summary>
    public class GetFlowNotApprovalDto
    {
        /// <summary>
        /// 流程Id
        /// </summary>
        public string FlowId { get; set; }

        /// <summary>
        /// 处理中执行人是否特殊处理
        /// </summary>
        public bool IsHandlePro { get; set; }
    }
}
