﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;
using Admin.Core.Service.Flow.Dto;

namespace Admin.Application.Wfs.FlowBusiness.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class FlowBusinessListDto : BaseDto
    {
        /// <summary>
        /// 主表数据审批状态
        /// </summary> 
        public int State { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary> 
        public string Type { get; set; }

        /// <summary>
        /// 日志标题
        /// </summary> 
        public string Title { get; set; }

        /// <summary>
        /// 知会人
        /// </summary>
        public string InformerNames { get; set; }

        /// <summary>
        /// 操作日期
        /// </summary>
        public DateTime OperateDate { get; set; }

        /// <summary>
        /// 处理人
        /// </summary>
        public List<FlowOperateRecordExecutors> Executors { get; set; }
    }

    /// <summary>
    /// 审批记录执行人
    /// </summary>
    public class FlowOperateRecordExecutors
    {
        /// <summary>
        /// 记录主表Id
        /// </summary>
        public string RecordId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public EnumOperateState State { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string OperateUserName { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime OperateDate { get; set; }

        /// <summary>
        /// 头像地址
        /// </summary>
        public string HeadPic { get; set; }
    }

    /// <summary>
    /// FlowProcVM
    /// </summary>
    public class FlowProcVM
    {
        #region 基础字段
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// State
        /// </summary> 
        public int State { get; set; }

        /// <summary>
        ///环节节点审批类型
        /// </summary>
        public EnumProcApprovalType ProcApprovalType { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary> 
        public string Type { get; set; }

        /// <summary>
        /// 节点类型
        /// </summary>
        public EnumProcType ProcType { get; set; }

        /// <summary>
        /// 日志标题
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        public EnumProcStatus ProcStatus { get; set; }

        /// <summary>
        /// IsSelectNextProc
        /// </summary>
        public bool IsSelectNextProc { get; set; }

        /// <summary>
        /// IsSelectNextExecutor
        /// </summary>
        public bool IsSelectExecutor { get; set; }

        #endregion

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 知会人
        /// </summary>
        public string InformerNames { get; set; }

        /// <summary>
        /// 是否处理中节点
        /// </summary>
        public bool IsHangindg { get; set; }

        /// <summary>
        /// 处理人
        /// </summary>
        public List<ExcutorWorkListAPIVM> Executors { get; set; }
    }
}
