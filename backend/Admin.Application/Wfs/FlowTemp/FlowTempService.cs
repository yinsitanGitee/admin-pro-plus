﻿using Admin.Application.Wfs.FlowTemp.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Admin.Repository.Page;
using Mapster;

namespace Admin.Application.Wfs.FlowTemp
{
    /// <summary>
    /// 定义接口服务实现
    /// </summary>
    /// <param name="flowTempRepository"></param>
    public class FlowTempService(IRepository<FlowTempEntity, string> flowTempRepository) : BaseService, IFlowTempService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<FlowTempListDto>> Query(GetFlowTempInputDto dto)
        {
            var r = flowTempRepository.Select.From<SysOrganizeEntity, ActionEntity>()
                .LeftJoin(a => a.t1.OrgId == a.t2.Id)
                .LeftJoin(a => a.t1.ActionCode == a.t3.Code)
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.t1.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.ActionCode), t => t.t1.ActionCode == dto.ActionCode)
                .WhereIf(!string.IsNullOrEmpty(dto.OrgId), t => t.t1.OrgId == dto.OrgId)
                .WhereIf(!string.IsNullOrEmpty(dto.Code), t => t.t1.Code.Contains(dto.Code));

            return new CommonPageOutputDto<FlowTempListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderBy(x => x.t1.Code).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new FlowTempListDto { OrgName = a.t2.Name, ActionName = a.t3.Name })
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<FlowTempEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<FlowTempEditDto>.Success(data: await flowTempRepository.GetAsync<FlowTempEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(FlowTempEditDto dto)
        {
            var entity = MapperObject.Map<FlowTempEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            await flowTempRepository.InsertAsync(entity);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(FlowTempEditDto dto)
        {
            var entity = await flowTempRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await flowTempRepository.UpdateAsync(entity);

                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await flowTempRepository.SoftDeleteAsync(a => dto.Ids.Contains(a.Id));
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 获取流程图
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<SchemeTempDto?> GetFlowScheme(InputDto dto)
        {
            var freeSql = IocManager.Instance.GetService<IFreeSql>();
            var liProcs = await freeSql.Select<ProcTempEntity>().Where(a => a.FlowTempId == dto.Id).ToListAsync();
            var liexes = await freeSql.Select<ExecutorTempEntity>().Where(a => a.FlowTempId == dto.Id).ToListAsync();
            var liLines = await freeSql.Select<LineTempEntity>().Where(a => a.FlowTempId == dto.Id).ToListAsync();
            var liConds = await freeSql.Select<CondTempEntity>().Where(a => a.FlowTempId == dto.Id).ToListAsync();

            var lineVMs = new List<LineTempDto>();
            if (liLines != null && liLines.Count != 0)
            {
                liConds = liConds ?? [];
                foreach (var item in liLines)
                {
                    var ll = item.Adapt<LineTempDto>();
                    ll.Conds = liConds.Where(t => t.RefId == item.Id && t.RefType == EnumFlowEle.Line).ToList();
                    lineVMs.Add(ll);
                }
            }

            var procVMs = new List<ProcTempDto>();
            if (liProcs != null && liProcs.Count != 0)
            {
                liexes ??= new List<ExecutorTempEntity>();
                var liProcVM = liProcs.Adapt<List<ProcTempDto>>();
                foreach (var item in liProcVM)
                {
                    item.Executors = liexes.Where(t => t.ProcId == item.Id).ToList();
                    procVMs.Add(item);
                }
            }
            var schVM = (await flowTempRepository.GetAsync(a => a.Id == dto.Id)).Adapt<SchemeTempDto>();
            if (schVM != null)
            {
                schVM.Lines = lineVMs;
                schVM.Procs = procVMs;
            }

            return schVM;
        }


        /// <summary>
        /// 保存流程图
        /// </summary>
        /// <param name="scheme"></param>
        /// <returns></returns>
        public async Task<BusinessResult> SaveFlowScheme(SchemeTempDto scheme)
        {
            //处理节点信息
            var lines = new List<LineTempEntity>();
            var procs = new List<ProcTempEntity>();
            var exeors = new List<ExecutorTempEntity>();
            var conds = new List<CondTempEntity>();
            //提取环节信息
            if (scheme.Procs != null && scheme.Procs.Count != 0)
            {
                foreach (var item in scheme.Procs)//处理环节
                {
                    item.Id = Guid.NewGuid().ToString("N");
                    item.FlowTempId = scheme.Id;
                    var proc = MapperObject.Map<ProcTempDto, ProcTempEntity>(item);
                    proc.Code = Guid.NewGuid().ToString("N");
                    if (item.Executors != null && item.Executors.Count != 0)
                    {
                        foreach (var exeItem in item.Executors) //执行人处理
                        {
                            exeItem.Id = Guid.NewGuid().ToString("N");
                            exeItem.FlowTempId = scheme.Id;
                            exeItem.ProcId = item.Id;
                            exeors.Add(exeItem);
                        }
                    }
                    procs.Add(proc);
                }
            }
            //提取线信息
            if (scheme.Lines != null && scheme.Lines.Count != 0)
            {
                foreach (var item in scheme.Lines)//流程线处理
                {
                    item.Id = Guid.NewGuid().ToString("N");
                    item.FlowTempId = scheme.Id;
                    var ll = MapperObject.Map<LineTempDto, LineTempEntity>(item);
                    ll.Code = Guid.NewGuid().ToString("N");
                    if (item.Conds != null && item.Conds.Count != 0)
                    {
                        foreach (var cond in item.Conds)
                        {
                            cond.Id = Guid.NewGuid().ToString("N");
                            cond.RefId = item.Id;
                            cond.FlowTempId = scheme.Id;
                            cond.RefType = EnumFlowEle.Line;
                            conds.Add(cond);
                        }
                    }
                    lines.Add(ll);
                }
            }

            var freeSql = IocManager.Instance.GetService<IFreeSql>(); ;

            await freeSql.Delete<ProcTempEntity>().Where(a => a.FlowTempId == scheme.Id).ExecuteAffrowsAsync();
            await freeSql.Delete<ExecutorTempEntity>().Where(a => a.FlowTempId == scheme.Id).ExecuteAffrowsAsync();
            await freeSql.Delete<LineTempEntity>().Where(a => a.FlowTempId == scheme.Id).ExecuteAffrowsAsync();
            await freeSql.Delete<CondTempEntity>().Where(a => a.FlowTempId == scheme.Id).ExecuteAffrowsAsync();

            await freeSql.Insert(exeors).ExecuteAffrowsAsync();
            await freeSql.Insert(conds).ExecuteAffrowsAsync();
            await freeSql.Insert(procs).ExecuteAffrowsAsync();
            await freeSql.Insert(lines).ExecuteAffrowsAsync();

            return BusinessResponse.Success();
        }
    }
}
