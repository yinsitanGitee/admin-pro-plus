﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Wfs.FlowTemp.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class FlowTempListDto : BaseDto
    {
        /// <summary>
        ///业务标识编号
        /// </summary>
        public string ActionCode { get; set; }

        /// <summary>
        /// 单据名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        ///编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        ///名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///所属机构
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumBool EnabledMark { get; set; }

        /// <summary>
        ///限定完成时间(天)
        /// </summary>
        public int LimtTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///是否发送所有消息给执行人
        /// </summary>
        public bool EnableSendMessageToAllExcuter { get; set; }

        /// <summary>
        ///归档后发消息类型
        /// </summary>
        public EnumSendMessageToExcuterType SendMessageToExcuterType { get; set; }

        /// <summary>
        ///归档后发消息指定人员
        /// </summary>
        public string SendMessageSelectUsers { get; set; }

        /// <summary>
        /// 是否自动通过
        /// </summary>
        public bool IsApprovalPass { get; set; }
    }
}
