﻿using Admin.Repository.Page;

namespace Admin.Application.Wfs.FlowTemp.Dto
{
    /// <summary>
    /// 定义获取分页输入dto
    /// </summary>
    public class GetFlowTempInputDto : CommonPageInputDto
    {
        /// <summary>
        ///编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 业务标识
        /// </summary>
        public string ActionCode { get; set; }

        /// <summary>
        /// 机构Id
        /// </summary>
        public string OrgId { get; set; }
    }
}
