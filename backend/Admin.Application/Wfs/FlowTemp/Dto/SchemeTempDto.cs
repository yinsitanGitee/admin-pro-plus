﻿using Admin.Repository.Entities.Wfs;

namespace Admin.Application.Wfs.FlowTemp.Dto
{
    /// <summary>
    /// 流程dto
    /// </summary>
    public class SchemeTempDto : FlowEntity
    {
        /// <summary>
        /// 环节
        /// </summary>
        public List<ProcTempDto> Procs { get; set; }
        /// <summary>
        /// 线
        /// </summary>
        public List<LineTempDto> Lines { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ProcTempDto : ProcTempEntity
    {
        /// <summary>
        /// 执行人
        /// </summary>
        public List<ExecutorTempEntity> Executors { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LineTempDto : LineTempEntity
    {
        /// <summary>
        /// 条件信息
        /// </summary>
        public List<CondTempEntity> Conds { get; set; }
    }
}
