﻿using Admin.Application.Wfs.FlowTemp.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Wfs.FlowTemp
{
    /// <summary>
    /// 定义接口服务
    /// </summary>
    public interface IFlowTempService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<FlowTempListDto>> Query(GetFlowTempInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<FlowTempEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(FlowTempEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(FlowTempEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 获取流程图
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<SchemeTempDto?> GetFlowScheme(InputDto dto);

        /// <summary>
        /// 保存流程图
        /// </summary>
        /// <param name="scheme"></param>
        /// <returns></returns>
        Task<BusinessResult> SaveFlowScheme(SchemeTempDto scheme);
    }
}
