﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Wfs.Action.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class ActionListDto : BaseDto
    {
        /// <summary>
        ///编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///流程数据源
        /// </summary>
        public string FlowDataProvider { get; set; }

        /// <summary>
        ///流程事件
        /// </summary>
        public string FlowEvent { get; set; }

        /// <summary>
        ///环节事件
        /// </summary>
        public string ProcEvent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumBool EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }
}
