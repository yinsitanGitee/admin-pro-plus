﻿using Admin.Repository.Page;

namespace Admin.Application.Wfs.Action.Dto
{
    /// <summary>
    /// 定义获取分页输入dto
    /// </summary>
    public class GetActionInputDto : CommonPageInputDto
    {
        /// <summary>
        ///编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///名称
        /// </summary>
        public string Name { get; set; }
    }
}
