﻿using Admin.Application.Wfs.Action.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;
using Admin.Core.Helper;

namespace Admin.Application.Wfs.Action
{
    /// <summary>
    /// 定义接口服务
    /// </summary>
    public interface IActionService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<ActionListDto>> Query(GetActionInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<ActionEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(ActionEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(ActionEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 获取下拉框
        /// </summary>
        /// <returns></returns>
        Task<List<CommonSelectDto>> GetSelect();
    }
}
