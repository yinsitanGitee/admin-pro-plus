﻿using Admin.Application.Wfs.Action.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Helper;
using Admin.Repository;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Admin.Repository.Page;

namespace Admin.Application.Wfs.Action
{
    /// <summary>
    /// 定义接口服务实现
    /// </summary>
    /// <param name="actionRepository"></param>
    public class ActionService(IRepository<ActionEntity, string> actionRepository) : BaseService, IActionService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<ActionListDto>> Query(GetActionInputDto dto)
        {
            var r = actionRepository.Select
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.Code), t => t.Code.Contains(dto.Code));

            return new CommonPageOutputDto<ActionListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderBy(x => x.Code).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<ActionListDto>()
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<ActionEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<ActionEditDto>.Success(data: await actionRepository.GetAsync<ActionEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(ActionEditDto dto)
        {
            var entity = MapperObject.Map<ActionEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            await actionRepository.InsertAsync(entity);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(ActionEditDto dto)
        {
            var entity = await actionRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await actionRepository.UpdateAsync(entity);

                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await actionRepository.SoftDeleteAsync(a => dto.Ids.Contains(a.Id));
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 获取下拉框
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonSelectDto>> GetSelect() => await actionRepository.Where(a => a.EnabledMark == EnumBool.TRUE).ToListAsync(a => new CommonSelectDto
        {
            Label = a.Name,
            Value = a.Code
        });
    }
}
