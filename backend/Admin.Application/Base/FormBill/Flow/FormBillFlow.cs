﻿using Admin.Core;
using Admin.Core.Service.Flow;
using Admin.Core.Service.Flow.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Base;
using Admin.Repository.Enum;

namespace Admin.Application.Base.FormBill.Flow
{
    /// <summary>
    /// 定义接口服务实现
    /// </summary>
    /// <param name="formBillrepository"></param>
    public class FormBillFlow(IRepository<BaseFormBillEntity, string> formBillrepository) : BaseService, IFlowEvent
    {
        private readonly IRepository<BaseFormBillEntity, string> _formBillrepository = formBillrepository;

        /// <summary>
        ///  下一环节创建完成后触发接口 事件执行顺序:IProcEvent.AfterCurrentProcExecuted>IProcEvent.AfterNextProcCreated>IFlowEvent.EndFlowEvent
        /// </summary>
        /// <param name="e"></param>
        public async Task AfterNextProcCreated(AfterNextProcCreatedDto e)
        {
            if (e.NextProcApprovalInfo != null && e.NextProcApprovalInfo.Count > 0)
            {
                var approvalNames = string.Join(",", e.NextProcApprovalInfo.Select(a => a.AccountName));
                await _formBillrepository.UpdateAsync(a => new BaseFormBillEntity { NextApprovalName = approvalNames, NextApprovalProcName = e.NextProcApprovalInfo[0].ProcName }, a => a.Id == e.AppId);
            }
            else
                await _formBillrepository.UpdateAsync(a => new BaseFormBillEntity { NextApprovalName = "", NextApprovalProcName = "" }, a => a.Id == e.AppId);
        }

        /// <summary>
        ///  流程执行完成
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task EndFlowEvent(EndFlowEventDto e)
        {
            if (e.State == EnumProcApprovalStatus.Agree || e.State == EnumProcApprovalStatus.SystemSkip)
                await _formBillrepository.UpdateAsync(a => new BaseFormBillEntity { State = EnumFlowState.Through, NextApprovalName = "", NextApprovalProcName = "结束" }, a => a.Id == e.AppId);
            else await _formBillrepository.UpdateAsync(a => new BaseFormBillEntity { State = EnumFlowState.NoSubmit, NextApprovalName = "", NextApprovalProcName = "结束" }, a => a.Id == e.AppId);
        }

        /// <summary>
        /// 流程数据源获取接口 注意: 1. 不能返回空值,不能抛异常 2. 如果id为空,必须返回数据源的所有字段 3.工作流高度依赖此接口,请务必注意执行性能
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<List<FlowField>?>? GetDetailForWorkFlow(FlowDataProviderDto e)
        {
            return null;
        }

        /// <summary>
        /// 流程主动终止
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public Task InitiativeAbandonFlow(FlowDataProviderDto e)
        {
            return Task.CompletedTask;
        }
    }
}
