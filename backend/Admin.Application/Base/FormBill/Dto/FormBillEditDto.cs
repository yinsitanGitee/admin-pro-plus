﻿using Admin.Core.Service.Flow.Dto;
using Admin.Repository.Dto;

namespace Admin.Application.Base.FormBill.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class FormBillEditDto : BaseDto
    {
        /// <summary>
        /// 操作类型 1是提交
        /// </summary>
        public int OpeartType { get; set; }

        /// <summary>
        /// 表单id
        /// </summary>
        public string FormId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        ///流程id
        /// </summary>
        public string ProcessId { get; set; }

        /// <summary>
        /// 表单详情
        /// </summary>
        public string FormDetail { get; set; }

        /// <summary>
        /// 表单值详情
        /// </summary>
        public string FormDetailData { get; set; }

        /// <summary>
        /// 流程信息
        /// </summary>
        public FlowInfoAPIVM? FlowInfo { get; set; }
    }
}
