﻿using Admin.Repository.Page;

namespace Admin.Application.Base.FormBill.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetFormBillInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }   
        
        /// <summary>
        /// 查询类型
        /// 1、我提交的
        /// 2、待我审批的
        /// 3、我审批过的
        /// </summary>
        public int Type { get; set; }
    }
}
