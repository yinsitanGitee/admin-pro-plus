﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Base.FormBill.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class FormBillListDto : BaseDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrganizeName { get; set; }

        /// <summary>
        ///流程id
        /// </summary>
        public string ProcessId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public EnumFlowState State { get; set; }

        /// <summary>
        /// 下级节点处理人
        /// </summary>
        public string? NextApprovalName { get; set; }

        /// <summary>
        /// 下级节点名称
        /// </summary>
        public string? NextApprovalProcName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreateUserName { get; set; }
    }
}
