﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Base.FormBill.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class FormBillDetailDto : BaseDto
    {
        /// <summary>
        /// 表单id
        /// </summary>
        public string FormId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public EnumFlowState State { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }   
        
        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrganizeName { get; set; }

        /// <summary>
        ///流程id
        /// </summary>
        public string ProcessId { get; set; }

        /// <summary>
        /// 表单详情
        /// </summary>
        public string FormDetail { get; set; }

        /// <summary>
        /// 表单值详情
        /// </summary>
        public string FormDetailData { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreateUserName { get; set; }
    }
}
