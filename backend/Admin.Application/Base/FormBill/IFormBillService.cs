﻿using Admin.Application.Base.FormBill.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.Flow.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Base.FormBill
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IFormBillService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<FormBillListDto>> Query(GetFormBillInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<FormBillEditDto>> Get(InputDto dto);

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<FormBillDetailDto>> GetDetail(InputDto dto);

        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Approval(ApprovalAPIDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(FormBillEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(FormBillEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);
    }
}
