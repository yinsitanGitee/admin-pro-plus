﻿using Admin.Application.Base.FormBill.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core;
using Admin.Core.Auth;
using Admin.Core.Service.AutoCode;
using Admin.Core.Service.Flow;
using Admin.Core.Service.Flow.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Base;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Admin.Repository.Page;

namespace Admin.Application.Base.FormBill
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="formBillRepository"></param>
    /// <param name="currentUser"></param>
    public class FormBillService(IRepository<BaseFormBillEntity, string> formBillRepository, ICurrentUser currentUser) : BaseService, IFormBillService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<FormBillListDto>> Query(GetFormBillInputDto dto)
        {
            var userId = currentUser.UserId;
            var q = formBillRepository.Select.From<SysOrganizeEntity>()
                .LeftJoin(a => a.t1.OrganizeId == a.t2.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.OrganizeId), t => t.t1.OrganizeId == dto.OrganizeId)
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.t1.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.Code), t => t.t1.Code.Contains(dto.Code));
            var r = dto.Type switch
            {
                2 => q.Where(a => formBillRepository.Orm.Select<WorkListEntity>().Any(b => b.FlowId == a.t1.ProcessId && b.IsNeed && !b.IsHandled
                                      && b.AccountId == userId)).WithTempQuery(a => new FormBillListDto
                                      {
                                          NextApprovalName = a.t1.NextApprovalName,
                                          NextApprovalProcName = a.t1.NextApprovalProcName,
                                          Code = a.t1.Code,
                                          CreateDateTime = a.t1.CreateDateTime,
                                          CreateUserName = a.t1.CreateUserName,
                                          Id = a.t1.Id,
                                          Name = a.t1.Name,
                                          State = a.t1.State,
                                          OrganizeName = a.t2.Name
                                      }),
                3 => q.Where(a => formBillRepository.Orm.Select<WorkListEntity>().Any(b => b.FlowId == a.t1.ProcessId && b.IsNeed && b.IsHandled
                    && b.AccountId == userId)).Where(a => a.t1.State != EnumFlowState.NoSubmit).WithTempQuery(a => new FormBillListDto
                    {
                        NextApprovalName = a.t1.NextApprovalName,
                        NextApprovalProcName = a.t1.NextApprovalProcName,
                        Code = a.t1.Code,
                        CreateDateTime = a.t1.CreateDateTime,
                        CreateUserName = a.t1.CreateUserName,
                        Id = a.t1.Id,
                        Name = a.t1.Name,
                        State = a.t1.State,
                        OrganizeName = a.t2.Name
                    }),
                _ => q.Where(a => a.t1.CreateUserId == userId).WithTempQuery(a => new FormBillListDto
                {
                    NextApprovalName = a.t1.NextApprovalName,
                    NextApprovalProcName = a.t1.NextApprovalProcName,
                    Code = a.t1.Code,
                    CreateDateTime = a.t1.CreateDateTime,
                    CreateUserName = a.t1.CreateUserName,
                    Id = a.t1.Id,
                    Name = a.t1.Name,
                    State = a.t1.State,
                    OrganizeName = a.t2.Name
                }),
            };

            return new CommonPageOutputDto<FormBillListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync()
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<FormBillEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<FormBillEditDto>.Success(data: await formBillRepository.GetAsync<FormBillEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<FormBillDetailDto>> GetDetail(InputDto dto)
        {
            return BusinessResponse<FormBillDetailDto>.Success(data: await formBillRepository.Select.From<SysOrganizeEntity>().LeftJoin(a => a.t1.OrganizeId == a.t2.Id).Where(a => a.t1.Id == dto.Id).ToOneAsync(a => new FormBillDetailDto
            {
                OrganizeName = a.t2.Name
            }));
        }

        /// <summary>
        /// 处理数据
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private async Task<string> Pro(FormBillEditDto dto)
        {
            var form = await formBillRepository.Orm.Select<BaseFormEntity>().Where(a => a.Id == dto.FormId).ToOneAsync();
            if (form == null) return "表单不存在，请刷新重试";

            if (string.IsNullOrWhiteSpace(dto.Id))
            {
                dto.Id = dto.FormId;
                dto.Name = currentUser.UserName + "提交的" + form.Name;
            }

            if (dto.OpeartType == 1)
                dto.Code = await IocManager.Instance.GetService<IAutoCodeService>().GetBillCode("Ls.Form.Code");
            else dto.Code = string.Empty;

            return string.Empty;
        }


        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Approval(ApprovalAPIDto dto)
        {
            var entity = await formBillRepository.GetAsync(dto.AppId);
            if (entity == null) return BusinessResponse.Error(msg: "数据不存在,请刷新页面重试");
            if (entity.State != EnumFlowState.Approval) return BusinessResponse.Error(msg: "单据状态不是审批中，无法操作，请刷新重试");
            dto.FlowId = entity.ProcessId;
            var res = await TryFun(async () =>
            {
                await IocManager.Instance.GetService<IProcService>().ApprovalProc(dto);
            });

            if (!string.IsNullOrWhiteSpace(res)) return BusinessResponse.Error(msg: res);

            return BusinessResponse.Success();
        }


        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(FormBillEditDto dto)
        {
            var error = await Pro(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = MapperObject.Map<BaseFormBillEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            entity = await formBillRepository.InsertAsync(entity);
            entity.State = EnumFlowState.NoSubmit;
            if (dto.OpeartType == 1)
                await SubmitFlow(dto, entity);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(FormBillEditDto dto)
        {
            var entity = await formBillRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else if (entity.State != EnumFlowState.NoSubmit) return BusinessResponse.Error(msg: "单据不是待提交状态，无法修改，请刷新重新查看");
            else
            {
                var error = await Pro(dto);
                if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
                entity = MapperObject.Map(dto, entity);
                await formBillRepository.UpdateAsync(entity);
                if (dto.OpeartType == 1)
                    await SubmitFlow(dto, entity);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 提交审批
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        private async Task SubmitFlow(FormBillEditDto dto, BaseFormBillEntity entity)
        {
            if (dto.FlowInfo == null || string.IsNullOrWhiteSpace(dto.FlowInfo.FlowTempId))
                await formBillRepository.UpdateAsync(a => new BaseFormBillEntity { State = EnumFlowState.Through }, a => a.Id == entity.Id);
            else
            {
                var result = await IocManager.Instance.GetService<IFlowService>().SubmitFlow(new FlowAppAPIVM
                {
                    AppCode = entity.Code,
                    AppId = entity.Id,
                    Title = entity.Name + "审批任务",
                    SourceTypeValue = MsgTaskTypeEnum.Form,
                    Description = $"提交人:{currentUser.UserName},<br>提交时间:{entity.CreateDateTime:yyyy年MM月dd日 HH:mm}<br>",
                    Content = $"{entity.Name}，单据编号({entity.Code})",
                }, dto.FlowInfo ?? new());
                if (result.Status == EnumFlowCommonResult.Common)
                    await formBillRepository.UpdateAsync(a => new BaseFormBillEntity { State = EnumFlowState.Approval, ProcessId = result.Result }, a => a.Id == entity.Id);
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            if (await formBillRepository.Select.AnyAsync(a => a.State != EnumFlowState.NoSubmit && dto.Ids.Contains(a.Id)))
                return BusinessResponse.Error(msg: "请选择待提交的数据删除");
            await formBillRepository.SoftDeleteAsync(t => dto.Ids.Contains(t.Id));
            return BusinessResponse.Success();
        }
    }
}
