﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Base.Form.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class FormListDto : BaseDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }   
        
        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrganizeName { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 表单详情
        /// </summary>
        public string FormDetail { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
