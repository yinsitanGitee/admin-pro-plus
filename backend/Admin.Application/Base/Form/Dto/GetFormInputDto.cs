﻿using Admin.Repository.Page;

namespace Admin.Application.Base.Form.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetFormInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
    }
}
