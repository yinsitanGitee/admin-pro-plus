﻿using Admin.Application.Base.Form.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Repository.Page;

namespace Admin.Application.Base.Form
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IFormService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<FormListDto>> Query(GetFormInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<FormEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(FormEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(FormEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 获取下拉框
        /// </summary>
        /// <returns></returns>
        Task<List<CommonSelectDto>> GetSelect();
    }
}
