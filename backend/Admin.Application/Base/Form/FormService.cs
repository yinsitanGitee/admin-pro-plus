﻿using Admin.Application.Base.Form.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Helper;
using Admin.Core.Transactional;
using Admin.Repository;
using Admin.Repository.Entities.Base;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Entities.Wfs;
using Admin.Repository.Enum;
using Admin.Repository.Page;

namespace Admin.Application.Base.Form
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="formRepository"></param>
    /// <param name="actionRepository"></param>
    public class FormService(IRepository<BaseFormEntity, string> formRepository, Lazy<IRepository<ActionEntity, string>> actionRepository) : BaseService, IFormService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<FormListDto>> Query(GetFormInputDto dto)
        {
            var r = formRepository.Select.From<SysOrganizeEntity>()
                .LeftJoin(a => a.t1.OrganizeId == a.t2.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.OrganizeId), t => t.t1.OrganizeId == dto.OrganizeId)
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.t1.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.Code), t => t.t1.Code.Contains(dto.Code));

            return new CommonPageOutputDto<FormListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.t1.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new FormListDto
                {
                    OrganizeName = a.t2.Name
                })
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<FormEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<FormEditDto>.Success(data: await formRepository.GetAsync<FormEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 处理数据
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private async Task<string> Pro(FormEditDto dto)
        {
            if (string.IsNullOrWhiteSpace(dto.FormDetail))
                return "请添加表单配置";

            if (await formRepository.Select.AnyAsync(a => a.Code == dto.Code && a.Id != dto.Id))
                return "表单编号重复，请重新输入";

            if (await formRepository.Select.AnyAsync(a => a.Code == dto.Code && a.Id != dto.Id))
                return "表单名称重复，请重新输入";

            return string.Empty;
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Transactional]
        public async Task<BusinessResult> Add(FormEditDto dto)
        {
            var error = await Pro(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = MapperObject.Map<BaseFormEntity>(dto);
            await formRepository.InsertAsync(entity);
            await actionRepository.Value.InsertAsync(new ActionEntity
            {
                Code = "Ls.Form." + dto.Code,
                EnabledMark = EnumBool.TRUE,
                FlowEvent = "FormBillFlow",
                Name = dto.Name,
                Description = "该业务定义由系统自动生成，请勿修改"
            });
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Transactional]
        public async Task<BusinessResult> Edit(FormEditDto dto)
        {
            var error = await Pro(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = await formRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                var code = "Ls.Form." + dto.Code;
                await actionRepository.Value.UpdateAsync(a => new ActionEntity
                {
                    Name = dto.Name
                }, a => a.Code == code);
                entity = MapperObject.Map(dto, entity);
                await formRepository.UpdateAsync(entity, a => a.Code);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await formRepository.SoftDeleteAsync(t => dto.Ids.Contains(t.Id));
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 获取下拉框
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonSelectDto>> GetSelect() => await formRepository.Where(a => a.State == CommonStateEnum.Effective).ToListAsync(a => new CommonSelectDto
        {
            Label = a.Name,
            Value = a.Code
        });
    }
}
