﻿namespace Admin.Application.Sys.Dictionary.Dto
{
    /// <summary>
    /// 定义词典编辑dto
    /// </summary>
    public class DictionaryEditDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否树级
        /// </summary>
        public bool IsTree { get; set; }
    }
}
