﻿namespace Admin.Application.Sys.Dictionary.Dto
{
    /// <summary>
    /// 定义词典查询输入dto
    /// </summary>
    public class GetDictionaryDetailInputDto
    {
        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 单据id
        /// </summary>
        public string BillId { get; set; }

        /// <summary>
        /// 是否树
        /// </summary>
        public bool IsTree { get; set; }
    }
}
