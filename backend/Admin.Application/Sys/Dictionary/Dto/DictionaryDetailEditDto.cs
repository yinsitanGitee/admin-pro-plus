﻿namespace Admin.Application.Sys.Dictionary.Dto
{
    /// <summary>
    /// 定义词典明细dto
    /// </summary>
    public class DictionaryDetailDto : DictionaryDetailEditDto
    {
        public List<DictionaryDetailDto> Child { get; set; }
    }

    /// <summary>
    /// 定义词典编辑dto
    /// </summary>
    public class DictionaryDetailEditDto
    {
        /// <summary>
        /// 主键id
        /// </summary>
        public string BillId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool IsEffective { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
