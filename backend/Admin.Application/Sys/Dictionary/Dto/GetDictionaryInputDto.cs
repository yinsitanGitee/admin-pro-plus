﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.Dictionary.Dto
{
    /// <summary>
    /// 定义查询词典dto
    /// </summary>
    public class GetDictionaryInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
