﻿namespace Admin.Application.Sys.Dictionary.Dto
{
    /// <summary>
    /// 定义词典树形dto
    /// </summary>
    public class DictionaryDetailTreeListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool IsEffective { get; set; }

        /// <summary>
        /// 是否树
        /// </summary>
        public bool IsTree { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public List<DictionaryDetailTreeListDto>? Children { get; set; }
    }
}
