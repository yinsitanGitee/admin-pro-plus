﻿using Admin.Application.Sys.Dictionary.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Sys.Dictionary
{
    /// <summary>
    /// 词典服务
    /// </summary>
    public interface IDictionaryService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<DictionaryListDto>> Query(GetDictionaryInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<DictionaryEditDto>> Get(InputDto dto);

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(DictionaryEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(DictionaryEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<List<DictionaryDetailTreeListDto>?> QueryDetail(GetDictionaryDetailInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<DictionaryDetailEditDto>> GetDetail(InputDto dto);

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> AddDetail(DictionaryDetailEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> EditDetail(DictionaryDetailEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> DeleteDetail(InputDto dto);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<List<DictionaryDetailTreeListDto>?> GetCache(string code);
    }
}
