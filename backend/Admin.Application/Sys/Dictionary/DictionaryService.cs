﻿using Admin.Application.Sys.Dictionary.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;

namespace Admin.Application.Sys.Dictionary
{
    /// <summary>
    /// 词典服务
    /// </summary>
    /// <param name="dictionaryRepository"></param>
    /// <param name="dictionaryDetailRepository"></param>
    public class DictionaryService(IRepository<SysDictionaryEntity, string> dictionaryRepository, IRepository<SysDictionaryDetailEntity, string> dictionaryDetailRepository) : BaseService, IDictionaryService
    {
        #region dictionary

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<DictionaryListDto>> Query(GetDictionaryInputDto dto)
        {
            var r = dictionaryRepository.Select
                .WhereIf(!string.IsNullOrEmpty(dto.Code), t => t.Code.Contains(dto.Code))
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.Name.Contains(dto.Name));

            return new CommonPageOutputDto<DictionaryListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderBy(x => x.Code).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<DictionaryListDto>()
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<DictionaryEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<DictionaryEditDto>.Success(
                await dictionaryRepository.GetAsync<DictionaryEditDto>(a => a.Id == dto.Id)
                );
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(DictionaryEditDto dto)
        {
            var entity = MapperObject.Map<SysDictionaryEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            await dictionaryRepository.InsertAsync(entity);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(DictionaryEditDto dto)
        {
            var entity = await dictionaryRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await dictionaryRepository.UpdateAsync(entity);
                var keyValue = RedisKeyDto.DictionaryKey + dto.Code;
                await RedisManage.DelAsync(keyValue);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            var entity = await dictionaryRepository.Where(a => dto.Ids.Contains(a.Id)).ToListAsync();
            if (!entity.Any())
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                await dictionaryRepository.SoftDeleteAsync(a => dto.Ids.Contains(a.Id));
                await dictionaryDetailRepository.SoftDeleteAsync(a => dto.Ids.Contains(a.BillId));
                var keys = entity.Select(a => RedisKeyDto.DictionaryKey + a.Code).ToArray();
                await RedisManage.DelAsync(keys);
                return BusinessResponse.Success();
            }
        }

        #endregion

        #region dictionaryDetail

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<DictionaryDetailTreeListDto>?> QueryDetail(GetDictionaryDetailInputDto dto)
        {
            var r = await dictionaryDetailRepository.Select
                .WhereIf(!string.IsNullOrEmpty(dto.Value), t => t.Value.Contains(dto.Value))
                .Where(t => t.BillId == dto.BillId)
                .OrderBy(x => x.Sort)
                .ToListAsync<DictionaryDetailTreeListDto>();

            if (dto.IsTree) return FormGetDictoryDetailTree("0", r);

            return r;
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<DictionaryDetailEditDto>> GetDetail(InputDto dto)
        {
            var data = await dictionaryDetailRepository.GetAsync<DictionaryDetailEditDto>(a => a.Id == dto.Id);
            if (data == null) return BusinessResponse<DictionaryDetailEditDto>.Error(msg: "数据不存在，请刷新重试");
            return BusinessResponse<DictionaryDetailEditDto>.Success(data);
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> AddDetail(DictionaryDetailEditDto dto)
        {
            var entity = MapperObject.Map<SysDictionaryDetailEntity>(dto);
            var code = await dictionaryRepository.Where(a => a.Id == dto.BillId).ToOneAsync(a => a.Code);
            if (string.IsNullOrWhiteSpace(code)) return BusinessResponse.Error(msg: "词典不存在");

            await dictionaryDetailRepository.InsertAsync(entity);
            await RedisManage.DelAsync(RedisKeyDto.DictionaryKey + code);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> EditDetail(DictionaryDetailEditDto dto)
        {
            var entity = await dictionaryDetailRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                var code = await dictionaryRepository.Where(a => a.Id == dto.BillId).ToOneAsync(a => a.Code);
                if (string.IsNullOrWhiteSpace(code)) return BusinessResponse.Error(msg: "词典不存在");

                entity = MapperObject.Map(dto, entity);
                await dictionaryDetailRepository.UpdateAsync(entity);
                await RedisManage.DelAsync(RedisKeyDto.DictionaryKey + code);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> DeleteDetail(InputDto dto)
        {
            await dictionaryDetailRepository.SoftDeleteAsync(a => dto.Ids.Contains(a.Id));
            await RedisManage.DelAsync(RedisKeyDto.DictionaryKey + dto.BillCode);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 递归获取词典
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="dtos"></param>     
        /// <returns></returns>
        public List<DictionaryDetailTreeListDto>? FormGetDictoryDetailTree(string parentId, List<DictionaryDetailTreeListDto> dtos)
        {
            if (dtos != null && dtos.Count > 0)
            {
                var a = dtos.FindAll(p => p.ParentId == parentId);
                if (a.Count > 0)
                {
                    List<DictionaryDetailTreeListDto> dicts = new();

                    foreach (var item in a)
                    {
                        item.Children = FormGetDictoryDetailTree(item.Id, dtos);
                        dicts.Add(item);
                    }
                    return dicts;
                }
            }

            return null;
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<List<DictionaryDetailTreeListDto>?> GetCache(string code)
        {
            var k = RedisKeyDto.DictionaryKey + code;
            var data = await RedisManage.GetAsync<List<DictionaryDetailTreeListDto>>(k);
            if (data != null && data.Count > 0) return data;

            var ds = await dictionaryDetailRepository.Select.From<SysDictionaryEntity>()
                    .LeftJoin(a => a.t1.BillId == a.t2.Id)
                    .Where(a => a.t1.IsEffective && a.t2.Code == code).OrderBy(x => x.t1.Sort).ToListAsync(a => new DictionaryDetailTreeListDto
                    {
                        IsTree = a.t2.IsTree
                    });

            if (ds.Count > 0)
            {
                if (ds[0].IsTree)
                {
                    var retDs = FormGetDictoryDetailTree("0", ds) ?? new List<DictionaryDetailTreeListDto>();
                    await RedisManage.SetAsync(k, retDs, TimeSpan.FromMinutes(RedisKeyDto.CommonExpiresIn));
                    return retDs;
                }
            }

            await RedisManage.SetAsync(k, ds, TimeSpan.FromMinutes(RedisKeyDto.CommonExpiresIn));
            return ds;
        }

        #endregion
    }
}
