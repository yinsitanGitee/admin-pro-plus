﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.Role.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetRoleInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
