﻿using Admin.Application.Sys.Role.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;
using Admin.Core.Helper;

namespace Admin.Application.Sys.Role
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IRoleService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<RoleListDto>> Query(GetRoleInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<RoleEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(RoleEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(RoleEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 获取角色下拉框
        /// </summary>
        /// <returns></returns>
        Task<List<CommonSelectDto>> GetRoleSelect();
    }
}
