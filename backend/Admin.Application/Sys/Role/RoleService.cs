﻿using Admin.Application.Sys.Role.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Helper;
using Admin.Core.Transactional;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Admin.Repository.Page;

namespace Admin.Application.Sys.Role
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysRoleRepository"></param>
    /// <param name="sysAccountRoleRepository"></param>
    public class RoleService(IRepository<SysRoleEntity, string> sysRoleRepository, Lazy<IRepository<SysAccountRoleEntity, string>> sysAccountRoleRepository) : BaseService, IRoleService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<RoleListDto>> Query(GetRoleInputDto dto)
        {
            var r = sysRoleRepository.Select
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.Name.Contains(dto.Name));

            return new CommonPageOutputDto<RoleListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<RoleListDto>()
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<RoleEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<RoleEditDto>.Success(data: await sysRoleRepository.GetAsync<RoleEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(RoleEditDto dto)
        {
            var entity = MapperObject.Map<SysRoleEntity>(dto);
            await sysRoleRepository.InsertAsync(entity);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(RoleEditDto dto)
        {
            var entity = await sysRoleRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await sysRoleRepository.UpdateAsync(entity);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Transactional]
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await sysRoleRepository.SoftDeleteAsync(t => dto.Ids.Contains(t.Id));
            await sysAccountRoleRepository.Value.SoftDeleteAsync(t => dto.Ids.Contains(t.RoleId));
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 获取角色下拉框
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonSelectDto>> GetRoleSelect() => await sysRoleRepository.Where(a => a.State == CommonStateEnum.Effective).ToListAsync(a => new CommonSelectDto
        {
            Label = a.Name,
            Value = a.Id
        });
    }
}
