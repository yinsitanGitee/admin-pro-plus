﻿using Admin.Application.Sys.ConfigItem.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Json;
using Admin.Core;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;

namespace Admin.Application.Sys.ConfigItem
{
    /// <summary>
    /// 参数服务
    /// </summary>
    /// <param name="configItemRepository"></param>
    public class SysConfigItemService(IRepository<SysConfigItemEntity, string> configItemRepository) : BaseService, ISysConfigItemService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<SysConfigItemListDto>> Query(GetSysConfigItemInputDto dto)
        {
            var r = configItemRepository.Select
                .WhereIf(!string.IsNullOrEmpty(dto.ConfigKey), t => t.ConfigKey.Contains(dto.ConfigKey))
                .WhereIf(!string.IsNullOrEmpty(dto.Remark), t => t.Remark.Contains(dto.Remark));

            return new CommonPageOutputDto<SysConfigItemListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderBy(x => x.ConfigKey).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<SysConfigItemListDto>()
            };
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<SysConfigItemEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<SysConfigItemEditDto>.Success(data: await configItemRepository.GetAsync<SysConfigItemEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(SysConfigItemEditDto dto)
        {
            var entity = MapperObject.Map<SysConfigItemEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            await configItemRepository.InsertAsync(entity);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(SysConfigItemEditDto dto)
        {
            var entity = await configItemRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await configItemRepository.UpdateAsync(entity);
                var keyValue = RedisKeyDto.ConfigItemKey + entity.ConfigKey;
                await RedisManage.DelAsync(keyValue);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            var entity = await configItemRepository.Where(a => dto.Ids.Contains(a.Id)).ToListAsync(a => a.ConfigKey);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                await configItemRepository.SoftDeleteAsync(a => dto.Ids.Contains(a.Id));
                await RedisManage.DelAsync(entity.Select(a => RedisKeyDto.ConfigItemKey + a).ToArray());
                return BusinessResponse.Success();
            }
        }


        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        public async Task<string> GetConfigItemValue(string configKey)
        {
            var keyValue = RedisKeyDto.ConfigItemKey + configKey;
            var cache = await RedisManage.GetAsync<SysConfigItemEntity>(keyValue);

            if (cache == null)
            {
                cache = await configItemRepository.GetAsync(a => a.ConfigKey == configKey);
                if (cache != null)
                {
                    await RedisManage.SetAsync(keyValue, cache, TimeSpan.FromMinutes(RedisKeyDto.CommonLargeExpiresIn));
                    return cache.ConfigValue;
                }
                else
                {
                    return string.Empty;
                }
            }

            return cache.ConfigValue;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        public async Task<T?> GetConfigItemValue<T>(string configKey)
        {
            var keyValue = RedisKeyDto.ConfigItemKey + configKey;
            var cache = await RedisManage.GetAsync<SysConfigItemEntity>(keyValue);

            if (cache == null)
            {
                cache = await configItemRepository.GetAsync(a => a.ConfigKey == configKey);
                if (cache != null)
                {
                    await RedisManage.SetAsync(keyValue, cache, TimeSpan.FromMinutes(RedisKeyDto.CommonLargeExpiresIn));
                    return JsonHelper.DeserializeObject<T>(cache.ConfigValue);
                }
                else
                {
                    return default;
                }
            }

            return JsonHelper.DeserializeObject<T>(cache.ConfigValue);
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> GetConfigItemValues(List<string> keys)
        {
            keys = keys.Distinct().ToList();
            Dictionary<string, string> result = new Dictionary<string, string>();
            List<string> noCacheKeys = new List<string>();
            foreach (var k in keys)
            {
                var entity = await RedisManage.GetAsync<SysConfigItemEntity>(RedisKeyDto.ConfigItemKey + k);
                if (entity == null) noCacheKeys.Add(k);
                else result.Add(k, entity.ConfigValue);
            }

            if (noCacheKeys.Count > 0)
            {
                var entities = await configItemRepository.Where(a => noCacheKeys.Contains(a.ConfigKey)).ToListAsync();
                foreach (var e in entities)
                {
                    await RedisManage.SetAsync(RedisKeyDto.ConfigItemKey + e.ConfigKey, e, TimeSpan.FromMinutes(RedisKeyDto.CommonLargeExpiresIn));
                    result.Add(e.ConfigKey, e.ConfigValue);
                }
            }

            return result;
        }
    }
}
