﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.ConfigItem.Dto
{
    /// <summary>
    /// 定义获取参数分页输入dto
    /// </summary>
    public class GetSysConfigItemInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 键
        /// </summary>
        public string ConfigKey { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }
    }

    /// <summary>
    /// 定义获取参数键输入dto
    /// </summary>
    public class GetConfigItemValueInputDto
    {
        /// <summary>
        /// 键
        /// </summary>
        public string ConfigKey { get; set; }

        /// <summary>
        /// 键
        /// </summary>
        public List<string> ConfigKeys { get; set; }
    }
}
