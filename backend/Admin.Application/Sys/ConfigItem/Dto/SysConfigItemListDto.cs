﻿namespace Admin.Application.Sys.ConfigItem.Dto
{
    public class SysConfigItemListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// ConfigKey
        /// </summary>
        public string ConfigKey { get; set; }

        /// <summary>
        /// ConfigValue
        /// </summary>
        public string ConfigValue { get; set; }

        /// <summary>
        /// Remark
        /// </summary>
        public string Remark { get; set; }
    }
}
