﻿using Admin.Application.Sys.ConfigItem.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Sys.ConfigItem
{
    /// <summary>
    /// 定义参数服务
    /// </summary>
    public interface ISysConfigItemService
    {
        #region 业务

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<SysConfigItemListDto>> Query(GetSysConfigItemInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<SysConfigItemEditDto>> Get(InputDto dto);

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(SysConfigItemEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(SysConfigItemEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        #endregion

        #region tip

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        Task<string> GetConfigItemValue(string configKey);

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        Task<T?> GetConfigItemValue<T>(string configKey);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task<Dictionary<string, string>> GetConfigItemValues(List<string> keys);

        #endregion
    }
}
