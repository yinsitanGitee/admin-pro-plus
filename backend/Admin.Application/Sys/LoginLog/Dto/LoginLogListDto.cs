﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.LoginLog.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class LoginLogListDto : BaseDto
    {
        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string Request { get; set; }

        /// <summary>
        /// 响应参数
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public SuccessStateEnum Status { get; set; }
    }
}
