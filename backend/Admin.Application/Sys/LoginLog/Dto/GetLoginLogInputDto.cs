﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.LoginLog.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetLoginLogInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }
    }
}
