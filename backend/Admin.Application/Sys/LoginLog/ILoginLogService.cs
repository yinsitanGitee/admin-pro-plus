﻿using Admin.Application.Sys.LoginLog.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Sys.LoginLog
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface ILoginLogService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<LoginLogListDto>> Query(GetLoginLogInputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        Task Add(string body);
    }
}
