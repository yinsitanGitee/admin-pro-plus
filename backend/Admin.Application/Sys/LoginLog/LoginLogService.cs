﻿using Admin.Application.Sys.LoginLog.Dto;
using Admin.Common.Json;
using Admin.Core;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;

namespace Admin.Application.Sys.LoginLog
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysLoginLogRepository"></param>
    public class LoginLogService(IRepository<SysLoginLogEntity, string> sysLoginLogRepository) : BaseService, ILoginLogService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<LoginLogListDto>> Query(GetLoginLogInputDto dto)
        {
            var r = sysLoginLogRepository.Select
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.Account), t => t.Account.Contains(dto.Account));

            return new CommonPageOutputDto<LoginLogListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.RequestTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<LoginLogListDto>()
            };
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public async Task Add(string body)
        {
            var dto = JsonHelper.DeserializeObject<LoginLogEditDto>(body);
            if (dto != null)
            {
                var entity = MapperObject.Map<SysLoginLogEntity>(dto);
                await sysLoginLogRepository.InsertAsync(entity);
            }
        }
    }
}
