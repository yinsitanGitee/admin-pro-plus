﻿using Admin.Application.Sys.View.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Mapster;

namespace Admin.Application.Sys.View
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysViewRepository"></param>
    public class ViewService(IRepository<SysViewEntity, string> sysViewRepository) : BaseService, IViewService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public async Task<List<ViewListDto>?> Query()
        {
            return ForeachMenu(await GetViewCache<ViewListDto>(), "0");
        }

        /// <summary>
        /// 获取单挑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<ViewEditDto>> Get(InputDto dto)
        {
            var info = await sysViewRepository.GetAsync<ViewEditDto>(a => a.Id == dto.Id);
            if (info.ParentId != "0")
                info.ParentName = (await sysViewRepository.GetAsync<ViewEditDto>(a => a.Id == info.ParentId)).Name;
            return BusinessResponse<ViewEditDto>.Success(info);
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(ViewEditDto dto)
        {
            if (await sysViewRepository.Select.AnyAsync(a => a.Code == dto.Code && a.Path == dto.Path))
                return BusinessResponse.Error(msg: "当前菜单编号或菜单地址已存在，无法保存！");
            var entity = MapperObject.Map<SysViewEntity>(dto);
            entity.Id = Guid.NewGuid().ToString();
            await sysViewRepository.InsertAsync(entity);
            await RedisManage.DelAsync(RedisKeyDto.ViewKey);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns> 
        public async Task<BusinessResult> Edit(ViewEditDto dto)
        {
            var entity = await sysViewRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                if (await sysViewRepository.Select.AnyAsync(a => a.Code == dto.Code && a.Path == dto.Path && a.Id != dto.Id))
                    return BusinessResponse.Error(msg: "当前菜单编号或菜单地址已存在，无法保存！");
                entity = MapperObject.Map(dto, entity);
                await sysViewRepository.UpdateAsync(entity);
                await RedisManage.DelAsync(RedisKeyDto.ViewKey);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            //if (await sysViewRepository.Select.AnyAsync(a => a.ParentId == dto.Id))
            //    return BusinessResponse.Error(msg: "请先删除子节点");

            await sysViewRepository.SoftDeleteAsync(t => t.Id == dto.Id);
            await RedisManage.DelAsync(RedisKeyDto.ViewKey);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 递归获取菜单
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public List<ViewListDto>? ForeachMenu(List<ViewListDto>? list, string pid)
        {
            if (list != null && list.Count > 0)
            {
                var a = list.FindAll(p => p.ParentId == pid);
                if (a.Count > 0)
                {
                    List<ViewListDto> menu = new();
                    foreach (var item in a)
                    {
                        item.Children = ForeachMenu(list, item.Id);
                        menu.Add(item);
                    }
                    return menu;
                }
            }

            return null;
        }

        /// <summary>
        /// 获取view缓存
        /// </summary>
        /// <returns></returns>
        public async Task<List<T>?> GetViewCache<T>()
        {
            var cache = await RedisManage.GetAsync<List<T>>(RedisKeyDto.ViewKey);

            if (cache == null)
            {
                var e = await sysViewRepository.Select.OrderBy(a => a.Sort).DisableGlobalFilter(FilterSetting.DataAuth).ToListAsync<ViewListDto>();
                if (e.Any())
                {
                    await RedisManage.SetAsync(RedisKeyDto.ViewKey, e);
                    cache = e.Adapt<List<T>>();
                }
            }

            return cache;
        }
    }
}
