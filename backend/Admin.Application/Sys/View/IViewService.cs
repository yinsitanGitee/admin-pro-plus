﻿using Admin.Application.Sys.View.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;

namespace Admin.Application.Sys.View
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IViewService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        Task<List<ViewListDto>?> Query();

        /// <summary>
        /// 获取单挑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<ViewEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(ViewEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns> 
        Task<BusinessResult> Edit(ViewEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 递归获取菜单
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        List<ViewListDto>? ForeachMenu(List<ViewListDto>? list, string pid);

        /// <summary>
        /// 获取view缓存
        /// </summary>
        /// <returns></returns>
        Task<List<T>?> GetViewCache<T>();
    }
}
