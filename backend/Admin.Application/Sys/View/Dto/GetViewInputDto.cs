﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.View.Dto
{
    /// <summary>
    /// 定义输入dto
    /// </summary>
    public class GetViewInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        public string ParentId { get; set; }
    }
}
