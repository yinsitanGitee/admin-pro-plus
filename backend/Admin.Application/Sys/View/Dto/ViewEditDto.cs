﻿using Admin.Repository.Enum;
using System.ComponentModel.DataAnnotations;

namespace Admin.Application.Sys.View.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class ViewEditDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        [Required]
        public string ParentId { get; set; }

        /// <summary>
        /// 父级名称
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Required]
        public ViewTypeEnum Type { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 外链地址
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool? IsHide { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int? Sort { get; set; }

        /// <summary>
        /// 重定向
        /// </summary>
        public string Redirect { get; set; }

        /// <summary>
        /// 页面是否缓存
        /// </summary>
        public bool IsKeepAlive { get; set; }

        /// <summary>
        /// 页面是否固定，不能关闭
        /// </summary>
        public bool IsAffix { get; set; }

        /// <summary>
        /// 是否内嵌
        /// </summary>
        public bool IsIframe { get; set; }
    }
}
