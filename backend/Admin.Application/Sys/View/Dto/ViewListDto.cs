﻿using Admin.Repository.Enum;

namespace Admin.Application.Sys.View.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class ViewListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 外链地址
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public ViewTypeEnum Type { get; set; }

        /// <summary>
        ///路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool? IsHide { get; set; }

        /// <summary>
        /// 重定向
        /// </summary>
        public string Redirect { get; set; }

        /// <summary>
        /// 页面是否缓存
        /// </summary>
        public bool IsKeepAlive { get; set; }

        /// <summary>
        /// 页面是否固定，不能关闭
        /// </summary>
        public bool IsAffix { get; set; }

        /// <summary>
        /// 是否内嵌
        /// </summary>
        public bool IsIframe { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public List<ViewListDto>? Children { get; set; }
    }
}
