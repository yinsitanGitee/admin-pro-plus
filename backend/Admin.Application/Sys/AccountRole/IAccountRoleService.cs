﻿using Admin.Common.Dto;

namespace Admin.Application.Sys.AccountRole
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IAccountRoleService
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="roleIds"></param>
        /// <param name="isDelete"></param>
        /// <returns></returns>
        Task Submit(string accountId, List<string> roleIds, bool isDelete = false);

        /// <summary>
        /// 获取用户的角色
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<List<string>> GetAccountRole(InputDto dto);
    }
}
