﻿using Admin.Common.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Core;
using Admin.Core.Transactional;

namespace Admin.Application.Sys.AccountRole
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="accountRoleRepository"></param>
    public class AccountRoleService(IRepository<SysAccountRoleEntity, string> accountRoleRepository) : BaseService, IAccountRoleService
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="roleIds"></param>
        /// <param name="isDelete"></param>
        /// <returns></returns>
        [Transactional]
        public async Task Submit(string accountId, List<string> roleIds, bool isDelete = false)
        {
            if (isDelete)
                await accountRoleRepository.DeleteAsync(a => a.AccountId == accountId);

            if (roleIds != null && roleIds.Count > 0)
                await accountRoleRepository.InsertAsync(roleIds.Select(a => new SysAccountRoleEntity { AccountId = accountId, RoleId = a }).ToList());
        }

        /// <summary>
        /// 获取用户的角色
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<string>> GetAccountRole(InputDto dto)
        {
            return await accountRoleRepository.Where(a => a.AccountId == dto.Id).ToListAsync(a => a.RoleId);
        }
    }
}
