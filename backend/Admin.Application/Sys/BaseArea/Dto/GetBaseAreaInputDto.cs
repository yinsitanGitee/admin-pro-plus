﻿namespace Admin.Application.Sys.BaseArea.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetBaseAreaInputDto
    {
        public string ParentId { get; set; }
    }
}
