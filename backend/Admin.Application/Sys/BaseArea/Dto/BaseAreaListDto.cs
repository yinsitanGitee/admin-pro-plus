﻿using Admin.Repository.Dto;

namespace Admin.Application.Sys.BaseArea.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class BaseAreaListDto : BaseDto
    {
        /// <summary>
        /// 父节点编码
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 地域编码
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// 地域名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 快记符
        /// </summary>
        public string QuickQuery { get; set; }

        /// <summary>
        /// 层级
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string SortCode { get; set; }

        /// <summary>
        /// DataId
        /// </summary>
        public string DataId { get; set; }

        /// <summary>
        /// Lon
        /// </summary>
        public decimal Lon { get; set; }

        /// <summary>
        /// Lat
        /// </summary>
        public decimal Lat { get; set; }

        /// <summary>
        /// 附加说明
        /// </summary>
        public string Description { get; set; }

    }
}
