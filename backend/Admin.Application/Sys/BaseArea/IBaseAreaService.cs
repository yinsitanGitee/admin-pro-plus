﻿using Admin.Application.Sys.BaseArea.Dto;
using Admin.Common.BusinessResult;

namespace Admin.Application.Sys.BaseArea
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IBaseAreaService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<List<BaseAreaListDto>>> Query(GetBaseAreaInputDto dto);

    }
}
