﻿using Admin.Application.Sys.BaseArea.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Core;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;

namespace Admin.Application.Sys.BaseArea
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="baseAreaRepository"></param>
    public class BaseAreaService(IRepository<SysAreaEntity, string> baseAreaRepository) : BaseService, IBaseAreaService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<List<BaseAreaListDto>>> Query(GetBaseAreaInputDto dto)
        {
            var key = RedisKeyDto.CityKey + dto.ParentId;
            var cache = await RedisManage.GetAsync<List<BaseAreaListDto>>(key);
            if (cache != null) return BusinessResponse<List<BaseAreaListDto>>.Success(data: cache);
            cache = await baseAreaRepository.Where(t => t.ParentId == dto.ParentId).ToListAsync<BaseAreaListDto>();
            await RedisManage.SetAsync(key, cache);
            return BusinessResponse<List<BaseAreaListDto>>.Success(data: cache);
        }
    }
}
