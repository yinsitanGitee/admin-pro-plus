﻿using Admin.Application.Sys.ApiLog.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Sys.ApiLog
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IApiLogService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<ApiLogListDto>> Query(GetApiLogInputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        Task Add(string body);
    }
}
