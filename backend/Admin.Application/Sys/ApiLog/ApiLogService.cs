﻿using Admin.Application.Sys.ApiLog.Dto;
using Admin.Common.Json;
using Admin.Core;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;

namespace Admin.Application.Sys.ApiLog
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysApiLogRepository"></param>
    public class ApiLogService(IRepository<SysApiLogEntity, string> sysApiLogRepository) : BaseService, IApiLogService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<ApiLogListDto>> Query(GetApiLogInputDto dto)
        {
            var r = sysApiLogRepository.Select.From<SysAccountEntity>()
                .LeftJoin(a => a.t1.RequestUserId == a.t2.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.Api), t => t.t1.Api.Contains(dto.Api));

            return new CommonPageOutputDto<ApiLogListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.t1.RequestTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new ApiLogListDto
                {
                    Account = a.t2.Account,
                    Name = a.t2.Name
                })
            };
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public async Task Add(string body)
        {
            var dto = JsonHelper.DeserializeObject<ApiLogEditDto>(body);
            if (dto != null)
            {
                var entity = MapperObject.Map<SysApiLogEntity>(dto);
                await sysApiLogRepository.InsertAsync(entity);
            }
        }
    }
}
