﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.ApiLog.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class ApiLogListDto : BaseDto
    {
        /// <summary>
        /// 接口地址
        /// </summary>
        public string Api { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string Request { get; set; }

        /// <summary>
        /// 响应参数
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 请求人
        /// </summary>
        public string RequestUserId { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public SuccessStateEnum Status { get; set; }
    }
}
