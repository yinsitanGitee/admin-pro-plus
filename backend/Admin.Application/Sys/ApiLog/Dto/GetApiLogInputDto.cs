﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.ApiLog.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetApiLogInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 接口地址
        /// </summary>
        public string Api { get; set; }
    }
}
