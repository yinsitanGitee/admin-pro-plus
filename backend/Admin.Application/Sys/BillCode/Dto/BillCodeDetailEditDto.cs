﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.BillCode.Dto
{
    /// <summary>
    /// 定义编辑Dto
    /// </summary>
    public class BillCodeDetailEditDto : BaseDto
    {
        /// <summary>
        /// 主表id
        /// </summary>
        public string BillId { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public BillCodeDetailTypeEnum Type { get; set; }

        /// <summary>
        /// 规则
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
