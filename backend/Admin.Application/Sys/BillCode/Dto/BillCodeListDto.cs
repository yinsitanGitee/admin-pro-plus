﻿using Admin.Repository.Dto;

namespace Admin.Application.Sys.BillCode.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class BillCodeListDto : BaseDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 流水长度
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// 当前下标
        /// </summary>
        public long CurrentNum { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}