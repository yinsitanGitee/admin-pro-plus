﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.BillCode.Dto
{
    /// <summary>
    /// 定义查询输入dto
    /// </summary>
    public class GetBillCodeInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}