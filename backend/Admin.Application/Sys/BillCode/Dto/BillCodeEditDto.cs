﻿using Admin.Repository.Dto;

namespace Admin.Application.Sys.BillCode.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class BillCodeEditDto : BaseDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 流水长度
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// 当前下标
        /// </summary>
        public long CurrentNum { get; set; }

        /// <summary>
        /// 初始值
        /// </summary>
        public string SeedValue { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 单据编号配置明细
        /// </summary>
        public List<BillCodeDetailEditDto> Details { get; set; }
    }
}
