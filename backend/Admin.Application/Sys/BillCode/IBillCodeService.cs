﻿using Admin.Application.Sys.BillCode.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;

namespace Admin.Application.Sys.BillCode
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IBillCodeService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<BillCodeListDto>> Query(GetBillCodeInputDto dto);

        /// <summary>
        /// 获取明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<List<SysBillCodeDetailEntity>?>> GetDetail(InputDto dto);

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<BillCodeEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(BillCodeEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto">    </param>
        /// <returns></returns>
        Task<BusinessResult> Edit(BillCodeEditDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);
    }
}
