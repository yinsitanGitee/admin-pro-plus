﻿using Admin.Application.Sys.BillCode.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core;
using Admin.Core.Auth;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;
using Mapster;

namespace Admin.Application.Sys.BillCode
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="billCodeRepository"></param>
    /// <param name="billCodeDetailRepository"></param>
    public class BillCodeService(IRepository<SysBillCodeEntity, string> billCodeRepository, Lazy<IRepository<SysBillCodeDetailEntity, string>> billCodeDetailRepository) : BaseService, IBillCodeService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<BillCodeListDto>> Query(GetBillCodeInputDto dto)
        {
            var a = IocManager.Instance.GetService<ICurrentUser>();
            var r = billCodeRepository.Select
                 .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.Name.Contains(dto.Name));

            return new CommonPageOutputDto<BillCodeListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new BillCodeListDto())
            };
        }

        /// <summary>
        /// 获取明细
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<List<SysBillCodeDetailEntity>?>> GetDetail(InputDto dto)
        {
            return BusinessResponse<List<SysBillCodeDetailEntity>?>.Success(await IocManager.Instance.GetService<ICacheService>().GetBillCodeDetails(dto.Id));
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<BillCodeEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<BillCodeEditDto>.Success(await billCodeRepository.GetAsync<BillCodeEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(BillCodeEditDto dto)
        {
            var error = Ver(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = MapperObject.Map<SysBillCodeEntity>(dto);
            await billCodeRepository.InsertAsync(entity);

            if (dto.Details != null && dto.Details.Count > 0)
            {
                var eds = dto.Details.Adapt<List<SysBillCodeDetailEntity>>();
                foreach (var ed in eds)
                {
                    ed.Id = Guid.NewGuid().ToString();
                    ed.BillId = entity.Id;
                }
                await billCodeDetailRepository.Value.InsertAsync(eds);
            }

            return BusinessResponse.Success();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private string? Ver(BillCodeEditDto dto)
        {
            if (dto.Length > 10) return "流水长度不能超过10";
            for (int i = 0; i < dto.Length; i++)
                dto.SeedValue += "0";

            return null;
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto">    </param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(BillCodeEditDto dto)
        {
            var error = Ver(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = await billCodeRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await billCodeRepository.UpdateAsync(entity);
                await billCodeDetailRepository.Value.DeleteAsync(a => a.BillId == dto.Id);
                if (dto.Details != null && dto.Details.Count > 0)
                {
                    var eds = dto.Details.Adapt<List<SysBillCodeDetailEntity>>();
                    foreach (var ed in eds)
                    {
                        ed.Id = Guid.NewGuid().ToString();
                        ed.BillId = entity.Id;
                    }
                    await billCodeDetailRepository.Value.InsertAsync(eds);
                }
                await RedisManage.DelAsync(RedisKeyDto.BillCodeKey + dto.Id);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await billCodeRepository.SoftDeleteAsync(t => dto.Ids.Contains(t.Id));
            await billCodeDetailRepository.Value.SoftDeleteAsync(t => dto.Ids.Contains(t.BillId));
            await RedisManage.DelAsync(dto.Ids.Select(a => RedisKeyDto.BillCodeKey + a).ToArray());
            return BusinessResponse.Success();
        }
    }
}
