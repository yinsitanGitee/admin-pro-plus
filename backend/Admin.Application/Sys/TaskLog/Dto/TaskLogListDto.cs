﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.TaskLog.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class TaskLogListDto : BaseDto
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public string TaskId { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务编号
        /// </summary>
        public string TaskCode { get; set; }

        /// <summary>
        /// 响应数据
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public SuccessStateEnum Status { get; set; }

        /// <summary>
        /// 请求头信息
        /// </summary>
        public string RequestHeader { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestBody { get; set; }
    }
}
