﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.TaskLog.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetTaskLogInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public string TaskId { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestBody { get; set; }

        /// <summary>
        /// 请求头
        /// </summary>
        public string RequestHeader { get; set; }
    }
}
