﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.TaskLog.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class TaskLogEditDto : BaseDto
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public string TaskId { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// 执行时长
        /// </summary>
        public string ExecuteTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public SuccessStateEnum Status { get; set; }

        /// <summary>
        /// 请求头信息
        /// </summary>
        public string RequestHeader { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestBody { get; set; }
    }
}
