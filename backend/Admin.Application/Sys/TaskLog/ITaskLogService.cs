﻿using Admin.Application.Sys.TaskLog.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Sys.TaskLog
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface ITaskLogService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<TaskLogListDto>> Query(GetTaskLogInputDto dto);
    }
}
