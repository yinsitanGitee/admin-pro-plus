﻿using Admin.Application.Sys.TaskLog.Dto;
using Admin.Core;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;

namespace Admin.Application.Sys.TaskLog
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysTaskLogRepository"></param>
    public class TaskLogService(IRepository<SysTaskLogEntity, string> sysTaskLogRepository) : BaseService, ITaskLogService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<TaskLogListDto>> Query(GetTaskLogInputDto dto)
        {
            var r = sysTaskLogRepository.Select.From<SysTaskSchedulingEntity>()
                .LeftJoin(a => a.t1.TaskId == a.t2.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.TaskId), t => t.t1.TaskId == dto.TaskId)
                .WhereIf(!string.IsNullOrEmpty(dto.RequestBody), t => t.t1.RequestBody.Contains(dto.RequestBody))
                .WhereIf(!string.IsNullOrEmpty(dto.RequestHeader), t => t.t1.RequestHeader.Contains(dto.RequestHeader));

            return new CommonPageOutputDto<TaskLogListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.t1.RequestTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new TaskLogListDto
                {
                    TaskName = a.t2.TaskName,
                    TaskCode = a.t2.Code
                })
            };
        }
    }
}
