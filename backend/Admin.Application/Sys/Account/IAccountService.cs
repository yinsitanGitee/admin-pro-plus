﻿using Admin.Application.Auth.Dto;
using Admin.Application.Sys.Account.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;

namespace Admin.Application.Sys.Account
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<AccountListDto>> Query(GetAccountInputDto dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<List<AccountListDto>> QueryAll(GetAccountInputDto dto);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<AccountEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(AccountEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(AccountEditDto dto);

        /// <summary>
        /// 修改头像
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> EditAccountImage(AccountEditImageDto dto);

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(InputDto dto);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult?> ChangePassword(ChangePwdDto dto);

        /// <summary>
        /// 修改基础信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> EditSimpleInfo(AccountSimgpleDto dto);

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        Task<UserPermissionOutputDto?> GetCurrentUserInfo();

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        Task<UserInfoDto?> GetCurrentUser(string? userId = null);
    }
}
