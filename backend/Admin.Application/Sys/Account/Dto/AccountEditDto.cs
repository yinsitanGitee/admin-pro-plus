﻿using Admin.Repository.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.Account.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class AccountEditDto : BaseDto
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 账户和
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPic { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 部门id
        /// </summary>
        public string DepartmentId { get; set; }     
        
        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public List<string> Role { get; set; }
    }

    /// <summary>
    /// 定义AccountEditImageDto
    /// </summary>
    public class AccountEditImageDto
    {
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPic { get; set; }
    }

    /// <summary>
    /// 定义AccountSimgpleDto
    /// </summary>
    public class AccountSimgpleDto
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }
    }
}
