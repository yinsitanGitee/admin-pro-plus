﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.Account.Dto
{
    /// <summary>
    /// 定义查询dto
    /// </summary>
    public class GetAccountInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        ///     
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DepartmentId { get; set; }
    }
}
