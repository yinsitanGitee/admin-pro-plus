﻿using Admin.Application.Auth.Dto;
using Admin.Application.Sys.View.Dto;

namespace Admin.Application.Sys.Account.Dto
{
    /// <summary>
    /// 定义用户权限返回dto
    /// </summary>
    public class UserPermissionOutputDto
    {
        /// <summary>
        /// 用户信息
        /// </summary>
        public UserInfoDto UserInfo { get; set; }

        /// <summary>
        /// 拥有的权限编码
        /// </summary>
        public List<string> PermissionCodes { get; set; }

        /// <summary>
        /// 菜单信息
        /// </summary>
        public List<ViewListDto>? Menus { get; set; }
    }
}
