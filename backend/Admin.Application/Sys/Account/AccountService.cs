﻿using Mapster;
using Admin.Application.Auth.Dto;
using Admin.Application.Sys.Account.Dto;
using Admin.Application.Sys.AccountRole;
using Admin.Application.Sys.Permission;
using Admin.Common.Common.RegularHelper;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Page;
using Admin.Cache;
using Admin.Core;
using Admin.Core.Auth;
using Admin.Core.Helper;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Core.Transactional;
using Admin.Common.Ioc;
using Admin.Common.Security;
using Admin.Common.Dto;
using Admin.Common.BusinessResult;

namespace Admin.Application.Sys.Account
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysAccountRepository"></param>
    /// <param name="sysAccountRoleRepository"></param>
    public class AccountService(IRepository<SysAccountEntity, string> sysAccountRepository, Lazy<IRepository<SysAccountRoleEntity, string>> sysAccountRoleRepository) : BaseService, IAccountService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<AccountListDto>> Query(GetAccountInputDto dto)
        {
            var r = sysAccountRepository.Select.From<SysDepartmentEntity, SysOrganizeEntity>()
                .LeftJoin(a => a.t1.DepartmentId == a.t2.Id)
                .LeftJoin(a => a.t1.OrganizeId == a.t3.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.OrganizeId), t => t.t1.OrganizeId == dto.OrganizeId)
                .WhereIf(!string.IsNullOrEmpty(dto.DepartmentId), t => t.t1.DepartmentId == dto.DepartmentId)
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.t1.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.Phone), t => t.t1.Phone.Contains(dto.Phone));

            return new CommonPageOutputDto<AccountListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.t1.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync(a => new AccountListDto { DepartmentName = a.t2.Name, OrganizeName = a.t3.Name })
            };
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<AccountListDto>> QueryAll(GetAccountInputDto dto)
        {
            return await sysAccountRepository.Select.From<SysDepartmentEntity, SysOrganizeEntity>()
                .LeftJoin(a => a.t1.DepartmentId == a.t2.Id)
                .LeftJoin(a => a.t1.OrganizeId == a.t3.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.Name), t => t.t1.Name.Contains(dto.Name))
                .WhereIf(!string.IsNullOrEmpty(dto.Phone), t => t.t1.Phone.Contains(dto.Phone)).
                 OrderByDescending(x => x.t1.CreateDateTime)
                .ToListAsync(a => new AccountListDto { DepartmentName = a.t2.Name, OrganizeName = a.t3.Name });
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<AccountEditDto>> Get(InputDto dto)
        {
            var e = await sysAccountRepository.GetAsync<AccountEditDto>(a => a.Id == dto.Id);
            if (e == null) return BusinessResponse<AccountEditDto>.Error(msg: "数据不存在或已被删除，请刷新重试");
            e.Role = await sysAccountRoleRepository.Value.Where(a => a.AccountId == dto.Id).ToListAsync(a => a.RoleId);
            return BusinessResponse<AccountEditDto>.Success(data: e);
        }

        /// <summary>
        /// 验证用户有效性
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private async Task<string?> ExistAccount(AccountEditDto dto)
        {
            if (await sysAccountRepository.Select.AnyAsync(a => a.Id != dto.Id && a.Phone == dto.Phone))
                return "手机号重复";

            return null;
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(AccountEditDto dto)
        {
            var res = await ExistAccount(dto);
            if (!string.IsNullOrWhiteSpace(res)) return BusinessResponse.Error(msg: res);
            res = SysHelper.ExistPassword(dto.Password);
            if (!string.IsNullOrWhiteSpace(res)) return BusinessResponse.Error(msg: res);
            var entity = MapperObject.Map<SysAccountEntity>(dto);
            entity.Password = SysHelper.GetPassword(dto.Password);
            entity.Id = Guid.NewGuid().ToString("N");
            await sysAccountRepository.InsertAsync(entity);
            await IocManager.Instance.GetService<IAccountRoleService>().Submit(entity.Id, dto.Role);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(AccountEditDto dto)
        {
            var res = await ExistAccount(dto);
            if (!string.IsNullOrWhiteSpace(res)) return BusinessResponse.Error(msg: res);
            var entity = await sysAccountRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在,请刷新重试");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await sysAccountRepository.UpdateAsync(entity, a => a.Password);
                await IocManager.Instance.GetService<IAccountRoleService>().Submit(entity.Id, dto.Role, true);
                await RedisManage.DelAsync(RedisKeyDto.UserKey + entity.Id);
                await RedisManage.DelAsync(RedisKeyDto.UserPermissionsKey + entity.Id);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 修改头像
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> EditAccountImage(AccountEditImageDto dto)
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            await sysAccountRepository.UpdateAsync(a => new SysAccountEntity { HeadPic = dto.HeadPic }, a => a.Id == userId);
            await RedisManage.DelAsync(RedisKeyDto.UserKey + userId);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据删除
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Transactional]
        public async Task<BusinessResult> Delete(InputDto dto)
        {
            await sysAccountRepository.SoftDeleteAsync(t => dto.Ids.Contains(t.Id));
            await sysAccountRoleRepository.Value.SoftDeleteAsync(t => dto.Ids.Contains(t.AccountId));
            await RedisManage.DelAsync(dto.Ids.Select(a => RedisKeyDto.UserKey + a).ToArray());
            await RedisManage.DelAsync(dto.Ids.Select(a => RedisKeyDto.UserPermissionsKey + a).ToArray());
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult?> ChangePassword(ChangePwdDto dto)
        {
            var result = SysHelper.ExistPassword(dto.Password);
            if (string.IsNullOrWhiteSpace(result))
            {
                await sysAccountRepository.UpdateAsync(a => new SysAccountEntity { Password = SysHelper.GetPassword(dto.Password) }, a => dto.Ids.Contains(a.Id));
                return BusinessResponse.Success();
            }
            else return BusinessResponse.Error(msg: result);
        }

        /// <summary>
        /// 修改基础信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> EditSimpleInfo(AccountSimgpleDto dto)
        {
            var account = await IocManager.Instance.GetService<ICacheService>().GetUser();
            if (account != null)
            {
                account = MapperObject.Map(dto, account);
                await sysAccountRepository.UpdateAsync(account);
                await RedisManage.DelAsync(RedisKeyDto.UserKey + account.Id);
                return BusinessResponse.Success();
            }
            else return BusinessResponse.Error(msg: "系统繁忙，请稍后重试");
        }

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        public async Task<UserPermissionOutputDto?> GetCurrentUserInfo()
        {
            var userId = IocManager.Instance.GetService<ICurrentUser>().UserId;
            var user = await GetCurrentUser(userId);
            if (user == null) return null;
            var ps = await IocManager.Instance.GetService<ICacheService>().GetUserPermissionCache(userId);
            return new UserPermissionOutputDto
            {
                UserInfo = user.Adapt<UserInfoDto>(),
                Menus = await IocManager.Instance.GetService<IPermissionService>().GetAuthMenus([.. ps.Views, .. ps.HalfViews]),
                PermissionCodes = ps.Views
            };
        }

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        public async Task<UserInfoDto?> GetCurrentUser(string? userId = null)
        {
            userId ??= IocManager.Instance.GetService<ICurrentUser>().UserId;
            var user = await sysAccountRepository.Select.From<SysDepartmentEntity>().DisableGlobalFilter(FilterSetting.DataAuth).LeftJoin(a => a.t1.DepartmentId == a.t2.Id).Where(a => a.t1.Id == userId).ToOneAsync(a => new UserInfoDto
            {
                DepartmentName = a.t2.Name
            });
            if (user == null) return null;
            user.Phone = RegularHelper.PhoneParse(user.Phone, 1);
            return user.Adapt<UserInfoDto>();
        }
    }
}
