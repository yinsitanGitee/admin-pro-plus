﻿using Admin.Application.Sys.Organize.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core;
using Admin.Core.Helper;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Mapster;

namespace Admin.Application.Sys.Organize
{
    /// <summary>
    /// 机构服务
    /// </summary>
    /// <param name="organizeRepository"></param>
    public class OrganizeService(IRepository<SysOrganizeEntity, string> organizeRepository) : BaseService, IOrganizeService
    {
        /// <summary>
        /// 获取机构树
        /// </summary>
        /// <returns></returns>
        public async Task<List<TreeNode>?> GetDepartmentTree()
        {
            var cacheService = IocManager.Instance.GetService<ICacheService>();
            var orgs = MapperObject.Map<List<TreeNode>>(await cacheService.GetOrganizeCache() ?? []);
            var departs = MapperObject.Map<List<TreeNode>>(await cacheService.GetDepartmentCache() ?? []);
            if (orgs != null)
            {
                var proIds = new List<string>();

                if (departs != null)
                {
                    foreach (var item in departs)
                    {
                        if (item.ParentId == "0") item.ParentId = item.Attr1;
                        if (orgs.Any(a => a.Id == item.Attr1))
                            orgs.Add(item);
                    }
                }

                List<TreeNode> data = [];
                foreach (var item in orgs)
                {
                    if (!proIds.Any(a => a == item.Id))
                    {
                        item.Children = TreeNode.ForeachListDto(orgs, proIds, item.Id) ?? [];
                        data.Add(item);
                    }
                }

                return data;
            }

            return null;
        }

        /// <summary>
        /// 获取机构部门树形数据
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonOrgDepartDto>?> GetOrgDeparts()
        {
            var cacheService = IocManager.Instance.GetService<ICacheService>();
            var orgs = (await cacheService.GetOrganizeCache())?.Adapt<List<CommonOrgDepartDto>>();
            var departs = (await cacheService.GetDepartmentCache())?.Adapt<List<CommonOrgDepartDto>>();
            if (orgs != null)
            {
                var proIds = new List<string>();

                orgs.ForEach(a => a.Type = 1);
                if (departs != null)
                {
                    foreach (var item in departs)
                    {
                        item.Type = 2;
                        if (item.ParentId == "0") item.ParentId = item.OrganizeId;
                        if (orgs.Any(a => a.Id == item.OrganizeId))
                            orgs.Add(item);
                    }
                }

                List<CommonOrgDepartDto> data = [];
                foreach (var item in orgs)
                {
                    if (!proIds.Any(a => a == item.Id))
                    {
                        item.Children = ForeachOrgDepartListDto(orgs, proIds, item.Id) ?? [];
                        data.Add(item);
                    }
                }

                return data;
            }

            return null;
        }

        /// <summary>
        /// 递归获取机构
        /// </summary>
        /// <param name="list"></param>
        /// <param name="proIds"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        private List<CommonOrgDepartDto>? ForeachOrgDepartListDto(List<CommonOrgDepartDto>? list, List<string>? proIds, string pid)
        {
            if (list != null && list.Count > 0)
            {
                var a = list.FindAll(p => p.ParentId == pid);
                if (a.Count > 0)
                {
                    List<CommonOrgDepartDto> ds = [];
                    foreach (var item in a)
                    {
                        item.Children = ForeachOrgDepartListDto(list, proIds, item.Id);
                        ds.Add(item);

                        proIds?.Add(item.Id);
                    }
                    return ds;
                }
            }

            return null;
        }

        /// <summary>
        /// 获取所有机构，无递归
        /// </summary>
        /// <returns></returns>
        public async Task<List<OrganizeListDto>?> GetOrganizes(List<string>? organizeIds = null, Func<OrganizeListDto, bool>? where = null)
        {
            var data = await IocManager.Instance.GetService<ICacheService>().GetOrganizeCache();
            if (where == null)
            {
                if (organizeIds != null)
                    return data?.Where(a => organizeIds.Contains(a.Id)).ToList();

                return data;
            }
            else
            {
                if (organizeIds != null)
                    return data?.Where(a => organizeIds.Contains(a.Id)).Where(where).ToList();

                return data?.Where(where).ToList();
            }
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<OrganizeEditDto?>> Get(InputDto dto)
        {
            var data = await organizeRepository.GetAsync<OrganizeEditDto>(a => a.Id == dto.Id);
            return BusinessResponse<OrganizeEditDto?>.Success(data: data);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public async Task<List<OrganizeListDto>?> Query(GetOrganizeInputDto input)
        {
            var proIds = new List<string>();
            var orgs = await GetOrganizes(null, input.State.HasValue ? a => a.State == input.State : null);

            if (orgs != null && orgs.Count > 0)
            {
                List<OrganizeListDto> data = [];
                foreach (var item in orgs)
                {
                    if (!proIds.Any(a => a == item.Id))
                    {
                        item.Children = ForeachOrganizeListDto(orgs, proIds, item.Id) ?? [];
                        data.Add(item);
                    }
                }

                return data;
            }

            else return [];
        }

        /// <summary>
        /// 递归获取机构
        /// </summary>
        /// <param name="list"></param>
        /// <param name="proIds"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        private List<OrganizeListDto>? ForeachOrganizeListDto(List<OrganizeListDto>? list, List<string> proIds, string pid)
        {
            if (list != null && list.Count > 0)
            {
                var a = list.FindAll(p => p.ParentId == pid);
                if (a.Count > 0)
                {
                    List<OrganizeListDto> organizes = [];
                    foreach (var item in a)
                    {
                        item.Children = ForeachOrganizeListDto(list, proIds, item.Id);
                        organizes.Add(item);
                        proIds.Add(item.Id);
                    }
                    return organizes;
                }
            }

            return null;
        }

        /// <summary>
        /// 验证机构有效性
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private async Task<string?> ExistOrganize(OrganizeEditDto dto)
        {
            if (await organizeRepository.Select.AnyAsync(a => a.Id != dto.Id && a.Name == dto.Name))
                return "机构名称重复";

            return null;
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(OrganizeEditDto dto)
        {
            var error = await ExistOrganize(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = MapperObject.Map<SysOrganizeEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            await organizeRepository.InsertAsync(entity);
            await RedisManage.DelAllAsync(RedisKeyDto.OrganizeKey);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(OrganizeEditDto dto)
        {
            var error = await ExistOrganize(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = await organizeRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null) return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await organizeRepository.UpdateAsync(entity);
                await RedisManage.DelAllAsync(RedisKeyDto.OrganizeKey);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(string id)
        {
            if (await organizeRepository.Select.AnyAsync(a => a.ParentId == id)) return BusinessResponse.Error(msg: "请先删除下级机构");
            await organizeRepository.SoftDeleteAsync(a => a.Id == id);
            await RedisManage.DelAllAsync(RedisKeyDto.OrganizeKey);
            return BusinessResponse.Success();
        }
    }
}
