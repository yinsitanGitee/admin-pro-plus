﻿using Admin.Application.Sys.Organize.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Helper;
using Admin.Core.Service.Cache.Dto;

namespace Admin.Application.Sys.Organize
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IOrganizeService
    {
        /// <summary>
        /// 获取机构树
        /// V9使用
        /// </summary>
        /// <returns></returns>
        Task<List<TreeNode>?> GetDepartmentTree();

        /// <summary>
        /// 获取机构部门树形数据
        /// </summary>
        /// <returns></returns>
        Task<List<CommonOrgDepartDto>?> GetOrgDeparts();

        /// <summary>
        /// 获取所有机构，无递归
        /// </summary>
        /// <returns></returns>
        Task<List<OrganizeListDto>?> GetOrganizes(List<string>? organizeIds = null, Func<OrganizeListDto, bool>? where = null);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<OrganizeEditDto?>> Get(InputDto dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        Task<List<OrganizeListDto>?> Query(GetOrganizeInputDto input);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(OrganizeEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(OrganizeEditDto dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(string id);
    }
}
