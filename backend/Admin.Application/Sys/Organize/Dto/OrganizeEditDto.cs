﻿using Admin.Common.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.Organize.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class OrganizeEditDto : InputDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnumBool State { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public string Remark { get; set; }
    }
}
