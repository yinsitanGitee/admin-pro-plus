﻿using Mapster;
using Admin.Core.Helper;
using Admin.Core.Service.Cache.Dto;

namespace Admin.Application.Sys.Organize.Dto.Mapper
{
    /// <summary>
    /// 定义映射
    /// </summary>
    public class PubAccountMapper : IRegister
    {
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="config"></param>
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<OrganizeListDto, TreeNode>()
                .Map(dest => dest.IconSkin, src => "fa i-Organize")
                .Map(dest => dest.Type, src => "1")
                .Map(dest => dest.Code, src => src.Name);

            config.ForType<DepartmentListDto, TreeNode>()
                .Map(dest => dest.IconSkin, src => "fa i-Department")
                .Map(dest => dest.Attr1, src => src.OrganizeId)
                .Map(dest => dest.Type, src => "2")
                .Map(dest => dest.Code, src => src.Name);
        }
    }
}