﻿using Admin.Repository.Enum;

namespace Admin.Application.Sys.Organize.Dto
{
    /// <summary>
    /// 定义获取输入dto
    /// </summary>
    public class GetOrganizeInputDto
    {
        /// <summary>
        /// 状态
        /// </summary>
        public EnumBool? State { get; set; }
    }
}
