﻿using Admin.Application.Sys.Permission.Dto;
using Admin.Application.Sys.View;
using Admin.Application.Sys.View.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.Permission
{
    /// <summary>
    /// 权限服务
    /// </summary>
    /// <param name="freesql"></param>
    public class PermissionService(IFreeSql freesql) : BaseService, IPermissionService
    {
        /// <summary>
        /// 获取应用授权菜单
        /// </summary>
        /// <param name="codes"></param>
        /// <returns></returns>
        public async Task<List<ViewListDto>?> GetAuthMenus(List<string>? codes)
        {
            if (codes != null && codes.Count > 0)
            {
                var service = IocManager.Instance.GetService<IViewService>();
                var allViews = await service.GetViewCache<ViewListDto>();

                if (allViews != null)
                    return service.ForeachMenu(allViews.Where(a => codes.Contains(a.Code) && a.Type != ViewTypeEnum.Permission).ToList(), "0");
            }

            return null;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public async Task<PermissionListOutputDto> Query(GetPermissionInputDto dto)
        {
            var cacheService = IocManager.Instance.GetService<ICacheService>();
            var result = new PermissionListOutputDto();
            if (!string.IsNullOrWhiteSpace(dto.Id))
            {
                string view = string.Empty;
                string halfView = string.Empty;
                switch (dto.Type)
                {
                    case 1:
                        var u = await cacheService.GetUser(dto.Id);
                        if (u != null)
                        {
                            halfView = u.HalfViews;
                            view = u.Views;
                        }
                        break;
                    case 2:
                        var r = await freesql.Select<SysRoleEntity>().Where(a => a.Id == dto.Id).ToOneAsync();
                        if (r != null)
                        {
                            halfView = r.HalfViews;
                            view = r.Views;
                        }
                        break;
                }

                result.HalfViews = halfView;
                result.Views = view;
            }

            var service = IocManager.Instance.GetService<IViewService>();
            var allViews = await service.GetViewCache<ViewListDto>();

            result.AllViews = service.ForeachMenu(allViews, "0");

            return result;
        }

        /// <summary>
        /// 保存授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> RoleSubmit(PermissionEditDto dto)
        {
            await freesql.Update<SysRoleEntity>().SetDto(new { dto.Views, dto.HalfViews }).Where(a => dto.Ids.Contains(a.Id)).ExecuteAffrowsAsync();
            var relas = await freesql.Select<SysAccountRoleEntity>().Where(a => dto.Ids.Contains(a.RoleId)).ToListAsync(a => a.AccountId);
            List<string> userKeys = [];
            List<string> permissKeys = [];
            foreach (var item in relas.Distinct())
            {
                userKeys.Add(RedisKeyDto.UserKey + item);
                permissKeys.Add(RedisKeyDto.UserPermissionsKey + item);
            }
            await RedisManage.DelAsync([.. userKeys]);
            await RedisManage.DelAsync([.. permissKeys]);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 保存授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> AccountSubmit(PermissionEditDto dto)
        {
            await freesql.Update<SysAccountEntity>().SetDto(new { dto.Views, dto.HalfViews }).Where(a => dto.Ids.Contains(a.Id)).ExecuteAffrowsAsync();
            List<string> userKeys = [];
            List<string> permissKeys = [];
            foreach (var item in dto.Ids)
            {
                userKeys.Add(RedisKeyDto.UserKey + item);
                permissKeys.Add(RedisKeyDto.UserPermissionsKey + item);
            }
            await RedisManage.DelAsync([.. userKeys]);
            await RedisManage.DelAsync([.. permissKeys]);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 获取数据权限
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<DataPermissionEditDto> GetDataAuth(InputDto dto)
        {
            return await freesql.Select<SysAccountEntity>().Where(a => a.Id == dto.Id).ToOneAsync(a => new DataPermissionEditDto
            {
                CustDataAuth = a.CustDataAuth,
                DataAuth = a.DataAuth,
            });
        }

        /// <summary>
        /// 数据权限保存
        /// </summary>
        /// <returns></returns>
        public async Task<BusinessResult> DataAuthSubmit(DataPermissionEditDto dto)
        {
            await freesql.Update<SysAccountEntity>().SetDto(new { dto.CustDataAuth, dto.DataAuth }).Where(a => dto.Ids.Contains(a.Id)).ExecuteAffrowsAsync();
            List<string> userKeys = [];
            List<string> organizeKeys = [];
            foreach (var item in dto.Ids)
            {
                userKeys.Add(RedisKeyDto.UserKey + item);
                organizeKeys.Add(RedisKeyDto.OrganizeKey + item);
            }

            await RedisManage.DelAsync([.. userKeys]);
            await RedisManage.DelAsync([.. organizeKeys]);
            return BusinessResponse.Success();
        }
    }
}
