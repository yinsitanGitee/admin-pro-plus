﻿using Admin.Application.Sys.Permission.Dto;
using Admin.Application.Sys.View.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;

namespace Admin.Application.Sys.Permission
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IPermissionService
    {
        /// <summary>
        /// 获取应用授权菜单
        /// </summary>
        /// <param name="codes"></param>
        /// <returns></returns>
        Task<List<ViewListDto>?> GetAuthMenus(List<string>? codes);

        /// <summary>
        /// 查询授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<PermissionListOutputDto> Query(GetPermissionInputDto dto);

        /// <summary>
        /// 保存授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> RoleSubmit(PermissionEditDto dto);

        /// <summary>
        /// 保存授权
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> AccountSubmit(PermissionEditDto dto);

        /// <summary>
        /// 获取数据权限
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<DataPermissionEditDto> GetDataAuth(InputDto dto);

        /// <summary>
        /// 数据权限保存
        /// </summary>
        /// <returns></returns>
        Task<BusinessResult> DataAuthSubmit(DataPermissionEditDto dto);
    }
}
