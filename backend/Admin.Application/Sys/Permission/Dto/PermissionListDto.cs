﻿using Admin.Application.Sys.View.Dto;
using Admin.Core.Service.Cache.Dto;

namespace Admin.Application.Sys.Permission.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class PermissionListOutputDto : AccountRolePermissionDto
    {
        /// <summary>
        /// 所有视图
        /// </summary>
        public List<ViewListDto>? AllViews { get; set; }
    }
}
