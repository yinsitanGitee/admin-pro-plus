﻿using Admin.Repository.Enum;

namespace Admin.Application.Sys.Permission.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class DataPermissionEditDto
    {
        /// <summary>
        /// 用户ids | 角色ids
        /// </summary>
        public List<string> Ids { get; set; }

        /// <summary>
        /// 数据权限
        /// </summary>
        public EnumDataAuth DataAuth { get; set; }

        /// <summary>
        /// 自定义权限
        /// </summary>
        public string CustDataAuth { get; set; }
    }
}
