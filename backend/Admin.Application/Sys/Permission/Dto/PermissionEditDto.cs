﻿using Admin.Core.Service.Cache.Dto;

namespace Admin.Application.Sys.Permission.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class PermissionEditDto : AccountRolePermissionDto
    {
        /// <summary>
        /// 用户ids | 角色ids
        /// </summary>
        public List<string> Ids { get; set; }
    }
}
