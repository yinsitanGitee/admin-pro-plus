﻿using Microsoft.AspNetCore.Http;

namespace Admin.Application.Sys.Permission.Dto
{
    /// <summary>
    /// 定义输入dto
    /// </summary>
    public class GetPermissionInputDto
    {
        /// <summary>
        /// 1 => 用户
        /// 2 => 角色
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
    }
}
