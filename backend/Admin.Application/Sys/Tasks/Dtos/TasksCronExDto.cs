﻿namespace Admin.Application.Sys.Tasks.Dtos
{
    /// <summary>
    /// 定义任务Cron dto
    /// </summary>
    public class TasksCronExDto
    {
        /// <summary>
        /// 间隔Cron表达式
        /// </summary>
        public string Interval { get; set; }
    }
}
