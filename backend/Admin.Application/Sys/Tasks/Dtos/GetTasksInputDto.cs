﻿using Admin.Repository.Page;

namespace Admin.Application.Sys.Tasks.Dtos
{
    /// <summary>
    /// 定义获取任务列表输入dto
    /// </summary>
    public class GetCommTasksInputDto : CommonPageInputDto
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
    }
}
