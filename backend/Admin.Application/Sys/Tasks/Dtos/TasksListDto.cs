﻿using Admin.Repository.Enum;

namespace Admin.Application.Sys.Tasks.Dtos
{
    /// <summary>
    /// 定义任务列表dto
    /// </summary>
    public class TasksListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public APITaskType Type { get; set; }

        /// <summary>
        /// 执行内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 间隔Cron表达式
        /// </summary>
        public string Interval { get; set; }

        /// <summary>
        /// RequestType
        /// </summary>
        public APIRequestTypeEnum RequestType { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public APIStatusEnum Status { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastRunTime { get; set; }

        /// <summary>
        /// 请求头信息
        /// </summary>
        public string RequestHeader { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestBody { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDateTime { get; set; }
    }
}
