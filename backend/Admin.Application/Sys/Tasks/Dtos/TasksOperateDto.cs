﻿using Admin.Repository.Enum;

namespace Admin.Application.Sys.Tasks.Dtos
{
    /// <summary>
    /// 定义任务操作dto
    /// </summary>
    public class TasksOperateDto
    {
        /// <summary>
        /// Ids
        /// </summary>
        public List<string> Ids { get; set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        public JobOperateEnum Operate { get; set; }
    }
}
