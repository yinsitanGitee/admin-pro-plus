﻿using Admin.Repository.Enum;

namespace Admin.Application.Sys.Tasks.Dtos
{
    /// <summary>
    /// 定义任务编辑dto
    /// </summary>
    public class TasksEditDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public APITaskType Type { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 间隔Cron表达式
        /// </summary>
        public string Interval { get; set; }

        /// <summary>
        /// RequestType
        /// </summary>
        public APIRequestTypeEnum RequestType { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 请求头信息
        /// </summary>
        public string RequestHeader { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestBody { get; set; }
    }
}
