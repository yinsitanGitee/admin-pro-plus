﻿using Admin.Application.Sys.Tasks.Dtos;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Repository.Page;
using Admin.Core.Helper;

namespace Admin.Application.Sys.Tasks
{
    /// <summary>
    /// 定义任务应用服务层接口
    /// </summary>
    public interface ITasksService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<CommonPageOutputDto<TasksListDto>> Query(GetCommTasksInputDto dto);

        /// <summary>
        /// 获取所有任务数据
        /// </summary>
        /// <returns></returns>
        Task<List<CommonTreeDto>> GetAll();

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<TasksEditDto>> Get(InputDto dto);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(TasksEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(TasksEditDto dto);

        /// <summary>
        /// 操作
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Operate(TasksOperateDto dto);
    }
}
