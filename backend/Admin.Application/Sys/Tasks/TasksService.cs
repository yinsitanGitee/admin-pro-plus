﻿using Admin.Application.Sys.Tasks.Dtos;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Core.Helper;
using Admin.Core.Tasks;
using Admin.Repository;
using Admin.Repository.Entities.Sys;
using Admin.Repository.Enum;
using Admin.Repository.Page;
using Quartz;

namespace Admin.Application.Sys.Tasks
{
    /// <summary>
    /// 定义任务应用服务层实现
    /// </summary>
    /// <param name="schedulerFactory"></param>
    /// <param name="taskSchedulingRepository"></param>
    public class TasksService(ISchedulerFactory schedulerFactory, IRepository<SysTaskSchedulingEntity, string> taskSchedulingRepository) : BaseService, ITasksService
    {
        /// <summary>
        /// 验证任务
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private async Task<string> VerTaskEntity(TasksEditDto dto)
        {
            if (await taskSchedulingRepository.Select.AnyAsync(a => a.Code == dto.Code && a.Id != dto.Id))
                return "任务编号重复";

            (bool isSuccess, string message) = dto.Interval.IsValidExpression();
            if (!isSuccess) return message;

            return string.Empty;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<CommonPageOutputDto<TasksListDto>> Query(GetCommTasksInputDto dto)
        {
            var r = taskSchedulingRepository.Select
                .WhereIf(!string.IsNullOrWhiteSpace(dto.TaskName), a => dto.TaskName.Contains(a.TaskName))
                .WhereIf(!string.IsNullOrWhiteSpace(dto.Content), a => dto.Content.Contains(a.Content))
                .WhereIf(!string.IsNullOrWhiteSpace(dto.Code), a => dto.TaskName.Contains(a.Code));

            return new CommonPageOutputDto<TasksListDto>(dto.CurPage, dto.PageSize)
            {
                Total = await r.CountAsync(),
                Info = await r.OrderByDescending(x => x.CreateDateTime).Page(dto.CurPage, dto.PageSize)
                .ToListAsync<TasksListDto>()
            };
        }

        /// <summary>
        /// 获取所有任务数据
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonTreeDto>> GetAll()
        {
            return await taskSchedulingRepository.Select.ToListAsync(a => new CommonTreeDto { Label = a.TaskName, Attr1 = a.Code, Attr2 = a.Content });
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<TasksEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<TasksEditDto>.Success(
                await taskSchedulingRepository.GetAsync<TasksEditDto>(a => a.Id == dto.Id)
                );
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(TasksEditDto dto)
        {
            var error = await VerTaskEntity(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = MapperObject.Map<SysTaskSchedulingEntity>(dto);
            entity.Id = Guid.NewGuid().ToString();
            await taskSchedulingRepository.InsertAsync(entity);
            string message = await schedulerFactory.AddJob(entity, true);
            if (!string.IsNullOrWhiteSpace(message)) return BusinessResponse.Error(msg: message);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(TasksEditDto dto)
        {
            var entity = await taskSchedulingRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null)
                return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                if (entity.Status == APIStatusEnum.Normal)
                {
                    string message = await schedulerFactory.Update(entity);
                    if (!string.IsNullOrWhiteSpace(message)) return BusinessResponse.Error(msg: message);
                }

                await taskSchedulingRepository.UpdateAsync(entity);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 操作
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Operate(TasksOperateDto dto)
        {
            var result = new BusinessResult();
            var entities = await taskSchedulingRepository.Where(a => dto.Ids.Contains(a.Id)).ToListAsync();
            if (entities == null) result.BusinessError("未查询到任务，请刷新重试");
            else
            {
                foreach (var entity in entities)
                {
                    (bool isSuccess, string message) = entity.Interval.IsValidExpression();
                    if (isSuccess)
                    {
                        switch (dto.Operate)
                        {
                            case JobOperateEnum.Delete:
                                message = await schedulerFactory.Delete(entity);
                                entity.Status = APIStatusEnum.Parse;
                                entity.IsDelete = true;
                                break;
                            case JobOperateEnum.Parse:
                                if (entity.Status == APIStatusEnum.Parse) message = "作业已暂停，无法执行暂停操作";
                                else
                                {
                                    message = await schedulerFactory.Parse(entity);
                                    entity.Status = APIStatusEnum.Parse;
                                }

                                break;
                            case JobOperateEnum.Start:
                                if (entity.Status == APIStatusEnum.Normal) message = "作业已正常运行，无法执行开始操作";
                                else
                                {
                                    entity.Status = APIStatusEnum.Normal;
                                    message = await schedulerFactory.Start(entity);
                                }
                                break;
                            case JobOperateEnum.ExecuteImmediately:
                                message = await schedulerFactory.ExecuteImmediately(entity);
                                break;
                            default:
                                message = "不被支持的操作类型";
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(message))
                    {
                        result.BusinessError($"任务异常，任务名称：{entity.TaskName}，异常信息：{message}");
                        break;
                    }
                    else await taskSchedulingRepository.UpdateAsync(entity);
                }
            }

            return result;
        }
    }
}