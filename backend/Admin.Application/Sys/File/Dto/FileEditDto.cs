﻿using Admin.Repository.Dto;

namespace Admin.Application.Sys.File.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class FileEditDto : BaseDto
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 关联类型
        /// </summary>
        public int? RefType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RefId { get; set; }
    }
}
