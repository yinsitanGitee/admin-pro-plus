﻿using Admin.Repository.Dto;

namespace Admin.Application.Sys.File.Dto
{
    /// <summary>
    /// 定义列表dto
    /// </summary>
    public class FileListDto : BaseDto
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 关联类型
        /// </summary>
        public int? RefType { get; set; }

        /// <summary>
        /// RefId
        /// </summary>
        public string RefId { get; set; }
    }
}
