﻿using Admin.Application.Sys.File.Dto;
using Admin.Common.Dto;

namespace Admin.Application.Sys.File
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IBaseFileService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<List<FileListDto>> Query(InputDto dto);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Submit(List<FileEditDto> dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task Delete(string id);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task Delete(List<string> ids);
    }
}
