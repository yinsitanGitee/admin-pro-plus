﻿using Admin.Application.Sys.File.Dto;
using Admin.Common.Dto;
using Admin.Core;
using Admin.Repository;
using Admin.Repository.Entities.Sys;

namespace Admin.Application.Sys.File
{
    /// <summary>
    /// 定义底层应用层服务
    /// </summary>
    /// <param name="sysFileRepository"></param>
    public class BaseFileService(IRepository<SysFileEntity, string> sysFileRepository) : BaseService, IBaseFileService
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<FileListDto>> Query(InputDto dto)
        {
            return await sysFileRepository.Select.Where(a => a.RefId == dto.Id).OrderBy(a => a.CreateDateTime).ToListAsync<FileListDto>();
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Submit(List<FileEditDto> dto)
        {
            var entities = MapperObject.Map<List<SysFileEntity>>(dto);
            await sysFileRepository.InsertAsync(entities);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(string id)
        {
            await sysFileRepository.DeleteAsync(a => a.RefId == id);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task Delete(List<string> ids)
        {
            await sysFileRepository.DeleteAsync(a => ids.Contains(a.Id));
        }
    }
}