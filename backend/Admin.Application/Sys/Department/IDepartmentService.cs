﻿using Admin.Application.Sys.Department.Dto;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Core.Service.Cache.Dto;

namespace Admin.Application.Sys.Department
{
    /// <summary>
    /// 定义底层应用层接口
    /// </summary>
    public interface IDepartmentService
    {
        /// <summary>
        /// 获取所有部门，无递归
        /// </summary>
        /// <returns></returns>
        Task<List<DepartmentListDto>?> GetDepartments(List<string>? departmentIds = null);

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult<DepartmentEditDto>> Get(InputDto dto);

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        Task<List<DepartmentListDto>?> Query(GetDepartmentInputDto input);

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Add(DepartmentEditDto dto);

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<BusinessResult> Edit(DepartmentEditDto dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BusinessResult> Delete(string id);
    }
}
