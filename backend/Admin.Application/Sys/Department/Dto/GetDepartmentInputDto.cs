﻿using Admin.Repository.Enum;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.Department.Dto
{
    /// <summary>
    /// 定义获取输入dto
    /// </summary>
    public class GetDepartmentInputDto
    {
        /// <summary>
        /// 部门
        /// </summary>
        public List<string> DepartmentIds { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public CommonStateEnum? State { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }
    }
}
