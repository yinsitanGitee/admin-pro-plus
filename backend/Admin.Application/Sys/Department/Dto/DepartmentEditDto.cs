﻿using Admin.Common.Dto;
using Admin.Repository.Enum;

namespace Admin.Application.Sys.Department.Dto
{
    /// <summary>
    /// 定义编辑dto
    /// </summary>
    public class DepartmentEditDto : InputDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public CommonStateEnum State { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
