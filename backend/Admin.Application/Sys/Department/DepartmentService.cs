﻿using Admin.Application.Sys.Department.Dto;
using Admin.Cache;
using Admin.Common.BusinessResult;
using Admin.Common.Dto;
using Admin.Common.Ioc;
using Admin.Core;
using Admin.Core.Service.Cache;
using Admin.Core.Service.Cache.Dto;
using Admin.Repository;
using Admin.Repository.Entities.Sys;

namespace Admin.Application.Sys.Department
{
    /// <summary>
    /// 部门服务
    /// </summary>
    /// <param name="departmentRepository"></param>
    public class DepartmentService(IRepository<SysDepartmentEntity, string> departmentRepository) : BaseService, IDepartmentService
    {
        /// <summary>
        /// 获取所有部门，无递归
        /// </summary>
        /// <returns></returns>
        public async Task<List<DepartmentListDto>?> GetDepartments(List<string>? departmentIds = null)
        {
            if (departmentIds == null)
                return await IocManager.Instance.GetService<ICacheService>().GetDepartmentCache();

            return (await IocManager.Instance.GetService<ICacheService>().GetDepartmentCache())?.Where(a => departmentIds.Contains(a.Id)).ToList();
        }

        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<DepartmentEditDto>> Get(InputDto dto)
        {
            return BusinessResponse<DepartmentEditDto>.Success(data: await departmentRepository.GetAsync<DepartmentEditDto>(a => a.Id == dto.Id));
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public async Task<List<DepartmentListDto>?> Query(GetDepartmentInputDto input)
        {
            var data = await IocManager.Instance.GetService<ICacheService>().GetDepartmentCache();
            if (data == null) return null;
            if (input.State.HasValue)
                data = data.Where(a => a.State == input.State).ToList();

            if (!string.IsNullOrWhiteSpace(input.OrganizeId))
                data = data.Where(a => a.OrganizeId == input.OrganizeId).ToList();

            return ForeachDepartmentListDto(data, "0");
        }


        /// <summary>
        /// 递归获取部门
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        private List<DepartmentListDto>? ForeachDepartmentListDto(List<DepartmentListDto>? list, string pid)
        {
            if (list != null && list.Count > 0)
            {
                var a = list.FindAll(p => p.ParentId == pid);
                if (a.Count > 0)
                {
                    List<DepartmentListDto> departments = new();
                    foreach (var item in a)
                    {
                        item.Children = ForeachDepartmentListDto(list, item.Id);
                        departments.Add(item);
                    }
                    return departments;
                }
            }

            return null;
        }

        /// <summary>
        /// 验证部门有效性
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private async Task<string?> ExistDepartment(DepartmentEditDto dto)
        {
            if (await departmentRepository.Select.AnyAsync(a => a.Id != dto.Id && a.Name == dto.Name))
                return "部门名称重复";

            return null;
        }

        /// <summary>
        /// 数据添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Add(DepartmentEditDto dto)
        {
            var error = await ExistDepartment(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);

            var entity = MapperObject.Map<SysDepartmentEntity>(dto);
            entity.Id = Guid.NewGuid().ToString("N");
            await departmentRepository.InsertAsync(entity);
            await RedisManage.DelAsync(RedisKeyDto.DepartmentKey);
            return BusinessResponse.Success();
        }

        /// <summary>
        /// 数据编辑
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Edit(DepartmentEditDto dto)
        {
            var error = await ExistDepartment(dto);
            if (!string.IsNullOrWhiteSpace(error)) return BusinessResponse.Error(msg: error);
            var entity = await departmentRepository.GetAsync(a => a.Id == dto.Id);
            if (entity == null) return BusinessResponse.Error(msg: "数据不存在");
            else
            {
                entity = MapperObject.Map(dto, entity);
                await departmentRepository.UpdateAsync(entity);
                await RedisManage.DelAsync(RedisKeyDto.DepartmentKey);
                return BusinessResponse.Success();
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<BusinessResult> Delete(string id)
        {
            if (await departmentRepository.Select.AnyAsync(a => a.ParentId == id)) return BusinessResponse.Error(msg: "请先删除下级部门");
            await departmentRepository.SoftDeleteAsync(a => a.Id == id);
            await RedisManage.DelAsync(RedisKeyDto.DepartmentKey);
            return BusinessResponse.Success();
        }
    }
}
